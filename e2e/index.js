import {
  baseURL,
  browserType,
  launchConfig,
  contextConfig,
} from "./playwright.config";

const rootSelector = "#root";
let browser, context, p;

export const root = async () => await p.$(rootSelector);
export const page = async () => await p;

export const load = async (path) => {
  browser = await browserType.launch(launchConfig);
  context = await browser.newContext(contextConfig);
  p = await context.newPage();
  await p.goto(`${baseURL}${path}`);
};

export const close = async () => await browser.close();
