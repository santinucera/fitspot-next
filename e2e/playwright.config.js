import { chromium, devices } from "playwright";

const iPhone = devices["iPhone 6"];

module.exports = {
  baseURL: "http://localhost.fitspotapp.com/",
  browserType: chromium,
  launchConfig: {
    headless: false,
    slowMo: 10,
  },
  contextConfig: {
    viewport: iPhone.viewport,
    userAgent: iPhone.userAgent,
  },
};
