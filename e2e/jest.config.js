module.exports = {
  globals: {
    baseURL: "http://localhost.fitspotapp.com/dashboard",
  },
  testMatch: ["**/specs/*.js"],
  transform: {
    "\\.js$": "react-scripts/config/jest/babelTransform",
  },
  verbose: true,
};
