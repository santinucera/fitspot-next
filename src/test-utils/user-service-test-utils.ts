/**
 *
 *
 * Tests for user-service-test-utils
 *
 *
 */
import server from "services/_mocks/server";
import { rest } from "msw";
import {
  mockUserGetResponseData,
  userGetUrl,
  userPatchPasswordUrl,
  UserType,
  userPatchUrl,
} from "services/UserService";
import { User } from "services/types";

/**
 * Return a user object.
 */
export const createUser = (userType = UserType.admin): User => ({
  ...mockUserGetResponseData,
  userType,
});

/**
 * Set the request interceptor response.
 */
export const setUserResponse = (status: number, user?: User): void => {
  server.use(
    rest.get(userGetUrl, (req, res, ctx) => {
      return status === 200 && user
        ? res(ctx.status(status), ctx.json(user))
        : res(ctx.status(status));
    })
  );
};

/**
 * Password interceptor.
 */
export const setUserPasswordResetResponse: (
  status: number,
  response?: User,
  message?: string
) => void = (status, response, message) => {
  server.use(
    rest.patch(userPatchPasswordUrl, (req, res, ctx) => {
      return res(
        ctx.status(status),
        ctx.json(status === 200 ? response : { message })
      );
    })
  );
};

/**
 * Profile interceptor.
 */
export const setUserProfileErrorResponse = (): void => {
  server.use(
    rest.patch(userPatchUrl, (req, res, ctx) => {
      return res(ctx.status(400));
    })
  );
};
