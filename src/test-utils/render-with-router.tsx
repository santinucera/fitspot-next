/**
 *
 *
 * render-with-router
 *
 *
 */
import React, { Fragment, ReactElement } from "react";
import { MemoryRouter, Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { render } from "@testing-library/react";

/**
 * Wrap a component in BrowserRouter. Useful for
 * testing react-query when you don't care about
 * the route.
 */
export const renderWithRouter = (component: ReactElement) =>
  render(component, { wrapper: BrowserRouter });

/**
 * Wrap a component in MemoryRouter. Useful for
 * testing react-router hooks.
 */
export const renderWithMemoryRouter = (
  path: string,
  initialEntries: string[],
  component: ReactElement
) =>
  render(component, {
    wrapper: ({ children }) => (
      <MemoryRouter initialEntries={initialEntries}>
        <Routes>
          <Route path={path} element={<Fragment>{children}</Fragment>} />
        </Routes>
      </MemoryRouter>
    ),
  });
