/**
 *
 *
 * calendar-service-test-utils
 *
 *
 */
import { rest } from "msw";
import server from "services/_mocks/server";
import {
  calendarGetSettingsUrl,
  CalendarSettingsResponseData,
} from "services/CalendarService";
import {
  calendarPostSettingsBaseURL,
  calendarPostSettingsPATH,
} from "services/CalendarService";

export const setCalendarSettingsGetResponse = (
  status: number,
  response?: CalendarSettingsResponseData
) => {
  server.use(
    rest.get(calendarGetSettingsUrl, (__, res, ctx) => {
      return status === 200 && response
        ? res(ctx.status(status), ctx.json(response))
        : res(ctx.status(status));
    })
  );
};

export const setCalendarSettingsPostResponse = (
  status: number,
  response?: CalendarSettingsResponseData
): void => {
  server.use(
    rest.post(
      `${calendarPostSettingsBaseURL}${calendarPostSettingsPATH}`,
      (__, res, ctx) => {
        return status === 200 && response
          ? res(ctx.status(status), ctx.json(response))
          : res(ctx.status(status));
      }
    )
  );
};
