/**
 *
 *
 * expert-service-test-utils
 *
 * Utils for testing ExpertService
 */
import { rest } from "msw";
import {
  mockResponseData,
  ExpertResponseData,
  expertGetInfoUrl,
} from "services/ExpertService";
import server from "services/_mocks/server";

export const setExpertResponse = (
  status: number,
  expert?: ExpertResponseData
): void => {
  server.use(
    rest.get(`${expertGetInfoUrl}:expertId`, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(expert))
        : res(ctx.status(status));
    })
  );
};

export const createExpert = (): ExpertResponseData => mockResponseData;
