/**
 *
 *
 * session-service-test-utils
 *
 * Utils for testing SessionService
 */
import { rest } from "msw";
import dayjs from "dayjs";
import {
  CustomerDashboardSession,
  CustomerDashboardSessionModel,
  Session,
  sessionsGetUrl,
  sessionGetUrl,
  mockSessionGetResponseData,
  mockSessionsGetResponseData,
  mockSessionRsvpPostResponseData,
  mockFeaturedSessionsGetResponse,
  sessionRsvpPostUrl,
  mockNonRatedSessionsGetResponseData,
  sessionsGetNonRatedUrl,
  NonRatedSessionModel,
  NonRatedSessionsModel,
  rateSessionPostUrl,
  mockDismissSessionRatingPostResponseData,
  dismissSessionRatingPostUrl,
  rateSessionPatchUrl,
  sessionModel,
  SessionModel,
  TrainerSession,
  TrainerSessionModel,
  trainerSessionModel,
  TrainerSessionDetails,
  mockTrainersSessionsGetResponseData,
  mockTrainersSessionGetResponseData,
  trainersSessionsGetUrl,
  trainersSessionGetUrl,
  patchSessionStatusUrl,
  cancelSessionPatchServiceUrl,
  standardSessionModel,
  StandardSession,
  StandardSessionModel,
  featuredSessionsGetServiceUrl,
} from "services/SessionService";
import server from "services/_mocks/server";

/**
 *
 * Sessions
 *
 * With an "s". This data comes from `/customers/schedule/dashboard/` and
 * contains user-specific data.
 *
 */
const mockCustomerDashboardSession: CustomerDashboardSession =
  mockSessionsGetResponseData.sessions.sessions[0];

export const createCustomerDashboardSession = (): CustomerDashboardSession => ({
  ...mockCustomerDashboardSession,
  id: Math.floor(Math.random() * 1000),
});

export const createCustomerDashboardSessionModel = (): CustomerDashboardSessionModel => ({
  ...mockCustomerDashboardSession,
  activityCategory: "Move",
  id: Math.floor(Math.random() * 1000),
  startDate: dayjs(),
  endDate: dayjs(),
});

export const setSessionsResponse = (
  sessions: CustomerDashboardSession[] | undefined
): void => {
  server.use(
    rest.get(sessionsGetUrl, (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          sessions: {
            sessions,
          },
        })
      );
    })
  );
};

/**
 *
 * Session
 *
 * Session Detail. Note Session is is it's own type,
 * NOT equivalent to a single CustomerDashboardSession.
 */
export const createSession = (): Session => ({ ...mockSessionGetResponseData });

export const createSessionModel = (data?: Partial<Session>): SessionModel => {
  return sessionModel({
    ...mockSessionGetResponseData,
    ...data,
  });
};

export const setSessionResponse = (status: number, session?: Session): void => {
  server.use(
    rest.get(`${sessionGetUrl}:sessionId`, (req, res, ctx) => {
      return status === 200 && session
        ? res(ctx.status(status), ctx.json(session))
        : res(ctx.status(status));
    })
  );
};

/**
 * Set response from useMutationsSessionRsvpPostService request interceptor.
 */
export const setSessionRsvpResponse = (status: number): void => {
  server.use(
    rest.post(sessionRsvpPostUrl, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(mockSessionRsvpPostResponseData))
        : res(ctx.status(status));
    })
  );
};

export const createNonRatedSessionModel = (): NonRatedSessionModel => {
  const session = mockNonRatedSessionsGetResponseData[0];
  return {
    ...session,
    id: Math.floor(Math.random() * 1000),
    startDate: dayjs(session.dtEnd),
    endDate: dayjs(session.date),
  };
};

export const setNonRatedSessionsResponse = (
  sessions: NonRatedSessionsModel,
  status: number = 200
): void => {
  server.use(
    rest.get(sessionsGetNonRatedUrl, (req, res, ctx) => {
      return res(ctx.status(status), ctx.json(sessions));
    })
  );
};

/**
 * Set response from useMutationRateSessionPostService request interceptor.
 */
export const setRateSessionResponse = (status: number): void => {
  server.use(
    rest.post(`${rateSessionPostUrl}/:sessionId`, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(mockSessionGetResponseData))
        : res(ctx.status(status));
    })
  );
};

/**
 * Set response from useMutationRateSessionPatchService request interceptor.
 */
export const setRateSessionPatchResponse = (status: number): void => {
  server.use(
    rest.patch(`${rateSessionPatchUrl}/:sessionId`, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(mockSessionGetResponseData))
        : res(ctx.status(status));
    })
  );
};

/**
 * Set response from useMutationDismissSessionRatingService request interceptor.
 */
export const setDismissSessionResponse = (status: number): void => {
  server.use(
    rest.post(`${dismissSessionRatingPostUrl}/:sessionId`, (req, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json(mockDismissSessionRatingPostResponseData)
          )
        : res(ctx.status(status));
    })
  );
};

/**
 * Return a single result from /trainers/sessions
 */
export const createTrainerSession = (): TrainerSession => ({
  ...mockTrainersSessionsGetResponseData[0],
});

/**
 * Return a single result from /trainers/sessions transformed
 * into a TrainerSessionModel.
 */
export const createTrainerSessionModel = (): TrainerSessionModel =>
  trainerSessionModel({
    ...mockTrainersSessionsGetResponseData[0],
  });

/**
 * Set response from useQueryTrainersSessionsGetService request interceptor.
 */
export const setTrainerSessionsResponse = (
  status: number,
  sessions?: TrainerSession[],
  message?: string
): void => {
  server.use(
    rest.get(trainersSessionsGetUrl, (req, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json(sessions || mockTrainersSessionsGetResponseData)
          )
        : res(ctx.status(status), ctx.json({ message }));
    })
  );
};

/**
 * Return a response object from /trainers/session/:sessionId
 */
export const createTrainerSessionDetails = (): TrainerSessionDetails => ({
  ...mockTrainersSessionGetResponseData,
});

/**
 * Set response from useQueryTrainersSessionGetService (/trainers/session/:sessionId)
 * request interceptor.
 */
export const setTrainerSessionResponse = (
  status: number,
  session?: TrainerSessionDetails
): void => {
  server.use(
    rest.get(`${trainersSessionGetUrl}:sessionId`, (req, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json(session || mockTrainersSessionGetResponseData)
          )
        : res(ctx.status(status));
    })
  );
};

/**
 * Set response from useMutationSessionStatusPatchService (/workouts/status/:sessionId)
 * request interceptor.
 */
export const setSessionStatusUpdateResponse = (
  status: number,
  session?: Session,
  reason?: string
): void => {
  server.use(
    rest.patch(`${patchSessionStatusUrl}:sessionId`, (__, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json(session || mockTrainersSessionGetResponseData)
          )
        : res(ctx.status(status), ctx.json({ message: reason }));
    })
  );
};

/**
 * Set response from useMutationSessionStatusPatchService (/workouts/status/:sessionId)
 * request interceptor.
 */
export const setSessionCancelResponse = (
  status: number,
  session?: Session,
  reason?: string
): void => {
  server.use(
    rest.patch(`${cancelSessionPatchServiceUrl}:sessionId`, (__, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json(session || mockTrainersSessionGetResponseData)
          )
        : res(ctx.status(status), ctx.json({ message: reason }));
    })
  );
};

/**
 * Return a StandardSession object.
 */
export const createStandardSession = (): StandardSession => ({
  ...mockFeaturedSessionsGetResponse.data[0],
});

/**
 * Return a StandardSessionModel object.
 */
export const createStandardSessionModel = (): StandardSessionModel =>
  standardSessionModel(createStandardSession());

/**
 * Set response from useQueryFeaturedSessionsGetService (/sessions/featured)
 * request interceptor.
 */
export const setFeaturedSessionsResponse = (
  status: number,
  sessions?: StandardSession[],
  reason?: string
): void => {
  server.use(
    rest.get(featuredSessionsGetServiceUrl, (__, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json({ data: sessions ? sessions : [createStandardSession()] })
          )
        : res(ctx.status(status), ctx.json({ message: reason }));
    })
  );
};
