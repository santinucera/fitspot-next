/**
 *
 *
 * user-service-test-utils
 *
 *
 */
import { mockResponseData } from "services/ActivityFeedService";
import { activityFeedModel } from "services/ActivityFeedService/models";
import { createActivityFeedModel } from "./activity-feed-service-test-utils";

describe("createActivityFeedModel", () => {
  it("should return an array of activity feed", () => {
    expect(createActivityFeedModel()).toEqual(
      mockResponseData.map(activityFeedModel)
    );
  });
});
