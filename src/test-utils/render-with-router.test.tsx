/**
 *
 *
 * Tests for render-with-router
 *
 *
 */
import React, { FunctionComponent } from "react";
import { renderWithRouter, renderWithMemoryRouter } from "./render-with-router";
import { useParams } from "react-router";

describe("renderWithRouter", () => {
  it("should render children", () => {
    const text = "some text";
    const El: FunctionComponent = () => <div>{text}</div>;
    const { getByText } = renderWithRouter(<El />);
    expect(getByText(text)).toBeInTheDocument();
  });
});

describe("renderWithMemoryRouter", () => {
  it("should be on the specified route", () => {
    const id = "1";
    const El: FunctionComponent = () => {
      const { id } = useParams();
      return <div>{id}</div>;
    };
    const { getByText } = renderWithMemoryRouter("/:id", [`/${id}`], <El />);

    expect(getByText(id)).toBeInTheDocument();
  });
});
