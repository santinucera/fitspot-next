/**
 *
 *
 * challenge-service-test-utils
 *
 * Utils for testing ChallengeService
 */
import { rest } from "msw";
import {
  Challenge,
  challengesGetCurrentUrl,
  challengeGetUrl,
  mockChallengeGetResponseData,
  challengesGetCompletedUrl,
  mockChallengesGetCurrentResponseData,
  ChallengeRecord,
  ChallengeLeaderBoardItem,
  challengeReportPostUrl,
  challengeRsvpPostUrl,
  mockChallengeRsvpPostResponseData,
} from "services/ChallengeService";
import server from "services/_mocks/server";
import dayjs from "utils/days";

/**
 * Create a currently active challenge.
 *
 * Active challenges have a `dtStart` before today
 * and a `dtEnd` of after today.
 */
export const createChallenge = ({
  startDate,
  endDate,
}: {
  startDate?: string;
  endDate?: string;
} = {}): Challenge => {
  return {
    ...mockChallengeGetResponseData,
    dtStart: dayjs(startDate)
      .utc()
      .subtract(1, "day")
      .startOf("day")
      .toISOString(),
    dtEnd: dayjs(endDate).utc().add(1, "day").startOf("day").toISOString(),
  };
};

export const createChallengeLeaderBoardItem = (
  userId: number,
  rank: number,
  isCurrentUser: boolean
): ChallengeLeaderBoardItem => ({
  userId,
  rank,
  lastName: "ChallengeLeaderBoardItemLastName",
  firstName: "ChallengeLeaderBoardItemFirstName",
  points: 1,
  avatar: {
    url: "https://example.com/avatar.png",
    userId,
    mimetype: "image/png",
    filename: "blob",
    size: 0,
    id: userId,
  },
  isCurrentUser,
});

export const createNonJoinedChallenge = () => {
  return {
    ...mockChallengeGetResponseData,
    leaderboard: [
      {
        lastName: "Wong",
        userId: 2,
        avatar: {
          url: "",
          userId: 1,
          mimetype: "image/png",
          filename: "blob",
          size: 0,
          id: 5456,
        },
        points: 0,
        rank: 1,
        firstName: "John",
      },
    ],
  };
};

export const createCompletedChallenge = () => {
  return {
    ...mockChallengesGetCurrentResponseData[0],
    id: 22,
    dtStart: dayjs().subtract(10, "day").format(),
    dtEnd: dayjs().subtract(5, "day").format(),
  };
};

export const createChallengeLeaderBoard = () =>
  mockChallengeGetResponseData.leaderboard;

export const setChallengeResponse = (challenge: Challenge): void => {
  server.use(
    rest.get(`${challengeGetUrl}:id`, (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(challenge));
    })
  );
};

export const setChallengesResponse = (challenges: Challenge[]): void => {
  server.use(
    rest.get(challengesGetCurrentUrl, (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(challenges));
    })
  );
};

export const setChallengesCompletedResponse = (
  challenges: Challenge[]
): void => {
  server.use(
    rest.get(challengesGetCompletedUrl, (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(challenges));
    })
  );
};

export const setChallengesErrorResponse = (): void => {
  server.use(
    rest.get(challengesGetCurrentUrl, (req, res, ctx) => {
      return res(ctx.status(403), ctx.text("An error occurred"));
    })
  );
};

export const setChallengeErrorResponse = (): void => {
  server.use(
    rest.get(`${challengeGetUrl}:id`, (req, res, ctx) => {
      return res(ctx.status(403), ctx.text("An error occurred"));
    })
  );
};

export const setChallengeReportPostResponse = (
  status: number,
  challengeRecords?: ChallengeRecord[]
): void => {
  server.use(
    rest.post(`${challengeReportPostUrl}/:challengeId`, (req, res, ctx) => {
      return status === 200
        ? res(
            ctx.status(status),
            ctx.json({ success: true, recordsAdded: challengeRecords })
          )
        : res(ctx.status(status));
    })
  );
};

export const setChallengeRSVPPostResponse = (status: number): void => {
  server.use(
    rest.post(`${challengeRsvpPostUrl}/:challengeId`, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(mockChallengeRsvpPostResponseData))
        : res(ctx.status(status));
    })
  );
};
