/**
 *
 *
 * user-service-test-utils
 *
 *
 */
import React, { FunctionComponent } from "react";
import { render, waitFor } from "@testing-library/react";
import { createUser, setUserResponse } from "./user-service-test-utils";
import {
  mockUserGetResponseData,
  useQueryUserGetService,
} from "services/UserService";

describe("createUser", () => {
  it("should return a user object", () => {
    expect(createUser()).toEqual(mockUserGetResponseData);
  });
});

describe("setUserResponse", () => {
  it("should set the success response of the useQueryUserGetService", async () => {
    jest.restoreAllMocks();
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserResponse(200, createUser());
    const El: FunctionComponent = () => {
      const { data } = useQueryUserGetService();
      return (
        <div>
          {data && <div data-testid="data">{JSON.stringify(data)}</div>}
        </div>
      );
    };
    const { getByTestId } = render(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(mockUserGetResponseData)
      );
    });
  });

  it("should set the error response of the useQueryUserGetService", async () => {
    jest.restoreAllMocks();
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserResponse(500);
    const El: FunctionComponent = () => {
      const { error } = useQueryUserGetService({
        queryConfig: {
          retry: false,
        },
      });
      return (
        <div>
          {error && (
            <div data-testid="error">{JSON.stringify(error.message)}</div>
          )}
        </div>
      );
    };
    const { getByTestId } = render(<El />);
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        "Request failed with status code 500"
      );
    });
  });
});
