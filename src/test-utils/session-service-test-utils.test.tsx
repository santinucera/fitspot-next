/**
 *
 *
 * Test for session-service-test-utils
 *
 *
 */
import React, { FunctionComponent } from "react";
import { fireEvent, waitFor } from "@testing-library/react";
import { ReactQueryConfigProvider } from "react-query";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  createCustomerDashboardSession,
  createCustomerDashboardSessionModel,
  setSessionsResponse,
  createSession,
  setSessionResponse,
  setSessionRsvpResponse,
  setNonRatedSessionsResponse,
  createNonRatedSessionModel,
  createStandardSession,
  setFeaturedSessionsResponse,
} from "./session-service-test-utils";
import {
  useQuerySessionsGetService,
  sessionsModel,
  useQuerySessionGetService,
  sessionModel,
  mockSessionsGetResponseData,
  mockSessionGetResponseData,
  useMutationSessionRsvpPostService,
  mockSessionRsvpPostResponseData,
  mockNonRatedSessionsGetResponseData,
  useQuerySessionsGetNonRatedService,
  standardSessionModel,
  useQueryFeaturedSessionsGetService,
} from "services/SessionService";

describe("createCustomerDashboardSession", () => {
  it("should return a mock session", () => {
    expect(createCustomerDashboardSession()).toEqual({
      ...mockSessionsGetResponseData.sessions.sessions[0],
      id: expect.any(Number),
    });
  });
});

describe("createCustomerDashboardSessionModel", () => {
  it("should return a mock CustomerDashboardSessionModel", () => {
    expect(createCustomerDashboardSessionModel()).toEqual({
      ...mockSessionsGetResponseData.sessions.sessions[0],
      id: expect.any(Number),
      startDate: expect.any(Object), // dayjs object
      endDate: expect.any(Object), // dayjs object
    });
  });
});

describe("setSessionsResponse", () => {
  it("should set the response data of the useQuerySessionsGetService", async () => {
    const El: FunctionComponent = () => {
      const { data } = useQuerySessionsGetService();

      return data ? <div data-testid="data">{JSON.stringify(data)}</div> : null;
    };

    const sessions = [createCustomerDashboardSession()];
    setSessionsResponse(sessions);
    const { getByTestId } = renderWithRouter(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(sessionsModel({ sessions: { sessions: sessions } }))
      );
    });
  });
});

describe("createSession", () => {
  it("should return a session", () => {
    expect(createSession()).toEqual(mockSessionGetResponseData);
  });
});

describe("setSessionResponse", () => {
  it("should set the response data of the useQuerySessionGetService", async () => {
    const El: FunctionComponent = () => {
      const { data } = useQuerySessionGetService({
        urlParams: { sessionId: "1" },
      });

      return data ? <div data-testid="data">{JSON.stringify(data)}</div> : null;
    };

    const session = createSession();
    setSessionResponse(200, session);
    const { getByTestId } = renderWithRouter(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(sessionModel(session))
      );
    });
  });
});

describe("setSessionRsvpResponse", () => {
  it("should set the response of the useMutationsSessionRsvpPostService", async () => {
    jest.restoreAllMocks();
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const El: FunctionComponent = () => {
      const [
        makeRequest,
        { data, error },
      ] = useMutationSessionRsvpPostService();
      return (
        <div>
          <button
            type="button"
            onClick={() => makeRequest({ sessionId: 1, isRSVP: true })}
          >
            Go
          </button>
          {data && <div data-testid="data">{JSON.stringify(data)}</div>}
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };

    setSessionRsvpResponse(200);
    const { getByTestId, getByRole } = renderWithRouter(<El />);
    const goButton = getByRole("button");
    fireEvent.click(goButton);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(mockSessionRsvpPostResponseData)
      );
    });
    setSessionRsvpResponse(500);
    fireEvent.click(goButton);
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        "Request failed with status code 500"
      );
    });
  });
});

describe("createNonRatedSessionModel", () => {
  it("should return a session", () => {
    expect(createNonRatedSessionModel()).toEqual({
      ...mockNonRatedSessionsGetResponseData[0],
      id: expect.any(Number),
      startDate: expect.any(Object), // dayjs object
      endDate: expect.any(Object), // dayjs object
    });
  });
});

describe("setNonRatedSessionsResponse", () => {
  it("should set the response data of the useQuerySessionsGetNonRatedService", async () => {
    const El: FunctionComponent = () => {
      const { data } = useQuerySessionsGetNonRatedService();

      return data ? <div data-testid="data">{JSON.stringify(data)}</div> : null;
    };
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const { getByTestId } = renderWithRouter(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(JSON.stringify(sessions));
    });
  });
});

describe("setFeaturedSessionsResponse", () => {
  beforeEach(async () => {
    jest.restoreAllMocks();
  });

  it("should set the response data of the useQueryFeaturedSessionsGetService", async () => {
    const El: FunctionComponent = () => {
      const { data } = useQueryFeaturedSessionsGetService();

      return data ? <div data-testid="data">{JSON.stringify(data)}</div> : null;
    };
    const name = "some featured session";
    const session = { ...createStandardSession(), name };
    setFeaturedSessionsResponse(200, [session]);
    const { getByTestId } = renderWithRouter(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify([standardSessionModel(session)])
      );
    });
  });

  it("should make useQueryFeaturedSessionsGetService fail", async () => {
    // Swallow server error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const El: FunctionComponent = () => {
      const { isError, error } = useQueryFeaturedSessionsGetService();

      return isError ? (
        <div data-testid="error">{error?.response?.data.message}</div>
      ) : (
        <div>Loading</div>
      );
    };
    const message = "some message";
    setFeaturedSessionsResponse(500, undefined, message);
    const { getByTestId } = renderWithRouter(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <El />
      </ReactQueryConfigProvider>
    );
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(message);
    });
  });
});
