/**
 *
 *
 * activity-feed-service-test-utils
 *
 *
 */
import server from "services/_mocks/server";
import { rest } from "msw";
import {
  activityFeedGetUrl,
  ActivityFeedModel,
  mockResponseData,
} from "services/ActivityFeedService";
import { activityFeedModel } from "services/ActivityFeedService/models";

/**
 * Return a activity feed array.
 */
export const createActivityFeedModel = (): ActivityFeedModel[] => [
  ...mockResponseData.map(activityFeedModel),
];

/**
 * Set the request interceptor response.
 */
export const setActivityFeedResponse = (
  status: number,
  feed?: ActivityFeedModel[]
): void => {
  server.use(
    rest.get(activityFeedGetUrl, (req, res, ctx) => {
      return status === 200 && feed
        ? res(ctx.status(status), ctx.json(feed))
        : res(ctx.status(status));
    })
  );
};
