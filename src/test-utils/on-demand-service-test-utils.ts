/**
 *
 *
 * on-demand-service-test-utils
 *
 * Utils for testing OnDemandService
 */
import { rest } from "msw";

import {
  OnDemandVideosResponseData,
  onDemandVideosGetUrl,
  videoMockResponseData,
} from "services/OnDemandVideoService";
import { onDemandVideoModel } from "services/OnDemandVideoService/models";

import server from "services/_mocks/server";
import range from "utils/range";

export const setOnDemandResponse = (
  status: number,
  onDemand?: OnDemandVideosResponseData | {}
): void => {
  server.use(
    rest.get(onDemandVideosGetUrl, (req, res, ctx) => {
      return status === 200
        ? res(ctx.status(status), ctx.json(onDemand))
        : res(ctx.status(status));
    })
  );
};

export const setEmptyOnDemandResponse = (): void => {
  server.use(
    rest.get(onDemandVideosGetUrl, (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          data: [],
          meta: {
            pagination: {
              hasMore: false,
              limit: 0,
              offset: 0,
              total: 0,
            },
          },
        })
      );
    })
  );
};

export const createOnDemand = ({
  count = 72,
  limit = 9,
  offset = 0,
}: {
  count?: number;
  limit?: number;
  offset?: number;
}): OnDemandVideosResponseData => {
  const videos = range(0, count).map((index) => {
    return {
      ...videoMockResponseData.data,
      id: index + offset,
    };
  });

  return {
    data: videos.slice(0, limit),
    meta: {
      pagination: {
        hasMore: true,
        limit,
        offset,
        total: videos.length,
      },
    },
  };
};
export const createOnDemandVideoModel = () => ({
  data: onDemandVideoModel(videoMockResponseData.data),
});
