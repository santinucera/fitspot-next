/**
 *
 *
 * Shared types
 *
 *
 */

/**
 * Type for output of AxiosError.toJSON().
 *
 * Strangely, this is missing in axios typings.
 */
export interface AxiosErrorJSON {
  message: string;
  name: string;
  stack: string;
  config: object;
  code: string;
}

declare global {
  interface Window {
    HumanConnect: any;
  }
}
