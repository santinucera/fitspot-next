/**
 *
 *
 * Mock for @cometchat-pro/chat
 *
 *
 */

import { CometChat } from "@cometchat-pro/chat";
import {} from "services/CometChatService";

CometChat.init = jest.fn() as jest.Mocked<typeof CometChat.init>;

CometChat.login = jest.fn().mockResolvedValue({
  uid: "",
  name: "",
  authToken: "",
  avatar: "",
  lastActiveAt: 0,
  link: "",
  metadata: "",
  role: "",
  status: "",
  statusMessage: "",
  getUid: () => "",
  getName: () => "",
  getAuthToken: () => "",
  getAvatar: () => "",
  getLastActiveAt: () => "",
  getLink: () => "",
  getMetadata: () => ({}),
  getRole: () => "",
  getStatus: () => "",
  getStatusMessage: () => "",
}) as jest.Mocked<typeof CometChat.login>;

CometChat.logout = jest.fn() as jest.Mocked<typeof CometChat.logout>;

CometChat.sendMessage = jest.fn() as jest.Mocked<typeof CometChat.sendMessage>;

CometChat.MessagesRequestBuilder = jest.fn().mockImplementation(function () {
  return {
    setGUID: function () {
      return this;
    },
    setLimit: function () {
      return this;
    },
    build: () => {},
  };
}) as jest.Mocked<typeof CometChat.MessagesRequestBuilder>;

CometChat.addConnectionListener = jest.fn() as jest.Mocked<
  typeof CometChat.addConnectionListener
>;
CometChat.removeConnectionListener = jest.fn() as jest.Mocked<
  typeof CometChat.removeConnectionListener
>;

CometChat.addLoginListener = jest.fn() as jest.Mocked<
  typeof CometChat.addLoginListener
>;
CometChat.removeLoginListener = jest.fn() as jest.Mocked<
  typeof CometChat.removeLoginListener
>;

CometChat.addMessageListener = jest.fn() as jest.Mocked<
  typeof CometChat.addMessageListener
>;
CometChat.removeMessageListener = jest.fn() as jest.Mocked<
  typeof CometChat.removeMessageListener
>;

CometChat.addGroupListener = jest.fn() as jest.Mocked<
  typeof CometChat.addGroupListener
>;
CometChat.removeGroupListener = jest.fn() as jest.Mocked<
  typeof CometChat.removeGroupListener
>;

CometChat.getGroup = jest.fn().mockImplementation(() => ({
  getHasJoined: () => false,
})) as jest.Mocked<typeof CometChat.getGroup>;
CometChat.joinGroup = jest.fn().mockResolvedValue({}) as jest.Mocked<
  typeof CometChat.joinGroup
>;

export { CometChat };
