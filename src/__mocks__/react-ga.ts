/**
 *
 *
 * Mock for react-ga
 *
 *
 */
import ReactGA from "react-ga";

ReactGA.initialize = jest.fn() as jest.Mocked<typeof ReactGA.initialize>;
ReactGA.set = jest.fn() as jest.Mocked<typeof ReactGA.set>;
ReactGA.pageview = jest.fn() as jest.Mocked<typeof ReactGA.pageview>;
ReactGA.event = jest.fn() as jest.Mocked<typeof ReactGA.event>;

export default ReactGA;
