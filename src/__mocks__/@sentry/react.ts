/**
 *
 *
 * Mock for @sentry/react
 *
 *
 */

export default jest.createMockFromModule("@sentry/react");
