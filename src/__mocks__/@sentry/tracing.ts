/**
 *
 *
 * Mock for @sentry/tracing
 *
 *
 */

export default jest.createMockFromModule("@sentry/tracing");
