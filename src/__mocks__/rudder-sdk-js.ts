/**
 *
 *
 * Mock for rudder-sdk-js
 *
 *
 */
// Yes, this is how the import is supposed to work :shrug:
// See: https://github.com/rudderlabs/rudder-sdk-js
import * as rudder from "rudder-sdk-js";

export const load = jest.fn() as jest.Mocked<typeof rudder.load>;
export const ready = jest.fn().mockImplementation(
  (callback: any): Promise<any> => {
    return callback();
  }
) as jest.Mocked<typeof rudder.ready>;
export const reset = jest.fn() as jest.Mocked<typeof rudder.reset>;
export const identify = jest.fn() as jest.Mocked<typeof rudder.identify>;
export const track = jest.fn() as jest.Mocked<typeof rudder.track>;
export const page = jest.fn() as jest.Mocked<typeof rudder.page>;

// No default export
