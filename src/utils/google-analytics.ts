/**
 *
 *
 * google-analytics
 *
 *
 */
import { default as googleAnalytics } from "react-ga";
import isProduction from "utils/is-production";

const { REACT_APP_GA_TRACKING_CODE } = process.env;

interface ReportGAEventOptions {
  category: string;
  action: string;
  label: string;
  sessionId: number | null;
  companyId: number | null;
  activityCategory: string | null;
}

/**
 * Report events to Google Analytics.
 *
 * ReactGA is initialized in index.ts. Page tracking
 * is enabled in the App container.
 */
export const reportGAEvent = ({
  category,
  action,
  label,
  sessionId,
  companyId,
  activityCategory,
}: ReportGAEventOptions): void => {
  if (isProduction() && REACT_APP_GA_TRACKING_CODE) {
    // Create tracker
    googleAnalytics.initialize(REACT_APP_GA_TRACKING_CODE, {
      gaOptions: { siteSpeedSampleRate: 100 },
    });

    googleAnalytics.set({
      dimension1: sessionId,
      dimension2: companyId,
      dimension3: activityCategory,
    });

    googleAnalytics.event({
      category,
      action,
      label,
    });
  }
};

export default googleAnalytics;
