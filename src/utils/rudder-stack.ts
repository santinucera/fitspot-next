/**
 *
 *
 * rudder-stack
 *
 *
 */
import isProduction from "utils/is-production";
import * as rudderStack from "rudder-sdk-js";

if (
  isProduction() &&
  process.env.REACT_APP_RUDDER_WRITE_KEY &&
  process.env.REACT_APP_RUDDER_DATA_PLANE_URI
) {
  rudderStack.load(
    process.env.REACT_APP_RUDDER_WRITE_KEY,
    process.env.REACT_APP_RUDDER_DATA_PLANE_URI
  );
}

/**
 * Only call rudderStack.track in production.
 */
export const rudderStackTrack = (eventTitle: string, data: object): void => {
  if (isProduction()) rudderStack.track(eventTitle, data);
};

export default rudderStack;
