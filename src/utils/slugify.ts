/**
 *
 *
 * slugify
 *
 *
 */
const slugify = (delimiter: string, value: string) =>
  value.toLowerCase().replace(/\W/g, delimiter);

export default slugify;
