/**
 *
 *
 * Tests for is-production
 *
 *
 */
import isProduction from "./is-production";

it("should return true in the test env", () => {
  expect(isProduction()).toBe(true);
});
