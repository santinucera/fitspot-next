import axios from "axios";

export const api = axios.create({
  baseURL: "https://app-dev.fitspotapp.com/api/v1/",
  withCredentials: true,
});
