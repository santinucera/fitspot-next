/**
 *
 *
 * Tests for slugify
 */
import slugify from "./slugify";

it("should transform to kebab case", () => {
  expect(slugify("-", "Some_string-with all sorts of special&stuff 1")).toBe(
    "some_string-with-all-sorts-of-special-stuff-1"
  );
});

it("should transform to snake case", () => {
  expect(slugify("_", "Some_string-with all sorts of special&stuff 1")).toBe(
    "some_string_with_all_sorts_of_special_stuff_1"
  );
});
