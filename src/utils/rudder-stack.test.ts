/**
 *
 *
 * Tests for rudder-stack
 *
 *
 */
import rudderStack, { rudderStackTrack } from "./rudder-stack";

describe("googleAnalytics", () => {
  it("should be mocked", () => {
    expect(jest.isMockFunction(rudderStack.load)).toBe(true);
    expect(jest.isMockFunction(rudderStack.ready)).toBe(true);
    expect(jest.isMockFunction(rudderStack.reset)).toBe(true);
    expect(jest.isMockFunction(rudderStack.identify)).toBe(true);
    expect(jest.isMockFunction(rudderStack.track)).toBe(true);
  });
});

describe("rudderStackTrack", () => {
  it("should call track", () => {
    const spy = jest.spyOn(rudderStack, "track");
    const eventTitle = "some_title";
    const data = {
      foo: "bar",
    };
    rudderStackTrack(eventTitle, data);
    expect(spy).toHaveBeenCalledWith(eventTitle, data);
  });
});
