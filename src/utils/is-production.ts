/**
 *
 *
 * is-production
 *
 *
 */

/**
 * Returns true if this is the "production" or "test" environment.
 */
const isProduction = (): boolean =>
  process.env.NODE_ENV === "production" || process.env.NODE_ENV === "test";

export default isProduction;
