/**
 *
 *
 * Tests for google-analytics
 *
 *
 */
import googleAnalytics, { reportGAEvent } from "./google-analytics";

describe("googleAnalytics", () => {
  it("should be mocked", () => {
    expect(jest.isMockFunction(googleAnalytics.initialize)).toBe(true);
    expect(jest.isMockFunction(googleAnalytics.set)).toBe(true);
    expect(jest.isMockFunction(googleAnalytics.pageview)).toBe(true);
    expect(jest.isMockFunction(googleAnalytics.event)).toBe(true);
  });
});

describe("reportGAEvent", () => {
  it("should call set and event methods", () => {
    const options = {
      category: "some category",
      action: "some action",
      label: "some label",
      sessionId: 1,
      companyId: 2,
      activityCategory: "some activity category",
    };
    reportGAEvent(options);

    expect(googleAnalytics.set).toHaveBeenCalledWith({
      dimension1: options.sessionId,
      dimension2: options.companyId,
      dimension3: options.activityCategory,
    });
    expect(googleAnalytics.event).toHaveBeenCalledWith({
      category: options.category,
      action: options.action,
      label: options.label,
    });
  });
});
