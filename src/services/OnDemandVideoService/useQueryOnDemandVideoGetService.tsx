/**
 *
 *
 *
 * useQueryOnDemandVideoGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { OnDemandVideoResponseData, OnDemandVideoModel } from "./types";
import { onDemandVideoModel } from "services/OnDemandVideoService/models";

export interface OnDemandVideoGetUrlParams {
  videoId: number;
}

export interface OnDemandVideoGetServiceParams {
  queryConfig?: QueryConfig<OnDemandVideoResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams?: OnDemandVideoGetUrlParams;
}

export const onDemandVideoGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}customers/on-demand/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<OnDemandVideoModel> =>
  // This _ is the queryKey string
  (_: string, { videoId }: OnDemandVideoGetUrlParams) =>
    api
      .get<OnDemandVideoResponseData>(`${onDemandVideoGetUrl}${videoId}`, {
        ...axiosConfig,
      })
      .then(({ data: { data } }) => {
        return {
          data: onDemandVideoModel(data),
        };
      });

export const useQueryOnDemandVideoGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: OnDemandVideoGetServiceParams = {}) => {
  return useQuery<OnDemandVideoModel, AxiosError>({
    queryKey: ["on-demand-video", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryOnDemandVideoGetData = (
  urlParams?: OnDemandVideoGetUrlParams
): OnDemandVideoModel | undefined =>
  queryCache.getQueryData<OnDemandVideoModel>(["on-demand-video", urlParams]);
