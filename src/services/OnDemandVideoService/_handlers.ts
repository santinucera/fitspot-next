/**
 *
 *
 * handlers for OnDemandVideo service mocks
 *
 *
 */

import {
  videoMockResponseData,
  videosMockResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import { onDemandVideoGetUrl } from "./useQueryOnDemandVideoGetService";
import { onDemandVideosGetUrl } from "./useQueryOnDemandVideosGetService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(`${onDemandVideoGetUrl}:videoId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(videoMockResponseData));
  }),
  rest.get(onDemandVideosGetUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(videosMockResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
