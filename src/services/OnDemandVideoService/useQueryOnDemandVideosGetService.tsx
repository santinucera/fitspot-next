/**
 *
 *
 *
 * useQueryOnDemandVideosGetService
 *
 *
 */
import {
  usePaginatedQuery,
  QueryConfig,
  QueryFunction,
  queryCache,
} from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { OnDemandVideosResponseData, OnDemandVideosModel } from "./types";
import { onDemandVideoModel } from "services/OnDemandVideoService/models";

export interface OnDemandVideosGetUrlParams {
  startDate?: string;
  endDate?: string;
  limit?: number;
  activityId?: number | null;
  categoryId?: number | null;
  trainerId?: number | null;
  offset?: number;
}

export interface OnDemandVideosGetServiceParams {
  queryConfig?: QueryConfig<OnDemandVideosResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  queryParams?: OnDemandVideosGetUrlParams;
}

export const onDemandVideosGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}customers/on-demand`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<OnDemandVideosModel> =>
  // This _ is the queryKey string
  (_: string, queryParams: OnDemandVideosGetUrlParams) =>
    api
      .get<OnDemandVideosResponseData>(onDemandVideosGetUrl, {
        ...axiosConfig,
        params: queryParams,
      })
      .then(({ data: { data, meta } }) => {
        return {
          data: data.map(onDemandVideoModel),
          meta,
        };
      });

export const useQueryOnDemandVideosGetService = ({
  queryConfig = {},
  axiosConfig = {},
  queryParams = {},
}: OnDemandVideosGetServiceParams = {}) => {
  return usePaginatedQuery<OnDemandVideosModel, AxiosError>({
    queryKey: ["on-demand-videos", queryParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryOnDemandVideosGetData = (
  urlParams?: OnDemandVideosGetUrlParams
): OnDemandVideosModel | undefined =>
  queryCache.getQueryData<OnDemandVideosModel>(["on-demand-videos", urlParams]);
