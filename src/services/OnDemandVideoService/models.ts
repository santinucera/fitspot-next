/**
 *
 *
 * Models for transforming OnDemandVideo response data
 *
 *
 */
import dayjs from "utils/days";
import { OnDemandVideo, VideoModel } from "./types";

/**
 * Transform /customers/on-demand response data
 */
export const onDemandVideoModel = (data: OnDemandVideo): VideoModel => {
  const { startDate: start, endDate: end, ...props } = data;
  const startDate = dayjs(start);
  const endDate = dayjs(end);
  const diff = dayjs.duration(endDate.diff(startDate));

  return {
    duration: `${diff.asMinutes()} min`,
    startDate: dayjs(startDate),
    endDate: dayjs(endDate),
    ...props,
  };
};
