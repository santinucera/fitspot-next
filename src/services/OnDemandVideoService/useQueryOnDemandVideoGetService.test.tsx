/**
 *
 *
 * Tests for useQueryOnDemandVideoGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryOnDemandVideoGetService } from "./useQueryOnDemandVideoGetService";
import { videoMockResponseData } from "./mockResponseData";
import { onDemandVideoModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryOnDemandVideoGetService({
      urlParams: {
        videoId: 123,
      },
    });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryOnDemandVideoGetService({
      urlParams: {
        videoId: 123,
      },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify({
        data: onDemandVideoModel(videoMockResponseData.data),
      })
    );
  });
});
