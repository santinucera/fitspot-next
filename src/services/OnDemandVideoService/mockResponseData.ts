/**
 *
 *
 * Mock Response Data for OnDemand
 *
 *
 */

import { OnDemandVideosResponseData, OnDemandVideoResponseData } from "./types";

export const videosMockResponseData: OnDemandVideosResponseData = {
  data: [
    {
      additionalInfo: "This is some additional info!",
      attachment: {
        url:
          "https://appdevfitspot-public.s3.amazonaws.com/413/355/931/haryopit.png",
      },
      attachmentId: 7942,
      description:
        "What do the healthiest, happiest people do every single day to thrive? In this webinar, you'll learn how to create a well-rounded balanced lifestyle, reduce overall stress, improve your relationships, and achieve your personal goals. Your dream life awaits!",
      endDate: new Date("2020-07-28T18:30:00Z"),
      localEndDate: new Date("2020-07-28T12:30:00"),
      thumbnailUrl:
        "https://tenspot.com/wp-content/uploads/2020/07/confetti-small.jpg",
      activity: "Boxing",

      id: 5838,
      trainer: "Bo-Bo B",
      videoUrl:
        "https://d1frml0vad4kq2.cloudfront.net/clients/TenSpot/2020-06-01_TenSpot_Making-Sustainable-Choices-at-Home_Kristen-F_45-Minutes_9686.mp4",
      name: "Johns intro to street fighting ",
      startDate: new Date("2020-07-28T18:00:00Z"),
      trainerId: 5937,
      localStartDate: new Date("2020-07-28T12:00:00"),
      trainerAvatar:
        "https://appdevfitspot.s3.amazonaws.com/978/485/945/trmzmvie.jpg?AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Signature=RLtijhDbxcxR4f%2BqyeiiC56Pdo0%3D&Expires=1609872594",
      activityCategory: "Chill",
      trainerPublicCode: "jennyj254",
    },
  ],
  meta: {
    pagination: {
      hasMore: true,
      total: 1,
      limit: 1,
      offset: 1,
    },
  },
};

export const videoMockResponseData: OnDemandVideoResponseData = {
  data: {
    id: 10452,
    activity: "Personal Development",
    additionalInfo: "This is some additional info!",
    attachment: {
      url:
        "https://appdevfitspot-public.s3.amazonaws.com/413/355/931/haryopit.png",
    },
    attachmentId: 7942,
    description:
      "What do the healthiest, happiest people do every single day to thrive? In this webinar, you'll learn how to create a well-rounded balanced lifestyle, reduce overall stress, improve your relationships, and achieve your personal goals. Your dream life awaits!",
    endDate: new Date("2020-09-17T17:00:00Z"),
    thumbnailUrl:
      "https://d1frml0vad4kq2.cloudfront.net/clients/TenSpot/Thumbnails/2020-09-17_TenSpot_Top-10-Lifestyle-Hacks-for-a-Healthier-Happier-Life_Jenny-J_60-Minutes_10452.jpg",
    trainer: "Jenny J",
    trainerAvatar:
      "https://appdevfitspot.s3.amazonaws.com/978/485/945/trmzmvie.jpg?AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Signature=RLtijhDbxcxR4f%2BqyeiiC56Pdo0%3D&Expires=1609872594",
    videoUrl:
      "https://d1frml0vad4kq2.cloudfront.net/clients/TenSpot/2020-06-01_TenSpot_Making-Sustainable-Choices-at-Home_Kristen-F_45-Minutes_9686.mp4",
    name: "Johns intro to street fighting ",
    startDate: new Date("2020-07-28T18:00:00Z"),
    trainerId: 5937,
    localStartDate: new Date("2020-07-28T12:00:00"),
    localEndDate: new Date("2020-07-28T13:00:00"),
    activityCategory: "Chill",
    trainerPublicCode: "jennyj254",
  },
};
