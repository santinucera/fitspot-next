/**
 *
 *
 * Tests for useQueryOnDemandVideosGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryOnDemandVideosGetService } from "./useQueryOnDemandVideosGetService";
import { videosMockResponseData } from "./mockResponseData";
import { onDemandVideoModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryOnDemandVideosGetService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryOnDemandVideosGetService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify({
        ...videosMockResponseData,
        data: videosMockResponseData.data.map(onDemandVideoModel),
      })
    );
  });
});
