/**
 *
 *
 * Types for OnDemandVideo
 *
 *
 */

import { Dayjs } from "dayjs";
import { Pillar, FileUrl } from "services/types";

export interface Pagination {
  pagination: {
    hasMore: boolean;
    limit: number;
    offset: number;
    total: number;
  };
}

export interface OnDemandVideosResponseData {
  data: OnDemandVideo[];
  meta: Pagination;
}

export interface OnDemandVideoResponseData {
  data: OnDemandVideo;
}

export interface OnDemandVideosModel {
  data: VideoModel[];
  meta: Pagination;
}

export interface OnDemandVideoModel {
  data: VideoModel;
}

export interface OnDemandVideo {
  endDate: Date;
  localEndDate: Date;
  thumbnailUrl: string;
  activity: string;
  id: number;
  trainer: string;
  videoUrl: string;
  name: string;
  startDate: Date;
  trainerId: number;
  localStartDate: Date;
  additionalInfo: string | null;
  attachmentId: number | null;
  attachment: FileUrl | null;
  description: string;
  trainerAvatar: string;
  activityCategory: Pillar;
  trainerPublicCode: string;
}

export interface VideoModel
  extends Omit<OnDemandVideo, "endDate" | "startDate"> {
  startDate: Dayjs;
  endDate: Dayjs;
  duration: string;
}
