/**
 *
 *
 * Mock Response Data for Notification
 *
 *
 */

import { ResponseData } from "./types";

const mockResponseData: ResponseData = {};

export const mockNotificationsGetSettingsResponseData: ResponseData = {
  email: true,
  group: "",
  id: 1,
  name: "",
  push: true,
  text: true,
};

export default mockResponseData;
