/**
 *
 *
 * Types for Notification
 *
 *
 */

export interface ResponseData {}

export interface NotificationsGetSettingsResponseData {
  email: boolean;
  group: string;
  id: number;
  name: string;
  push: boolean;
  text: boolean;
}

export interface NotificationsPostSettingsResponseData {
  email: true;
  id: 1;
  notification: true;
}
