/**
 *
 *
 * Tests for useQueryNotificationsGetSettingsService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryNotificationsGetSettingsService } from "./useQueryNotificationsGetSettingsService";
import { mockNotificationsGetSettingsResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryNotificationsGetSettingsService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryNotificationsGetSettingsService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockNotificationsGetSettingsResponseData)
    );
  });
});
