/**
 *
 *
 *
 * useMutationNotificationsPostSettingsService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { NotificationsPostSettingsResponseData } from "./types";

export const notificationsPostSettingsUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/notification-settings`;

const api = axios.create({ withCredentials: true });

interface RequestData {}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<NotificationsPostSettingsResponseData, RequestData> => (
  requestData: RequestData
) =>
  api
    .post<NotificationsPostSettingsResponseData>(
      notificationsPostSettingsUrl,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationNotificationsPostSettingsService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    NotificationsPostSettingsResponseData,
    AxiosError,
    RequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
