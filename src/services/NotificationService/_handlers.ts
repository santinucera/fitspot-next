/**
 *
 *
 * handlers for Notification service mocks
 *
 *
 */

import mockResponseData, {
  mockNotificationsGetSettingsResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import { notificationsGetSettingsUrl } from "./useQueryNotificationsGetSettingsService";
import { notificationsPostSettingsUrl } from "./useMutationNotificationsPostSettingsService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(notificationsGetSettingsUrl, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([mockNotificationsGetSettingsResponseData])
    );
  }),
  rest.post(notificationsPostSettingsUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
