/**
 *
 *
 *
 * useQueryNotificationsGetSettingsService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { NotificationsGetSettingsResponseData } from "./types";

export interface NotificationsGetSettingsServiceParams {
  queryConfig?: QueryConfig<NotificationsGetSettingsResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const notificationsGetSettingsUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/notification-settings`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<NotificationsGetSettingsResponseData> => () =>
  api
    .get<NotificationsGetSettingsResponseData>(
      notificationsGetSettingsUrl,
      axiosConfig
    )
    .then(({ data }) => data);

export const useQueryNotificationsGetSettingsService = ({
  queryConfig = {},
  axiosConfig = {},
}: NotificationsGetSettingsServiceParams = {}) => {
  return useQuery<NotificationsGetSettingsResponseData, AxiosError>({
    queryKey: "notifications-settings",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryNotificationsGetSettingsData = ():
  | NotificationsGetSettingsResponseData
  | undefined =>
  queryCache.getQueryData<NotificationsGetSettingsResponseData>(
    "notifications-settings"
  );
