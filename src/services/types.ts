/**
 *
 *
 * Shared types for services
 *
 *
 */

import { UserType } from "./UserService";

/**
 * User object from /users/me.
 *
 * From the `full_user_format` python serializer.
 */
export interface User {
  id: number;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  phone: string | null;
  userType: UserType;
  isVerified: boolean;
  isValidPhoneNumber: boolean;
  waiver: string | null;
  timezone: string | null;
  avatar: Avatar | null;
  gender: number | null;
  calendarPreference: number;
  trainer: UserTrainer | null; // trainer_format, maybe the only place this is used
  customer: Customer;
  userProfileReady: boolean;
  publicId: string | null;
  sessionInfo: ChatSession | null;
  companyList: CompanyWithUserRole[];
  inviteList: Invite[];
  requiredPasswordChange: boolean | null;
  balance: number | null;
  disabledAccount: boolean;
  disabledAt: string | null;
  humanApiConnected: boolean;
}

/**
 * Like a user, only less things.
 *
 * From the `short_user_format` Python serializer.
 */
export interface ShortUser {
  id: number;
  firstName: string | null;
  lastName: string | null;
  timezone: string | null;
  avatar: Avatar | null;
  gender: number | null;
  trainer: PublicTrainer | null;
  customer: Customer | null;
  publicId: string | null;
  sessionInfo: ChatSession | null;
  ratingAvg: number | null;
  ratingSum: number | null;
  ratingCount: number | null;
}

/**
 * Session Participant
 *
 * From `workout_participant` Python serializer.
 */
export interface Participant {
  userId: number;
  user: ShortUser;
  slotId: number | null;
  slot: Slot | null;
  isRSVP: boolean;
  wasAttended: boolean | null;
  ratingPoints: number | null;
  ratingText: string | null;
  ratingTrainer: number | null;
  ratingLocation: number | null;
  ratingFutureAttend: number | null;
  ratingStreamQuality: number | null;
}

/**
 * From `workout_slot` Python serializer.
 */
export interface Slot {
  id: number;
  dtStart: string;
  dtEnd: string;
  localDtStart: string;
  localDtEnd: string;
  limit: number;
  numParticipants: number;
  break: boolean;
}

/**
 * Avatars are part of a Trainer, Company, User.
 *
 * From the `file_format` Python serializer.
 */
export interface Avatar {
  id: number;
  filename: string | null;
  mimetype: string;
  size: number;
  url: string;
  userId: number | null;
}

/**
 * Truncated Avatar.
 *
 * From the `file_url` Python serializer.
 */
export interface FileUrl {
  url: string | null;
}

/**
 * Trainer object just for User. Most of the time
 * `trainer` is a ShortUser.
 */
export interface UserTrainer {
  insurance: Avatar;
  insuranceExpiry: string | null;
  certification: Avatar | null;
  certificationExpiry: Avatar | null;
  driverLic: Avatar | null;
  driverLicState: string | null;
  driverLicExpiry: string | null;
  bio: string | null;
  activities: Activity[];
  trainerProfileReady: boolean;
  isApproved: boolean;
  lat: number | null;
  lon: number | null;
  homeRadius: number | null;
  address: string | null;
  city: string | null;
  state: string | null;
  zipcode: string | null;
  tshirtSize: number | null;
  payoutTierName: string | null;
  payoutTierId: number | null;
  tipaltiId: string | null;
}

/**
 * Like a Trainer, only much less.
 *
 * From the `public_trainer_format` Python serializer.
 */
export interface PublicTrainer {
  bio: string | null;
  activities: Activity[];
}

/**
 * Sub-category of a session.
 *
 * From the `activity_format` Python serializer.
 */
export interface Activity {
  id: number;
  name: string;
  description: string | null;
  icon: Avatar | null;
  enterprise: boolean;
  category: string | null;
  maxParticipants: number | null;
}

/**
 * A Customer is part of the user data in Session participants.
 */
export interface Customer {
  birthday: string | null; // YYYY-MM-DD
  disabledEmailNotifications: boolean;
  numWorkouts: number | null;
  weight: number | null;
  disabledTextNotifications: boolean;
  height: string | null; // Sometimes height is an empty string.
  disabledPushNotifications: boolean;
  customerProfileReady: boolean;
  preferredTrainerGender: number | null;
}

export interface Company {
  id: number;
  name: string;
  entityType: number;
  logo: Avatar | null;
  eventLogo: FileUrl | null;
  departments: Department[];
  officeLocations: OfficeLocation[];
  participants: CompanyUser[];
  employeeLimit: number | null;
  environment: string | null;
  industry: string | null;
  parkingNotes: string | null;
  website: string | null;
  otherServices: string | null;
  companySize: string | null;
  disabledChallenges: boolean;
  disabledPartnerships: boolean;
  servicesTabVisible: boolean;
  challengesTabVisible: boolean;
  coBrandPortal: boolean;
  activeSince: string | null;
  portalTheme: string | null;
}

/**
 * Company object with some user stuff as returned by User.companyList.
 *
 * From the `company_with_user_role` Python serializer.
 */
export interface CompanyWithUserRole extends Omit<Company, "participants"> {
  publicCode: string | null;
  roleWithinCompany: number | null;
  allowedDepartments: number[];
  assignedDepartment: Department | null;
  assignedOfficeLocation: OfficeLocation | null;
  deactivated: boolean;
  deactivatedAt: string | null;
}

/**
 * User type for Company.participants.
 *
 * From the `user_association` Python serializer.
 */
export interface CompanyUser {
  userType: UserType;
  department: Department | null;
  officeLocation: OfficeLocation | null;
  user: ShortUser | null;
  userCode: string | null;
  deactivated: boolean;
  activatedAt: string | null;
  deactivatedAt: string | null;
  deactivationLog: string | null;
  allowedDepartments: number[];
}

/**
 * Like a company, only less stuff.
 *
 * From the `company_short_info` Python serializer.
 */
export interface ShortCompany {
  name: string;
  logo: File;
  eventLogo: File;
  departments: Department[];
  officeLocations: OfficeLocation[];
  environment: string;
  industry: string;
  parkingNotes: string;
  website: string;
  otherServices: string;
  companySize: string;
}

export interface Department {
  name?: string;
  isActive?: boolean;
  id?: number;
}

export interface OfficeLocation {
  name: string | null;
  isActive: boolean;
  state: string | null;
  city: string | null;
  zipcode: string | null;
  address: string | null;
  id?: number;
}

/**
 * From `office_location_with_places` Python serializer.
 */
export interface OfficeLocationWithPlaces extends OfficeLocation {
  places: MeetingPlace[];
}

/**
 * From `meeting_place` Python serializer.
 */
export interface MeetingPlace {
  id: number;
  name: string;
  officeLocationId: number | null;
  address: string | null;
  city: string | null;
  state: string | null;
  zipcode: string | null;
  isActive: boolean;
  maxParticipants: number | null;
  maxParticipantsOverride: boolean;
}

export interface AppSettings {
  enterpriseActivities: EnterpriseActivity[];
  sessionPeriod: number;
  maxRadius: number;
  priceBase: number;
  cancellationReasons: string[];
  priceFriend: number;
  lateReasons: string[];
  categories: Category[];
  braintreeClientToken: null;
}

export interface Category {
  id: number;
  name: Pillar;
}

export interface EnterpriseActivity {
  id: number;
  name: string;
  description: string;
  icon: Avatar;
  category: string;
  enterprise: boolean;
  maxParticipants: number;
}

/**
 * Not a session at all, but instead describes a CometChat group.
 *
 * From the `chat_session` Python serializer.
 */
export interface ChatSession {
  sessionId: number;
  createdDate: string;
  lastUserReadDate: string;
  lastTrainerReadDate: string;
  userUnreadMessagesNumber: number;
  trainerUnreadMessagesNumber: number;
  lastMessage: ChatMessage;
  lastMessageDate: string;
  customer: ChatUser;
  trainer: ChatUser;
  like: boolean;
}

export interface ChatMessage {
  id: number;
  text: string;
  createdAt: string;
  user: ChatUser;
  sessionId: number;
}

/**
 * A CometChat user.
 *
 * From the `user_chat_format` Python serializer.
 */
export interface ChatUser {
  id: number;
  name: string;
  avatar: File;
  email: string;
}

export interface Invite {
  id: number;
  userId: number;
  companyId: number;
  company: InviteCompany;
  email: string | null;
  firstName: string | null;
  lastName: string | null;
  userDepartment: Department | null;
  userOfficeLocation: OfficeLocation | null;
}

/**
 * Truncated Company just for Invite.
 *
 * From the `company_public_info_within_invite` Python serializer.
 */
export interface InviteCompany {
  id: number;
  name: string | null;
  publicCode: string | null;
  logo: FileUrl;
  eventLogo: FileUrl;
  departments: Department[];
  officeLocations: OfficeLocation[];
}

/**
 *
 */
export enum Gender {
  Male = 0,
  Female = 1,
  Unspecified = 2,
}

export type Pillar = "Move" | "Play" | "Chill" | "Eat" | "Thrive";
