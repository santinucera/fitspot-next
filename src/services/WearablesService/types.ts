/**
 *
 *
 * Types for Wearables
 *
 *
 */

export interface ResponseData {
  avatarUrl: string;
  id: number;
  name: string;
  total: number;
  rank: number;
}
