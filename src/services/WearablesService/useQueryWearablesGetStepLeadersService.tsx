/**
 *
 *
 *
 * useQueryWearablesGetStepLeadersService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ResponseData } from "./types";

export interface WearablesGetStepLeadersUrlParams {
  type: number;
  dateStart: string;
  dateEnd: string;
  limit: number;
  companyId: number;
}

export interface WearablesGetStepLeadersServiceParams {
  queryConfig?: QueryConfig<ResponseData[], AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: WearablesGetStepLeadersUrlParams;
}

export const wearablesGetStepLeadersUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/wearables-leaderboard/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ResponseData[]> =>
  // This _ is the queryKey string
  (_: string, { companyId, ...params }: WearablesGetStepLeadersUrlParams) =>
    api
      .get<ResponseData[]>(`${wearablesGetStepLeadersUrl}${companyId}`, {
        ...axiosConfig,
        params,
      })
      .then(({ data }) => data);

export const useQueryWearablesGetStepLeadersService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: WearablesGetStepLeadersServiceParams) => {
  return useQuery<ResponseData[], AxiosError>({
    queryKey: ["step-leaders", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryWearablesGetStepLeadersData = (
  urlParams: WearablesGetStepLeadersUrlParams
): ResponseData[] | undefined =>
  queryCache.getQueryData<ResponseData[]>(["step-leaders", urlParams]);
