/**
 *
 *
 * handlers for Wearables service mocks
 *
 *
 */

import { mockResponseData } from "./mockResponseData";
import { rest } from "msw";
import { wearablesGetStepLeadersUrl } from "./useQueryWearablesGetStepLeadersService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(`${wearablesGetStepLeadersUrl}:companyId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
