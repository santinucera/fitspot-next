/**
 *
 *
 * Tests for useQueryWearablesGetStepLeadersService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryWearablesGetStepLeadersService } from "./useQueryWearablesGetStepLeadersService";
import { mockResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryWearablesGetStepLeadersService({
      urlParams: {
        companyId: 1,
        type: 0,
        limit: 5,
        dateEnd: "date",
        dateStart: "date",
      },
    });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryWearablesGetStepLeadersService({
      urlParams: {
        companyId: 1,
        type: 0,
        limit: 5,
        dateEnd: "date",
        dateStart: "date",
      },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockResponseData)
    );
  });
});
