/**
 *
 *
 * Mock Response Data for Wearables
 *
 *
 */

import { ResponseData } from "./types";

export const mockResponseData: ResponseData[] = [
  {
    avatarUrl: "",
    id: 2,
    name: "bob",
    total: 1,
    rank: 1,
  },
];
