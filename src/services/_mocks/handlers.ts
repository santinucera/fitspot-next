/**
 *
 *
 * handlers for service mocks
 *
 *
 */
import { rest } from "msw";
import userServiceHandlers from "../UserService/_handlers";
import sessionServiceHandlers from "../SessionService/_handlers";
import calendarServiceHandlers from "../CalendarService/_handlers";
import notificationServiceHandlers from "../NotificationService/_handlers";
import wearablesServiceHandlers from "../WearablesService/_handlers";
import challengeServiceHandlers from "../ChallengeService/_handlers";
import cometChatServiceHandlers from "../CometChatService/_handlers";
import appsServiceHandlers from "../AppsService/_handlers";
import expertServiceHandlers from "../ExpertService/_handlers";
import activityFeedServiceHandlers from "../ActivityFeedService/_handlers";
import authServiceHandlers from "../AuthService/_handlers";
import humanApiServiceHandlers from "../HumanApiService/_handlers";
import onDemandVideoServiceHandlers from "../OnDemandVideoService/_handlers";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  ...userServiceHandlers,
  ...sessionServiceHandlers,
  ...calendarServiceHandlers,
  ...notificationServiceHandlers,
  ...wearablesServiceHandlers,
  ...challengeServiceHandlers,
  ...cometChatServiceHandlers,
  ...appsServiceHandlers,
  ...expertServiceHandlers,
  ...activityFeedServiceHandlers,
  ...authServiceHandlers,
  ...humanApiServiceHandlers,
  ...onDemandVideoServiceHandlers,
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly

  rest.get("*", (req, res, ctx) => {
    console.error("Unhandled request", req);
    return res(ctx.status(500));
  }),
];

export default handlers;
