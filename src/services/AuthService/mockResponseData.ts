/**
 *
 *
 * Mock Response Data for Auth
 *
 *
 */

import { LogoutResponseData, DeleteAccountResponseData } from "./types";

export const mockLogoutResponseData: LogoutResponseData = {
  success: true,
};

export const mockDeleteAccountResponseData: DeleteAccountResponseData = {
  success: true,
};
