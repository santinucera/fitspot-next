/**
 *
 *
 *
 * useMutationAuthPostLogoutService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { LogoutResponseData } from "./types";

export const authPostLogoutUrl = `${process.env.REACT_APP_TENSPOT_API_V1}auth/logout`;

const api = axios.create({ withCredentials: true });

interface RequestData {}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<LogoutResponseData, RequestData> => (
  requestData: RequestData
) =>
  api
    .post<LogoutResponseData>(authPostLogoutUrl, requestData, axiosConfig)
    .then(({ data }) => data);

export const useMutationAuthPostLogoutService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    LogoutResponseData,
    AxiosError,
    RequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
