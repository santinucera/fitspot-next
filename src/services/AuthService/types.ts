/**
 *
 *
 * Types for Auth
 *
 *
 */

export interface LogoutResponseData {
  success: boolean;
}

export interface DeleteAccountResponseData {
  success: boolean;
}
