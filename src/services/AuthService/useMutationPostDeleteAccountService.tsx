/**
 *
 *
 *
 * useMutationPostDeleteAccountService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { DeleteAccountResponseData } from "./types";

export const postDeleteAccountUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/deactivate-self-all/`;

const api = axios.create({ withCredentials: true });

interface RequestData {}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<DeleteAccountResponseData, RequestData> => (
  requestData: RequestData
) =>
  api
    .post<DeleteAccountResponseData>(
      postDeleteAccountUrl,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationPostDeleteAccountService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    DeleteAccountResponseData,
    AxiosError,
    RequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
