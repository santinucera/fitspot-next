/**
 *
 *
 * handlers for Auth service mocks
 *
 *
 */

import {
  mockLogoutResponseData,
  mockDeleteAccountResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import { authPostLogoutUrl } from "./useMutationAuthPostLogoutService";
import { postDeleteAccountUrl } from "./useMutationPostDeleteAccountService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.post(authPostLogoutUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockLogoutResponseData));
  }),
  rest.post(postDeleteAccountUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockDeleteAccountResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
