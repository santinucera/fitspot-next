/**
 *
 *
 * Tests for useMutationCalendarPostSettingsService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationCalendarPostSettingsService } from "./useMutationCalendarPostSettingsService";
import { mockCalendarPostSettingsResponseData } from "./mockResponseData";

it("CalendarPostSettings should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationCalendarPostSettingsService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("CalendarPostSettings should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationCalendarPostSettingsService();
    return (
      <div>
        <button onClick={() => mutate()}>Mutate</button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockCalendarPostSettingsResponseData)
    );
  });
});
