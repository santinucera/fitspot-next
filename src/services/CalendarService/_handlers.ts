/**
 *
 *
 * handlers for Calendar service mocks
 *
 *
 */

import {
  mockCalendarGetSettingsResponseData,
  mockCalendarPostSettingsResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import {
  calendarPostSettingsBaseURL,
  calendarPostSettingsPATH,
} from "./useMutationCalendarPostSettingsService";
import { calendarGetSettingsUrl } from "./useQueryCalendarGetSettingsService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.post(
    `${calendarPostSettingsBaseURL}${calendarPostSettingsPATH}`,
    (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json(mockCalendarPostSettingsResponseData)
      );
    }
  ),
  rest.get(calendarGetSettingsUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockCalendarGetSettingsResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
