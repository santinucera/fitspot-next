/**
 *
 *
 *
 * useMutationCalendarPostSettingsService
 *
 *
 */

import { useMutation, MutationConfig } from "react-query";
import axios, { AxiosError } from "axios";
import { CalendarPreference, CalendarSettingsResponseData } from "./types";

export interface CalendarPostSettingsParams {
  preference: CalendarPreference;
}

export interface CalendarPostSettingsUrlParams {}

export const calendarPostSettingsBaseURL = process.env.REACT_APP_TENSPOT_API_V1;

export const calendarPostSettingsPATH = "users/calendar-preferences";

const api = axios.create({
  baseURL: calendarPostSettingsBaseURL,
  withCredentials: true,
});

const queryFn = (params: CalendarPostSettingsParams) =>
  api
    .post<CalendarSettingsResponseData>(calendarPostSettingsPATH, params)
    .then(({ data }) => data);

export const useMutationCalendarPostSettingsService = (
  config: MutationConfig<
    CalendarSettingsResponseData,
    AxiosError,
    CalendarPostSettingsParams,
    () => void
  > = {}
) => {
  return useMutation(queryFn, config);
};
