/**
 *
 *
 *
 * useQueryCalendarGetSettingsService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { CalendarSettingsResponseData } from "./types";

export interface CalendarGetSettingsServiceParams {
  queryConfig?: QueryConfig<CalendarSettingsResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const calendarGetSettingsUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/calendar-preferences`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<CalendarSettingsResponseData> => () =>
  api
    .get<CalendarSettingsResponseData>(calendarGetSettingsUrl, axiosConfig)
    .then(({ data }) => data);

export const useQueryCalendarGetSettingsService = ({
  queryConfig = {},
  axiosConfig = {},
}: CalendarGetSettingsServiceParams = {}) => {
  return useQuery<CalendarSettingsResponseData, AxiosError>({
    queryKey: "calendar-settings",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryCalendarGetSettingsData = ():
  | CalendarSettingsResponseData
  | undefined =>
  queryCache.getQueryData<CalendarSettingsResponseData>("calendar-settings");
