/**
 *
 *
 * Mock Response Data for Calendar
 *
 *
 */

import { CalendarSettingsResponseData } from "./types";

export const mockCalendarGetSettingsResponseData: CalendarSettingsResponseData = {
  preference: 0,
};

export const mockCalendarPostSettingsResponseData: CalendarSettingsResponseData = {
  preference: 0,
};
