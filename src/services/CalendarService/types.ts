/**
 *
 *
 * Types for Calendar
 *
 *
 */

export interface CalendarSettingsResponseData {
  preference: CalendarPreference;
}

export enum CalendarPreference {
  SHOW_PROMPTS_NO_CALENDAR_SELECTED = 0,
  HIDE_PROMPTS = 1,
  GOOGLE = 2,
  APPLE = 3,
  YAHOO = 4,
  OUTLOOK_WEB = 5,
  OUTLOOK_DESKTOP = 6,
  ICAL = 7,
}
