/**
 *
 *
 * Mock Response Data for OnDemand
 *
 *
 */

import { OnDemandResponseData } from "./types";

export const mockResponseData: OnDemandResponseData[] = [
  {
    endDate: new Date("2020-07-28T18:30:00Z"),
    localEndDate: new Date("2020-07-28T12:30:00"),
    thumbnailUrl:
      "https://tenspot.com/wp-content/uploads/2020/07/confetti-small.jpg",
    activity: "Boxing",
    id: 5838,
    trainer: "Bo-Bo B",
    videoUrl:
      "https://d1frml0vad4kq2.cloudfront.net/clients/TenSpot/2020-06-01_TenSpot_Making-Sustainable-Choices-at-Home_Kristen-F_45-Minutes_9686.mp4",
    name: "Johns intro to street fighting ",
    startDate: new Date("2020-07-28T18:00:00Z"),
    trainerId: 5937,
    localStartDate: new Date("2020-07-28T12:00:00"),
    activityCategory: "Chill",
  },
];
