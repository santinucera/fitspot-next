/**
 *
 *
 * Types for OnDemand
 *
 *
 */

import { Dayjs } from "dayjs";
import { Pillar } from "services/types";

export interface OnDemandResponseData {
  endDate: Date;
  localEndDate: Date;
  thumbnailUrl: string;
  activity: string;
  id: number;
  trainer: string;
  videoUrl: string;
  name: string;
  startDate: Date;
  trainerId: number;
  localStartDate: Date;
  activityCategory: Pillar;
}

export interface OnDemandModel
  extends Omit<OnDemandResponseData, "endDate" | "startDate"> {
  startDate: Dayjs;
  endDate: Dayjs;
  duration: string;
}
