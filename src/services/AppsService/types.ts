/**
 *
 *
 * Types for Apps
 *
 *
 */

export interface AppsResponseData {
  category: string;
  id: number;
  loginUrl: string;
  priority: number;
  scene1: string;
  scene2: string;
  scene3: string;
}

export interface AppsModel {
  category: string;
  content: Array<string>;
  logo: string;
  url: string;
  id: number;
}
