/**
 *
 *
 * Models for transforming AppsService response data
 *
 *
 */
import { AppsResponseData, AppsModel } from "./types";

/**
 * Transform /workouts/:id response data
 */
export const appsModel = (responseData: AppsResponseData[]): AppsModel[] => {
  return responseData.map(
    ({ category, scene1, scene2, loginUrl, id, ...props }) => {
      const content = [];
      const matches = scene2.matchAll(/>(.*?)<\/p>/g);
      for (const match of Array.from(matches)) content.push(match[1].trim());
      return {
        category,
        content,
        logo: scene1,
        url: loginUrl,
        id,
      };
    }
  );
};
