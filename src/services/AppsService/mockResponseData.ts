/**
 *
 *
 * Mock Response Data for Apps
 *
 *
 */

import { AppsResponseData } from "./types";

export const mockResponseData: AppsResponseData[] = [
  {
    category: "Mental Wellness",
    id: 20,
    loginUrl: "https://www.whil.com/companies",
    priority: 0,
    scene1:
      "https://appdevfitspot.s3.amazonaws.com/988/725/794/jhyichus.png?AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Expires=1601390898&Signature=t6ZbwQJudqzieOsmSGjjE9SkOtA%3D",
    scene2:
      "<p> Digital resilience, mindfulness and emotional intelligence training </p>, <p style='' > Whil < /p>",
    scene3:
      "https://appdevfitspot.s3.amazonaws.com/680/686/305/vogoaedl.png?AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Expires=1601390898&Signature=GUhcDyeNfCrVr9Eow0fFti2rxnI%3D",
  },
];
