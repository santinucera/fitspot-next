/**
 *
 *
 *
 * useQueryAppsGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { AppsResponseData, AppsModel } from "./types";
import { appsModel } from "./models";

export interface AppsGetUrlParams {
  companyId?: number;
}

export interface AppsGetServiceParams {
  queryConfig?: QueryConfig<AppsResponseData[], AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: AppsGetUrlParams;
}

export const appsGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/company-partners/`;

const api = axios.create({ withCredentials: true });

const queryFn = (axiosConfig: AxiosRequestConfig): QueryFunction<AppsModel[]> =>
  // This _ is the queryKey string
  (_: string, { companyId }: AppsGetUrlParams) =>
    api
      .get<AppsResponseData[]>(`${appsGetUrl}${companyId}`, axiosConfig)
      .then(({ data }) => appsModel(data));

export const useQueryAppsGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: AppsGetServiceParams) => {
  return useQuery<AppsModel[], AxiosError>({
    queryKey: ["apps", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryAppsGetServiceData = (
  urlParams: AppsGetUrlParams
): AppsModel[] | undefined =>
  queryCache.getQueryData<AppsModel[]>(["apps", urlParams]);
