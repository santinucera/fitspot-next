/**
 *
 *
 * UserService models
 *
 *
 */
import { User } from "services/types";
import { RolesWithinCompany } from "services/UserService";

export const isAdmin = (user?: User): boolean =>
  user?.companyList?.some(
    ({ roleWithinCompany }) => roleWithinCompany === RolesWithinCompany.admin
  ) || false;

export const hasAppsAccess = (user?: User): boolean =>
  user?.companyList?.some(({ servicesTabVisible }) => servicesTabVisible) ||
  false;
