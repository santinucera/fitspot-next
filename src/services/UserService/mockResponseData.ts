/**
 *
 *
 * Mock Response Data for User
 *
 *
 */
import { User } from "../types";
import { ImageResponseData, AppSettings } from "./types";

/**
 * Response from /users/me
 */
export const mockUserGetResponseData: User = {
  userProfileReady: true,
  id: 12345,
  trainer: null,
  humanApiConnected: true,
  waiver: null,
  publicId: "public_id",
  balance: 0,
  inviteList: [],
  isValidPhoneNumber: false,
  requiredPasswordChange: null,
  phone: null,
  timezone: "US/Eastern",
  avatar: null,
  disabledAccount: false,
  companyList: [
    {
      activeSince: "2017-05-11T15:40:15Z",
      website: "google.com",
      roleWithinCompany: 2,
      allowedDepartments: [],
      disabledChallenges: true,
      entityType: 0,
      otherServices:
        "Other Services Other Services Other Services Other Services Other Services ",
      publicCode: "test987",
      employeeLimit: 500,
      id: 3,
      assignedDepartment: {
        name: "Engineering",
        isActive: true,
        id: 280,
      },
      coBrandPortal: false,
      deactivated: false,
      parkingNotes: "Parking details ",
      officeLocations: [
        {
          address: "200 North Avenue",
          city: "Los Angeles",
          name: "Los Angeles",
          state: "CA",
          id: 20,
          isActive: true,
          zipcode: "90034",
        },
      ],
      eventLogo: {
        url:
          "https://appdevfitspot.s3.amazonaws.com/896/950/808/pqxvjail?Signature=lwljipUlYs%2BjLGKZ1MfzhDdadhw%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Expires=1600729558",
      },
      environment: "Company environment ",
      servicesTabVisible: true,
      deactivatedAt: null,
      departments: [{ name: "Sales", id: 22, isActive: true }],
      industry: "Some Industry",
      assignedOfficeLocation: {
        address: "21 Peachtree Pl NW #400 TEST",
        city: "Atlanta",
        isActive: true,
        state: "GA",
        id: 152,
        name: "Atlanta",
        zipcode: "30309",
      },
      logo: {
        size: 420909,
        filename: "blob",
        userId: null,
        id: 4561,
        url:
          "https://appdevfitspot-public.s3.amazonaws.com/819/596/913/ofpaywkf",
        mimetype: "image/png",
      },
      challengesTabVisible: false,
      name: "Test Company",
      disabledPartnerships: true,
      companySize: "150",
      portalTheme: "Blue",
    },
  ],
  firstName: "Jane",
  lastName: "Doe",
  userType: 3,
  disabledAt: null,
  sessionInfo: null,
  email: "example@tenspot.com",
  isVerified: true,
  customer: {
    customerProfileReady: true,
    preferredTrainerGender: 0,
    disabledEmailNotifications: false,
    disabledTextNotifications: false,
    weight: null,
    disabledPushNotifications: false,
    numWorkouts: 0,
    height: "",
    birthday: null,
  },
  gender: 0,
  calendarPreference: 0,
};

export const mockImageResponseData: ImageResponseData = {
  id: "123",
};

export const mockUserAppSettingsGetResponseData: AppSettings = {
  priceBase: 69.0,
  braintreeClientToken: null,
  cancellationReasons: [
    "Trainer Sick",
    "Trainer Did Not Show",
    "Customer Cancelled",
  ],
  activities: [],
  maxRadius: 20,
  priceFriend: 15.0,
  categories: [
    {
      name: "Chill",
      id: 4,
    },
    {
      name: "Experience",
      id: 3,
    },
    {
      name: "Move",
      id: 6,
    },
    {
      name: "Play",
      id: 7,
    },
    {
      name: "Thrive",
      id: 8,
    },
  ],
  enterpriseActivities: [
    {
      name: "Entertainment",
      category: "Other",
      description:
        "Come and meet your FitExperts! Set goals, ask questions and learn more about your upcoming wellness program with Fitspot.",
      icon: {
        mimetype: "image/svg+xml",
        filename: "kickoff.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/160/463/958/cmiegbjk.svg?Expires=1601933223&Signature=Bv8%2FRkG5hf9aspDGfCWbvp0m4A0%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 2512,
        id: 3038,
        userId: 6277,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 34,
    },
    {
      name: "Personal Development",
      category: "Thrive",
      description: "Stretching",
      icon: {
        mimetype: "image/svg+xml",
        filename: "stretching.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/257/53/891/ypnrfgcg.svg?Expires=1601933223&Signature=xj7LQYZ%2FY3oxuwzn%2FCV3Ymr979s%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 958,
        id: 3047,
        userId: 6277,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 15,
    },
    {
      name: "Professional Development",
      category: "Thrive",
      description: "",
      icon: {
        mimetype: "image/png",
        filename: "Ellipse_73.png",
        url:
          "https://appdevfitspot.s3.amazonaws.com/131/696/451/otrxoltu.png?Expires=1601933223&Signature=PhP%2FsgW88mrHXRexOG6QtcS8ebw%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 0,
        id: 5306,
        userId: 7087,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 45,
    },
    {
      name: "Test Bug Activity",
      category: "Eat",
      description: "Test Bug Activity",
      icon: null,
      maxParticipants: 15,
      enterprise: true,
      id: 39,
    },
    {
      name: "Meditation",
      category: "Chill",
      description: "",
      icon: {
        mimetype: "image/svg+xml",
        filename: "meditation.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/171/995/196/aaudalvx.svg?Expires=1601933223&Signature=NBfFaMcKqtJ7rrvz0SnCrPow3%2Bw%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 3932,
        id: 3025,
        userId: 6277,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 24,
    },
    {
      name: "Full Body Workout",
      category: "Move",
      description: "",
      icon: {
        mimetype: "image/svg+xml",
        filename: "Bootcamp.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/711/899/475/oqfgbxvp.svg?Expires=1601933223&Signature=jCnngNFYQg8nCwuCPUsgzctWLCU%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 3068,
        id: 3027,
        userId: 6277,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 14,
    },
    {
      name: "Food/Drink Workshop",
      category: "Eat",
      description: "",
      icon: {
        mimetype: "image/svg+xml",
        filename: "self_defense_1.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/625/370/933/znecaqnd.svg?Expires=1601933223&Signature=gBLP5y3MeyBmITsD4tJaYCVTYXE%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 1601,
        id: 3650,
        userId: 5540,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 25,
    },
    {
      name: "Boxing",
      category: null,
      description:
        "Jab and move through a cardio workout that is a TKO for frustration.",
      icon: {
        mimetype: "image/svg+xml",
        filename: "boxing.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/806/1001/154/zeuidefv.svg?Expires=1601933223&Signature=rJEMyKICVgBE22ONBC4%2FFNDgCg0%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 6918,
        id: 3028,
        userId: 6277,
      },
      maxParticipants: 15,
      enterprise: true,
      id: 5,
    },
    {
      name: "Yoga",
      category: "Move",
      description: "This is the most awesome Yoga!",
      icon: {
        mimetype: "image/svg+xml",
        filename: "yoga.svg",
        url:
          "https://appdevfitspot.s3.amazonaws.com/184/869/130/ikktecrv.svg?Expires=1601933223&Signature=ixGC1wRlFmtEQP2G7fDuuz9DSDs%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
        size: 4410,
        id: 3050,
        userId: null,
      },
      maxParticipants: 2,
      enterprise: true,
      id: 10,
    },
  ],
  sessionPeriod: 1,
  lateReasons: [
    "Forgot to Check In",
    "Traffic",
    "Running Late",
    "Parking Issue",
    "No Phone Service",
    "Issues Entering Building",
    "Other",
  ],
};
