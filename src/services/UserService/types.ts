/**
 *
 *
 * Types for User
 *
 *
 */
export interface ImageResponseData {
  id: string;
}

export interface AppSettings {
  priceBase: number;
  braintreeClientToken: null;
  cancellationReasons: string[];
  activities: any[];
  maxRadius: number;
  priceFriend: number;
  categories: Category[];
  enterpriseActivities: EnterpriseActivity[];
  sessionPeriod: number;
  lateReasons: string[];
}

export interface Category {
  name: string;
  id: number;
}

export interface EnterpriseActivity {
  name: string;
  category: null | string;
  description: string;
  icon: Icon | null;
  maxParticipants: number;
  enterprise: boolean;
  id: number;
}

export interface Icon {
  mimetype: string;
  filename: string;
  url: string;
  size: number;
  id: number;
  userId: number | null;
}

export enum RolesWithinCompany {
  unknown = 0,
  employee = 1,
  admin = 2,
  manager = 3,
}

export enum UserType {
  unknown = 0,
  customer = 1,
  trainer = 2,
  admin = 3,
  kiosk = 4,
}
