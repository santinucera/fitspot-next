/**
 *
 *
 * Tests for UserService models
 *
 *
 */
import { isAdmin } from "./models";
import { mockUserGetResponseData } from "./mockResponseData";
import { RolesWithinCompany } from "./types";

describe("isAdmin", () => {
  it("should determine whether a user is an admin", () => {
    const [company] = mockUserGetResponseData.companyList;
    const user = {
      ...mockUserGetResponseData,
      companyList: [
        { ...company, roleWithinCompany: RolesWithinCompany.admin },
      ],
    };
    expect(isAdmin(user)).toBe(true);
  });

  it("should return false", () => {
    const [company] = mockUserGetResponseData.companyList;
    const user = {
      ...mockUserGetResponseData,
      companyList: [
        { ...company, roleWithinCompany: RolesWithinCompany.unknown },
      ],
    };
    expect(isAdmin(user)).toBe(false);
  });
});
