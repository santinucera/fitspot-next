/**
 *
 *
 *
 * useQueryUserGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { User } from "../types";

export interface UserGetServiceParams {
  queryConfig?: QueryConfig<User, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const userGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/me`;

const api = axios.create({ withCredentials: true });

const queryFn = (axiosConfig: AxiosRequestConfig): QueryFunction<User> => () =>
  api.get<User>(userGetUrl, axiosConfig).then(({ data }) => data);

export const useQueryUserGetService = ({
  queryConfig = {},
  axiosConfig = {},
}: UserGetServiceParams = {}) => {
  return useQuery<User, AxiosError>({
    queryKey: "user",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryUserGetData = (): User => {
  const userData = queryCache.getQueryData<User>("user");
  /**
   * If this is being called without user cache there should be an error.
   */
  if (!userData)
    throw Error("You are trying to access user cache that does not exist.");
  return userData;
};
