/**
 *
 *
 *
 * useMutationUserPatchService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { User, Gender } from "../types";

export const userPatchUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/edit`;

const api = axios.create({ withCredentials: true });

interface RequestData {
  firstName?: string | null;
  lastName?: string | null;
  gender?: Gender | null;
  avatarId?: number | null;
  customer?: { birthday: string } | null;
  trainer?: {
    bio: string;
    address: string;
    city: string;
    state: string;
    zipcode: string;
  };
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<User, RequestData> => (requestData: RequestData) =>
  api
    .patch<User>(userPatchUrl, requestData, axiosConfig)
    .then(({ data }) => data);

export const useMutationUserPatchService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<User, AxiosError, RequestData, () => void>;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
