/**
 *
 *
 * handlers for User service mocks
 *
 *
 */

import {
  mockUserGetResponseData,
  mockImageResponseData,
  mockUserAppSettingsGetResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import { userGetUrl } from "./useQueryUserGetService";
import { userAppSettingsGetUrl } from "./useQueryUserAppSettingsGetService";
import { userPostImageUrl } from "./useMutationUserPostImageService";
import { userPatchUrl } from "./useMutationUserPatchService";
import { userPatchPasswordUrl } from "./useMutationUserPatchPasswordService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(userGetUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockUserGetResponseData));
  }),
  rest.get(userAppSettingsGetUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockUserAppSettingsGetResponseData));
  }),
  rest.post(userPostImageUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockImageResponseData));
  }),
  rest.patch(userPatchUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockUserGetResponseData));
  }),
  rest.patch(userPatchPasswordUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockUserGetResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
