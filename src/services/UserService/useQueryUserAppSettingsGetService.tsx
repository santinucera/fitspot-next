/**
 *
 *
 *
 * useQueryUserAppSettingsGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { AppSettings } from "../types";

export interface UserAppSettingsGetServiceParams {
  queryConfig?: QueryConfig<AppSettings, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const userAppSettingsGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}users/app-settings`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<AppSettings> => () =>
  api
    .get<AppSettings>(userAppSettingsGetUrl, axiosConfig)
    .then(({ data }) => data);

export const useQueryUserAppSettingsGetService = ({
  queryConfig = {},
  axiosConfig = {},
}: UserAppSettingsGetServiceParams = {}) => {
  return useQuery<AppSettings, AxiosError>({
    queryKey: "user-app-settings",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryUserAppSettingsGetData = (): AppSettings | undefined =>
  queryCache.getQueryData<AppSettings>("user-app-settings");
