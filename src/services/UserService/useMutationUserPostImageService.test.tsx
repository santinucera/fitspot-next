/**
 *
 *
 * Tests for useMutationUserPostImageService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationUserPostImageService } from "./useMutationUserPostImageService";
import { mockImageResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationUserPostImageService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationUserPostImageService();
    return (
      <div>
        <button onClick={() => mutate()}>Mutate</button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockImageResponseData)
    );
  });
});
