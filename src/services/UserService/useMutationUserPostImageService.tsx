/**
 *
 *
 *
 * useMutationUserPostImageService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { User } from "../types";

export const userPostImageUrl = `${process.env.REACT_APP_TENSPOT_API_V1}files/upload`;

const api = axios.create({ withCredentials: true });

interface RequestData extends FormData {}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<User, RequestData> => (requestData: RequestData) =>
  api
    .post<User>(userPostImageUrl, requestData, axiosConfig)
    .then(({ data }) => data);

export const useMutationUserPostImageService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<User, AxiosError, RequestData, () => void>;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
