/**
 *
 *
 *
 * useMutationUserPatchPasswordService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { User } from "../types";

export const userPatchPasswordUrl = `${process.env.REACT_APP_TENSPOT_API_V1}auth/reset-password`;

const api = axios.create({ withCredentials: true });

export interface PasswordPatchRequestData {
  email: string;
  newPassword: string;
  oldPassword: string;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<User, PasswordPatchRequestData> => (
  requestData: PasswordPatchRequestData
) =>
  api
    .patch<User>(userPatchPasswordUrl, requestData, axiosConfig)
    .then(({ data }) => data);

export const useMutationUserPatchPasswordService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    User,
    AxiosError,
    PasswordPatchRequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
