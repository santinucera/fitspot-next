/**
 *
 *
 *
 * useQueryExpertGetInfoService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ExpertResponseData, ExpertModel } from "./types";
import { expertModel } from "./models";

export interface ExpertGetInfoUrlParams {
  expertId: string;
}

export interface ExpertGetInfoServiceParams {
  queryConfig?: QueryConfig<ExpertResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: ExpertGetInfoUrlParams;
}

export const expertGetInfoUrl = `${process.env.REACT_APP_TENSPOT_API_V1}trainers/public/`;

const api = axios.create({ withCredentials: true });

const queryFn = (axiosConfig: AxiosRequestConfig): QueryFunction<ExpertModel> =>
  // This _ is the queryKey string
  (_: string, { expertId }: ExpertGetInfoUrlParams) =>
    api
      .get<ExpertResponseData>(`${expertGetInfoUrl}${expertId}`, axiosConfig)
      .then(({ data }) => expertModel(data));

export const useQueryExpertGetInfoService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: ExpertGetInfoServiceParams) => {
  return useQuery<ExpertModel, AxiosError>({
    queryKey: ["expert", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryExpertGetInfoData = (
  urlParams: ExpertGetInfoUrlParams
): ExpertModel | undefined =>
  queryCache.getQueryData<ExpertModel>(["expert", urlParams]);
