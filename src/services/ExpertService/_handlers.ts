/**
 *
 *
 * handlers for Expert service mocks
 *
 *
 */

import { rest } from "msw";
import { mockResponseData as expertMockData } from "./mockResponseData";
import { expertGetInfoUrl } from "./useQueryExpertGetInfoService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(`${expertGetInfoUrl}:expertId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(expertMockData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
