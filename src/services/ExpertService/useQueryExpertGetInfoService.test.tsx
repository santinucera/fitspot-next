/**
 *
 *
 * Tests for useQueryExpertGetInfoService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryExpertGetInfoService } from "./useQueryExpertGetInfoService";
import { mockResponseData } from "./mockResponseData";
import { expertModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryExpertGetInfoService({ urlParams: { expertId: "1" } });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryExpertGetInfoService({
      urlParams: { expertId: "1" },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(expertModel(mockResponseData))
    );
  });
});
