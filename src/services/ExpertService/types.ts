/**
 *
 *
 * Types for Expert
 *
 *
 */

import { ShortUser, Activity, Avatar, ChatSession } from "../types";
import {
  SessionsModel,
  CustomerDashboardSession,
} from "services/SessionService/types";

export interface ExpertResponseData extends ShortUser {
  sessions: CustomerDashboardSession[];
}

export interface ExpertModel {
  id: number;
  firstName: string | null;
  lastName: string | null;
  publicId: string | null;
  bio: string | null;
  activities: Activity[];
  ratingSum: number | null;
  gender: number | null;
  avatar: Avatar | null;
  ratingAvg: number | null;
  timezone: string | null;
  sessionInfo: ChatSession | null;
  ratingCount: number | null;
  sessions: SessionsModel;
}
