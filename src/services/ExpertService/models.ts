/**
 *
 *
 * Models for transforming ExpertService response data
 *
 *
 */
import { ExpertResponseData, ExpertModel } from "./types";

import { sessionsModel } from "services/SessionService/models";

/**
 * Transform /workouts/:id response data
 */
export const expertModel = (responseData: ExpertResponseData): ExpertModel => {
  const {
    id,
    firstName,
    lastName,
    publicId,
    trainer,
    ratingSum,
    gender,
    avatar,
    ratingAvg,
    timezone,
    sessionInfo,
    ratingCount,
    sessions,
  } = responseData;

  return {
    id,
    firstName,
    lastName,
    publicId,
    bio: trainer?.bio || null,
    activities: trainer?.activities || [],
    ratingSum,
    gender,
    avatar,
    ratingAvg,
    timezone,
    sessionInfo,
    ratingCount,
    sessions: sessionsModel({ sessions: { sessions } }),
  };
};
