/**
 *
 *
 *
 * useQueryHumanApiGetTokenService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ResponseData } from "./types";

export interface HumanApiGetTokenServiceParams {
  queryConfig?: QueryConfig<ResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const humanApiGetTokenUrl = `${process.env.REACT_APP_TENSPOT_API_V1}tracking/humanapi/token`;

const api = axios.create({ withCredentials: true });

/**
 * After we are connected to the human api via `/session-token` use  the `/id-token` endpoint
 */
const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ResponseData> => () =>
  api
    .get<ResponseData>(humanApiGetTokenUrl, axiosConfig)
    .then(({ data }) => data);

export const useQueryHumanApiGetTokenService = ({
  queryConfig = {},
  axiosConfig = {},
}: HumanApiGetTokenServiceParams = {}) => {
  return useQuery<ResponseData, AxiosError>({
    queryKey: "human-api-token",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryHumanApiGetTokenData = (): ResponseData | undefined =>
  queryCache.getQueryData<ResponseData>("human-api-token");
