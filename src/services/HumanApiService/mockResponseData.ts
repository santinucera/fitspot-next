/**
 *
 *
 * Mock Response Data for HumanApi
 *
 *
 */

import { ResponseData } from "./types";

export const mockResponseData: ResponseData = {
  token: "123",
};
