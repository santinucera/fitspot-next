/**
 *
 *
 * handlers for HumanApi service mocks
 *
 *
 */

import { mockResponseData } from "./mockResponseData";

import { rest } from "msw";
import { humanApiGetTokenUrl } from "./useQueryHumanApiGetTokenService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(`${humanApiGetTokenUrl}`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
