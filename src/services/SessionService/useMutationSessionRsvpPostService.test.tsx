/**
 *
 *
 * Tests for useMutationSessionRsvpPostService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationSessionRsvpPostService } from "./useMutationSessionRsvpPostService";
import { mockSessionRsvpPostResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationSessionRsvpPostService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationSessionRsvpPostService();
    return (
      <div>
        <button onClick={() => mutate({ sessionId: 1, isRSVP: true })}>
          Mutate
        </button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockSessionRsvpPostResponseData)
    );
  });
});
