/**
 *
 *
 * Tests for useQueryTrainersSessionGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryTrainersSessionGetService } from "./useQueryTrainersSessionGetService";
import { mockTrainersSessionGetResponseData } from "./mockResponseData";
import { trainerSessionDetailsModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryTrainersSessionGetService({ urlParams: { sessionId: 1 } });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryTrainersSessionGetService({
      urlParams: { sessionId: 1 },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(
        trainerSessionDetailsModel(mockTrainersSessionGetResponseData)
      )
    );
  });
});
