/**
 *
 *
 *
 * useMutationDismissSessionRatingPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { DismissSessionRatingPostResponseData } from "./types";

export const dismissSessionRatingPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/set-decline-rating-flag`;

const api = axios.create({ withCredentials: true });

export interface DismissSessionRatingParams {
  sessionId: number;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<
  DismissSessionRatingPostResponseData,
  DismissSessionRatingParams
> => ({ sessionId }: DismissSessionRatingParams) =>
  api
    .post<DismissSessionRatingPostResponseData>(
      `${dismissSessionRatingPostUrl}/${sessionId}`,
      { value: true },
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationDismissSessionRatingPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    DismissSessionRatingPostResponseData,
    AxiosError,
    DismissSessionRatingParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
