/**
 *
 *
 *
 * useQueryTrainersSessionsGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError } from "axios";
import { TrainerSession, TrainerSessionModel } from "./types";
import { trainerSessionModel } from "./models";

export interface TrainersSessionsGetUrlParams {
  dtStart: string;
  dtEnd: string;
  statuses?: number[];
}

export interface TrainersSessionsGetServiceParams {
  queryConfig?: QueryConfig<TrainerSessionModel[], AxiosError>;
  urlParams: TrainersSessionsGetUrlParams;
}

export const trainersSessionsGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}trainers/sessions`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  urlParams: TrainersSessionsGetUrlParams
): QueryFunction<TrainerSessionModel[]> =>
  // This _ is the queryKey string
  () =>
    api
      .get<TrainerSession[]>(trainersSessionsGetUrl, {
        params: urlParams,
      })
      .then(({ data }) => data.map(trainerSessionModel));

export const useQueryTrainersSessionsGetService = ({
  queryConfig = {},
  urlParams,
}: TrainersSessionsGetServiceParams) => {
  return useQuery<TrainerSessionModel[], AxiosError>({
    queryKey: "trainers-sessions",
    config: queryConfig,
    queryFn: queryFn(urlParams),
  });
};

export const useQueryTrainersSessionsGetData = ():
  | TrainerSessionModel[]
  | undefined =>
  queryCache.getQueryData<TrainerSessionModel[]>("trainers-sessions");
