/**
 *
 *
 * handlers for Session service mocks
 *
 *
 */

import {
  mockSessionsGetResponseData,
  mockSessionGetResponseData,
  mockSessionRsvpPostResponseData,
  mockSessionAttendedPostResponseData,
  mockNonRatedSessionsGetResponseData,
  mockDismissSessionRatingPostResponseData,
  mockTrainersSessionsGetResponseData,
  mockTrainersSessionGetResponseData,
  mockFeaturedSessionsGetResponse,
} from "./mockResponseData";
import { rest } from "msw";
import { sessionsGetUrl } from "./useQuerySessionsGetService";
import { sessionGetUrl } from "./useQuerySessionGetService";
import { sessionsGetNonRatedUrl } from "./useQuerySessionsGetNonRatedService";
import { sessionRsvpPostUrl } from "./useMutationSessionRsvpPostService";
import { sessionAttendedPostUrl } from "./useMutationSessionAttendedPostService";
import { rateSessionPostUrl } from "./useMutationRateSessionPostService";
import { rateSessionPatchUrl } from "./useMutationRateSessionPatchService";
import { dismissSessionRatingPostUrl } from "./useMutationDismissSessionRatingPostService";
import { trainersSessionsGetUrl } from "./useQueryTrainersSessionsGetService";
import { trainersSessionGetUrl } from "./useQueryTrainersSessionGetService";
import { patchSessionStatusUrl } from "./useMutationSessionStatusPatchService";
import { cancelSessionPatchServiceUrl } from "./useMutationCancelSessionPatchService";
import { featuredSessionsGetServiceUrl } from "./useQueryFeaturedSessionsGetService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(sessionsGetUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionsGetResponseData));
  }),
  rest.get(`${sessionGetUrl}:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionGetResponseData));
  }),
  rest.get(sessionsGetNonRatedUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockNonRatedSessionsGetResponseData));
  }),
  rest.post(sessionRsvpPostUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionRsvpPostResponseData));
  }),
  rest.post(`${sessionAttendedPostUrl}/:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionAttendedPostResponseData));
  }),
  rest.post(`${rateSessionPostUrl}/:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionGetResponseData));
  }),
  rest.patch(`${rateSessionPatchUrl}/:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionGetResponseData));
  }),
  rest.post(`${dismissSessionRatingPostUrl}/:sessionId`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(mockDismissSessionRatingPostResponseData)
    );
  }),
  rest.get(trainersSessionsGetUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTrainersSessionsGetResponseData));
  }),
  rest.get(`${trainersSessionGetUrl}:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTrainersSessionGetResponseData));
  }),
  rest.patch(`${patchSessionStatusUrl}:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionGetResponseData));
  }),
  rest.patch(`${cancelSessionPatchServiceUrl}:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockSessionGetResponseData));
  }),
  rest.get(featuredSessionsGetServiceUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockFeaturedSessionsGetResponse));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
