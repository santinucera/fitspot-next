/**
 *
 *
 *
 * useQuerySessionsGetNonRatedService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import {
  NonRatedSessionsGetResponseData,
  NonRatedSessionsModel,
} from "./types";
import { nonRatedSessionsModel } from "./models";

export interface SessionsGetNonRatedServiceParams {
  queryConfig?: QueryConfig<NonRatedSessionsGetResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const sessionsGetNonRatedUrl = `${process.env.REACT_APP_TENSPOT_API_V1}customers/non-rated-sessions`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<NonRatedSessionsModel> => () =>
  api
    .get<NonRatedSessionsGetResponseData>(sessionsGetNonRatedUrl, axiosConfig)
    .then(({ data }) => nonRatedSessionsModel(data));

export const useQuerySessionsGetNonRatedService = ({
  queryConfig = {},
  axiosConfig = {},
}: SessionsGetNonRatedServiceParams = {}) => {
  return useQuery<NonRatedSessionsModel, AxiosError>({
    queryKey: "non-rated-sessions",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQuerySessionsGetNonRatedData = ():
  | NonRatedSessionsModel
  | undefined =>
  queryCache.getQueryData<NonRatedSessionsModel>("non-rated-sessions");
