/**
 *
 *
 * Tests for useMutationSessionStatusPatchService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationSessionStatusPatchService } from "./useMutationSessionStatusPatchService";
import { mockSessionGetResponseData } from "./mockResponseData";
import { SessionStatus } from "./types";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationSessionStatusPatchService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationSessionStatusPatchService();
    return (
      <div>
        <button
          onClick={() =>
            mutate({
              sessionId: 1,
              status: SessionStatus.inProgress,
              message: "some message",
            })
          }
        >
          Mutate
        </button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockSessionGetResponseData)
    );
  });
});
