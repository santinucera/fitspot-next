/**
 *
 *
 * Tests for useMutationDismissSessionRatingPostService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationDismissSessionRatingPostService } from "./useMutationDismissSessionRatingPostService";
import { mockDismissSessionRatingPostResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationDismissSessionRatingPostService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationDismissSessionRatingPostService();
    return (
      <div>
        <button onClick={() => mutate({ sessionId: 1 })}>Mutate</button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockDismissSessionRatingPostResponseData)
    );
  });
});
