/**
 *
 *
 * Tests for useQuerySessionGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQuerySessionGetService } from "./useQuerySessionGetService";
import { mockSessionGetResponseData } from "./mockResponseData";
import { sessionModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQuerySessionGetService({ urlParams: { sessionId: "1" } });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQuerySessionGetService({
      urlParams: { sessionId: "1" },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(sessionModel(mockSessionGetResponseData))
    );
  });
});
