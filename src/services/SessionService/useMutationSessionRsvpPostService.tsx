/**
 *
 *
 *
 * useMutationSessionRsvpPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { SessionRsvpPostResponseData } from "./types";

export const sessionRsvpPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/rsvp-platform-wide-session`;

const api = axios.create({ withCredentials: true });

export interface SessionRsvpPostParams {
  sessionId: number;
  isRSVP: boolean;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<SessionRsvpPostResponseData, SessionRsvpPostParams> => ({
  sessionId,
  isRSVP,
}: SessionRsvpPostParams) =>
  api
    .post<SessionRsvpPostResponseData>(
      sessionRsvpPostUrl,
      { sessionId, status: isRSVP },
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationSessionRsvpPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    SessionRsvpPostResponseData,
    AxiosError,
    SessionRsvpPostParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
