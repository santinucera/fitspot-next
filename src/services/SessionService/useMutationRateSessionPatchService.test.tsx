/**
 *
 *
 * Tests for useMutationRateSessionPatchService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationRateSessionPatchService } from "./useMutationRateSessionPatchService";
import { mockSessionGetResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationRateSessionPatchService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationRateSessionPatchService();
    return (
      <div>
        <button
          onClick={() =>
            mutate({
              sessionId: 1,
              rating: {
                streamQuality: 1,
                overall: 1,
                text: "",
                trainer: 1,
                gym: 1,
                location: 1,
              },
            })
          }
        >
          Mutate
        </button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockSessionGetResponseData)
    );
  });
});
