/**
 *
 *
 *
 * useMutationRateSessionPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { RateSessionPostResponseData } from "./types";

export const rateSessionPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/rate-session`;

const api = axios.create({ withCredentials: true });

export interface RateSessionPostParams {
  sessionId: number;
  rating: { overall: number };
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<RateSessionPostResponseData, RateSessionPostParams> => ({
  sessionId,
  ...requestData
}: RateSessionPostParams) =>
  api
    .post<RateSessionPostResponseData>(
      `${rateSessionPostUrl}/${sessionId}`,
      requestData.rating,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationRateSessionPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    RateSessionPostResponseData,
    AxiosError,
    RateSessionPostParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
