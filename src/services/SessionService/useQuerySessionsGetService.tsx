/**
 *
 *
 *
 * useQuerySessionsGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { SessionsGetResponseData, SessionsModel } from "./types";
import { sessionsModel } from "./models";

export interface SessionsGetQueryParams {
  limit?: number;
  short?: boolean;
  attended?: boolean;
  desc?: boolean;
  dtEnd?: string;
  dtStart?: string;
  isRsvp?: boolean;
}

export interface SessionsGetServiceParams {
  queryConfig?: QueryConfig<SessionsGetResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams?: SessionsGetQueryParams;
}

export const sessionsGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}customers/schedule/dashboard`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<SessionsModel> =>
  // This _ is the queryKey string
  (_: string, urlParams: SessionsGetQueryParams) =>
    api
      .get<SessionsGetResponseData>(sessionsGetUrl, {
        ...axiosConfig,
        params: urlParams,
      })
      .then(({ data }) => sessionsModel(data));

export const useQuerySessionsGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams = {},
}: SessionsGetServiceParams = {}) => {
  return useQuery<SessionsModel, AxiosError>({
    queryKey: ["sessions", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQuerySessionsGetData = (
  urlParams?: SessionsGetQueryParams
): SessionsModel | undefined =>
  queryCache.getQueryData<SessionsModel>(["sessions", urlParams]);
