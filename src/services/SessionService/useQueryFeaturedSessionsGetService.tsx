/**
 *
 *
 *
 * useQueryFeaturedSessionsGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { FeaturedSessionsResponseData, StandardSessionModel } from "./types";
import { standardSessionModel } from "./models";

export interface FeaturedSessionsGetServiceParams {
  queryConfig?: QueryConfig<FeaturedSessionsResponseData, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const featuredSessionsGetServiceUrl = `${process.env.REACT_APP_TENSPOT_API_V1}sessions/featured`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<StandardSessionModel[]> => () =>
  api
    .get<FeaturedSessionsResponseData>(
      featuredSessionsGetServiceUrl,
      axiosConfig
    )
    .then(({ data }) => data.data.map(standardSessionModel));

export const useQueryFeaturedSessionsGetService = ({
  queryConfig = {},
  axiosConfig = {},
}: FeaturedSessionsGetServiceParams = {}) => {
  return useQuery<StandardSessionModel[], AxiosError>({
    queryKey: "featured-sessions",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryFeaturedSessionsGetServiceData = ():
  | StandardSessionModel[]
  | undefined =>
  queryCache.getQueryData<StandardSessionModel[]>("featured-sessions");
