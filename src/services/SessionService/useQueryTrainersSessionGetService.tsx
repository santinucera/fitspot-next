/**
 *
 *
 *
 * useQueryTrainersSessionGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { TrainerSessionDetails, TrainerSessionDetailsModel } from "./types";
import { trainerSessionDetailsModel } from "./models";

export interface TrainersSessionGetUrlParams {
  sessionId: number;
}

export interface TrainersSessionGetServiceParams {
  queryConfig?: QueryConfig<TrainerSessionDetails, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams?: TrainersSessionGetUrlParams;
}

export const trainersSessionGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}trainers/session/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<TrainerSessionDetailsModel> =>
  // This _ is the queryKey string
  (_: string, { sessionId }: TrainersSessionGetUrlParams) =>
    api
      .get<TrainerSessionDetails>(
        `${trainersSessionGetUrl}${sessionId}`,
        axiosConfig
      )
      .then(({ data }) => trainerSessionDetailsModel(data));

export const useQueryTrainersSessionGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: TrainersSessionGetServiceParams) => {
  return useQuery<TrainerSessionDetailsModel, AxiosError>({
    queryKey: ["trainer-session", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryTrainersSessionGetData = (
  urlParams: TrainersSessionGetUrlParams
): TrainerSessionDetailsModel | undefined =>
  queryCache.getQueryData<TrainerSessionDetailsModel>([
    "trainer-session",
    urlParams,
  ]);
