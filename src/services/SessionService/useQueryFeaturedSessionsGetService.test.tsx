/**
 *
 *
 * Tests for useQueryFeaturedSessionsGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryFeaturedSessionsGetService } from "./useQueryFeaturedSessionsGetService";
import { mockFeaturedSessionsGetResponse } from "./mockResponseData";
import { standardSessionModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryFeaturedSessionsGetService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryFeaturedSessionsGetService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(
        mockFeaturedSessionsGetResponse.data.map(standardSessionModel)
      )
    );
  });
});
