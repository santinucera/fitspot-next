/**
 *
 *
 *
 * useMutationSessionStatusPatchService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Session, SessionStatus } from "./types";

export const patchSessionStatusUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/status`;

const api = axios.create({ withCredentials: true });

interface RequestData {
  sessionId: number;
  status: SessionStatus;
  message?: string;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<Session, RequestData> => ({
  sessionId,
  ...requestData
}: RequestData) =>
  api
    .patch<Session>(
      `${patchSessionStatusUrl}/${sessionId}`,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

/**
 * Update the status of a Session and TrainerSession.
 *
 * Cannot be used to change status to 3, canceled. Use
 * useMutationCancelSessionPatchService instead.
 */
export const useMutationSessionStatusPatchService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    Session,
    AxiosError<{ message: string }>,
    RequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
