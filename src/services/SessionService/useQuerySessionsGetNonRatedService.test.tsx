/**
 *
 *
 * Tests for useQuerySessionsGetNonRatedService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQuerySessionsGetNonRatedService } from "./useQuerySessionsGetNonRatedService";
import { mockNonRatedSessionsGetResponseData } from "./mockResponseData";
import { nonRatedSessionsModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQuerySessionsGetNonRatedService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQuerySessionsGetNonRatedService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(nonRatedSessionsModel(mockNonRatedSessionsGetResponseData))
    );
  });
});
