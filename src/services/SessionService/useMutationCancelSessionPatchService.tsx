/**
 *
 *
 *
 * useMutationCancelSessionPatchService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Session } from "./types";

export const cancelSessionPatchServiceUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/cancel`;

const api = axios.create({ withCredentials: true });

interface RequestData {
  sessionId: number;
  message?: string;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<Session, RequestData> => ({
  sessionId,
  ...requestData
}: RequestData) =>
  api
    .patch<Session>(
      `${cancelSessionPatchServiceUrl}/${sessionId}`,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationCancelSessionPatchService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    Session,
    AxiosError<{ message: string }>,
    RequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
