/**
 *
 *
 * Models for transforming SessionService response data
 *
 *
 */
import dayjs from "utils/days";
import { User, ShortUser, PublicTrainer, UserTrainer } from "../types";
import {
  CustomerDashboardSession,
  CategorizedSessionsModel,
  Session,
  SessionModel,
  SessionsGetResponseData,
  SessionsModel,
  CustomerDashboardSessionModel,
  NonRatedSessionsGetResponseData,
  NonRatedSession,
  NonRatedSessionsModel,
  SessionsGroupedByDateRangeModel,
  WeekRange,
  TrainerSession,
  TrainerSessionModel,
  TrainerSessionDetails,
  TrainerSessionDetailsModel,
  StandardSession,
  StandardSessionModel,
} from "./types";
import { Participant, Pillar } from "services/types";

/**
 * Transform /workouts/:id response data
 */
export const sessionModel = (responseData: Session): SessionModel => {
  const {
    id,
    name,
    description,
    trainer,
    date,
    participants,
    dt_end,
    liveSession,
    status,
    liveSessionUrl,
    isOnPlatform,
    streamKey,
    companyId,
    additionalInfo,
    attachment,
    trainerId,
    trainerRating,
    onDemandActive,
    videoUrl,
    activityCategory,
    isReplay,
  } = responseData;

  return {
    id,
    name,
    description,
    trainer: trainer
      ? {
          firstName: trainer.firstName || "",
          lastName: trainer.lastName || "",
          avatar: trainer.avatar?.url || "",
          bio: trainer.trainer?.bio || "",
          publicId: trainer.publicId || "",
        }
      : null,
    participants: participants,
    startDate: dayjs(date),
    endDate: dayjs(dt_end),
    isLiveSession: liveSession,
    isOnPlatform: isOnPlatform,
    status,
    liveSessionUrl,
    streamKey,
    companyId,
    activityCategory: activityCategory.trim() as Pillar,
    additionalInfo,
    attachment,
    trainerId,
    trainerRating,
    onDemandActive,
    videoUrl,
    isReplay,
  };
};

/**
 * Transform CustomerDashboardSession into CustomerDashboardSessionModel.
 */
export const customerDashboardSessionModel = (
  session: CustomerDashboardSession
): CustomerDashboardSessionModel => ({
  ...session,
  // Some activity categories have trailing whitespace.
  activityCategory: session.activityCategory?.trim() as Pillar,
  startDate: dayjs(session.date),
  endDate: dayjs(session.dt_end),
});

/**
 * Transform /api/v1/customers/dashboard response data
 */
export const sessionsModel = (
  responseData: SessionsGetResponseData
): SessionsModel => {
  return responseData.sessions.sessions.map(customerDashboardSessionModel);
};

export const categorizeSessionsModel = (
  sessions?: SessionsModel
): CategorizedSessionsModel => {
  const initialValue: CategorizedSessionsModel = {
    upComingSessions: [],
    rsvpSessions: [],
  };
  if (!sessions) {
    return initialValue;
  } else {
    return sessions.reduce(
      (
        acc: CategorizedSessionsModel,
        session: CustomerDashboardSessionModel
      ): CategorizedSessionsModel => {
        return session.isRSVP
          ? { ...acc, rsvpSessions: [...acc.rsvpSessions, session] }
          : { ...acc, upComingSessions: [...acc.upComingSessions, session] };
      },
      initialValue
    );
  }
};

export const sessionsGroupedByDayRangesModel = (
  sessions: SessionsModel,
  weeks: WeekRange
) => {
  const sessionsGroupedByDay = sessions?.reduce(
    (acc: SessionsGroupedByDateRangeModel, session) => {
      const start = session.startDate.format("YYYY-MM-DD");
      if (!acc[start]) acc[start] = [];
      acc[start].push(session);
      return acc;
    },
    {}
  );

  const weekRanges = weeks.map((week) => {
    return week.reduce(
      (acc: { [key: string]: CustomerDashboardSessionModel[] }, day) => {
        const d = day.format("YYYY-MM-DD");
        acc[d] = sessionsGroupedByDay[d] || [];
        return acc;
      },
      {}
    );
  });

  return weekRanges;
};

export const trainerToPublicTrainer = ({
  bio,
  activities,
}: UserTrainer): PublicTrainer => ({
  bio,
  activities,
});

export const userToShortUser = ({
  id,
  firstName,
  lastName,
  timezone,
  avatar,
  gender,
  trainer,
  customer,
  publicId,
  sessionInfo,
}: User): ShortUser => ({
  id,
  firstName,
  lastName,
  timezone,
  avatar,
  gender,
  trainer: trainer ? trainerToPublicTrainer(trainer) : null,
  customer,
  publicId,
  sessionInfo,
  ratingAvg: 0,
  ratingSum: 0,
  ratingCount: 0,
});

/**
 * Returns a participant object. Useful for optimistic updates to
 * Sessions.
 */
export const createParticipant = (
  user: User,
  isRSVP = true,
  wasAttended = false
): Participant => ({
  userId: user.id,
  user: userToShortUser(user),
  slotId: null,
  slot: null,
  isRSVP,
  wasAttended,
  ratingPoints: null,
  ratingText: null,
  ratingTrainer: null,
  ratingLocation: null,
  ratingFutureAttend: null,
  ratingStreamQuality: null,
});

/**
 * Transform /api/v1/customers/dashboard response data.
 */
export const nonRatedSessionsModel = (
  responseData: NonRatedSessionsGetResponseData
): NonRatedSessionsModel =>
  responseData.map((session: NonRatedSession) => ({
    ...session,
    startDate: dayjs(session.date),
    endDate: dayjs(session.dtEnd),
  }));

/**
 * Transform a TrainerSession object from the /trainers/sessions
 * response to a TrainerSessionModel.
 */
export const trainerSessionModel = ({
  date,
  dt_end,
  ...rest
}: TrainerSession): TrainerSessionModel => ({
  ...rest,
  startDate: dayjs(date),
  endDate: dayjs(dt_end),
});

/**
 * Transform a TrainerSessionDetails object from the /trainers/sessions/:sessionId
 * response to a TrainerSessionDetailsModel.
 */
export const trainerSessionDetailsModel = ({
  date,
  dt_end,
  ...rest
}: TrainerSessionDetails): TrainerSessionDetailsModel => ({
  ...rest,
  startDate: dayjs(date),
  endDate: dayjs(dt_end),
});

export type CategorizedTrainerSessions = {
  inProgress: TrainerSessionModel[];
  pending: TrainerSessionModel[];
  upcoming: TrainerSessionModel[];
};

export const standardSessionModel = (
  session: StandardSession
): StandardSessionModel => ({
  ...session,
  dates: {
    start: dayjs(session.dates.start),
    startLocal: dayjs(session.dates.startLocal),
    end: dayjs(session.dates.end),
    endLocal: dayjs(session.dates.endLocal),
  },
});
