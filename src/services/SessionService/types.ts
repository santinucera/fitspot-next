/**
 *
 *
 * Types for SessionsGet
 *
 *
 */
import { Dayjs } from "dayjs";
import {
  Participant,
  Pillar,
  FileUrl,
  OfficeLocation,
  ShortUser,
  ShortCompany,
  OfficeLocationWithPlaces,
  Slot,
  Avatar,
  Gender,
} from "../types";

/**
 * Used in the response from from /workouts/:session_id and others but
 * quite different from CustomerDashboardSession.
 *
 * From the `workout_session` Python serializer.
 */
export interface Session {
  id: number;
  activityId: number;
  additionalInfo: string | null;
  address: string | null;
  attachment: FileUrl | null;
  attachmentId: number | null;
  bookingFlowType: number | null;
  city: string | null;
  company: ShortCompany | null;
  companyId: number | null;
  createdByUser: ShortUser;
  createdByUserId: number;
  date: string;
  dateLocal: string | null;
  description: string | null;
  dtEndLocal: string;
  dt_end: string;
  gym: Gym | null;
  gymId: number | null;
  invitedDepartments: InvitedDepartment[];
  invitedLocations: InvitedLocation[];
  isFeatured: boolean;
  isOnPlatform: boolean;
  isReplay: boolean;
  isSub: boolean;
  lat: number | null;
  liveSession: boolean;
  liveSessionUrl: string | null;
  lon: number | null;
  maxParticipants: number | null;
  meetingPlaceName: string | null;
  name: string;
  numFriends: number | null;
  numberAttended: number | null;
  officeLocation: OfficeLocationWithPlaces | null;
  onDemandActive: boolean;
  participants: Participant[];
  platformWide: boolean;
  price: number | null;
  rating: number | null;
  slots: Slot[];
  state: string | null;
  status: SessionStatus;
  // Null for off-platform sessions
  streamKey: string | null;
  // Null for off-platform sessions
  streamUrlExternal: string | null;
  takeAttendance: boolean;
  activityCategory: Pillar;
  thumbnailImage: FileUrl | null;
  thumbnailImageId: number | null;
  thumbnailUrl: string | null;
  trainer: ShortUser | null;
  trainerId: number | null;
  trainerRating: number | null;
  videoUrl: string | null;
  waitlistRecords: WaitListRecord[];
  zipcode: string | null;
}

/**
 * From the `standardized_session` Python serializer.
 */
export interface StandardSession {
  id: number;
  activity: StandardActivity | null;
  additionalInfo: string | null;
  attachment: FileUrl | null;
  attachmentId: number | null;
  attended: number | null;
  currentUserRating: UserRating | null;
  dates: Dates;
  description: string | null;
  isFeatured: boolean;
  isOnPlatform: boolean;
  isRSVP: boolean;
  isReplay: boolean;
  isSub: boolean;
  liveSession: boolean;
  liveSessionUrl: string | null;
  maxParticipants: number | null;
  name: string;
  onDemandActive: boolean;
  participants: number | null;
  platformWide: boolean;
  rating: number | null; // Float
  status: SessionStatus;
  streamKey: string | null; // null if off-platform
  // https://live.tenspot.co/watch.php?id={:userPublicId}, null if off-platform
  streamUrlExternal: string | null;
  takeAttendance: boolean;
  thumbnailImage: FileUrl | null;
  thumbnailImageId: number | null;
  thumbnailUrl: string | null;
  trainer: Trainer | null;
  videoUrl: string | null;
  wasAttended: boolean;
}

export interface StandardSessionModel extends Omit<StandardSession, "dates"> {
  dates: {
    start: Dayjs;
    startLocal: Dayjs;
    end: Dayjs;
    endLocal: Dayjs;
  };
}

/**
 * From the `activity` Python serializer.
 */
export interface StandardActivity {
  id: number;
  category: Pillar;
  name: string;
}

/**
 * From the `user_rating` python serializer.
 *
 * Number values are integers.
 */
export interface UserRating {
  ratingFutureAttended: number | null;
  location: number | null;
  overall: number | null;
  streamQuality: number | null;
  comment: string | null;
  trainer: number | null;
}

/**
 * From the `rating` Python serializer.
 */
export interface Rating {
  average: number | null; // Float
  count: number | null; // Int
}

/**
 * From the `dates` Python serializer.
 */
export interface Dates {
  start: string;
  startLocal: string;
  end: string;
  endLocal: string;
}

/**
 * Form the `trainer` Python serializer.
 */
export interface Trainer {
  avatar: Avatar;
  bio: string | null;
  firstName: string;
  fullName: string;
  gender: Gender;
  id: number;
  lastName: string;
  publicId: string;
  rating: Rating;
  timezone: string;
}

/**
 * From the `invited_department` Python serializer.
 */
export interface InvitedDepartment {
  departmentId: number;
}

/**
 * From the `invited_location` Python serializer.
 */
export interface InvitedLocation {
  officeId: number;
}

/**
 * From the `gym_format` Python serializer.
 */
export interface Gym {
  id: number;
  name: string;
  description: string;
  logo: FileUrl;
  photos: {
    file_model: FileUrl[];
  };
  address: string;
  city: string;
  state: string;
  zipcode: string;
  cost: number;
  lat: number;
  lon: number;
  rating_avg: number;
  activityAssociations: { activityId: number }[];
  facilityAssociations: { facilityId: number }[];
}

/**
 * From the `wait_list_record` Python serializer.
 */
export interface WaitListRecord {
  userId: number;
  user: ShortUser;
}

// Individual session
export interface SessionModel {
  id: number;
  name: string;
  trainer: {
    firstName: string | null;
    lastName: string | null;
    avatar: string | null;
    bio: string | null;
    publicId: string | null;
  } | null;
  description: string | null;
  participants: Participant[];
  startDate: Dayjs;
  endDate: Dayjs;
  isLiveSession: boolean;
  isOnPlatform: boolean;
  isReplay: boolean;
  status: number;
  liveSessionUrl: string | null;
  streamKey: string | null;
  companyId: number | null;
  activityCategory: Pillar;
  additionalInfo: string | null;
  trainerId: number | null;
  trainerRating: number | null;
  onDemandActive: boolean;
  videoUrl: string | null;
  attachment: FileUrl | null;
}

/**
 * SessionsGet
 */
interface Company {
  name: string;
}

export interface UserSessionRating {
  ratingPoints: null | number;
  ratingFutureAttend: null | number;
  ratingLocation: null | number;
  ratingTrainer: null | number;
  ratingStreamQuality: null | number;
  ratingText: null | string;
}

/**
 * Single session item from the response returned
 * by /api/v1/customers/dashboard.
 *
 * Note this is different than the Session type in that
 * it defines a session from the perspective of a user.
 */
export interface CustomerDashboardSession {
  rating: number | null;
  activityName: string;
  name: string;
  date: string;
  trainerId: number;
  status: SessionStatus;
  id: number;
  liveSessionUrl: string;
  maxParticipants: number;
  platformWide: boolean;
  description: string;
  // Would be a Pillar but some have trailing whitespace that is corrected
  // by the model.
  activityCategory: Pillar;
  participantsCount: number;
  company: Company | null;
  createdByUserId: number;
  dt_end: string; // YYYY-MM-DDTHH:MM:SSZ
  activityId: number;
  companyId: number | null;
  trainerAvatar: string | null;
  liveSession: boolean;
  isOnPlatform: boolean;
  trainerBio: string;
  trainerName: string;
  trainerRating: number;
  wasAttended: boolean;
  onDemandActive: boolean;
  isRSVP: boolean;
  isSub: boolean;
  isReplay: boolean;
  userSessionRating?: UserSessionRating;
  currentUserIsRSVP?: boolean | null;
  thumbnailImage: { url: string } | null;
}

/**
 * Response date from /api/v1/customers/dashboard
 */
export interface SessionsGetResponseData {
  sessions: {
    sessions: CustomerDashboardSession[];
  };
}

/**
 * Same as CustomerDashboardSession but dates are Dayjs dates.
 */
export type CustomerDashboardSessionModel = Omit<
  CustomerDashboardSession,
  "activityCategory" | "date" | "dt_end"
> & {
  activityCategory: Pillar;
  startDate: Dayjs;
  endDate: Dayjs;
};

export type WeekRange = Dayjs[][];

/**
 * Sessions grouped by a range of days
 */
export interface SessionsGroupedByDateRangeModel {
  [key: string]: CustomerDashboardSessionModel[];
}

/**
 * Result of passing the /api/v1/customers/dashboard response
 * through sessionsModel.
 */
export type SessionsModel = CustomerDashboardSessionModel[];

/**
 * Sessions split into categories.
 */
export type CategorizedSessionsModel = {
  upComingSessions: CustomerDashboardSessionModel[];
  rsvpSessions: CustomerDashboardSessionModel[];
};

/**
 * RSVP POST
 */
export interface SessionRsvpPostResponseData {
  success: boolean;
}

/**
 * Attended POST
 */
export interface SessionAttendedPostResponseData {
  success: boolean;
}

/**
 * Single session item from the response returned
 * by /api/v1/customers/non-rated-sessions.
 */
export interface NonRatedSession {
  activityName: string;
  date: string; // YYYY-MM-DDTHH:MM:SSZ
  dateLocal: string; // YYYY-MM-DDTHH:MM:SSZ
  dtEnd: string; // YYYY-MM-DDTHH:MM:SSZ
  dtEndLocal: string; // YYYY-MM-DDTHH:MM:SSZ
  id: number;
  liveSession: boolean;
  name: string;
  timezone: string;
  trainerAvatar: {
    url: string;
  };
  trainerId: number;
  trainerName: string;
}

/**
 * Response date from /api/v1/customers/non-rated-sessions
 */
export type NonRatedSessionsGetResponseData = NonRatedSession[];

/**
 * Same as NonRatedSession but dates are Dayjs dates.
 */
export type NonRatedSessionModel = Omit<NonRatedSession, "date" | "dtEnd"> & {
  startDate: Dayjs;
  endDate: Dayjs;
};

/**
 * Result of passing the /api/v1/customers/non-rated-sessions response
 * through NonRatedSessionModel.
 */
export type NonRatedSessionsModel = NonRatedSessionModel[];

/**
 * Rate session POST
 */
export type RateSessionPostResponseData = Session;

/**
 * Rate session PATCH
 */
export type RateSessionPatchResponseData = Session;

/**
 * Attended POST
 */
export interface DismissSessionRatingPostResponseData {
  success: boolean;
}

/**
 * Response from /trainers/sessions
 *
 * From the `workout_session_with_trainer_fee` Python serializer.
 */
export interface TrainerSession {
  id: number;
  name: string;
  date?: string;
  dt_end?: string;
  lat: number | null;
  lon: number | null;
  gymId: number | null;
  gym: Gym | null;
  address: string | null;
  city: string | null;
  state: string | null;
  zipcode: string | null;
  officeLocation: OfficeLocation | null;
  meetingPlaceName: string | null;
  trainerId: number;
  trainer: ShortUser;
  createdByUserId: number;
  createdByUser: ShortUser;
  companyId: number | null;
  company: ShortCompany | null;
  activityId: number | null;
  trainerPayout: number | null;
  takeAttendance: boolean;
  numberAttended: number | null;
  status: SessionStatus;
  participants: Participant[];
  streamUrlBroadcast: string | null;
}

/**
 * TrainerSession with dates transformed to Dayjs objects.
 */
export type TrainerSessionModel = Omit<TrainerSession, "date" | "dt_end"> & {
  startDate: Dayjs;
  endDate: Dayjs;
};

export enum SessionStatus {
  pending = 0,
  accepted = 1,
  completed = 2,
  canceled = 3,
  failed = 4,
  new = 5,
  inProgress = 6,
}

/**
 * Like TrainerSession, but from the /trainers/session/:sessionId endpoint.
 */
export type TrainerSessionDetails = Omit<
  TrainerSession,
  | "createdByUser"
  | "gym"
  | "gymId"
  | "officeLocation"
  | "trainer"
  | "participants"
> & {
  participants: TrainerSessionDetailsParticipant[];
};

/**
 *  Participant object only used by /trainers/session/:sessionId.
 */
export type TrainerSessionDetailsParticipant = Omit<
  Participant,
  "user" | "slot"
> & {
  user: NameAvatarUrl;
  attendedSlotId: number;
};

/**
 * From the `name_avatar_url` python serializer.
 */
export type NameAvatarUrl = {
  id: number;
  firstName: string;
  lastName: string;
  avatar: FileUrl;
};

/**
 * TrainerSessionDetails with dates transformed to Dayjs objects.
 */
export type TrainerSessionDetailsModel = Omit<
  TrainerSessionDetails,
  "date" | "dt_end"
> & {
  startDate: Dayjs;
  endDate: Dayjs;
};

/**
 * Response from sessions/featured.
 */
export type FeaturedSessionsResponseData = {
  data: StandardSession[];
};
