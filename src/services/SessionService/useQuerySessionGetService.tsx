/**
 *
 *
 *
 * useQuerySessionGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Session, SessionModel } from "./types";
import { sessionModel } from "./models";

export interface SessionGetUrlParams {
  sessionId: string;
}

export interface SessionGetServiceParams {
  queryConfig?: QueryConfig<Session, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: SessionGetUrlParams;
}

export const sessionGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<SessionModel> =>
  // This _ is the queryKey string
  (_: string, { sessionId }: SessionGetUrlParams) =>
    api
      .get<Session>(`${sessionGetUrl}${sessionId}`, axiosConfig)
      .then(({ data }) => sessionModel(data));

export const useQuerySessionGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: SessionGetServiceParams) => {
  return useQuery<SessionModel, AxiosError>({
    queryKey: ["session", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQuerySessionGetData = (
  urlParams: SessionGetUrlParams
): SessionModel | undefined =>
  queryCache.getQueryData<SessionModel>(["session", urlParams]);
