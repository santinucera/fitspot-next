/**
 *
 *
 *
 * useMutationRateSessionPatchService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { RateSessionPatchResponseData } from "./types";

export const rateSessionPatchUrl = `${process.env.REACT_APP_TENSPOT_API_V1}workouts/rate-session`;

const api = axios.create({ withCredentials: true });

export interface RateSessionPatchParams {
  sessionId: number;
  rating: {
    streamQuality: number;
    location: number;
    trainer: number;
    overall: number;
    gym?: number;
    text: string;
  };
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<RateSessionPatchResponseData, RateSessionPatchParams> => ({
  sessionId,
  ...requestData
}: RateSessionPatchParams) =>
  api
    .patch<RateSessionPatchResponseData>(
      `${rateSessionPatchUrl}/${sessionId}`,
      requestData.rating,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationRateSessionPatchService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    RateSessionPatchResponseData,
    AxiosError,
    RateSessionPatchParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
