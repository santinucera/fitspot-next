/**
 *
 *
 * Tests for useQueryTrainersSessionsGetService
 *
 *
 */

import React from "react";
import { ReactQueryConfigProvider } from "react-query";
import { render, waitFor, screen } from "@testing-library/react";
import { useQueryTrainersSessionsGetService } from "./useQueryTrainersSessionsGetService";
import { mockTrainersSessionsGetResponseData } from "./mockResponseData";
import dayjs from "utils/days";
import { setTrainerSessionsResponse } from "test-utils/session-service-test-utils";
import { trainerSessionModel } from "./models";

const { getByTestId } = screen;

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryTrainersSessionsGetService({
      urlParams: {
        dtStart: dayjs().utc().format(),
        dtEnd: dayjs().utc().format(),
      },
    });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryTrainersSessionsGetService({
      urlParams: {
        dtStart: dayjs().utc().format(),
        dtEnd: dayjs().utc().format(),
      },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(
        mockTrainersSessionsGetResponseData.map(trainerSessionModel)
      )
    );
  });
});

it("should render error", async () => {
  // Swallow request error
  jest.spyOn(global.console, "error").mockImplementation(() => {});
  setTrainerSessionsResponse(500);
  const El = () => {
    const { error } = useQueryTrainersSessionsGetService({
      urlParams: {
        dtStart: dayjs().utc().format(),
        dtEnd: dayjs().utc().format(),
      },
    });
    return <div data-testid="data">{error?.message}</div>;
  };

  render(
    <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
      <El />
    </ReactQueryConfigProvider>
  );

  await waitFor(() =>
    expect(getByTestId("data")).toHaveTextContent(
      "Request failed with status code 500"
    )
  );
});
