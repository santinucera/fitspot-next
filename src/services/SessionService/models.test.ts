/**
 *
 *
 * Tests for SessionService models
 *
 *
 */
import dayjs from "utils/days";
import {
  sessionModel,
  sessionsModel,
  categorizeSessionsModel,
  createParticipant,
  trainerSessionModel,
  standardSessionModel,
} from "./models";
import {
  SessionModel,
  SessionsModel,
  CustomerDashboardSessionModel,
} from "./types";
import { User, Participant } from "../types";
import {
  createSession,
  createCustomerDashboardSession,
  createTrainerSession,
  createStandardSession,
} from "test-utils/session-service-test-utils";

describe("sessionModel", () => {
  it("should transform response into model", () => {
    const session = createSession();
    const {
      id,
      name,
      description,
      trainer,
      participants,
      date,
      dt_end,
      liveSession,
      status,
      liveSessionUrl,
      isOnPlatform,
      companyId,
      streamKey,
    } = session;
    const expected: SessionModel = {
      id,
      name,
      description,
      trainer: trainer
        ? {
            firstName: trainer.firstName,
            lastName: trainer.lastName,
            avatar: trainer.avatar?.url || "",
            bio: trainer.trainer?.bio || null,
            publicId: trainer.publicId,
          }
        : null,
      participants,
      startDate: dayjs(date),
      endDate: dayjs(dt_end),
      isLiveSession: liveSession,
      status,
      liveSessionUrl,
      isOnPlatform,
      streamKey,
      companyId,
      activityCategory: "Play",
      attachment: null,
      additionalInfo: "",
      trainerId: 13543,
      trainerRating: 4.7,
      onDemandActive: false,
      videoUrl: null,
      isReplay: false,
    };
    expect(sessionModel(session)).toEqual(expected);
  });
});

describe("sessionsModel", () => {
  it("should transform response into model ", () => {
    const session = {
      ...createCustomerDashboardSession(),
    };
    const expected: SessionsModel = [
      {
        ...session,
        startDate: dayjs(session.date),
        endDate: dayjs(session.dt_end),
      },
    ];
    expect(sessionsModel({ sessions: { sessions: [session] } })).toEqual(
      expected
    );
  });
});

describe("categorizeSessionsModel", () => {
  it("should split sessions into categories", () => {
    const customerDashboardSession = createCustomerDashboardSession();
    const session: CustomerDashboardSessionModel = {
      ...customerDashboardSession,
      activityCategory: "Play",
      startDate: dayjs(customerDashboardSession.date),
      endDate: dayjs(customerDashboardSession.dt_end),
    };
    const sessions: SessionsModel = [
      { ...session, isRSVP: false },
      { ...session, isRSVP: true },
    ];
    expect(categorizeSessionsModel(sessions)).toEqual({
      upComingSessions: [sessions[0]],
      rsvpSessions: [sessions[1]],
    });
  });

  it("should return empty arrays if there are no sessions", () => {
    expect(categorizeSessionsModel(undefined)).toEqual({
      upComingSessions: [],
      rsvpSessions: [],
    });
  });
});

describe("createParticipant", () => {
  it("should return a participant object", () => {
    const user: User = {
      userProfileReady: true,
      id: 12345,
      trainer: null,
      humanApiConnected: true,
      waiver: null,
      publicId: "public_id",
      balance: 0,
      inviteList: [],
      isValidPhoneNumber: false,
      requiredPasswordChange: null,
      phone: null,
      timezone: "US/Eastern",
      avatar: null,
      disabledAccount: false,
      companyList: [
        {
          activeSince: "2017-05-11T15:40:15Z",
          website: "google.com",
          roleWithinCompany: 2,
          allowedDepartments: [],
          disabledChallenges: true,
          entityType: 0,
          otherServices:
            "Other Services Other Services Other Services Other Services Other Services ",
          publicCode: "test987",
          employeeLimit: 500,
          id: 3,
          assignedDepartment: {
            name: "Engineering",
            isActive: true,
            id: 280,
          },
          coBrandPortal: false,
          deactivated: false,
          parkingNotes: "Parking details ",
          officeLocations: [
            {
              address: "200 North Avenue",
              city: "Los Angeles",
              name: "Los Angeles",
              state: "CA",
              id: 20,
              isActive: true,
              zipcode: "90034",
            },
          ],
          eventLogo: {
            url:
              "https://appdevfitspot.s3.amazonaws.com/896/950/808/pqxvjail?Signature=lwljipUlYs%2BjLGKZ1MfzhDdadhw%3D&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG&Expires=1600729558",
          },
          environment: "Company environment ",
          servicesTabVisible: true,
          deactivatedAt: null,
          departments: [{ name: "Sales", id: 22, isActive: true }],
          industry: "Some Industry",
          assignedOfficeLocation: {
            address: "21 Peachtree Pl NW #400 TEST",
            city: "Atlanta",
            isActive: true,
            state: "GA",
            id: 152,
            name: "Atlanta",
            zipcode: "30309",
          },
          logo: {
            size: 420909,
            filename: "blob",
            userId: null,
            id: 4561,
            url:
              "https://appdevfitspot-public.s3.amazonaws.com/819/596/913/ofpaywkf",
            mimetype: "image/png",
          },
          challengesTabVisible: false,
          name: "Test Company",
          disabledPartnerships: true,
          companySize: "150",
          portalTheme: "Blue",
        },
      ],
      firstName: "Jane",
      lastName: "Doe",
      userType: 3,
      disabledAt: null,
      sessionInfo: null,
      email: "example@tenspot.com",
      isVerified: true,
      customer: {
        customerProfileReady: true,
        preferredTrainerGender: 0,
        disabledEmailNotifications: false,
        disabledTextNotifications: false,
        weight: null,
        disabledPushNotifications: false,
        numWorkouts: 0,
        height: null,
        birthday: null,
      },
      gender: 0,
      calendarPreference: 0,
    };

    const expected: Participant = {
      slotId: null,
      ratingLocation: null,
      ratingPoints: null,
      wasAttended: false,
      ratingFutureAttend: null,
      ratingText: null,
      slot: null,
      user: {
        firstName: "Jane",
        customer: {
          birthday: null,
          disabledEmailNotifications: false,
          numWorkouts: 0,
          weight: null,
          disabledTextNotifications: false,
          height: null,
          disabledPushNotifications: false,
          customerProfileReady: true,
          preferredTrainerGender: 0,
        },
        publicId: "public_id",
        trainer: null,
        ratingSum: 0,
        lastName: "Doe",
        gender: 0,
        avatar: null,
        ratingAvg: 0,
        timezone: "US/Eastern",
        sessionInfo: null,
        id: 12345,
        ratingCount: 0,
      },
      ratingTrainer: null,
      isRSVP: true,
      ratingStreamQuality: null,
      userId: 12345,
    };
    expect(createParticipant(user)).toEqual(expected);
  });
});

describe("trainerSessionModel", () => {
  const session = createTrainerSession();
  const { date, dt_end, ...rest } = session;
  const expected = { ...rest, startDate: dayjs(date), endDate: dayjs(dt_end) };
  expect(trainerSessionModel(session)).toEqual(expected);
});

describe("standardSessionModel", () => {
  it("should transform StandardSession in a StandardSessionModel", () => {
    const {
      dates: { start, startLocal, end, endLocal },
    } = standardSessionModel(createStandardSession());

    expect(start.isValid()).toBe(true);
    expect(startLocal.isValid()).toBe(true);
    expect(end.isValid()).toBe(true);
    expect(endLocal.isValid()).toBe(true);
  });
});
