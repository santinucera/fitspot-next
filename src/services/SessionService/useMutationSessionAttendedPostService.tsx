/**
 *
 *
 *
 * useMutationSessionAttendedPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { SessionAttendedPostResponseData } from "./types";

export const sessionAttendedPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/attended`;

const api = axios.create({ withCredentials: true });

export interface SessionAttendedPostRequestData {
  sessionId: number;
  wasAttended: boolean;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<
  SessionAttendedPostResponseData,
  SessionAttendedPostRequestData
> => ({ sessionId, ...requestData }: SessionAttendedPostRequestData) =>
  api
    .post<SessionAttendedPostResponseData>(
      `${sessionAttendedPostUrl}/${sessionId}`,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationSessionAttendedPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    SessionAttendedPostResponseData,
    AxiosError,
    SessionAttendedPostRequestData,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
