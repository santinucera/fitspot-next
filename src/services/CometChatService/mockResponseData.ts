/**
 *
 *
 * Mock Response Data for CometChatService
 *
 *
 */
import { CometChat } from "@cometchat-pro/chat";
import { CometApiResponseData } from "./types";

export const cometApiResponseData: CometApiResponseData = {
  uid: "some uid",
  guid: "some guid",
  authToken: "some auth token",
  appId: "some app ID",
  region: "some region",
};

export const userObj: CometChat.UserObj = {
  uid: "some uid",
  name: "some name",
  authToken: "some auth token",
  avatar: "some url",
  lastActiveAt: 1234,
  link: "some url",
  metadata: "some metadata",
  role: "some role",
  status: "some status",
  statusMessage: "some status message",
};
