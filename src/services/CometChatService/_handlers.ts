/**
 *
 *
 * handlers for CometChatService
 *
 *
 */

import { rest } from "msw";
import { cometApiResponseData } from "./mockResponseData";
import { cometApiUrl } from "./CometChatService";

export const handlers = [
  rest.get(`${cometApiUrl}/:sessionId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(cometApiResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
