/**
 *
 *
 *
 * CometChatService
 *
 * Provider and hook for creating chat interfaces.
 *
 * Warning: I swallow most thrown errors. The most recent error
 * is available `state.error` and most of them are added to
 * `state.messages`.
 *
 * @example
 * ```
 * const MyContainer = () => {
 *   const { messages, connect, disconnect } = useCometChatService();
 *
 *   useEffect(() => {
 *     addSystemMessage("Connecting...");
 *     connect();
 *     return () => disconnect();
 *   }, [])
 *
 *   useEffect(() => {
 *     if (isConnected) {
 *       addSystemMessage("Connected")
 *     }
 *   }, [isConnected])
 *
 *   return (
 *     <ul>
 *        {messages?.map((message, index) => (
 *          <li>
 *            <cite>{message.getSender().getName()}</cite>
 *            <p>{message.getText()}</p>
 *          </li>
 *        ))}
 *     </ul>
 *   )
 * }
 *
 * export default <CometChatService><MyContainer /></CometChatService>
 * ```
 */

import React, { ReactNode, useContext, useRef, useReducer } from "react";
import axios from "axios";
import { CometChat } from "@cometchat-pro/chat";
import {
  CometChatServiceError,
  State,
  ActionTypes,
  Action,
  AddMessageAction,
  AddMessagesAction,
  Status,
  Context,
  CometApiResponseData,
  ConnectMethod,
  DisconnectMethod,
  DismountMethod,
  SendMessageMethod,
  ErrorMessages,
  AddSystemMessageMethod,
  Message,
  SystemMessage,
  UpdateStatusAction,
  FailureAction,
  UpdateUserAction,
  SuccessAction,
  SetChatRoomDataAction,
  GroupObj,
  handleDismountMethod,
} from "./types";

/**
 * ############################################################################
 *
 * Constants
 *
 * ############################################################################
 */
export const cometApiUrl = `${process.env.REACT_APP_TENSPOT_API_V1}comet`;

/**
 * ############################################################################
 *
 * State
 *
 * ############################################################################
 */
const initialState: State = {
  status: Status.notAsked,
  messages: [],
};

/**
 * ############################################################################
 *
 * Actions
 *
 * ############################################################################
 */

/**
 * Add a single message.
 */
export const addMessageAction = (
  message: Message | SystemMessage
): AddMessageAction => ({
  type: ActionTypes.ADD_MESSAGE,
  message,
});

/**
 * Add several messages at once.
 */
export const addMessagesAction = (messages: Message[]): AddMessagesAction => ({
  type: ActionTypes.ADD_MESSAGES,
  messages,
});

/**
 * Set status to any Status.
 */
export const updateStatusAction = (status: Status): UpdateStatusAction => ({
  type: ActionTypes.UPDATE_STATUS,
  status,
});

/**
 * Set status to SUCCESS.
 */
export const successAction = (status: Status): SuccessAction => ({
  type: ActionTypes.SUCCESS,
  status,
});

/**
 * Set status to FAILURE
 */
export const failureAction = (error: CometChatServiceError): FailureAction => ({
  type: ActionTypes.FAILURE,
  error,
});

/**
 * Store the user object.
 */
export const updateUserAction = (
  user: CometChat.UserObj
): UpdateUserAction => ({
  type: ActionTypes.UPDATE_USER,
  user,
});

/**
 * Store the CometChat user ID and group ID.
 */
export const setChatRoomDataAction = (
  uid: string,
  guid: string
): SetChatRoomDataAction => ({
  type: ActionTypes.SET_CHAT_ROOM_DATA,
  uid,
  guid,
});

/**
 * ############################################################################
 *
 * Reducer
 *
 * ############################################################################
 */
export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.ADD_MESSAGE:
      return { ...state, messages: [...state.messages, action.message] };
    case ActionTypes.ADD_MESSAGES:
      return { ...state, messages: [...state.messages, ...action.messages] };
    case ActionTypes.UPDATE_STATUS:
      return { ...state, status: action.status };
    case ActionTypes.SUCCESS:
      return { ...state, status: action.status, error: undefined };
    case ActionTypes.FAILURE:
      return { ...state, status: Status.failure, error: action.error };
    case ActionTypes.UPDATE_USER:
      return { ...state, user: action.user };
    case ActionTypes.SET_CHAT_ROOM_DATA:
      return { ...state, uid: action.uid, guid: action.guid };
  }
};

/**
 * ############################################################################
 *
 * Provider
 *
 * ############################################################################
 */
/**
 * Create a Context with placeholder values.
 */
export const CometChatServiceContext: React.Context<Context> = React.createContext<
  Context
>({
  status: initialState.status,
  messages: [],
  error: undefined,
  user: undefined,
  hasNotBeenAsked: true,
  isConnecting: false,
  isConnected: false,
  isDisconnecting: false,
  isDisconnected: false,
  isFailure: false,
  connect: () => Promise.resolve(),
  disconnect: () => Promise.resolve(),
  handleDismount: () => {},
  dismount: () => Promise.resolve(),
  sendMessage: () => Promise.resolve(),
  addSystemMessage: () => {},
  fetchPreviousMessages: () => Promise.resolve(),
});

/**
 * ############################################################################
 *
 * Utils
 *
 * ############################################################################
 */
/**
 * Type guard because CometChat doesn't return real Errors :facepalm:
 */
export function isCometChatException(
  error: Error | CometChat.CometChatException
): error is CometChat.CometChatException {
  // Assume anything that is not an Error is a CometChatException
  return !(error instanceof Error);
}

/**
 * Type guard for CometChat user
 */
export function isCometChatUser(
  entity: CometChat.User | CometChat.Group
): entity is CometChat.User {
  return entity instanceof CometChat.User;
}

/**
 * CometChat errors are usually missing the `message` prop, but
 * usually have a descriptive `name`.
 */
export const getCometChatErrorMessage = (
  error: Error | CometChat.CometChatException
) =>
  isCometChatException(error) ? error.name || error.message : error.message;

/**
 * Type guard because system messages (i.e. messages meant for the current user that
 * are not broadcast) probably need different handling than messages from
 * other users.
 */
export function isSystemMessage(
  message: SystemMessage | Message
): message is SystemMessage {
  return (message as SystemMessage).metadata?.isSystemMessage;
}

export function isCometChatTextMessage(
  message: CometChat.BaseMessage
): message is CometChat.TextMessage {
  return message instanceof CometChat.TextMessage;
}

/**
 * Turn CometChat.User into a POJO.
 */
export function decodeCometChatUser(
  cometChatUser: CometChat.User
): CometChat.UserObj {
  return {
    uid: cometChatUser.getUid(),
    name: cometChatUser.getName(),
    authToken: cometChatUser.getAuthToken(),
    avatar: cometChatUser.getAvatar(),
    lastActiveAt: cometChatUser.getLastActiveAt(),
    link: cometChatUser.getLink(),
    // The SDK defines metadata incorrectly as a string, so ignore.
    // @ts-ignore-next-line
    metadata: cometChatUser.getMetadata(),
    role: cometChatUser.getRole(),
    status: cometChatUser.getStatus(),
    statusMessage: cometChatUser.getStatusMessage(),
  };
}

/**
 * Turn a CometChat.Group into a POJO.
 */
export function decodeCometChatGroup(group: CometChat.Group): GroupObj {
  return {
    guid: group.getGuid(),
    name: group.getName(),
  };
}

/**
 * Turn a CometChat.TextMessage into a POJO.
 */
export function decodeCometChatMessage(
  message: CometChat.BaseMessage,
  sender?: CometChat.UserObj
): Message {
  const receiver = message.getReceiver();

  return {
    sender: sender
      ? sender
      : message.getSender()
      ? decodeCometChatUser(message.getSender())
      : undefined,
    receiver: receiver
      ? isCometChatUser(receiver)
        ? decodeCometChatUser(receiver)
        : decodeCometChatGroup(receiver)
      : undefined,
    metadata: isCometChatTextMessage(message) ? message.getMetadata() : {},
    data: isCometChatTextMessage(message) ? message.getData() : {},
    text: isCometChatTextMessage(message) ? message.getText() : "",
    processedText: isCometChatTextMessage(message)
      ? message.getProcessedText()
      : "",
    // CometChat timestamps are mangled.
    // See: https://forum.cometchat.com/t/getting-wrong-sentat-in-conversation-listing/511
    sentAt: message.getSentAt() * 1000,
  };
}

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface Props {
  children: ReactNode;
}

export function CometChatService({ children }: Props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const isDismounted = useRef<boolean>(false);
  const connectionId = useRef<string | null>(null);
  const listenerId = useRef<string | null>(null);
  const messageRequest = useRef<CometChat.MessagesRequest | null>(null);
  const fetchPreviousMessagesLimitDefault = 100;
  const fetchPreviousMessagesLimit = useRef<number>(
    fetchPreviousMessagesLimitDefault
  );

  /**
   * Dispatch an action only if component was not dismounted
   *
   */
  const dispatchIfNotDismounted = (action: Action): void => {
    !isDismounted.current && dispatch(action);
  };

  /**
   * Add a message  to messages with metadata indicating this is a system message.
   *
   * Note this message is not broadcast, only the current user will see it.
   */
  const addSystemMessage: AddSystemMessageMethod = (level, text) => {
    dispatchIfNotDismounted(
      addMessageAction({
        text,
        metadata: {
          isSystemMessage: true,
          level,
        },
        sentAt: Date.now(),
      })
    );
  };

  /**
   * Login to a CometChat App.
   */
  const connect: ConnectMethod = async (groupId) => {
    // Set status to loading, store groupId, preserve previous data or errors.
    dispatchIfNotDismounted(updateStatusAction(Status.connecting));

    /**
     * Get client initialization data
     */
    // No need to store this stuff, it's only used on login;
    let authToken;
    let appId;
    let region;
    let uid;
    let guid;
    try {
      let { data } = await axios.get<CometApiResponseData>(
        `${cometApiUrl}/${groupId}`,
        {
          baseURL: cometApiUrl,
          headers: {
            Accept: "application/json",
          },
          withCredentials: true,
        }
      );
      authToken = data.authToken;
      appId = data.appId;
      region = data.region;
      uid = data.uid;
      guid = data.guid;
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error.toJSON(),
          message: `${ErrorMessages.AUTH_TOKEN} Error: ${error.message}`,
        })
      );
      return;
    }

    // Store uid and guid.
    dispatchIfNotDismounted(setChatRoomDataAction(uid, guid));

    /**
     * Initialize CometChat client
     */
    try {
      await CometChat.init(
        appId,
        new CometChat.AppSettingsBuilder()
          .subscribePresenceForAllUsers()
          .setRegion(region)
          .build()
      );
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error,
          message: `${
            ErrorMessages.CHAT_CLIENT_INIT
          } Error: ${getCometChatErrorMessage(error)}`,
        })
      );
      return;
    }

    CometChat.setSource("dashboard", "web", "reactjs");

    if (!connectionId.current) connectionId.current = `connection_${uid}`;

    /**
     * Log in to CometChat "App"
     */
    let user: CometChat.User;
    try {
      user = await CometChat.login(authToken);
      dispatchIfNotDismounted(updateUserAction(decodeCometChatUser(user)));
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error,
          message: `${ErrorMessages.LOG_IN} Error: ${getCometChatErrorMessage(
            error
          )}`,
        })
      );
      return;
    }

    /**
     * Register event listeners.
     */
    if (!listenerId.current)
      listenerId.current = `${uid}_${guid}_${new Date().getTime()}`;

    CometChat.addMessageListener(
      listenerId.current,
      new CometChat.MessageListener({
        onTextMessageReceived: (textMessage: CometChat.TextMessage): void => {
          dispatchIfNotDismounted(
            addMessageAction(decodeCometChatMessage(textMessage))
          );
        },
        // ignoring MediaMessage and CustomMessage for now.
      })
    );

    /**
     * Get group info.
     */
    let group: CometChat.Group;
    try {
      group = await CometChat.getGroup(guid);
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error,
          message: `${
            ErrorMessages.GET_GROUP
          } Error: ${getCometChatErrorMessage(error)}`,
        })
      );
      return;
    }

    /**
     * Join the group if we haven't already joined.
     */
    if (!group.getHasJoined()) {
      try {
        await CometChat.joinGroup(guid, CometChat.GROUP_TYPE.PUBLIC);
      } catch (error) {
        dispatchIfNotDismounted(
          failureAction({
            ...error,
            message: `${
              ErrorMessages.JOIN_GROUP
            } Error: ${getCometChatErrorMessage(error)}`,
          })
        );
        return;
      }
    }

    /**
     * And we're done.
     */
    dispatchIfNotDismounted(successAction(Status.connected));
  };

  /**
   * Log out user from CometChat and update status.
   *
   * Note this will cause errors when used as a useEffect cleanup
   * callback because it continues setting state. If you need to
   * disconnect when a component unmounts, use `dismount()`
   * instead.
   */
  const disconnect: DisconnectMethod = async () => {
    if (!listenerId.current) {
      dispatchIfNotDismounted(
        failureAction(
          new Error(
            `${ErrorMessages.DISCONNECT} Error: You must be connected before you can disconnect.`
          )
        )
      );
      return;
    }

    try {
      // Set state first so we're not waiting on component updates.
      dispatchIfNotDismounted(updateStatusAction(Status.disconnecting));
      await CometChat.logout();
      CometChat.removeMessageListener(listenerId.current);
      dispatchIfNotDismounted(successAction(Status.disconnected));
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error,
          message: `${
            ErrorMessages.DISCONNECT
          } Error: ${getCometChatErrorMessage(error)}`,
        })
      );
    }
  };

  const handleDismount: handleDismountMethod = () => {
    isDismounted.current = true;
    dismount();
  };

  /**
   * Disconnect a user from CometChat without changing state.
   *
   * Useful as a useEffect cleanup method.
   *
   * @example
   * ```
   * const { connect, dismount } = useCometChatService();
   * useEffect(() => {
   *   connect(groupId);
   *   return () => { dismount() };
   * })
   * ```
   */
  const dismount: DismountMethod = async () => {
    // Bail if user is not logged in.
    if (!listenerId.current) return;
    try {
      CometChat.removeMessageListener(listenerId.current);
      await CometChat.logout();
    } catch (error) {
      console.error(
        `${ErrorMessages.DISCONNECT} Error: ${getCometChatErrorMessage(error)}`
      );
    }
  };

  const sendMessage: SendMessageMethod = async (messageText) => {
    if (!state.guid) {
      dispatchIfNotDismounted(
        failureAction(
          new Error(
            `${ErrorMessages.SEND_MESSAGE}. You must connect to the chat room first.`
          )
        )
      );
      return;
    }

    const message = new CometChat.TextMessage(
      state.guid,
      messageText,
      CometChat.RECEIVER_TYPE.GROUP
    );
    // CometChat timestamps are mangled.
    // See: https://forum.cometchat.com/t/getting-wrong-sentat-in-conversation-listing/511
    message.setSentAt(Math.floor(Date.now() / 1000));

    try {
      // Optimistically add the message
      dispatchIfNotDismounted(
        addMessageAction(decodeCometChatMessage(message, state.user))
      );
      // Then broadcast the message to the group
      await CometChat.sendMessage(message);
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction({
          ...error,
          message: `${
            ErrorMessages.SEND_MESSAGE
          } Error: ${getCometChatErrorMessage(error)}`,
        })
      );
      return;
    }
  };

  /**
   * Fetch a batch of previous messages. If called several times
   */
  const fetchPreviousMessages = async (
    limit: number = fetchPreviousMessagesLimitDefault
  ) => {
    if (!state.guid) {
      dispatchIfNotDismounted(
        failureAction(
          new Error(
            `${ErrorMessages.FETCH_PREVIOUS_MESSAGES}. You must connect to the chat room first.`
          )
        )
      );
      return;
    }

    if (limit !== fetchPreviousMessagesLimit.current) {
      messageRequest.current = new CometChat.MessagesRequestBuilder()
        .setGUID(state.guid)
        .setLimit(limit)
        .build();
      fetchPreviousMessagesLimit.current = limit;
    }

    if (!messageRequest.current) {
      messageRequest.current = new CometChat.MessagesRequestBuilder()
        .setGUID(state.guid)
        .setLimit(fetchPreviousMessagesLimit.current)
        .build();
    }

    let previousMessages: CometChat.BaseMessage[];
    try {
      previousMessages = await messageRequest.current.fetchPrevious();
    } catch (error) {
      dispatchIfNotDismounted(
        failureAction(
          new Error(
            `${ErrorMessages.FETCH_PREVIOUS_MESSAGES}. You must connect to the chat room first.`
          )
        )
      );
      return;
    }

    dispatchIfNotDismounted(
      addMessagesAction(
        previousMessages
          // Empty messages have probably been deleted by an admin
          .filter(
            (message) => isCometChatTextMessage(message) && message.getText()
          )
          .map((message) => decodeCometChatMessage(message))
      )
    );
  };

  /**
   * Status booleans
   */
  const hasNotBeenAsked: boolean = state.status === Status.notAsked;
  const isConnecting: boolean = state.status === Status.connecting;
  const isConnected: boolean = state.status === Status.connected;
  const isDisconnecting: boolean = state.status === Status.disconnecting;
  const isDisconnected: boolean = state.status === Status.disconnected;
  const isFailure: boolean = state.status === Status.failure;

  return (
    <CometChatServiceContext.Provider
      value={{
        status: state.status,
        messages: state.messages,
        error: state.error,
        user: state.user,
        hasNotBeenAsked,
        isConnecting,
        isConnected,
        isDisconnecting,
        isDisconnected,
        isFailure,
        connect,
        disconnect,
        handleDismount,
        dismount,
        sendMessage,
        addSystemMessage,
        fetchPreviousMessages,
      }}
    >
      {children}
    </CometChatServiceContext.Provider>
  );
}

export const useCometChatService = () => {
  return useContext(CometChatServiceContext);
};

export default CometChatService;
