/**
 *
 * Tests for CometChatService
 *
 */

import React, { useEffect, useState } from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { rest } from "msw";
import { CometChat } from "@cometchat-pro/chat";
import server from "services/_mocks/server";
import CometChatService, {
  cometApiUrl,
  reducer,
  addMessageAction,
  addMessagesAction,
  updateStatusAction,
  successAction,
  failureAction,
  useCometChatService,
  updateUserAction,
  setChatRoomDataAction,
  isCometChatException,
  isCometChatUser,
  getCometChatErrorMessage,
  isSystemMessage,
  decodeCometChatUser,
  decodeCometChatGroup,
  decodeCometChatMessage,
} from "./CometChatService";
import {
  State,
  ActionTypes,
  ErrorMessages,
  Status,
  TextMessage,
  SystemMessage,
} from "./types";

beforeEach(() => {
  jest.resetAllMocks();
});

const MockCometChat = CometChat as jest.Mocked<typeof CometChat>;

const createUser = (): CometChat.UserObj => ({
  uid: "",
  name: "",
  authToken: "",
  avatar: "",
  lastActiveAt: 0,
  link: "",
  metadata: "",
  role: "",
  status: "",
  statusMessage: "",
});

const createTextMessage = (text: string = "some text"): TextMessage => ({
  sender: createUser(),
  receiver: createUser(),
  metadata: {},
  data: "",
  text,
  processedText: "",
  sentAt: Date.now(),
});

const createSystemMessage = (text: string = "some text"): SystemMessage => ({
  text,
  metadata: {
    isSystemMessage: true,
    level: "error",
  },
  sentAt: Date.now(),
});

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  render(<CometChatService>foo</CometChatService>);
  expect(spy).not.toHaveBeenCalled();
  spy.mockRestore();
});

describe("utils", () => {
  describe("isCometChatException", () => {
    it("should return true if this is not a regular Error", () => {
      expect(isCometChatException(new CometChat.CometChatException({}))).toBe(
        true
      );
    });
    it("should return false if this is a regular Error", () => {
      expect(isCometChatException(new Error())).toBe(false);
    });
  });
  describe("isCometChatUser", () => {
    it("should return true if this is a CometChat User", () => {
      expect(isCometChatUser(new CometChat.User(createUser()))).toBe(true);
    });
    it("should return false if this is a CometChat Group", () => {
      expect(
        isCometChatUser(new CometChat.Group("1234", "aGroup", "aType"))
      ).toBe(false);
    });
  });
  describe("getCometChatErrorMessage", () => {
    it("should return the name prop if this is a CometChatException", () => {
      expect(
        getCometChatErrorMessage(
          new CometChat.CometChatException({ name: "aName" })
        )
      ).toBe("aName");
    });
    it("should return the message prop if this is a regular error", () => {
      expect(getCometChatErrorMessage(new Error("aName"))).toBe("aName");
    });
  });
  describe("isSystemMessage", () => {
    it("should return true if this is a SystemMessage", () => {
      expect(isSystemMessage(createSystemMessage("test"))).toBe(true);
    });
    it("should return false if this is a regular TextMessage", () => {
      expect(isSystemMessage(createTextMessage("test"))).toBeFalsy();
    });
  });
  describe("decodeCometChatUser", () => {
    it("should turn a CometChat.User into a POJO", () => {
      expect(
        decodeCometChatUser(new CometChat.User(createUser()))
      ).toMatchObject({
        uid: "",
        name: "",
      });
    });
  });
  describe("decodeCometChatGroup", () => {
    it("should turn a CometChat.Group into a POJO", () => {
      expect(
        decodeCometChatGroup(new CometChat.Group("1234", "aGroup", "aType"))
      ).toMatchObject({
        guid: "1234",
        name: "aGroup",
      });
    });
  });
  describe("decodeCometChatMessage", () => {
    it("should turn a CometChat.TextMessage into a POJO", () => {
      expect(
        decodeCometChatMessage(
          new CometChat.TextMessage(
            "some receiver id",
            "some text",
            CometChat.RECEIVER_TYPE.GROUP
          )
        )
      ).toMatchObject({
        data: {
          text: "some text",
        },
        text: "some text",
      });
    });
  });
});

describe("actions", () => {
  describe("addMessageAction", () => {
    it("should return AddMessageAction", () => {
      const message = createSystemMessage("some message");
      const expected = {
        type: ActionTypes.ADD_MESSAGE,
        message,
      };
      expect(addMessageAction(message)).toEqual(expected);
    });
  });
  describe("addMessagesAction", () => {
    it("should return AddMessagesAction", () => {
      const messages = [createTextMessage("some message")];
      const expected = {
        type: ActionTypes.ADD_MESSAGES,
        messages,
      };
      expect(addMessagesAction(messages)).toEqual(expected);
    });
  });
  describe("updateStatusAction", () => {
    it("should return UpdateStatusAction", () => {
      const expected = {
        type: ActionTypes.UPDATE_STATUS,
        status: Status.connecting,
      };
      expect(updateStatusAction(Status.connecting)).toEqual(expected);
    });
  });
  describe("successAction", () => {
    it("should return SuccessAction", () => {
      const expected = {
        type: ActionTypes.SUCCESS,
        status: Status.connecting,
      };
      expect(successAction(Status.connecting)).toEqual(expected);
    });
  });
  describe("failureAction", () => {
    it("should return FailureAction", () => {
      const error = new Error("some error");
      const expected = {
        type: ActionTypes.FAILURE,
        error,
      };
      expect(failureAction(error)).toEqual(expected);
    });
  });
  describe("updateUserAction", () => {
    it("should return UpdateUserAction", () => {
      const user = createUser();
      const expected = {
        type: ActionTypes.UPDATE_USER,
        user,
      };
      expect(updateUserAction(user)).toEqual(expected);
    });
  });
  describe("setChatRoomDataAction", () => {
    it("should return SetChatRoomDataAction", () => {
      const uid = "some uid";
      const guid = "some guid";
      const expected = {
        type: ActionTypes.SET_CHAT_ROOM_DATA,
        uid,
        guid,
      };
      expect(setChatRoomDataAction(uid, guid)).toEqual(expected);
    });
  });
});

describe("reducer", () => {
  let state: State;
  beforeEach(async () => {
    state = {
      status: Status.notAsked,
      messages: [],
    };
  });

  describe("ADD_MESSAGE", () => {
    it("should add a system message", () => {
      const message1 = createSystemMessage("first message");

      expect(reducer(state, addMessageAction(message1))).toEqual({
        ...state,
        messages: [message1],
      });
    });

    it("should add a text message", () => {
      const message1 = createTextMessage("first message");

      expect(reducer(state, addMessageAction(message1))).toEqual({
        ...state,
        messages: [message1],
      });
    });
  });

  describe("ADD_MESSAGES", () => {
    it("should add a list of  messages", () => {
      const messages = [createTextMessage("first message")];

      expect(reducer(state, addMessagesAction(messages))).toEqual({
        ...state,
        messages,
      });
    });
  });

  describe("UPDATE_STATUS", () => {
    it("should update status", () => {
      expect(reducer(state, updateStatusAction(Status.connecting))).toEqual({
        ...state,
        status: Status.connecting,
      });
    });
  });

  describe("SUCCESS", () => {
    it("should set status and clear error", () => {
      const error = new Error("some error");
      expect(
        reducer({ ...state, error }, successAction(Status.connected))
      ).toEqual({
        status: Status.connected,
        messages: [],
        error: undefined,
      });
    });
  });

  describe("FAILURE", () => {
    it("should set status to failure and add error", () => {
      const error = new Error("some error");
      expect(reducer(state, failureAction(error))).toEqual({
        ...state,
        status: Status.failure,
        error,
      });
    });
  });

  describe("UPDATE_USER", () => {
    it("should set user", () => {
      const user = createUser();
      expect(reducer(state, updateUserAction(user))).toEqual({
        ...state,
        user,
      });
    });
  });

  describe("SET_CHAT_ROOM_DATA", () => {
    it("should set uid and guid", () => {
      const uid = "some uid";
      const guid = "some guid";
      expect(reducer(state, setChatRoomDataAction(uid, guid))).toEqual({
        ...state,
        uid,
        guid,
      });
    });
  });
});

describe("addSystemMessage", () => {
  it("should add a message regardless of connect status", async () => {
    const messageText1 = "first message";
    const messageText2 = "second message";
    const El = () => {
      const { messages, addSystemMessage } = useCometChatService();

      return (
        <div>
          <button
            type="button"
            data-testid="button1"
            onClick={() => addSystemMessage("error", messageText1)}
          ></button>
          <button
            type="button"
            data-testid="button2"
            onClick={() => addSystemMessage("error", messageText2)}
          ></button>
          <ol>
            {messages?.map((message, index) => (
              <li key={index} data-testid={"message-" + index}>
                <p>{message.text}</p>
              </li>
            ))}
          </ol>
        </div>
      );
    };
    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );
    fireEvent.click(getByTestId("button1"));
    await waitFor(() => {
      expect(getByTestId("message-0")).toHaveTextContent(messageText1);
    });
    fireEvent.click(getByTestId("button2"));
    await waitFor(() => {
      expect(getByTestId("message-1")).toHaveTextContent(messageText2);
    });
  });
});

describe.skip("connect", () => {
  beforeEach(async () => {
    jest.resetAllMocks();
    MockCometChat.init.mockResolvedValue(true);
    MockCometChat.login.mockResolvedValue(new CometChat.User(createUser()));
  });

  it("should error if chatroom service fails", async () => {
    server.use(
      rest.get(`${cometApiUrl}/:guid`, (req, res, ctx) => res(ctx.status(500)))
    );
    const El = () => {
      const { connect, status, error } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };
    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.AUTH_TOKEN} Error: Request failed with status code 500`
      );
    });
  });

  it("should error if CometChat init fails", async () => {
    // CometChat doesn't return real errors
    const error: CometChat.CometChatException = {
      code: "some code",
      name: "some name",
    };
    MockCometChat.init.mockRejectedValue(error);

    const El = () => {
      const { connect, status, error } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };

    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.CHAT_CLIENT_INIT} Error: ${error.name}`
      );
    });
  });

  it("should error if CometChat login fails", async () => {
    // CometChat doesn't return real errors
    const error: CometChat.CometChatException = {
      code: "some code",
      name: "some name",
    };
    MockCometChat.login.mockRejectedValue(error);
    const El = () => {
      const { connect, status, error } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };
    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.LOG_IN} Error: ${error.name}`
      );
    });
  });

  it("should connect", async () => {
    const El = () => {
      const { connect, status, error } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
          {error && <div>{error.message}</div>}
        </div>
      );
    };
    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );
    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connecting);
    });
    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
  });
});

describe("receive messages", () => {
  it.todo("should add received message to messages");
});

describe.skip("disconnect", () => {
  beforeEach(async () => {
    jest.resetAllMocks();
    MockCometChat.init.mockResolvedValue(true);
    MockCometChat.login.mockResolvedValue(new CometChat.User(createUser()));
    MockCometChat.logout.mockResolvedValue({});
  });

  it("should error if disconnect fails", async () => {
    // CometChat doesn't return real errors
    const error: CometChat.CometChatException = {
      code: "some code",
      name: "some name",
    };
    MockCometChat.logout.mockRejectedValue(error);

    const El = () => {
      const { connect, disconnect, status, error } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <button type="button" onClick={disconnect}>
            Disconnect
          </button>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };

    const { getByTestId, getByRole } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    fireEvent.click(getByRole("button"));
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.DISCONNECT} Error: ${error.name}`
      );
    });
  });

  it("should disconnect", async () => {
    const El = () => {
      const { connect, disconnect, status } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <button type="button" onClick={disconnect}>
            Disconnect
          </button>
          <div data-testid="status">{status}</div>
        </div>
      );
    };

    const { getByTestId, getByRole } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    fireEvent.click(getByRole("button"));
    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.disconnecting);
    });
    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.disconnected);
    });
  });

  it("should error if user tries to disconnect without connecting first", async () => {
    const El = () => {
      const { disconnect, error } = useCometChatService();
      return (
        <div>
          <button type="button" onClick={disconnect}>
            Disconnect
          </button>
          {error && <div data-testid="error">{error.message}</div>}
        </div>
      );
    };

    const { getByTestId, getByRole } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    fireEvent.click(getByRole("button"));
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.DISCONNECT} Error: You must be connected before you can disconnect.`
      );
    });
  });
});

describe.skip("dismount", () => {
  beforeEach(async () => {
    jest.resetAllMocks();
    MockCometChat.init.mockResolvedValue(true);
    MockCometChat.login.mockResolvedValue(new CometChat.User(createUser()));
    MockCometChat.logout.mockResolvedValue({});
  });

  it("should log warning to console if user is not connected", async () => {
    const consoleSpy = jest
      .spyOn(global.console, "warn")
      .mockImplementation(() => {});

    const El = () => {
      const { dismount } = useCometChatService();
      const [done, setDone] = useState(false);
      useEffect(() => {
        dismount();
        setDone(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return <div>{done && <div data-testid="done" />}</div>;
    };

    const { getByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("done")).toBeInTheDocument();
    });
    // Should log an error
    expect(consoleSpy).toHaveBeenCalledWith(
      `${ErrorMessages.DISCONNECT} Error: You must be connected before you can disconnect.`
    );
    consoleSpy.mockRestore();
  });

  it("should log error to console if logout fails", async (done) => {
    const consoleSpy = jest
      .spyOn(global.console, "error")
      .mockImplementation(() => {});
    // CometChat doesn't return real errors
    const error: CometChat.CometChatException = {
      code: "some code",
      name: "some name",
    };
    MockCometChat.logout.mockRejectedValue(error);

    const El = () => {
      const { connect, dismount, status } = useCometChatService();
      useEffect(() => {
        connect("some groupId");
        return () => {
          dismount();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
        </div>
      );
    };

    const { getByTestId, unmount } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    unmount();

    process.nextTick(() => {
      expect(consoleSpy).toHaveBeenCalledWith(
        `${ErrorMessages.DISCONNECT} Error: ${error.name}`
      );
      consoleSpy.mockRestore();
      done();
    });
  });

  it("should disconnect without changing state", async () => {
    const consoleSpy = jest
      .spyOn(global.console, "error")
      .mockImplementation(() => {});
    const El = () => {
      const { connect, dismount, status } = useCometChatService();
      useEffect(() => {
        connect("some groupId");
        return () => {
          dismount();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <div data-testid="status">{status}</div>
        </div>
      );
    };

    const { getByTestId, unmount } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    unmount();
    expect(consoleSpy).not.toHaveBeenCalled();
    consoleSpy.mockRestore();
  });
});

describe.skip("sendMessage", () => {
  beforeEach(async () => {
    jest.resetAllMocks();
    MockCometChat.init.mockResolvedValue(true);
    MockCometChat.login.mockResolvedValue(new CometChat.User(createUser()));
    MockCometChat.sendGroupMessage.mockResolvedValue(
      new CometChat.TextMessage(
        "some receiver id",
        "some text",
        CometChat.RECEIVER_TYPE.GROUP
      )
    );
  });

  it("should error if the user has not logged in", async () => {
    const messageText = "some message";
    const El = () => {
      const { sendMessage, messages, error } = useCometChatService();
      return (
        <div>
          <button type="button" onClick={() => sendMessage(messageText)}>
            Send
          </button>
          {error && <div data-testid="error">{error.message}</div>}
          <ol>
            {messages.map((message, index) => (
              <li key={index} data-testid="message">
                {message.text}
              </li>
            ))}
          </ol>
        </div>
      );
    };

    const { getByTestId, getByRole, queryByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    fireEvent.click(getByRole("button"));
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.SEND_MESSAGE}. You must connect to the chat room first.`
      );
      // Message should not post
      expect(queryByTestId("message")).toBeNull();
    });
  });

  it("should error if CometChat.sendGroupMessage fails", async () => {
    // CometChat doesn't return real errors
    const error: CometChat.CometChatException = {
      code: "some code",
      name: "some name",
    };
    MockCometChat.sendGroupMessage.mockRejectedValue(error);
    const messageText = "some message";
    const El = () => {
      const {
        connect,
        status,
        sendMessage,
        messages,
        error,
      } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <button type="button" onClick={() => sendMessage(messageText)}>
            Send
          </button>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
          <ol>
            {messages.map((message, index) => (
              <li key={index} data-testid="message">
                {message.text}
              </li>
            ))}
          </ol>
        </div>
      );
    };

    const { getByTestId, getByRole } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    fireEvent.click(getByRole("button"));
    // Should post the message even if there is an error sending it
    await waitFor(() => {
      expect(getByTestId("message")).toHaveTextContent(messageText);
    });
    // Then error
    await waitFor(() => {
      expect(getByTestId("error")).toHaveTextContent(
        `${ErrorMessages.SEND_MESSAGE} Error: ${error.name}`
      );
    });
  });

  it("should send a message", async () => {
    const messageText = "some message";
    const El = () => {
      const {
        connect,
        status,
        sendMessage,
        messages,
        error,
      } = useCometChatService();
      useEffect(() => {
        connect("1234");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return (
        <div>
          <button type="button" onClick={() => sendMessage(messageText)}>
            Send
          </button>
          <div data-testid="status">{status}</div>
          {error && <div data-testid="error">{error.message}</div>}
          <ol>
            {messages.map((message, index) => (
              <li key={index} data-testid="message">
                {message.text}
              </li>
            ))}
          </ol>
        </div>
      );
    };

    const { getByTestId, getByRole, queryByTestId } = render(
      <CometChatService>
        <El />
      </CometChatService>
    );

    await waitFor(() => {
      expect(getByTestId("status")).toHaveTextContent(Status.connected);
    });
    fireEvent.click(getByRole("button"));
    await waitFor(() => {
      expect(getByTestId("message")).toHaveTextContent(messageText);
    });
    // Should not error
    expect(queryByTestId("error")).toBeNull();
  });
});

describe("boolean methods", () => {
  it.todo("should cycle through booleans");
  it.todo("should render on isFailure");
});
