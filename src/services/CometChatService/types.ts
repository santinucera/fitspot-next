/**
 *
 *
 * Types for CometChatService
 *
 *
 */
import { CometChat } from "@cometchat-pro/chat";
import { AxiosErrorJSON } from "../../types";

/**
 * Errors
 */
// Require the message property
export interface CometChatException extends CometChat.CometChatException {
  message: string;
}

// Acceptable error types
export type CometChatServiceError =
  | Error
  | AxiosErrorJSON
  | CometChatException
  | undefined;

// Error levels for message metadata
export type Level = "error" | "info";

export enum ErrorMessages {
  SYSTEM_MESSAGE = "Failed to add system message.",
  AUTH_TOKEN = "Failed to fetch auth token.",
  CHAT_CLIENT_INIT = "Failed to initialize chat client.",
  LOG_IN = "Failed to log in to chat app.",
  LOG_OUT = "Failed to log out.",
  SEND_MESSAGE = "Failed to send message.",
  DISCONNECT = "Failed to disconnect.",
  GET_GROUP = "Failed to get group.",
  JOIN_GROUP = "Failed to join group.",
  FETCH_PREVIOUS_MESSAGES = "Failed to fetch previous messages.",
}

/**
 * State
 */
export interface State {
  status: Status;
  error?: CometChatServiceError;
  messages: Message[];
  user?: CometChat.UserObj;
  uid?: string;
  guid?: string;
}

/**
 * Actions
 */
export enum ActionTypes {
  ADD_MESSAGE,
  ADD_MESSAGES,
  UPDATE_STATUS,
  SUCCESS,
  FAILURE,
  UPDATE_USER,
  SET_CHAT_ROOM_DATA,
}

export interface AddMessageAction {
  type: ActionTypes.ADD_MESSAGE;
  message: Message;
}

export interface AddMessagesAction {
  type: ActionTypes.ADD_MESSAGES;
  messages: Message[];
}

export interface UpdateStatusAction {
  type: ActionTypes.UPDATE_STATUS;
  status: Status;
}

export interface SuccessAction {
  type: ActionTypes.SUCCESS;
  status: Status;
}

export interface FailureAction {
  type: ActionTypes.FAILURE;
  error: CometChatServiceError;
}

export interface UpdateUserAction {
  type: ActionTypes.UPDATE_USER;
  user: CometChat.UserObj;
}

export interface SetChatRoomDataAction {
  type: ActionTypes.SET_CHAT_ROOM_DATA;
  guid: string;
  uid: string;
}

export type Action =
  | AddMessageAction
  | AddMessagesAction
  | UpdateStatusAction
  | SuccessAction
  | FailureAction
  | UpdateUserAction
  | SetChatRoomDataAction;

/**
 * Status
 */
export enum Status {
  notAsked = "notAsked",
  connecting = "connecting",
  connected = "connected",
  disconnecting = "disconnecting",
  disconnected = "disconnected",
  failure = "failure",
}

/**
 * Context value passed to the provider
 */
export interface Context {
  status: Status;
  messages: Message[];
  error: CometChatServiceError;
  user?: CometChat.UserObj;
  hasNotBeenAsked: boolean;
  isConnecting: boolean;
  isConnected: boolean;
  isDisconnecting: boolean;
  isDisconnected: boolean;
  isFailure: boolean;
  connect: ConnectMethod;
  disconnect: DisconnectMethod;
  handleDismount: handleDismountMethod;
  dismount: DismountMethod;
  sendMessage: SendMessageMethod;
  addSystemMessage: AddSystemMessageMethod;
  fetchPreviousMessages: FetchPreviousMessagesMethod;
}

/**
 * Methods supplied by the provider
 */
export type ConnectMethod = (groupId: string) => Promise<void>;
export type DisconnectMethod = () => Promise<void>;
export type handleDismountMethod = () => void;
export type DismountMethod = () => Promise<void>;
export type SendMessageMethod = (messageText: string) => Promise<void>;
export type AddSystemMessageMethod = (level: Level, text: string) => void;
export type FetchPreviousMessagesMethod = () => Promise<void>;

/**
 * CometChat POJOs
 *
 * Data > classes.
 */
// Note this is not every property available on CometChat.Group.
export interface GroupObj {
  guid: string;
  name: string;
}

export interface TextMessage {
  sender?: CometChat.UserObj;
  receiver?: CometChat.UserObj | GroupObj;
  metadata: object;
  data: any;
  text: string;
  processedText: string;
  sentAt: number;
}

export interface SystemMessage {
  text: string;
  metadata: {
    isSystemMessage: boolean;
    level: Level;
  };
  sentAt: number;
}

export type Message = TextMessage | SystemMessage;

/**
 * /chatroom API response
 */
export interface CometApiResponseData {
  uid: string;
  guid: string;
  authToken: string;
  appId: string;
  region: string;
}
