/**
 *
 *
 * Models for transforming ChallengeGet response data
 *
 *
 */

import dayjs from "utils/days";
import {
  Challenge,
  ChallengeModel,
  ChallengeRecord,
  PointsEarnedToday,
  CategorizedChallengeModel,
  IndividualPointsSummary,
  ActivityModel,
  Activity,
} from "./types";

/**
 * Transforms challenge activities
 */
export const transformChallengeActivities = (
  activities: Activity[],
  individualPointsSummary: IndividualPointsSummary,
  pointsEarnedToday: PointsEarnedToday
): ActivityModel[] => {
  return activities.map((activity) => {
    let maxPointsReached = false;
    let pointsReached = 0;
    let currentPoints = 0;
    const foundActivity = individualPointsSummary.pointsByActivity.find(
      (byActivity) => activity.id === byActivity.activityId
    );

    if (activity.rule === "Per Day" || activity.rule === "Per Night") {
      if (foundActivity && pointsEarnedToday[foundActivity.activityId]) {
        pointsReached = pointsEarnedToday[foundActivity.activityId].reduce(
          (acc: number, a: ChallengeRecord) => acc + a.points,
          0
        );
        maxPointsReached = pointsReached === activity.maxPoints;
        currentPoints = pointsReached;
      }
    }

    if (activity.rule === "Per Challenge") {
      currentPoints = foundActivity?.points || 0;
      if (foundActivity && foundActivity.points === activity.maxPoints) {
        maxPointsReached = true;
      }
    }

    return {
      ...activity,
      currentPoints,
      canAddPoints: currentPoints !== activity.maxPoints,
      canAddIndividualPoints: activity.points !== activity.maxPoints,
      maxPointsReached,
      pointsReached,
    };
  });
};

/**
 * Transforms Challenge and Current Challenges response
 */
export const challengeModel = (responseData: Challenge): ChallengeModel => {
  const {
    id,
    leaderboard,
    dtEnd,
    dtStart,
    activities,
    individualPointsSummary,
    name,
    description,
    prizes,
    individualPointsLog,
    totalPossiblePoints,
    rules,
  } = responseData;
  const isJoined = leaderboard.some(
    (p) => p.userId === individualPointsSummary.userId
  );
  const me = leaderboard.find(
    (p) => p.userId === individualPointsSummary.userId
  );
  const endDate = dayjs(dtEnd).endOf("day");
  const startDate = dayjs(dtStart).startOf("day");
  const nowDate = dayjs();
  const daysFromStart = nowDate.diff(startDate, "day");
  const totalDays = endDate.diff(startDate, "day");
  const daysLeft = totalDays - daysFromStart;
  const hasEnded = nowDate.isAfter(endDate);
  const isUpcoming = nowDate.isBefore(startDate);
  const isActive =
    hasEnded === true ? false : nowDate.isBetween(startDate, endDate);
  const nowDateFormatted = nowDate.format("YYYY-MM-DD");

  const pointsEarnedToday = individualPointsLog.reduce(
    (acc: PointsEarnedToday, log) => {
      if (log.date === nowDateFormatted)
        acc[log.activityRuleId] = [...(acc[log.activityRuleId] || []), log];
      return acc;
    },
    {}
  );

  const transformedActivities = transformChallengeActivities(
    activities,
    individualPointsSummary,
    pointsEarnedToday
  );

  const hasParticipants = leaderboard.length > 0;
  const participantsCount = isJoined
    ? leaderboard.length - 1
    : leaderboard.length;
  const myRank = me?.rank || 0;

  const transformedPrizes = prizes.map((prize) => {
    let hostname;
    try {
      const { hostname: host } = new URL(prize.url);
      hostname = host;
    } catch (_) {
      hostname = prize.url;
    }
    return {
      hostname,
      ...prize,
    };
  });

  return {
    id,
    individualPointsSummary,
    name,
    description,
    isJoined,
    isActive,
    hasEnded,
    isUpcoming,
    leaderboard,
    hasParticipants,
    participantsCount,
    endDate,
    startDate,
    daysLeft,
    totalDays,
    myRank,
    totalPossiblePoints,
    prizes: transformedPrizes,
    activities: transformedActivities,
    rules,
  };
};

/**
 *
 * Transforms challenges into filtered groups
 * @param challenges
 * @returns {CategorizedChallengeModel}
 *
 */
export const filteredChallengesModel = (
  challenges: ChallengeModel[]
): CategorizedChallengeModel => {
  return {
    all: challenges,
    completed: challenges.filter((challenge) => challenge.hasEnded === true),
    active: challenges.filter((challenge) => challenge.isActive === true),
    upcoming: challenges.filter((challenge) => challenge.isUpcoming === true),
  };
};
