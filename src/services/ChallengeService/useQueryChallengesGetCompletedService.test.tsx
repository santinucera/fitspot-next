/**
 *
 *
 * Tests for useQueryChallengesGetCompletedService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryChallengesGetCompletedService } from "./useQueryChallengesGetCompletedService";
import { challengeModel } from "./models";
import { createCompletedChallenge } from "test-utils/challenge-service-test-utils";

it("ChallengesGetCompleted should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryChallengesGetCompletedService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("ChallengesGetCompleted should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryChallengesGetCompletedService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(
        [createCompletedChallenge()].map((data) => challengeModel(data))
      )
    );
  });
});
