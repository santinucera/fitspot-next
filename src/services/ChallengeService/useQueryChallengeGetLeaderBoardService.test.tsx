/**
 *
 *
 * Tests for useQueryChallengeGetLeaderBoardService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryChallengeGetLeaderBoardService } from "./useQueryChallengeGetLeaderBoardService";
import { mockChallengeLeaderBoardGetResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryChallengeGetLeaderBoardService({ urlParams: { challengeId: 1 } });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryChallengeGetLeaderBoardService({
      urlParams: { challengeId: 1 },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockChallengeLeaderBoardGetResponseData)
    );
  });
});
