/**
 *
 *
 *
 * useQueryChallengesGetCompletedService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ChallengeModel, Challenge } from "./types";
import { challengeModel } from "./models";

export interface ChallengesGetCompletedServiceParams {
  queryConfig?: QueryConfig<Challenge[], AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const challengesGetCompletedUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/historical-challenges`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ChallengeModel[]> => () =>
  api
    .get<Challenge[]>(challengesGetCompletedUrl, axiosConfig)
    .then(({ data }) => data.map((challenge) => challengeModel(challenge)));

export const useQueryChallengesGetCompletedService = ({
  queryConfig = {},
  axiosConfig = {},
}: ChallengesGetCompletedServiceParams = {}) => {
  return useQuery<ChallengeModel[], AxiosError>({
    queryKey: "completed-challenges",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryChallengesGetCompletedData = ():
  | ChallengeModel
  | undefined =>
  queryCache.getQueryData<ChallengeModel>("completed-challenges");
