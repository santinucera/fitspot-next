/**
 *
 *
 *
 * useQueryChallengesGetCurrentService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Challenge, ChallengeModel } from "./types";
import { challengeModel } from "./models";

export interface ChallengesGetCurrentServiceParams {
  queryConfig?: QueryConfig<Challenge[], AxiosError>;
  axiosConfig?: AxiosRequestConfig;
}

export const challengesGetCurrentUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/current-challenges`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ChallengeModel[]> => () =>
  api
    .get<Challenge[]>(challengesGetCurrentUrl, axiosConfig)
    .then(({ data }) => data.map((challenge) => challengeModel(challenge)));

export const useQueryChallengesGetCurrentService = ({
  queryConfig = {},
  axiosConfig = {},
}: ChallengesGetCurrentServiceParams = {}) => {
  return useQuery<ChallengeModel[], AxiosError>({
    queryKey: "current-challenges",
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryChallengesGetCurrentData = ():
  | ChallengeModel[]
  | undefined =>
  queryCache.getQueryData<ChallengeModel[]>("current-challenges");
