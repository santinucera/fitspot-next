/**
 *
 *
 *
 * useMutationChallengeRsvpPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ChallengeRsvpResponseData } from "./types";

export const challengeRsvpPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/accept-challenge`;

const api = axios.create({ withCredentials: true });

export interface ChallengeRsvpPostParams {
  isRSVP: boolean;
  challengeId: number;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<ChallengeRsvpResponseData, ChallengeRsvpPostParams> => ({
  challengeId,
  ...requestData
}: ChallengeRsvpPostParams) =>
  api
    .post<ChallengeRsvpResponseData>(
      `${challengeRsvpPostUrl}/${challengeId}`,
      requestData,
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationChallengeRsvpPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    ChallengeRsvpResponseData,
    AxiosError,
    ChallengeRsvpPostParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
