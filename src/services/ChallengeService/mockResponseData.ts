/**
 *
 *
 * Mock Response Data for Challenge
 *
 *
 */
import {
  ChallengeRsvpResponseData,
  ChallengeReportPostResponseData,
  LeaderBoardItem,
  Challenge,
} from "./types";

/**
 * Response from /enterprise/challenge/:challengeId
 *
 * This challenge has been RSVP-ed for (i.e. individualPointsSummary.userId
 * is in the leaderboard),
 */
export const mockChallengeGetResponseData: Challenge = {
  displayTimeStart: "12:00 AM",
  leaderboard: [
    {
      userId: 1,
      rank: 1,
      lastName: "SomeLastName",
      firstName: "SomeFirstName",
      avatar: {
        userId: 1,
        size: 0,
        filename: "blob",
        url: "https://appdevfitspot.s3.amazonaws.com/example.png",
        mimetype: "image/png",
        id: 1234,
      },
      isCurrentUser: true,
      points: 2,
    },
    {
      userId: 2,
      rank: 2,
      lastName: "OtherLastName",
      firstName: "OtherFirstName",
      avatar: {
        userId: 1,
        size: 0,
        filename: "blob",
        url: "https://appdevfitspot.s3.amazonaws.com/example.png",
        mimetype: "image/png",
        id: 1235,
      },
      isCurrentUser: false,
      points: 1,
    },
    {
      userId: 3,
      rank: 3,
      lastName: "Parker",
      firstName: "Peter",
      avatar: {
        userId: 3,
        size: 0,
        filename: "blob",
        url: "https://appdevfitspot.s3.amazonaws.com/example.png",
        mimetype: "image/png",
        id: 1235,
      },
      isCurrentUser: false,
      points: 0,
    },
  ],
  rules: [
    {
      id: 2067,
      orderId: 1,
      title: "Some Rule Title",
    },
  ],
  daysRemaining: 22,
  individualPointsSummary: {
    pointsByActivity: [],
    userId: 1,
    pointsToday: 0,
    pointsThisWeek: 0,
    totalPoints: 0,
  },
  description: "Some Description",
  lengthInDays: 42,
  displayTimeEnd: "11:59 PM",
  departments: [
    {
      id: 379,
      name: "Test 6",
      isActive: true,
    },
  ],
  dtEnd: "2020-10-23T03:59:59Z",
  prizes: [
    {
      description: "Some Prize Description",
      orderId: 1,
      rank: 1,
      url: "a.b",
      user: null,
      id: 2408,
      title: "Some Prize Title",
    },
  ],
  companyId: 3,
  participants: [
    {
      userId: 1,
      isRSVP: true,
      firstName: "SomeFirstName",
      avatar: {
        userId: 7087,
        size: 0,
        filename: "blob",
        url: "https://appdevfitspot.s3.amazonaws.com/example.png",
        mimetype: "image/png",
        id: 5344,
      },
      lastName: "SomeLastName",
      deactivated: false,
    },
    {
      userId: 2,
      isRSVP: null,
      firstName: "OtherFirstName",
      avatar: null,
      lastName: "OtherLastName",
      deactivated: false,
    },
  ],
  timezone: "US/Eastern",
  id: 1226,
  draft: false,
  dtStart: "2020-09-10T04:00:00Z",
  individualPointsLog: [],
  totalPossiblePoints: 168,
  activities: [
    {
      maxPoints: 100,
      totalPossiblePoints: 100,
      orderId: 1,
      name: "Some Activity Name",
      adminOnly: false,
      points: 10,
      pointType: "Manual",
      rule: "Per Challenge",
      id: 1,
    },
  ],
  locations: [
    {
      id: 1,
      name: "Some City",
      isActive: true,
    },
  ],
  name: "Some Name",
};

/**
 * Response from /enterprise/current-challenges response
 */
export const mockChallengesGetCurrentResponseData: Challenge[] = [
  {
    displayTimeStart: "12:00 AM",
    leaderboard: [
      {
        userId: 1,
        rank: 1,
        lastName: "SomeLastName",
        firstName: "SomeFirstName",
        avatar: {
          userId: 1,
          size: 0,
          filename: "blob",
          url: "http://example.com/example.png",
          mimetype: "image/png",
          id: 1234,
        },
        isCurrentUser: true,
        points: 14,
      },
    ],
    rules: [
      {
        id: 2073,
        orderId: 1,
        title: "Try Whil or Headspace for meditation.",
      },
      {
        id: 2074,
        orderId: 2,
        title: "Have tips on being mindful? Share with the team!",
      },
    ],
    daysRemaining: 13,
    individualPointsSummary: {
      pointsByActivity: [
        {
          activityId: 5002,
          name: "1",
          points: 10,
        },
        {
          activityId: 5003,
          name: "2",
          points: 2,
        },
        {
          activityId: 5004,
          name: "3",
          points: 2,
        },
      ],
      userId: 10064,
      pointsToday: 14,
      pointsThisWeek: 14,
      totalPoints: 14,
    },
    description:
      "Take 10 minutes out of your day to be mindful: meditate, breath work, lunch break, walk - whatever works for you!",
    lengthInDays: 16,
    displayTimeEnd: "02:00 AM",
    departments: [
      {
        id: 159,
        name: "Developers",
        isActive: true,
      },
    ],
    dtEnd: "2020-10-13T09:00:00Z",
    prizes: [
      {
        description: "We will donate $100 to your charity of choice.",
        orderId: 1,
        rank: 1,
        url: "www.giveback.com",
        user: null,
        id: 2418,
        title: "$100 Donation",
      },
      {
        description: "An Amazon Gift Card",
        orderId: 2,
        rank: 2,
        url: "www.amazon.com",
        user: null,
        id: 2419,
        title: "Amazon Gift Card",
      },
      {
        description: "We will donate $25 to your charity of choice.",
        orderId: 3,
        rank: 3,
        url: "www.giveback.com",
        user: null,
        id: 2420,
        title: "$25 Donation",
      },
    ],
    companyId: 3,
    participants: [
      {
        userId: 10064,
        isRSVP: true,
        firstName: "Ammon",
        avatar: {
          userId: 10064,
          size: 0,
          filename: "blob",
          url:
            "https://appdevfitspot.s3.amazonaws.com/982/416/907/bdrvrejl?Signature=XKkuCooZXiMAx8jlD2lLfFJAERo%3D&Expires=1601499811&AWSAccessKeyId=AKIAYXSM57QQ4CGGYSMG",
          mimetype: "image/png",
          id: 5461,
        },
        lastName: "Morris",
        deactivated: false,
      },
      {
        userId: 7174,
        isRSVP: null,
        firstName: "Test",
        avatar: null,
        lastName: "Test",
        deactivated: false,
      },
    ],
    timezone: "US/Mountain",
    id: 1234,
    draft: false,
    dtStart: "2020-09-27T07:00:00Z",
    individualPointsLog: [
      {
        id: 5272,
        points: 10,
        activityRuleId: 5002,
        date: "2020-09-30",
      },
    ],
    totalPossiblePoints: 165,
    activities: [
      {
        maxPoints: 10,
        totalPossiblePoints: 160,
        orderId: 1,
        name: "1",
        adminOnly: false,
        points: 10,
        pointType: "Manual",
        rule: "Per Day",
        id: 5002,
      },
    ],
    locations: [
      {
        id: 280,
        name: "United Kingdom",
        isActive: true,
      },
    ],
    name: "Be Mindful!",
  },
];

export const mockChallengeReportPostResponseData: ChallengeReportPostResponseData = {
  success: true,
  recordsAdded: [
    { id: 5278, points: 2, activityRuleId: 5004, date: "2020-09-30" },
  ],
};

export const mockChallengeReportPostErrorData = {
  details: ["The maximum points has been reached for 3"],
  error: "Tried to report with wrong points.",
  success: false,
};

export const mockChallengeLeaderBoardGetResponseData: LeaderBoardItem[] = [
  {
    location: "Atlanta",
    userId: 10064,
    department: "Engineering",
    fullName: "Ammon Morris",
    points: 14,
  },
];

export const mockChallengeRsvpPostResponseData: ChallengeRsvpResponseData = {
  success: true,
};
