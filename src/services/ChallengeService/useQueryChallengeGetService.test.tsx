/**
 *
 *
 * Tests for useQueryChallengeGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryChallengeGetService } from "./useQueryChallengeGetService";
import { mockChallengeGetResponseData } from "./mockResponseData";
import { challengeModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryChallengeGetService({ urlParams: { challengeId: 1 } });
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryChallengeGetService({
      urlParams: { challengeId: 1 },
    });
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(challengeModel(mockChallengeGetResponseData))
    );
  });
});
