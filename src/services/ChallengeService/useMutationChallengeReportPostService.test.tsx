/**
 *
 *
 * Tests for useMutationChallengeReportPostService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationChallengeReportPostService } from "./useMutationChallengeReportPostService";
import { mockChallengeReportPostResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationChallengeReportPostService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationChallengeReportPostService();
    return (
      <div>
        <button
          onClick={() =>
            mutate({ challengeId: 1, activityRuleId: 1, points: 5, date: "" })
          }
        >
          Mutate
        </button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockChallengeReportPostResponseData)
    );
  });
});
