/**
 *
 *
 *
 * useQueryChallengeGetLeaderBoardService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { LeaderBoardItem } from "./types";

export interface ChallengeGetLeaderBoardUrlParams {
  challengeId: number;
}

export interface ChallengeGetLeaderBoardServiceParams {
  queryConfig?: QueryConfig<LeaderBoardItem, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: ChallengeGetLeaderBoardUrlParams;
}

export const challengeGetLeaderBoardUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/challenge-leaderboard/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<LeaderBoardItem> =>
  // This _ is the queryKey string
  (_: string, { challengeId }: ChallengeGetLeaderBoardUrlParams) =>
    api
      .get<LeaderBoardItem>(
        `${challengeGetLeaderBoardUrl}${challengeId}`,
        axiosConfig
      )
      .then(({ data }) => data);

export const useQueryChallengeGetLeaderBoardService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: ChallengeGetLeaderBoardServiceParams) => {
  return useQuery<LeaderBoardItem, AxiosError>({
    queryKey: ["challenge-leaderboard", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryChallengeGetLeaderBoardData = (
  urlParams: ChallengeGetLeaderBoardUrlParams
): LeaderBoardItem | undefined =>
  queryCache.getQueryData<LeaderBoardItem>([
    "challenge-leaderboard",
    urlParams,
  ]);
