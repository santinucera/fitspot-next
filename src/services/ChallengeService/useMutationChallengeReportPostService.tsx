/**
 *
 *
 *
 * useMutationChallengeReportPostService
 *
 *
 */
import { useMutation, MutationConfig, MutationFunction } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ChallengeReportPostResponseData } from "./types";

export const challengeReportPostUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/challenge-report`;

const api = axios.create({ withCredentials: true });

export interface ChallengeReportPostParams {
  activityRuleId: number;
  date: string;
  points: number;
  challengeId: number;
}

const queryFn = (
  axiosConfig: AxiosRequestConfig = {}
): MutationFunction<
  ChallengeReportPostResponseData,
  ChallengeReportPostParams
> => ({ challengeId, ...requestData }: ChallengeReportPostParams) =>
  api
    .post<ChallengeReportPostResponseData>(
      `${challengeReportPostUrl}/${challengeId}`,
      [requestData],
      axiosConfig
    )
    .then(({ data }) => data);

export const useMutationChallengeReportPostService = ({
  mutationConfig = {},
  axiosConfig = {},
}: {
  mutationConfig?: MutationConfig<
    ChallengeReportPostResponseData,
    AxiosError,
    ChallengeReportPostParams,
    () => void
  >;
  axiosConfig?: AxiosRequestConfig;
} = {}) => {
  return useMutation(queryFn(axiosConfig), mutationConfig);
};
