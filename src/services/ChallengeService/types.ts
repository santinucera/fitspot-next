/**
 *
 *
 * Types for Challenge
 *
 *
 */

import { Dayjs } from "dayjs";

/**
 * /enterprise/challenge/:challengeId returns Challenge.
 * /enterprise/current-challenges returns Challenge[].
 * /enterprise/historical-challenges returns Challenge[].
 */
export interface Challenge {
  id: number;
  displayTimeStart: string; // "12:00 AM"
  leaderboard: ChallengeLeaderBoardItem[];
  rules: RuleElement[];
  daysRemaining: number;
  individualPointsSummary: IndividualPointsSummary;
  description: string;
  lengthInDays: number;
  displayTimeEnd: string;
  departments: Department[];
  dtEnd: string;
  prizes: Prize[];
  companyId: number;
  participants: Participant[];
  timezone: string; // "US/Mountain"
  draft: boolean;
  dtStart: string; // "2020-09-27T07:00:00Z"
  individualPointsLog: ChallengeRecord[];
  totalPossiblePoints: number;
  activities: Activity[];
  locations: ChallengeLocation[];
  name: string;
}

/**
 *
 * Transform CurrentChallenge and Challenge responses.
 *
 */
export interface ChallengeModel {
  id: number;
  isJoined: boolean;
  isActive: boolean;
  hasEnded: boolean;
  isUpcoming: boolean;
  name: string;
  description: string;
  leaderboard: ChallengeLeaderBoardItem[];
  hasParticipants: boolean;
  participantsCount: number;
  startDate: Dayjs;
  endDate: Dayjs;
  daysLeft: number;
  totalDays: number;
  totalPossiblePoints: number;
  myRank: number;
  individualPointsSummary: {
    totalPoints: number;
  };
  prizes: PrizeModel[];
  activities: ActivityModel[];
  rules: RuleElement[];
}

export interface Activity {
  maxPoints: number;
  totalPossiblePoints: number;
  orderId: number;
  name: string;
  adminOnly: boolean;
  points: number;
  pointType: PointType;
  rule: Rule;
  id: number;
}

export type PointType = "Manual" | "Steps (per 1000)";

export type Rule = "Per Day" | "Per Night" | "Per Challenge";

export interface Department {
  id: number;
  isActive: boolean;
  name: string;
}

export interface ChallengeLocation {
  id: number;
  isActive: boolean;
  name: string;
}

export interface PointsByActivity {
  activityId: number;
  name: string;
  points: number;
}

export interface IndividualPointsSummary {
  totalPoints: number;
  userId: number;
  pointsThisWeek: number;
  pointsToday: number;
  pointsByActivity: PointsByActivity[];
}

export interface Participant {
  avatar: Avatar | null;
  firstName: string;
  isRSVP: boolean | null;
  deactivated: boolean;
  userId: number;
  lastName: string;
}

export interface Avatar {
  mimetype: any;
  id: number;
  size: number;
  filename: any;
  userId: number;
  url: string;
}

export interface Prize {
  id: number;
  description: string;
  orderId: number;
  title: string;
  user: null;
  url: string;
  rank: number;
}

export interface PrizeModel extends Prize {
  hostname: string;
}

export interface RuleElement {
  id: number;
  title: string;
  orderId: number;
}

export interface ChallengeLeaderBoardItem {
  userId: number;
  rank: number;
  lastName: string | null;
  firstName: string | null;
  points: number;
  avatar: Avatar | null;
  isCurrentUser?: boolean; // only in /enterprise/challenge/:challengeId response.
}

/**
 *
 * ActivityModel model
 * adds extra calculated props
 *
 */
export interface ActivityModel extends Activity {
  maxPointsReached: boolean;
  currentPoints: number;
  canAddPoints: boolean;
  canAddIndividualPoints: boolean;
  pointsReached: number;
}

/**
 * Response from POST /enterprise/challenge-report
 * (i.e. adding points to a challenge activity)
 */
export interface ChallengeReportPostResponseData {
  recordsAdded: ChallengeRecord[];
  success: boolean;
}

export interface ChallengeRecord {
  activityRuleId: number;
  date: Date | string;
  id: number;
  points: number;
}

export interface PointsEarnedToday {
  [key: string]: ChallengeRecord[];
}

/**
 * Challenges into filtered categories.
 */
export type CategorizedChallengeModel = {
  all: ChallengeModel[];
  upcoming: ChallengeModel[];
  active: ChallengeModel[];
  completed: ChallengeModel[];
};

/**
 * /enterprise/challenge-leaderboard/:challengeId returns LeaderBoardItem[].
 */
export interface LeaderBoardItem {
  location: string;
  userId: number;
  department: string;
  fullName: string;
  points: number;
}

/**
 * Response data from POST /enterprise/accept-challenge/:challengeId
 */
export interface ChallengeRsvpResponseData {
  success: boolean;
}
