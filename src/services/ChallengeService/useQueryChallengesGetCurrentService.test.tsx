/**
 *
 *
 * Tests for useQueryChallengesGetCurrentService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryChallengesGetCurrentService } from "./useQueryChallengesGetCurrentService";
import { mockChallengesGetCurrentResponseData } from "./mockResponseData";
import { challengeModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryChallengesGetCurrentService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryChallengesGetCurrentService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(
        mockChallengesGetCurrentResponseData.map((data) => challengeModel(data))
      )
    );
  });
});
