/**
 *
 *
 * Tests for ChallengeService models
 *
 *
 */

import { createChallenge } from "test-utils/challenge-service-test-utils";
import MockDate from "mockdate";
import { challengeModel } from "./models";

describe("challengeModel", () => {
  beforeEach(() => {
    MockDate.set("12/13/2010");
  });
  afterAll(() => {
    MockDate.reset();
  });
  it("should transform response into a challenge model that's in progress", () => {
    const challenge = createChallenge({
      startDate: "12/12/2010",
      endDate: "12/20/2010",
    });
    expect(challengeModel(challenge)).toMatchInlineSnapshot(`
      Object {
        "activities": Array [
          Object {
            "adminOnly": false,
            "canAddIndividualPoints": true,
            "canAddPoints": true,
            "currentPoints": 0,
            "id": 1,
            "maxPoints": 100,
            "maxPointsReached": false,
            "name": "Some Activity Name",
            "orderId": 1,
            "pointType": "Manual",
            "points": 10,
            "pointsReached": 0,
            "rule": "Per Challenge",
            "totalPossiblePoints": 100,
          },
        ],
        "daysLeft": 8,
        "description": "Some Description",
        "endDate": "2010-12-21T23:59:59.999Z",
        "hasEnded": false,
        "hasParticipants": true,
        "id": 1226,
        "individualPointsSummary": Object {
          "pointsByActivity": Array [],
          "pointsThisWeek": 0,
          "pointsToday": 0,
          "totalPoints": 0,
          "userId": 1,
        },
        "isActive": true,
        "isJoined": true,
        "isUpcoming": false,
        "leaderboard": Array [
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1234,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "SomeFirstName",
            "isCurrentUser": true,
            "lastName": "SomeLastName",
            "points": 2,
            "rank": 1,
            "userId": 1,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "OtherFirstName",
            "isCurrentUser": false,
            "lastName": "OtherLastName",
            "points": 1,
            "rank": 2,
            "userId": 2,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 3,
            },
            "firstName": "Peter",
            "isCurrentUser": false,
            "lastName": "Parker",
            "points": 0,
            "rank": 3,
            "userId": 3,
          },
        ],
        "myRank": 1,
        "name": "Some Name",
        "participantsCount": 2,
        "prizes": Array [
          Object {
            "description": "Some Prize Description",
            "hostname": "a.b",
            "id": 2408,
            "orderId": 1,
            "rank": 1,
            "title": "Some Prize Title",
            "url": "a.b",
            "user": null,
          },
        ],
        "rules": Array [
          Object {
            "id": 2067,
            "orderId": 1,
            "title": "Some Rule Title",
          },
        ],
        "startDate": "2010-12-11T00:00:00.000Z",
        "totalDays": 10,
        "totalPossiblePoints": 168,
      }
    `);
  });
  it("should transform response into a challenge model that's ended", () => {
    const challenge = createChallenge({
      startDate: "12/1/2010",
      endDate: "12/10/2010",
    });
    expect(challengeModel(challenge)).toMatchInlineSnapshot(`
      Object {
        "activities": Array [
          Object {
            "adminOnly": false,
            "canAddIndividualPoints": true,
            "canAddPoints": true,
            "currentPoints": 0,
            "id": 1,
            "maxPoints": 100,
            "maxPointsReached": false,
            "name": "Some Activity Name",
            "orderId": 1,
            "pointType": "Manual",
            "points": 10,
            "pointsReached": 0,
            "rule": "Per Challenge",
            "totalPossiblePoints": 100,
          },
        ],
        "daysLeft": -2,
        "description": "Some Description",
        "endDate": "2010-12-11T23:59:59.999Z",
        "hasEnded": true,
        "hasParticipants": true,
        "id": 1226,
        "individualPointsSummary": Object {
          "pointsByActivity": Array [],
          "pointsThisWeek": 0,
          "pointsToday": 0,
          "totalPoints": 0,
          "userId": 1,
        },
        "isActive": false,
        "isJoined": true,
        "isUpcoming": false,
        "leaderboard": Array [
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1234,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "SomeFirstName",
            "isCurrentUser": true,
            "lastName": "SomeLastName",
            "points": 2,
            "rank": 1,
            "userId": 1,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "OtherFirstName",
            "isCurrentUser": false,
            "lastName": "OtherLastName",
            "points": 1,
            "rank": 2,
            "userId": 2,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 3,
            },
            "firstName": "Peter",
            "isCurrentUser": false,
            "lastName": "Parker",
            "points": 0,
            "rank": 3,
            "userId": 3,
          },
        ],
        "myRank": 1,
        "name": "Some Name",
        "participantsCount": 2,
        "prizes": Array [
          Object {
            "description": "Some Prize Description",
            "hostname": "a.b",
            "id": 2408,
            "orderId": 1,
            "rank": 1,
            "title": "Some Prize Title",
            "url": "a.b",
            "user": null,
          },
        ],
        "rules": Array [
          Object {
            "id": 2067,
            "orderId": 1,
            "title": "Some Rule Title",
          },
        ],
        "startDate": "2010-11-30T00:00:00.000Z",
        "totalDays": 11,
        "totalPossiblePoints": 168,
      }
    `);
  });
  it("should transform response into a challenge model that's in has not started", () => {
    const challenge = createChallenge({
      startDate: "12/20/2010",
      endDate: "12/22/2010",
    });
    expect(challengeModel(challenge)).toMatchInlineSnapshot(`
      Object {
        "activities": Array [
          Object {
            "adminOnly": false,
            "canAddIndividualPoints": true,
            "canAddPoints": true,
            "currentPoints": 0,
            "id": 1,
            "maxPoints": 100,
            "maxPointsReached": false,
            "name": "Some Activity Name",
            "orderId": 1,
            "pointType": "Manual",
            "points": 10,
            "pointsReached": 0,
            "rule": "Per Challenge",
            "totalPossiblePoints": 100,
          },
        ],
        "daysLeft": 10,
        "description": "Some Description",
        "endDate": "2010-12-23T23:59:59.999Z",
        "hasEnded": false,
        "hasParticipants": true,
        "id": 1226,
        "individualPointsSummary": Object {
          "pointsByActivity": Array [],
          "pointsThisWeek": 0,
          "pointsToday": 0,
          "totalPoints": 0,
          "userId": 1,
        },
        "isActive": false,
        "isJoined": true,
        "isUpcoming": true,
        "leaderboard": Array [
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1234,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "SomeFirstName",
            "isCurrentUser": true,
            "lastName": "SomeLastName",
            "points": 2,
            "rank": 1,
            "userId": 1,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 1,
            },
            "firstName": "OtherFirstName",
            "isCurrentUser": false,
            "lastName": "OtherLastName",
            "points": 1,
            "rank": 2,
            "userId": 2,
          },
          Object {
            "avatar": Object {
              "filename": "blob",
              "id": 1235,
              "mimetype": "image/png",
              "size": 0,
              "url": "https://appdevfitspot.s3.amazonaws.com/example.png",
              "userId": 3,
            },
            "firstName": "Peter",
            "isCurrentUser": false,
            "lastName": "Parker",
            "points": 0,
            "rank": 3,
            "userId": 3,
          },
        ],
        "myRank": 1,
        "name": "Some Name",
        "participantsCount": 2,
        "prizes": Array [
          Object {
            "description": "Some Prize Description",
            "hostname": "a.b",
            "id": 2408,
            "orderId": 1,
            "rank": 1,
            "title": "Some Prize Title",
            "url": "a.b",
            "user": null,
          },
        ],
        "rules": Array [
          Object {
            "id": 2067,
            "orderId": 1,
            "title": "Some Rule Title",
          },
        ],
        "startDate": "2010-12-19T00:00:00.000Z",
        "totalDays": 4,
        "totalPossiblePoints": 168,
      }
    `);
  });
});
