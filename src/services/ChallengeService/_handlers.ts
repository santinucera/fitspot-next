/**
 *
 *
 * handlers for Challenge service mocks
 *
 *
 */

import {
  mockChallengeGetResponseData,
  mockChallengeReportPostResponseData,
  mockChallengeLeaderBoardGetResponseData,
  mockChallengesGetCurrentResponseData,
  mockChallengeRsvpPostResponseData,
} from "./mockResponseData";

import { rest } from "msw";
import { createCompletedChallenge } from "test-utils/challenge-service-test-utils";
import { challengesGetCurrentUrl } from "./useQueryChallengesGetCurrentService";
import { challengesGetCompletedUrl } from "./useQueryChallengesGetCompletedService";
import { challengeGetUrl } from "./useQueryChallengeGetService";
import { challengeGetLeaderBoardUrl } from "./useQueryChallengeGetLeaderBoardService";
import { challengeRsvpPostUrl } from "./useMutationChallengeRsvpPostService";
import { challengeReportPostUrl } from "./useMutationChallengeReportPostService";
// [IMPORT MOCK URL AND RESPONSE ABOVE] < Needed for generating services seamlessly

export const handlers = [
  rest.get(challengesGetCurrentUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockChallengesGetCurrentResponseData));
  }),
  rest.get(challengesGetCompletedUrl, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json([createCompletedChallenge()]));
  }),
  rest.get(`${challengeGetUrl}:challengeId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockChallengeGetResponseData));
  }),
  rest.get(`${challengeGetLeaderBoardUrl}:challengeId`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(mockChallengeLeaderBoardGetResponseData)
    );
  }),
  rest.post(`${challengeRsvpPostUrl}/:challengeId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockChallengeRsvpPostResponseData));
  }),
  rest.post(`${challengeReportPostUrl}/:challengeId`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockChallengeReportPostResponseData));
  }),
  // [APPEND NEW SERVICE MOCK HANDLER ABOVE] < Needed for generating services seamlessly
];

export default handlers;
