/**
 *
 *
 *
 * useQueryChallengeGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { Challenge, ChallengeModel } from "./types";
import { challengeModel } from "./models";

export interface ChallengeGetUrlParams {
  challengeId: number;
}

export interface ChallengeGetServiceParams {
  queryConfig?: QueryConfig<Challenge, AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams: ChallengeGetUrlParams;
}

export const challengeGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}enterprise/challenge/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ChallengeModel> =>
  // This _ is the queryKey string
  (_: string, { challengeId }: ChallengeGetUrlParams) =>
    api
      .get<Challenge>(`${challengeGetUrl}${challengeId}`, axiosConfig)
      .then(({ data }) => challengeModel(data));

export const useQueryChallengeGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams,
}: ChallengeGetServiceParams) => {
  return useQuery<ChallengeModel, AxiosError>({
    queryKey: ["challenge", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryChallengeGetData = (
  urlParams: ChallengeGetUrlParams
): ChallengeModel | undefined =>
  queryCache.getQueryData<ChallengeModel>(["challenge", urlParams]);
