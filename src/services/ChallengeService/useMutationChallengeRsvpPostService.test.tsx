/**
 *
 *
 * Tests for useMutationChallengeRsvpPostService
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useMutationChallengeRsvpPostService } from "./useMutationChallengeRsvpPostService";
import { mockChallengeRsvpPostResponseData } from "./mockResponseData";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useMutationChallengeRsvpPostService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mutated mock response data", async () => {
  const El = () => {
    const [mutate, { data }] = useMutationChallengeRsvpPostService();
    return (
      <div>
        <button onClick={() => mutate({ challengeId: 1, isRSVP: true })}>
          Mutate
        </button>
        {data && <div data-testid="data">{JSON.stringify(data)}</div>}
      </div>
    );
  };
  const { getByTestId, getByText } = render(<El />);
  fireEvent.click(getByText("Mutate"));
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockChallengeRsvpPostResponseData)
    );
  });
});
