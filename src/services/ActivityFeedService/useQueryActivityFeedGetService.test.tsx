/**
 *
 *
 * Tests for useQueryActivityFeedGetService
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import { useQueryActivityFeedGetService } from "./useQueryActivityFeedGetService";
import { mockResponseData } from "./mockResponseData";
import { activityFeedModel } from "./models";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useQueryActivityFeedGetService();
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should render mock response data", async () => {
  const El = () => {
    const { data } = useQueryActivityFeedGetService();
    return (
      <div>{data && <div data-testid="data">{JSON.stringify(data)}</div>}</div>
    );
  };
  const { getByTestId } = render(<El />);
  await waitFor(() => {
    expect(getByTestId("data")).toHaveTextContent(
      JSON.stringify(mockResponseData.map(activityFeedModel))
    );
  });
});
