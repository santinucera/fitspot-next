/**
 *
 *
 *
 * useQueryActivityFeedGetService
 *
 *
 */
import { useQuery, QueryConfig, QueryFunction, queryCache } from "react-query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { ActivityFeedResponseData, ActivityFeedModel } from "./types";
import { activityFeedModel } from "./models";

export interface ActivityFeedGetUrlParams {
  challengeId?: number;
  limit?: number;
}

export interface ActivityFeedGetServiceParams {
  queryConfig?: QueryConfig<ActivityFeedResponseData[], AxiosError>;
  axiosConfig?: AxiosRequestConfig;
  urlParams?: ActivityFeedGetUrlParams;
}

export const activityFeedGetUrl = `${process.env.REACT_APP_TENSPOT_API_V1}activity-feed/`;

const api = axios.create({ withCredentials: true });

const queryFn = (
  axiosConfig: AxiosRequestConfig
): QueryFunction<ActivityFeedModel[]> => (
  // This _ is the queryKey string
  _: string,
  urlParams: ActivityFeedGetUrlParams
) =>
  api
    .get<ActivityFeedResponseData[]>(activityFeedGetUrl, {
      ...axiosConfig,
      params: urlParams,
    })
    .then(({ data }) => data.map((activity) => activityFeedModel(activity)));

export const useQueryActivityFeedGetService = ({
  queryConfig = {},
  axiosConfig = {},
  urlParams = {},
}: ActivityFeedGetServiceParams = {}) => {
  return useQuery<ActivityFeedModel[], AxiosError>({
    queryKey: ["activity-feed", urlParams],
    config: queryConfig,
    queryFn: queryFn(axiosConfig),
  });
};

export const useQueryUserGetData = (
  urlParams: ActivityFeedGetUrlParams
): ActivityFeedModel[] | undefined =>
  queryCache.getQueryData<ActivityFeedModel[]>(["activity-feed", urlParams]);
