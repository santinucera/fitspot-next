/**
 *
 *
 * Mock Response Data for ActivityFeed
 *
 *
 */

import { ActivityFeedResponseData } from "./types";

export const mockResponseData: ActivityFeedResponseData[] = [
  {
    id: 90,
    timestamp: "2020-10-08T23:50:38.015001Z",
    eventType: 1,
    eventKey: "CHALLENGE_ACTIVITY_COMPLETED",
    avatar: null,
    user: {
      id: 17580,
      name: "Bob L",
    },
    data: {
      id: 315,
      name: "Self Care Challenge",
      isRsvp: true,
      currentUserIsRsvp: true,
      challengeName: "Self Care Challenge",
      activeName: "Some activity name",
    },
  },
];
