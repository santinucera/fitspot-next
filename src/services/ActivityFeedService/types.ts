/**
 *
 *
 * Types for ActivityFeed
 *
 *
 */

import { Dayjs } from "dayjs";
import { Avatar } from "services/types";

export interface ActivityFeedModel {
  id: number;
  eventKey: ActivityFeedEventKey;
  eventType: number;
  user: User;
  timestamp: string;
  data: ActivityFeedData;
  date: Dayjs;
  avatar: Avatar | null;
}

export interface ActivityFeedData {
  id: number;
  onDemandActive?: boolean;
  isRsvp: boolean | null;
  name: string;
  currentUserIsRsvp: boolean | null;
  activeName?: string;
  challengeName?: string;
  points?: number;
}

export type ActivityFeedEventKey =
  | "CHALLENGE_ACTIVITY_COMPLETED"
  | "RSVP_SESSION"
  | "RSVP_CHALLENGE"
  | "JOINED_SESSION"
  | "WATCHING_ON_DEMAND"
  | "CONNECT_WEARABLE"
  | "JOINED_TEAM";

export interface User {
  id: number;
  name: string;
}

export type ActivityFeedResponseData = Omit<ActivityFeedModel, "date">;
