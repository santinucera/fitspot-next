/**
 *
 *
 * Models for transforming ActivityFeedService responses
 *
 *
 */

import { ActivityFeedResponseData, ActivityFeedModel } from "./types";
import dayjs from "utils/days";

export const activityFeedModel = (
  responseData: ActivityFeedResponseData
): ActivityFeedModel => {
  return {
    ...responseData,
    date: dayjs(responseData.timestamp),
  };
};
