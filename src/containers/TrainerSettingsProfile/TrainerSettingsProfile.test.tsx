/**
 *
 *
 * Tests for <TrainerSettingsProfile />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import TrainerSettingsProfile from "./TrainerSettingsProfile";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

describe("<TrainerSettingsProfile />", () => {
  beforeEach(async () => {
    queryCache.setQueryData("user", createUser());
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSettingsProfile />);
    expect(spy).not.toHaveBeenCalled();
  });
});
