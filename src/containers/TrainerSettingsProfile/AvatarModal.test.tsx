/**
 *
 *
 * Tests for <AvatarModal />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import AvatarModal from "./AvatarModal";
import { queryCache } from "react-query";

describe("<AvatarModal />", () => {
  it("should not log errors in console", () => {
    queryCache.setQueryData("user", { avatar: { url: "" } });
    const spy = jest.spyOn(global.console, "error");
    render(<AvatarModal />);
    expect(spy).not.toHaveBeenCalled();
  });
});
