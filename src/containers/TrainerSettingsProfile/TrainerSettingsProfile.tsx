/*
 *
 *
 * TrainerSettingsProfile
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button, Flex, Grid } from "theme-ui";
import { FunctionComponent } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Avatar from "components/Avatar";
import { useModal } from "containers/Modal/Modal";
import AvatarModal from "./AvatarModal";
import { useQueryUserGetData } from "services/UserService";
import useToast from "hooks/useToast";
import { useMutationUserPatchService } from "services/UserService";
import { queryCache } from "react-query";
import TrainerInput from "components/TrainerInput";

declare type Fields = {
  bio: string;
  specialties: string;
};

const TrainerSettingsProfile: FunctionComponent = () => {
  const user = useQueryUserGetData();
  const { setModal } = useModal();
  const { handleSubmit, control } = useForm<Fields>({
    defaultValues: {
      bio: user?.trainer?.bio || "",
      specialties:
        user.trainer?.activities.reduce(
          (accum, { name }, index) =>
            `${accum}${index > 0 ? "\n" : ""} ${name}`,
          ""
        ) || "",
    },
  });

  const { addToast } = useToast();

  const [handleUserPatch, { isLoading }] = useMutationUserPatchService({
    mutationConfig: {
      onSuccess(data) {
        addToast({
          title: "Success!",
          description: "Your profile has been updated.",
          type: "success",
        });
        queryCache.setQueryData("user", data);
      },
    },
  });

  const onSubmit: SubmitHandler<Fields> = ({ bio }) => {
    handleUserPatch({
      trainer: {
        bio,
        address: user?.trainer?.address || "",
        city: user?.trainer?.city || "",
        state: user?.trainer?.state || "",
        zipcode: user?.trainer?.zipcode || "",
      },
    });
  };

  const handleOpenImageEditor = () => setModal(<AvatarModal />);

  return (
    <Box>
      <Flex sx={{ my: 7, alignItems: "center" }}>
        <Avatar url={user?.avatar?.url} size={80} sx={{ mr: 7 }} />
        <Button variant="secondary" onClick={handleOpenImageEditor}>
          Update Picture
        </Button>
      </Flex>
      <Box as="form" onSubmit={handleSubmit(onSubmit)}>
        <Grid gap={5} mb={2} sx={{ maxWidth: 780 }}>
          <TrainerInput
            control={control}
            label="About Me"
            name="bio"
            multiline
          />
        </Grid>
        <Flex sx={{ maxWidth: 780, my: 2, flexDirection: "column" }}>
          <TrainerInput
            control={control}
            label="My specialties"
            name="specialties"
            disabled
            multiline
          />
        </Flex>
        <Button type="submit" mt={7} disabled={isLoading}>
          {isLoading ? "Saving..." : "Save Changes"}
        </Button>
      </Box>
    </Box>
  );
};

export default TrainerSettingsProfile;
