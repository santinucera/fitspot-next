/**
 *
 *
 * <AvatarModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Button, Box, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import Avatar from "components/Avatar";
import { useModal } from "containers/Modal/Modal";
import {
  useMutationUserPatchService,
  useQueryUserGetService,
} from "services/UserService";
import AvatarCropper from "containers/AvatarCropper";

const AvatarModal: FunctionComponent = () => {
  const { removeModal } = useModal();
  const [handleUpdateUser, { data }] = useMutationUserPatchService();

  const { data: { avatar } = {} } = useQueryUserGetService({
    queryConfig: {
      enabled: data,
      onSuccess() {
        removeModal();
      },
    },
  });

  const handleFinishUpload = ({ avatarId }: { avatarId: number }) => {
    handleUpdateUser({ avatarId });
  };

  return (
    <AvatarCropper
      onFinishUpload={handleFinishUpload}
      showInitialStepAfterUpload={false}
      initialComponent={({ onClick }) => (
        <Flex
          py={5}
          sx={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Box mb={6}>
            <Avatar url={avatar?.url} size={140} />
          </Box>
          <Button variant="secondary" onClick={onClick}>
            Change Profile Picture
          </Button>
        </Flex>
      )}
    />
  );
};

export default AvatarModal;
