/*
 *
 *
 * LiveSessions
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text } from "theme-ui";
import { Fragment, FC, useMemo, useRef, useLayoutEffect } from "react";
import dayjs from "utils/days";
import range from "utils/range";
import IconBoxArrow from "components/IconBoxArrow";
import chunk from "utils/chunk";
import {
  useQuerySessionsGetService,
  SessionsGetQueryParams,
  sessionsGroupedByDayRangesModel,
  WeekRange,
} from "services/SessionService";
import LiveSessionCard from "./LiveSessionCard";
import Spinner from "components/Spinner";
import useSessionsRSVP from "hooks/useSessionsRSVP";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */

/**
 * Creates a range on weeks starting on the current date/time.
 */
const createWeekRange = (): WeekRange => {
  const startDay = dayjs();
  const days = range(0, 21).map((day) => startDay.clone().add(day, "day"));
  return chunk(days, 7);
};

const sessionsQueryParams = (): SessionsGetQueryParams => ({
  limit: 9999,
  short: false,
  dtStart: dayjs().toISOString(),
  dtEnd: dayjs().add(21, "day").endOf("day").toISOString(),
});

const LiveSessions: FC = () => {
  const { weeks, currentWeek, todaysDate, queryParams } = useMemo(
    () => ({
      queryParams: sessionsQueryParams(),
      weeks: createWeekRange(),
      currentWeek: 0,
      todaysDate: dayjs().format("YYYY-MM-DD"),
    }),
    []
  );

  const { isFetchedAfterMount, data: sessions } = useQuerySessionsGetService({
    urlParams: queryParams,
    queryConfig: {
      refetchInterval: 1000 * 60,
    },
  });

  const groupedSessions = useMemo(() => {
    if (!sessions) return null;
    return sessionsGroupedByDayRangesModel(sessions, weeks);
  }, [sessions, weeks]);

  /**
   * DOM Node refs
   */
  const navigationNodeRefs = useRef<{ [key: string]: HTMLDivElement }>({});
  const navigationWeekRefs = useRef<{ [key: string]: HTMLDivElement }>({});
  const navigationArrowRefs = useRef<{
    prev: HTMLDivElement | null;
    next: HTMLDivElement | null;
  }>({ prev: null, next: null });

  const dayRefs = useRef<{ [key: string]: HTMLDivElement }>({});

  /**
   * Current week and date
   */
  const currentDayRef = useRef<string>(todaysDate);
  const currentWeekRef = useRef<number>(currentWeek);

  const changeWeek = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ): void => {
    const direction = event.currentTarget.dataset.direction === "prev" ? -1 : 1;
    const weekContainer =
      navigationWeekRefs.current[currentWeekRef.current + direction];
    if (!weekContainer) return;
    const el = weekContainer.children[0] as HTMLElement;
    if (el.dataset.day) {
      window.scrollTo({
        top:
          dayRefs.current[el.dataset.day].getBoundingClientRect().y! +
          window.scrollY -
          73,
        behavior: "smooth",
      });
    }
  };

  const setActiveDay = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const day = event.currentTarget.dataset.day;
    if (!day) return;
    const el = dayRefs.current[day];
    window.scrollTo({
      top: el?.getBoundingClientRect().y! + window.scrollY - 73,
      behavior: "smooth",
    });
  };

  useLayoutEffect(() => {
    if (isFetchedAfterMount === false) return;

    /**
     * Set the defaults when sessions mount
     */

    if (navigationArrowRefs.current.prev)
      navigationArrowRefs.current.prev.classList.add("hide");

    navigationNodeRefs.current[currentDayRef.current].classList.add("active");

    navigationWeekRefs.current[currentWeekRef.current].style.display = "flex";

    /**
     * Handles setting the current day when a user scrolls
     * to the closest day to the top of the page.
     */
    const scrollHandler = () => {
      const closestSessionToWindowTop = Object.entries(dayRefs.current).find(
        ([_, targetElement]) => {
          const rect = targetElement.getBoundingClientRect();
          return rect.top + rect.height - 73 > 0;
        }
      );

      /**
       * If there isn't a closest element do nothing.
       */
      if (!closestSessionToWindowTop) return;

      const [day, foundElement] = closestSessionToWindowTop;

      const currentWeek = Number(foundElement.parentElement?.dataset.weekIndex);

      /**
       * If the current week changes updated the UI.
       */
      if (currentWeekRef.current !== currentWeek) {
        if (currentWeekRef.current !== null) {
          navigationWeekRefs.current[currentWeekRef.current].style.display =
            "none";
        }

        currentWeekRef.current = currentWeek;

        /**
         * Hides and shows arrows.
         */
        if (currentWeekRef.current === 0 && navigationArrowRefs.current.prev)
          navigationArrowRefs.current.prev.classList.add("hide");
        if (currentWeekRef.current !== 0 && navigationArrowRefs.current.prev)
          navigationArrowRefs.current.prev.classList.remove("hide");
        if (currentWeekRef.current === 2 && navigationArrowRefs.current.next)
          navigationArrowRefs.current.next.classList.add("hide");
        if (currentWeekRef.current !== 2 && navigationArrowRefs.current.next)
          navigationArrowRefs.current.next.classList.remove("hide");

        navigationWeekRefs.current[currentWeekRef.current].style.display =
          "flex";
      }

      /**
       * Adds active state to the day in navigation.
       */
      if (currentDayRef.current !== day) {
        if (currentDayRef.current !== null) {
          navigationNodeRefs.current[currentDayRef.current].classList.remove(
            "active"
          );
        }
        currentDayRef.current = day;
        navigationNodeRefs.current[currentDayRef.current]!.classList.add(
          "active"
        );
      }
    };

    window.addEventListener("scroll", scrollHandler);
    /**
     * Removes the scroll listener on un-mount.
     */
    return () => window.removeEventListener("scroll", scrollHandler);
  }, [isFetchedAfterMount]);

  const { handleRSVP, handleCancelRSVP } = useSessionsRSVP({
    sessionsQueryParams: queryParams,
  });

  if (!isFetchedAfterMount) return <Spinner />;

  return (
    <Fragment>
      <Flex
        sx={{
          alignItems: "center",
          position: "sticky",
          top: 0,
          zIndex: 2,
          bg: "light-gray",
          py: 3,
          mb: 7,
        }}
      >
        <Flex
          data-direction="prev"
          onClick={changeWeek}
          sx={{
            cursor: "pointer",
            width: "50px",
            display: ["none", "flex"],
            "&.hide": {
              display: "none",
            },
          }}
          ref={(el) => {
            if (el) navigationArrowRefs.current.prev = el;
          }}
        >
          <IconBoxArrow direction="left" />
        </Flex>
        {weeks.map((week, idx) => {
          return (
            <Flex
              key={idx}
              ref={(el) => {
                if (el) navigationWeekRefs.current[idx] = el;
              }}
              sx={{
                display: "none",
              }}
            >
              {week.map((date) => {
                const formattedDate = date.format("YYYY-MM-DD");
                return (
                  <Flex
                    ref={(node) => {
                      if (node)
                        navigationNodeRefs.current[formattedDate] = node;
                    }}
                    key={formattedDate}
                    data-day={formattedDate}
                    onClick={setActiveDay}
                    sx={{
                      alignItems: "center",
                      flexDirection: "column",
                      color: "dark-gray",
                      cursor: "pointer",
                      px: 6,
                      transition: "color 75ms ease",

                      "&.active": {
                        color: "primary",
                      },
                    }}
                  >
                    <Text
                      sx={{
                        textTransform: "uppercase",
                        fontSize: 3,
                        fontWeight: 700,
                      }}
                    >
                      {date.format("ddd")}
                    </Text>
                    <Text>{date.format("D")}</Text>
                  </Flex>
                );
              })}
            </Flex>
          );
        })}
        <Flex
          data-direction="next"
          onClick={changeWeek}
          sx={{
            cursor: "pointer",
            width: "50px",
            justifyContent: "center",
            display: ["none", "flex"],
            "&.hide": {
              display: "none",
            },
          }}
          ref={(el) => {
            if (el) navigationArrowRefs.current.next = el;
          }}
        >
          <IconBoxArrow />
        </Flex>
      </Flex>
      <Box data-testid="live-sessions">
        {groupedSessions &&
          groupedSessions.map((week, i) => {
            return (
              <Box key={i} data-week-index={i}>
                {Object.entries(week).map(([key, sessions]) => {
                  return (
                    <Box
                      key={key}
                      data-testid={`live-sessions-${key}`}
                      data-session-card-day={key}
                      ref={(el) => {
                        if (el) dayRefs.current[key] = el;
                      }}
                    >
                      <Text
                        sx={{
                          color: "primary",
                          fontWeight: 700,
                          textTransform: "uppercase",
                          position: "sticky",
                          bg: "light-gray",
                          top: "73px",
                          py: 2,
                          zIndex: 1,
                        }}
                      >
                        {dayjs(key).format("dddd, MMM DD")}
                      </Text>
                      {sessions.length === 0 && (
                        <Text sx={{ color: "medium-gray", my: 3 }}>
                          Check back tomorrow for more upcoming live sessions!
                        </Text>
                      )}
                      {sessions.map((session) => {
                        return (
                          <LiveSessionCard
                            key={session.id}
                            session={session}
                            handleRSVP={handleRSVP}
                            handleCancelRSVP={handleCancelRSVP}
                          />
                        );
                      })}
                    </Box>
                  );
                })}
              </Box>
            );
          })}
      </Box>
      <Text sx={{ height: `calc(100vh)`, pt: 8 }}>
        That's all for now. Check back tomorrow for more upcoming live sessions!
      </Text>
    </Fragment>
  );
};

export default LiveSessions;
