/**
 *
 *
 * <LiveSessionCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Flex, Button, Grid } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { queryCache } from "react-query";
import PillarChip from "components/PillarChip";
import Avatar from "components/Avatar";
import IconStarFilled from "components/IconStarFilled";
import IconCheckMark from "components/IconCheckMark";
import {
  CustomerDashboardSessionModel,
  SessionRsvpPostParams,
} from "services/SessionService";
import { Link, useNavigate } from "react-router-dom";
import IconBolt from "components/IconBolt";
import TextTruncate from "react-text-truncate";
import { useModal } from "containers/Modal/Modal";
import useSessionStatus from "hooks/useSessionStatus";
import useToast from "hooks/useToast";
import SessionRatingButton from "components/SessionRatingButton";

interface LiveSessionCardProps {
  session: CustomerDashboardSessionModel;
  handleRSVP: (params: SessionRsvpPostParams) => void;
  handleCancelRSVP: (sessionId: number) => void;
}

const LiveSessionCard: FunctionComponent<LiveSessionCardProps> = ({
  session,
  handleRSVP,
  handleCancelRSVP,
}) => {
  const { addToast } = useToast();
  const { removeModal } = useModal();
  const navigate = useNavigate();
  const {
    id,
    startDate,
    endDate,
    activityCategory,
    name,
    trainerAvatar,
    trainerName,
    trainerRating,
    isRSVP,
    isSub,
    isReplay,
    wasAttended,
    userSessionRating,
    liveSession,
  } = session;

  const {
    isSessionLive,
    isSessionStartingSoon,
    isSessionCompleted,
  } = useSessionStatus({
    startDate,
    endDate,
  });

  const ratingPoints = userSessionRating?.ratingPoints || null;
  const ratingStreamQuality = userSessionRating?.ratingStreamQuality || null;
  const ratingTrainer = userSessionRating?.ratingTrainer || null;
  const ratingLocation = userSessionRating?.ratingLocation || null;

  return (
    <Flex
      data-testid={`session-id-${id}`}
      key={id}
      sx={{
        bg: "white",
        alignItems: "center",
        my: 2,
        p: 6,
        flexDirection: ["column", "row"],
      }}
    >
      <Flex
        onClick={() => {
          navigate(`/dashboard/sessions/${id}`);
        }}
        sx={{
          width: "100%",
          cursor: "pointer",
        }}
      >
        <Flex
          sx={{
            flexDirection: ["column", "row"],
          }}
        >
          <Grid
            sx={{
              minWidth: "150px",
              gap: "4px",
              gridTemplateRows: "repeat(3, 20px)",
            }}
          >
            <Box
              data-testid="session-live"
              sx={{ fontSize: 0, color: "forest-green" }}
            >
              {(isSessionLive || isSessionStartingSoon) && (
                <Text sx={{ fontWeight: 700 }}>
                  <IconBolt />
                  {isSessionLive ? "LIVE" : "STARTING SOON"}
                </Text>
              )}
            </Box>
            <Text>
              {`${startDate.format("h:mm")} - ${endDate.format("h:mma")}`}
            </Text>
            <Text
              data-testid="session-replay"
              sx={{ fontSize: 11, fontWeight: "bold" }}
            >
              {isReplay && "REPLAY"}
            </Text>
          </Grid>

          <Grid
            sx={{
              gap: "4px",
              gridTemplateRows: "repeat(2, min-content)",
            }}
          >
            <PillarChip
              pillar={activityCategory}
              sx={{
                position: "relative",
                top: 0,
                left: 0,
              }}
            />
            <Text sx={{ pr: [0, 7] }}>
              <TextTruncate line={2} text={name} />
            </Text>
          </Grid>
        </Flex>
        <Flex
          sx={{
            ml: "auto",
            alignItems: "center",
            width: ["auto", "220px"],
            flexDirection: ["column", "row"],
          }}
        >
          <Box sx={{ mr: [0, 4], px: [4, 0] }}>
            <Avatar url={trainerAvatar} size={64} />
          </Box>
          <Grid
            sx={{
              gap: "4px",
              gridTemplateRows: "repeat(3, 20px)",
            }}
          >
            <Text
              data-testid="session-sub"
              sx={{ fontSize: 0, color: "red", fontWeight: "bold" }}
            >
              {isSub && "SUB"}
            </Text>
            <Text data-testid="session-trainer-name">{trainerName}</Text>
            <Flex
              data-testid="session-trainer-rating"
              sx={{
                alignItems: "center",
                fontSize: 0,
                color: "dark-gray",
              }}
            >
              {trainerRating && (
                <Fragment>
                  <Text sx={{ mr: 1 }}>{trainerRating}</Text>
                  <IconStarFilled size={13} />
                </Fragment>
              )}
            </Flex>
          </Grid>
        </Flex>
      </Flex>
      <Flex
        sx={{
          mt: [3, 0],
          minWidth: ["100%", "200px"],
          justifyContent: "flex-end",
        }}
      >
        {isSessionCompleted ? (
          <Fragment>
            {wasAttended && (
              <SessionRatingButton
                ratings={{
                  ratingPoints,
                  ratingStreamQuality,
                  ratingTrainer,
                  ratingLocation,
                }}
                sessionName={name}
                avatar={trainerAvatar || ""}
                startDate={startDate}
                endDate={endDate}
                isLiveSession={liveSession}
                sessionId={id}
                onSuccess={() => {
                  addToast({
                    title: "Success!",
                    description: "You successfully rated the session.",
                    type: "success",
                  });
                  removeModal();
                  queryCache.invalidateQueries(["sessions"]);
                }}
                onError={() => {
                  addToast({
                    title: "Error!",
                    description: "An error occurred, please try again.",
                    type: "error",
                  });
                }}
              />
            )}
          </Fragment>
        ) : isSessionLive ? (
          <Link
            sx={{ variant: "buttons.primary", width: 150 }}
            to={`/dashboard/sessions/${id}`}
          >
            Join Session
          </Link>
        ) : isRSVP ? (
          <Button
            sx={{ width: 150 }}
            variant="secondary"
            onClick={() => handleCancelRSVP(id)}
          >
            <IconCheckMark sx={{ mr: 2 }} />
            Reserved
          </Button>
        ) : (
          <Button
            sx={{ width: 150 }}
            onClick={() => handleRSVP({ sessionId: id, isRSVP: true })}
          >
            Reserve a spot
          </Button>
        )}
      </Flex>
    </Flex>
  );
};

export default LiveSessionCard;
