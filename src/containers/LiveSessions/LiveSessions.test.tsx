/**
 *
 *
 * Tests for <LiveSessions />
 *
 *
 */

import React from "react";
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import LiveSessions from "./LiveSessions";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import {
  createCustomerDashboardSession,
  setSessionsResponse,
} from "test-utils/session-service-test-utils";
import MockDate from "mockdate";
import dayjs from "utils/days";
import { renderWithRouter } from "test-utils/render-with-router";
import range from "utils/range";

describe("<LiveSessions />", () => {
  beforeEach(() => {
    queryCache.setQueryData("user", createUser());
    MockDate.set("2010-12-01T11:30:00.000Z");
  });
  afterAll(() => {
    MockDate.reset();
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<LiveSessions />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render 21 days of upcoming sessions", async () => {
    const startOfDay = dayjs().startOf("day");
    setSessionsResponse(
      range(0, 21).map((idx) => {
        return {
          ...createCustomerDashboardSession(),
          date: startOfDay.add(idx, "day").hour(11).toISOString(),
          dt_end: startOfDay.add(idx, "day").toISOString(),
          id: idx,
        };
      })
    );

    renderWithRouter(<LiveSessions />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    /**
     * There should be 21 days of results
     */
    expect(screen.getAllByTestId(/session-id-/).length).toBe(21);
  });

  it("should render a message when days have zero sessions", async () => {
    const startOfDay = dayjs().startOf("day");
    // The last day should return 0 results.
    setSessionsResponse(
      range(0, 20).map((idx) => {
        return {
          ...createCustomerDashboardSession(),
          date: startOfDay.add(idx, "day").hour(11).toISOString(),
          dt_end: startOfDay.add(idx, "day").toISOString(),
          id: idx,
        };
      })
    );

    renderWithRouter(<LiveSessions />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.getByTestId("live-sessions-2010-12-21")).toHaveTextContent(
      "Check back tomorrow for more upcoming live sessions!"
    );
  });
});
