/**
 *
 *
 * Tests for <LiveSessionCard />
 *
 *
 */

import React from "react";
import LiveSessionCard from "./LiveSessionCard";
import dayjs from "utils/days";
import { renderWithRouter } from "test-utils/render-with-router";
import MockDate from "mockdate";
import { screen, within } from "@testing-library/react";
import { createCustomerDashboardSessionModel } from "test-utils/session-service-test-utils";

describe("<LiveSessionCard />", () => {
  beforeEach(() => {
    MockDate.set("2010-12-01T11:30:00.000Z");
  });
  afterAll(() => {
    MockDate.reset();
  });

  it.only("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs(),
          endDate: dayjs().add(1, "hour"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: false,
          wasAttended: true,
          isReplay: false,
          liveSession: true,
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a live session", () => {
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs("2010-12-01T11:00:00.000Z"),
          endDate: dayjs("2010-12-01T12:00:00.000Z"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: false,
          wasAttended: true,
          isReplay: false,
          liveSession: true,
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    /**
     * `LIVE` text should be in the DOM and a link to the session
     */
    expect(screen.getByTestId("session-live")).toMatchSnapshot();
  });

  it("should render a sub session", () => {
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs("2010-12-01T11:00:00.000Z"),
          endDate: dayjs("2010-12-01T12:00:00.000Z"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: true,
          wasAttended: true,
          isReplay: false,
          liveSession: true,
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    /**
     * `SUB` text should be in the DOM
     */
    expect(screen.getByTestId("session-sub")).toMatchSnapshot();
  });

  it("should render session ratings that has not been rated", () => {
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs("2010-12-01T09:00:00.000Z"),
          endDate: dayjs("2010-12-01T10:00:00.000Z"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: false,
          wasAttended: true,
          isReplay: false,
          liveSession: true,
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    /**
     * Ratings should be in the DOM
     */
    expect(screen.getByTestId("session-rating-button")).toMatchSnapshot();
  });

  it("should render session with 5 star rating", async () => {
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs("2010-12-01T09:00:00.000Z"),
          endDate: dayjs("2010-12-01T10:00:00.000Z"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: false,
          wasAttended: true,
          isReplay: false,
          liveSession: true,
          userSessionRating: {
            ratingFutureAttend: null,
            ratingLocation: null,
            ratingPoints: 5,
            ratingStreamQuality: 4,
            ratingText: null,
            ratingTrainer: 3,
          },
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    /**
     * Ratings should be in the DOM
     */
    expect(screen.getByTestId("session-rating-button")).toMatchSnapshot();
    const ratings = await within(
      screen.getByTestId("session-rating-button")
    ).getByLabelText("5 Stars");
    expect(ratings).toBeInTheDocument();
  });

  it("should render a replay session", () => {
    const session = createCustomerDashboardSessionModel();
    renderWithRouter(
      <LiveSessionCard
        session={{
          ...session,
          id: 1,
          name: "Kung Fury",
          startDate: dayjs("2010-12-01T09:00:00.000Z"),
          endDate: dayjs("2010-12-01T10:00:00.000Z"),
          trainerRating: 3.2,
          trainerAvatar: "...",
          trainerName: "Tommy Lee",
          activityCategory: "Chill",
          isRSVP: true,
          isSub: false,
          wasAttended: true,
          isReplay: true,
          liveSession: true,
        }}
        handleRSVP={() => {}}
        handleCancelRSVP={() => {}}
      />
    );
    /**
     * `REPLAY` text should be in the DOM
     */
    expect(screen.getByTestId("session-replay")).toMatchSnapshot();
  });
});
