/**
 *
 *
 * <Modal />
 *
 *
 */
/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui";
import {
  FunctionComponent,
  MouseEvent,
  Dispatch,
  SetStateAction,
  ReactElement,
} from "react";
import { useState, createContext, useContext, useCallback } from "react";
import { createPortal } from "react-dom";
import { AnimatePresence, motion } from "framer-motion";
import IconX from "components/IconX";

type ContextValue = {
  setModal: Dispatch<SetStateAction<ReactElement | null>>;
  removeModal: () => void;
};

const initialContext: ContextValue = {
  setModal: () => {},
  removeModal: () => {},
};

const ModalContext = createContext<ContextValue>(initialContext);

export const useModal = () => useContext(ModalContext);

interface ModalProps {
  remove: (event: MouseEvent<HTMLElement>) => void;
}

const Modal: FunctionComponent<ModalProps> = ({ children, remove }) => {
  return (
    <Box
      data-testid="modal"
      sx={{
        zIndex: 1300,
        width: "100vw",
        height: "100vh",
        position: "fixed",
        top: 0,
        bottom: 0,
        display: "block",
      }}
    >
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { duration: 0.2 } }}
        exit={{ opacity: 0, transition: { duration: 0.7 } }}
      >
        <Flex
          sx={{
            overflow: "auto",
            position: "fixed",
            width: "100%",
            height: "100vh",
            bg: "rgba(0,0,0,0.7)",
            boxShadow: "small",
            top: 0,
            left: 0,
            justifyContent: "center",
          }}
        >
          <motion.div
            initial={{ opacity: 0, y: 20, scale: 0.9 }}
            animate={{
              opacity: 1,
              y: 0,
              scale: 1,
              transition: { duration: 0.2 },
            }}
            exit={{ opacity: 0, scale: 0.9, transition: { duration: 0.1 } }}
          >
            <Flex
              sx={{
                bg: "white",
                p: 4,
                position: "relative",
                top: [0, 120],
                width: ["100vw", 520],
                height: ["100vh", "auto"],
              }}
            >
              <Box
                onClick={remove}
                sx={{
                  position: "absolute",
                  right: 0,
                  top: 0,
                  m: 3,
                  cursor: "pointer",
                }}
                data-testid="close-modal-button"
              >
                <IconX />
              </Box>
              {children}
            </Flex>
          </motion.div>
        </Flex>
      </motion.div>
    </Box>
  );
};

const ModalProvider: FunctionComponent = ({ children }) => {
  const [modal, setModal] = useState<ReactElement | null>(null);
  const removeModal = useCallback(() => {
    setModal(null);
  }, [setModal]);

  return (
    <ModalContext.Provider value={{ setModal, removeModal }}>
      {children}
      {createPortal(
        <AnimatePresence>
          {modal && <Modal remove={removeModal}>{modal}</Modal>}
        </AnimatePresence>,
        document.body
      )}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
