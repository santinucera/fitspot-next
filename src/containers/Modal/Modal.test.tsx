/**
 *
 *
 * Tests for <Modal />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Modal from "./Modal";

describe("<Modal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Modal />);
    expect(spy).not.toHaveBeenCalled();
  });
});
