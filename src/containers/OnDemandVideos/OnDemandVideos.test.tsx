/**
 *
 *
 * Tests for <OnDemandVideos />
 *
 *
 */

import React from "react";
import OnDemandVideos from "./OnDemandVideos";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  screen,
  waitForElementToBeRemoved,
  waitFor,
  fireEvent,
  within,
} from "@testing-library/react";
import {
  createOnDemand,
  setEmptyOnDemandResponse,
  setOnDemandResponse,
} from "test-utils/on-demand-service-test-utils";

describe("<OnDemandVideos />", () => {
  beforeAll(() => {
    window.scrollTo = jest.fn();
  });
  afterAll(() => {
    jest.clearAllMocks();
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<OnDemandVideos />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render an on-demand video", async () => {
    renderWithRouter(<OnDemandVideos />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.getAllByRole("heading")).toMatchInlineSnapshot(`
      Array [
        <h3
          class="css-10xjrqq-OnDemandItem"
        >
          Johns intro to street fighting 
        </h3>,
      ]
    `);
  });

  it("should render paginated on-demand videos", async () => {
    setOnDemandResponse(
      200,
      createOnDemand({
        limit: 9,
      })
    );
    renderWithRouter(<OnDemandVideos />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.getAllByRole("heading")).toHaveLength(9);
    expect(screen.getByTestId("pagination")).toBeInTheDocument();
    /**
     * Set the next paginated response.
     */
    setOnDemandResponse(
      200,
      createOnDemand({
        limit: 9,
        offset: 9,
      })
    );
    fireEvent.click(within(screen.getByTestId("pagination")).getByText("2"));
    await waitFor(() => {
      /**
       * The starting index is 0.
       * Loading the next results the first results id should be 9
       */
      expect(screen.getByTestId("on-demand-video-9")).toBeInTheDocument();
    });
  });

  it("should render no on-demand videos", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());

    setEmptyOnDemandResponse();

    const { getByText, getByTestId } = renderWithRouter(<OnDemandVideos />);

    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitFor(() =>
      expect(
        getByText(
          "There are no on demand videos that match the current filters."
        )
      ).toBeInTheDocument()
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
