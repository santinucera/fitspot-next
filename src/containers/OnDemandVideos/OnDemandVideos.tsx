/*
 *
 *
 * OnDemandVideos
 *
 *
 */

/** @jsx jsx */
import { jsx, Grid, Box, Flex, Button } from "theme-ui";
import {
  Fragment,
  FunctionComponent,
  useCallback,
  useMemo,
  useState,
} from "react";
import { useQueryOnDemandVideosGetService } from "services/OnDemandVideoService";
import Spinner from "components/Spinner";
import { useSearchParams } from "react-router-dom";
import { Dropdown, DropdownButton } from "components/Dropdown";
import IconChevron from "components/IconChevron";
import useFlyOut from "hooks/useFlyOut";
import OnDemandFilters from "./OnDemandFilters";
import OnDemandItem from "components/OnDemandItem";
import Pagination from "components/Pagination";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */

interface OnDemandOption {
  title: string;
  key: string;
}

enum SortByKeys {
  startDate = "startDate",
  name = "name",
}

const onDemandSortByOptions: OnDemandOption[] = [
  {
    title: "Date",
    key: SortByKeys.startDate,
  },
  {
    title: "Name",
    key: SortByKeys.name,
  },
];

interface OnDemandVideosProps {}

const OnDemandVideos: FunctionComponent<OnDemandVideosProps> = () => {
  const [searchParams] = useSearchParams();
  const { setFlyOut, removeFlyOut } = useFlyOut();

  const [sortByOption, setSortByOption] = useState<OnDemandOption>(
    onDemandSortByOptions[0]
  );

  const [page, setPage] = useState(0);
  const LIMIT = 36;

  const activityId = Number(searchParams.get("activity"));
  const categoryId = Number(searchParams.get("category"));

  const {
    isLoading,
    isFetching,
    resolvedData: onDemandVideos,
  } = useQueryOnDemandVideosGetService({
    queryParams: {
      limit: LIMIT,
      offset: page * LIMIT,
      activityId,
      categoryId,
      startDate: "2020-06-01T00:00:00Z",
    },
  });
  const paginationCount = useMemo(() => {
    return Math.ceil((onDemandVideos?.meta.pagination.total || 0) / LIMIT);
  }, [onDemandVideos]);

  const sortedOnDemandVideos = useMemo(() => {
    if (!onDemandVideos) return [];
    const { data } = onDemandVideos;
    if (sortByOption.key === SortByKeys.startDate)
      return data?.sort((a, b): number =>
        a.startDate.valueOf() > b.startDate.valueOf() ? -1 : 0
      );
    if (sortByOption.key === SortByKeys.name)
      return data?.sort((a, b): number =>
        a.name > b.name ? 1 : b.name > a.name ? -1 : 0
      );
    return data;
  }, [sortByOption.key, onDemandVideos]);

  const handleFlyOutMenu = useCallback(
    () =>
      setFlyOut(
        <OnDemandFilters
          removeFlyOut={() => {
            setPage(0);
            removeFlyOut();
          }}
        />
      ),
    [setFlyOut, removeFlyOut]
  );

  return (
    <Box>
      {isLoading || isFetching ? (
        <Spinner />
      ) : (
        <Fragment>
          <Flex sx={{ my: 6, alignItems: "center" }}>
            <Dropdown
              handle={
                <Flex sx={{ alignItems: "center" }}>
                  <Box>Sort By {sortByOption.title}</Box>
                  <IconChevron sx={{ transform: "rotate(90deg)", ml: 1 }} />
                </Flex>
              }
              align="left"
            >
              {onDemandSortByOptions.map(({ key, title }) => (
                <DropdownButton
                  key={key}
                  onClick={() => setSortByOption({ key, title })}
                >
                  {title}
                </DropdownButton>
              ))}
            </Dropdown>
            <Box sx={{ ml: "auto" }}>
              <Button onClick={handleFlyOutMenu}>Filter</Button>
            </Box>
          </Flex>
          {sortedOnDemandVideos.length ? (
            <Grid
              sx={{
                gap: 6,
                gridTemplateColumns: "repeat(auto-fill, minmax(330px, 1fr))",
              }}
            >
              {sortedOnDemandVideos?.map(
                ({ id, name, trainer, startDate, thumbnailUrl }) => (
                  <OnDemandItem
                    key={id}
                    id={id}
                    name={name}
                    trainer={trainer}
                    startDate={startDate}
                    thumbnailUrl={thumbnailUrl}
                  />
                )
              )}
            </Grid>
          ) : (
            "There are no on demand videos that match the current filters."
          )}
          <Flex sx={{ justifyContent: "center", mt: 8 }}>
            <Pagination
              page={page + 1}
              count={paginationCount}
              onChange={(event, value) => setPage(value - 1)}
            />
          </Flex>
        </Fragment>
      )}
    </Box>
  );
};

export default OnDemandVideos;
