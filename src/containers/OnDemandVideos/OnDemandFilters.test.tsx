/**
 *
 *
 * Tests for <OnDemandFilters />
 *
 *
 */

import React from "react";
import OnDemandFilters from "./OnDemandFilters";
import {
  renderWithMemoryRouter,
  renderWithRouter,
} from "test-utils/render-with-router";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { useSearchParams } from "react-router-dom";
import { mockUserAppSettingsGetResponseData } from "services/UserService";

jest.mock("services/UserService/useQueryUserAppSettingsGetService", () => ({
  useQueryUserAppSettingsGetData: () => mockUserAppSettingsGetResponseData,
}));

const byActivityText = /by activity/i;
const byCategoryText = /by category/i;
const entertainmentText = /entertainment/i;
const chillText = /chill/i;
const optionAnOptionText = /choose an option/i;

describe("<OnDemandFilters />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<OnDemandFilters removeFlyOut={() => {}} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a by activity filter without a selected option", async () => {
    renderWithRouter(<OnDemandFilters removeFlyOut={() => {}} />);
    await waitFor(() => {
      expect(screen.getByText(byActivityText)).toBeInTheDocument();
      expect(
        screen.getByText(byActivityText).nextElementSibling
      ).toHaveTextContent(optionAnOptionText);
    });
  });

  it("should pre select a 'By Activity' filter from url params", async () => {
    renderWithMemoryRouter(
      "/dashboard/on-demand",
      ["/dashboard/on-demand?activity=34"],
      <OnDemandFilters removeFlyOut={() => {}} />
    );
    await waitFor(() => {
      expect(screen.getByText(/entertainment/i)).toBeInTheDocument();
    });
  });

  it("should set the activity param", async () => {
    const handleFlyOut = jest.fn();

    const El = () => {
      const [params] = useSearchParams();
      return (
        <div>
          <div>param: {params.get("activity")}</div>
          <OnDemandFilters removeFlyOut={handleFlyOut} />
        </div>
      );
    };

    renderWithMemoryRouter(
      "/dashboard/on-demand",
      ["/dashboard/on-demand"],
      <El />
    );

    fireEvent.click(screen.getByText(byActivityText));
    fireEvent.click(screen.getByText(entertainmentText));

    // wait for the selection to be updated.
    await waitFor(() => {
      expect(
        screen.getByText(byActivityText).nextElementSibling
      ).toHaveTextContent(entertainmentText);
    });

    fireEvent.click(screen.getByText(/apply/i));
    await waitFor(() => {
      expect(handleFlyOut).toBeCalled();
      expect(screen.getByText(/param: 34/i)).toBeInTheDocument();
    });
  });

  it("should set the category param", async () => {
    const handleFlyOut = jest.fn();

    const El = () => {
      const [params] = useSearchParams();
      return (
        <div>
          <div>param: {params.get("category")}</div>
          <OnDemandFilters removeFlyOut={handleFlyOut} />
        </div>
      );
    };

    renderWithMemoryRouter(
      "/dashboard/on-demand",
      ["/dashboard/on-demand"],
      <El />
    );

    fireEvent.click(screen.getByText(byCategoryText));
    fireEvent.click(screen.getByText(chillText));

    // wait for the selection to be updated.
    await waitFor(() => {
      expect(
        screen.getByText(byCategoryText).nextElementSibling
      ).toHaveTextContent(chillText);
    });

    fireEvent.click(screen.getByText(/apply/i));
    await waitFor(() => {
      expect(handleFlyOut).toBeCalled();
      expect(screen.getByText(/param: 4/i)).toBeInTheDocument();
    });
  });
});
