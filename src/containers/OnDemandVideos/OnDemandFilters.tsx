/**
 *
 *
 * <OnDemandFilters />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Button } from "theme-ui";
import { FC, useMemo, useState } from "react";
import { useQueryUserAppSettingsGetData } from "services/UserService/useQueryUserAppSettingsGetService";
import H4 from "components/H4";
import DropDownDrawer from "components/DropDownDrawer";
import { useNavigate, useSearchParams } from "react-router-dom";

interface OnDemandFiltersProps {
  removeFlyOut: () => void;
}

interface FiltersState {
  category: number | null;
  activity: number | null;
}

const OnDemandFilters: FC<OnDemandFiltersProps> = ({ removeFlyOut }) => {
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams({
    activity: "",
    category: "",
  });

  const [filters, setFilters] = useState<FiltersState>({
    category: Number(searchParams.get("category")) || null,
    activity: Number(searchParams.get("activity")) || null,
  });

  const setByCategory = (category: number) => {
    setFilters((state) => ({
      ...state,
      category,
    }));
  };

  const setByActivity = (activity: number) => {
    setFilters((state) => ({
      ...state,
      activity,
    }));
  };

  const userAppSettings = useQueryUserAppSettingsGetData();

  const filterByActivityOptions = useMemo(() => {
    const enterpriseActivities = userAppSettings?.enterpriseActivities || [];
    return enterpriseActivities
      .sort((a, b) => a.name.localeCompare(b.name))
      .map(({ name, id }) => ({
        title: name,
        value: id,
      }));
  }, [userAppSettings]);

  const filterByCategoryOptions = useMemo(() => {
    const categories = userAppSettings?.categories || [];
    return categories
      .sort((a, b) => a.name.localeCompare(b.name))
      .map(({ name, id }) => ({
        title: name,
        value: id,
      }));
  }, [userAppSettings]);

  const handleApply = () => {
    const params = Object.entries(filters).reduce<Record<string, string>>(
      (acc, [key, value]: [string, number]) => {
        if (!value) return acc;
        acc[key] = value.toString();
        return acc;
      },
      {}
    );
    setSearchParams(params);
    removeFlyOut();
  };

  const handleClear = () => {
    navigate("dashboard/on-demand");
    removeFlyOut();
  };

  return (
    <Box
      sx={{ width: "100%", overflow: "auto", mb: 9 }}
      data-testid="on-demand-filters"
    >
      <H4 sx={{ my: 3 }}>Filters</H4>
      <Box sx={{ mb: 3 }}>
        <DropDownDrawer
          title="By Category"
          options={filterByCategoryOptions}
          onChange={setByCategory}
          value={filters.category}
        />
      </Box>
      <Box>
        <DropDownDrawer
          title="By Activity"
          options={filterByActivityOptions}
          onChange={setByActivity}
          value={filters.activity}
        />
      </Box>

      <Flex
        sx={{ position: "absolute", bottom: 0, left: 0, p: 5, width: "100%" }}
      >
        <Button onClick={handleApply}>Apply Filters</Button>
        <Button sx={{ ml: "auto" }} variant="secondary" onClick={handleClear}>
          Clear
        </Button>
      </Flex>
    </Box>
  );
};

export default OnDemandFilters;
