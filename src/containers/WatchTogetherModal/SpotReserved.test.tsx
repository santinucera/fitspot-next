/**
 *
 *
 * Tests for <SpotReserved />
 *
 *
 */

import React from "react";
import { fireEvent, render } from "@testing-library/react";
import SpotReserved from "./SpotReserved";
import dayjs from "dayjs";

describe("<SpotReserved />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <SpotReserved
        title="title"
        handleAddToCalendar={() => () => {}}
        setInviteColleague={() => {}}
        id={1}
        startDate={dayjs()}
        endDate={dayjs()}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render title and formatted date", () => {
    const title = "Title";
    const startDate = dayjs();
    const endDate = dayjs().add(1, "hour");
    const { getByText } = render(
      <SpotReserved
        title={title}
        handleAddToCalendar={() => () => {}}
        setInviteColleague={() => {}}
        id={1}
        startDate={startDate}
        endDate={endDate}
      />
    );
    expect(getByText(title)).toBeInTheDocument();
    expect(
      getByText(
        `${startDate.format("ddd MMM MM, h:mmA")} - ${endDate.format("h:mmA")}`
      )
    ).toBeInTheDocument();
  });
  it("should click handleAddToCalendar", () => {
    const mockHandleAddToCalendar = jest.fn();
    const { getByTestId } = render(
      <SpotReserved
        title="title"
        handleAddToCalendar={mockHandleAddToCalendar}
        setInviteColleague={() => {}}
        id={1}
        startDate={dayjs()}
        endDate={dayjs()}
      />
    );
    fireEvent.click(getByTestId("add-to-calendar-button"));
    expect(mockHandleAddToCalendar).toBeCalled();
  });
  it("should click setInviteColleague", () => {
    const mockSetInviteColleague = jest.fn();
    const { getByTestId } = render(
      <SpotReserved
        title="title"
        handleAddToCalendar={() => () => {}}
        setInviteColleague={mockSetInviteColleague}
        id={1}
        startDate={dayjs()}
        endDate={dayjs()}
      />
    );
    fireEvent.click(getByTestId("invite-a-colleague-button"));
    expect(mockSetInviteColleague).toBeCalled();
  });
});
