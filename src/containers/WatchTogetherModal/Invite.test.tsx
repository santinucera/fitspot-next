/**
 *
 *
 * Tests for <Invite />
 *
 *
 */

import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Invite from "./Invite";

describe("<Invite />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Invite
        publicId={null}
        companyName={"Your company"}
        type="session"
        isLive
        handleJoin={() => {}}
        id={1}
        handleAddToCalendar={() => () => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
  it("should show a links label if it's a session", () => {
    const companyName = "Company";
    const { getByText } = render(
      <Invite
        publicId={null}
        companyName={companyName}
        type="session"
        isLive
        handleJoin={() => {}}
        id={1}
        handleAddToCalendar={() => () => {}}
      />
    );
    expect(getByText("Watch with private group")).toBeInTheDocument();
    expect(getByText(`Watch with ${companyName}`)).toBeInTheDocument();
  });
  it("should not show a links label if it's a video", () => {
    const companyName = "Company";
    const { queryByText } = render(
      <Invite
        publicId={null}
        companyName={companyName}
        type="video"
        isLive
        handleJoin={() => {}}
        id={1}
        handleAddToCalendar={() => () => {}}
      />
    );
    expect(queryByText("Watch with private group")).toBeNull();
    expect(queryByText(`Watch with ${companyName}`)).toBeNull();
  });
  it("should join private group and company group if it's a live session", () => {
    const mockHandleJoin = jest.fn();
    const { getByTestId } = render(
      <Invite
        publicId={null}
        companyName="Company"
        type="session"
        isLive
        handleJoin={mockHandleJoin}
        id={1}
        handleAddToCalendar={() => () => {}}
      />
    );
    fireEvent.click(getByTestId("join-private-group-button"));
    fireEvent.click(getByTestId("join-company-group-button"));
    expect(mockHandleJoin).toBeCalledTimes(2);
  });
  it("should add to calendar private group and company group if it's not a live session", () => {
    const mockHandleAddToCalendar = jest.fn();
    const { getAllByTestId } = render(
      <Invite
        publicId={null}
        companyName="Company"
        type="session"
        isLive={false}
        handleJoin={() => {}}
        id={1}
        handleAddToCalendar={mockHandleAddToCalendar}
      />
    );
    getAllByTestId("add-to-calendar-button").forEach((button) => {
      fireEvent.click(button);
    });
    expect(mockHandleAddToCalendar).toBeCalledTimes(2);
  });
  it("should join private group if it's video", () => {
    const mockHandleJoin = jest.fn();
    const { getByTestId } = render(
      <Invite
        publicId={null}
        companyName="Company"
        type="video"
        isLive
        handleJoin={mockHandleJoin}
        id={1}
        handleAddToCalendar={() => () => {}}
      />
    );
    fireEvent.click(getByTestId("join-private-group-button"));
    expect(mockHandleJoin).toBeCalled();
  });
  it("should add to calendar private group if it's video", () => {
    const mockHandleAddToCalendar = jest.fn();
    const { getByTestId } = render(
      <Invite
        publicId={null}
        companyName="Company"
        type="video"
        isLive
        handleJoin={() => {}}
        id={1}
        handleAddToCalendar={mockHandleAddToCalendar}
      />
    );
    fireEvent.click(getByTestId("add-to-calendar-button"));
    expect(mockHandleAddToCalendar).toBeCalled();
  });
});
