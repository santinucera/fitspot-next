/**
 *
 *
 * <Invite />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button, Text, Flex } from "theme-ui";
import { Fragment, FunctionComponent } from "react";

import H2 from "components/H2";
import CopyToClipBoard from "components/CopyToClipBoard";
import IconArrow from "components/IconArrow";
import IconCalendar from "components/IconCalendar";
import useMedia from "use-media";

interface InviteProps {
  companyName: string;
  isLive: boolean;
  publicId: string | null;
  id: number;
  handleAddToCalendar: (path: string) => () => void;
  handleJoin: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  type: "video" | "session";
}

const Invite: FunctionComponent<InviteProps> = ({
  companyName,
  isLive,
  handleAddToCalendar,
  handleJoin,
  type,
  publicId,
  id,
}) => {
  const { origin } = window.location;
  const isSession = type === "session";
  const baseURL = `/dashboard/${isSession ? "sessions" : "on-demand"}`;
  const companyGroupSessionURL = `${baseURL}/${id}/company`;
  const privateGroupSessionURL = `${baseURL}/${id}/private/${publicId}`;
  const isWide = useMedia({ minWidth: "768px" });
  return (
    <Fragment>
      <H2 sx={{ fontSize: 6 }}>Watch together</H2>
      <Text sx={{ mt: 6, color: "medium-gray", textAlign: "center" }}>
        {isSession
          ? `Get the most out of Ten Spot with friends! Use the links below to watch
        this session with ${companyName} or invite your colleagues to watch this
         session with you in a private group.`
          : "Together is always better! Copy and share your unique group session link below to invite your collegues to watch this session together in a private group. "}
      </Text>
      <Box sx={{ width: "100%" }}>
        {isSession ? (
          [
            {
              label: "Watch with private group",
              url: privateGroupSessionURL,
              testID: "join-private-group-button",
            },
            {
              label: `Watch with ${companyName}`,
              url: companyGroupSessionURL,
              testID: "join-company-group-button",
            },
          ].map(({ label, url, testID }) => (
            <Flex
              key={url}
              sx={{
                alignItems: "flex-end",
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <CopyToClipBoard label={label} value={`${origin}${url}`} />
              {isLive ? (
                <Button
                  data-testid={testID}
                  data-url={url}
                  onClick={handleJoin}
                  sx={{
                    flex: "none",
                    ml: 2,
                  }}
                >
                  {isWide && "Join"}
                  <IconArrow direction="right" />
                </Button>
              ) : isWide ? (
                <Button
                  onClick={handleAddToCalendar(url)}
                  data-testid="add-to-calendar-button"
                  sx={{
                    flex: "none",
                    ml: 2,
                  }}
                >
                  Add to cal
                </Button>
              ) : (
                <Flex
                  variant="secondary"
                  onClick={handleAddToCalendar(url)}
                  data-testid="add-to-calendar-button"
                  sx={{
                    height: "52px",
                    justifyContent: "center",
                    alignItems: "center",
                    px: 3,
                    ml: 1,
                    bg: "gray",
                    cursor: "pointer",
                  }}
                >
                  <IconCalendar />
                </Flex>
              )}
            </Flex>
          ))
        ) : (
          <Box sx={{ mt: 4 }}>
            <CopyToClipBoard value={`${origin}${privateGroupSessionURL}`} />
            <Flex sx={{ mt: 4 }}>
              <Button
                onClick={handleJoin}
                data-testid="join-private-group-button"
                data-url={privateGroupSessionURL}
                sx={{ width: "100%", mr: 5 }}
              >
                Join now
              </Button>
              <Button
                onClick={handleAddToCalendar(privateGroupSessionURL)}
                data-testid="add-to-calendar-button"
                variant="secondary"
                sx={{ width: "100%" }}
              >
                Add to calendar
              </Button>
            </Flex>
          </Box>
        )}
      </Box>
    </Fragment>
  );
};

export default Invite;
