/**
 *
 *
 * <WatchTogetherModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Button, Text } from "theme-ui";
import { FunctionComponent, useState } from "react";
import useExternalCalendar from "hooks/useExternalCalendar";
import { Dayjs } from "dayjs";
import { useQueryUserGetData } from "services/UserService";
import { CalendarPreference } from "services/CalendarService";
import Invite from "./Invite";
import ExternalCalendarPicker from "containers/ExternalCalendarPicker";
import H2 from "components/H2";
import { useNavigate } from "react-router-dom";
import SpotReserved from "./SpotReserved";

interface WatchTogetherModalProps {
  handleClose: () => void;
  title: string | null;
  id: number;
  description: string | null;
  startDate: Dayjs;
  endDate: Dayjs;
  showInvite?: boolean;
  isLive?: boolean;
  type: "video" | "session";
}

const WatchTogetherModal: FunctionComponent<WatchTogetherModalProps> = ({
  handleClose,
  id,
  title,
  description,
  startDate,
  endDate,
  showInvite = false,
  isLive = false,
  type,
}) => {
  const navigate = useNavigate();
  const { calendarPreference, publicId, companyList } = useQueryUserGetData();
  const companyName = companyList[0].name;

  const defaultURL = `/dashboard/${
    type === "session" ? "sessions" : "on-demand"
  }/${id}`;

  const [inviteColleague, setInviteColleague] = useState(showInvite);
  const [showCalendarSettings, setCalendarSettings] = useState(false);
  const [privateGroup, setPrivateGroup] = useState({
    showPrivateGroup: false,
    url: "",
  });

  const { showPrivateGroup } = privateGroup;

  const hasDefaultCalendar =
    calendarPreference !== CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED;

  const { createExternalCalendarEvent } = useExternalCalendar();

  const handleAddToCalendar = (path: string) => () => {
    if (hasDefaultCalendar) {
      createExternalCalendarEvent({
        id,
        title,
        description,
        startDate,
        endDate,
        calendarPreference,
        path,
      });
      handleClose();
    } else {
      setCalendarSettings(true);
    }
  };

  const handleJoin = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (event.currentTarget.dataset.url)
      setPrivateGroup({
        showPrivateGroup: true,
        url: event.currentTarget.dataset.url,
      });
  };

  if (showPrivateGroup)
    return (
      <Box sx={{ p: 5, width: "100%" }}>
        <Flex
          sx={{
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            py: 6,
          }}
        >
          <H2
            sx={{
              fontSize: 6,
              textAlign: "center",
            }}
          >
            You are entering a private group
          </H2>
          <Text sx={{ my: 6, color: "medium-gray", textAlign: "center" }}>
            By clicking the join button below, you will enter a private group.
            Only the people in your group will be able to see, hear, and chat
            with you.
          </Text>
          <Flex
            sx={{
              flexDirection: "column",
            }}
          >
            <Button
              data-testid="join-group-button"
              onClick={() => {
                navigate(privateGroup.url);
                handleClose();
              }}
              sx={{
                mb: 3,
                width: "255px",
              }}
            >
              Join group
            </Button>
            <Button
              onClick={() => {
                navigate(defaultURL);
                handleClose();
              }}
              sx={{
                width: "255px",
              }}
              variant="secondary"
            >
              Watch on my own
            </Button>
          </Flex>
        </Flex>
      </Box>
    );

  if (showCalendarSettings)
    return (
      <ExternalCalendarPicker
        eventOptions={{
          id,
          title,
          description,
          startDate,
          endDate,
          path: defaultURL,
        }}
        handleClose={handleClose}
      />
    );

  return (
    <Box sx={{ p: 5, width: "100%" }}>
      <Flex
        sx={{
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          py: 6,
        }}
      >
        {inviteColleague ? (
          <Invite
            type={type}
            id={id}
            publicId={publicId}
            companyName={companyName || "Your company"}
            isLive={isLive}
            handleAddToCalendar={handleAddToCalendar}
            handleJoin={handleJoin}
          />
        ) : (
          <SpotReserved
            id={id}
            handleAddToCalendar={handleAddToCalendar}
            title={title}
            setInviteColleague={setInviteColleague}
            startDate={startDate}
            endDate={endDate}
          />
        )}
      </Flex>
    </Box>
  );
};

export default WatchTogetherModal;
