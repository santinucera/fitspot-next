/**
 *
 *
 * Tests for <WatchTogetherModal />
 *
 *
 */

import React from "react";
import { renderWithRouter } from "test-utils/render-with-router";
import WatchTogetherModal from "./WatchTogetherModal";
import dayjs from "utils/days";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import { User } from "services/types";

describe("<WatchTogetherModal />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const date = dayjs();
    renderWithRouter(
      <WatchTogetherModal
        type="session"
        handleClose={() => {}}
        title="some title"
        id={1}
        description="some description"
        startDate={date}
        endDate={date}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
