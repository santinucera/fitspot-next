/**
 *
 *
 * <SpotReserved />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Button, Text } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { Dayjs } from "dayjs";
import IconOutlineCircleCheck from "components/IconOutlineCircleCheck";

interface SpotReservedProps {
  title: string | null;
  id: number;
  handleAddToCalendar: (url: string) => () => void;
  setInviteColleague: (inviteColleague: boolean) => void;
  startDate: Dayjs;
  endDate: Dayjs;
}

const SpotReserved: FunctionComponent<SpotReservedProps> = ({
  title,
  setInviteColleague,
  id,
  handleAddToCalendar,
  startDate,
  endDate,
}) => {
  return (
    <Fragment>
      <IconOutlineCircleCheck size={58} sx={{ color: "purple", mb: 3 }} />
      <Heading
        as="h2"
        sx={{
          fontSize: 1,
          textAlign: "center",
        }}
      >
        Spot Reserved
      </Heading>
      <Box
        sx={{
          color: "medium-gray",
          mt: 6,
          mb: 7,
          lineHeight: "24px",
          textAlign: "center",
        }}
      >
        <Text>{title}</Text>
        <Text>
          {startDate.format("ddd MMM MM, h:mmA")} - {endDate.format("h:mmA")}
        </Text>
      </Box>
      <Button
        sx={{
          mb: 3,
          width: "255px",
        }}
        onClick={() => setInviteColleague(true)}
        data-testid="invite-a-colleague-button"
      >
        Watch together
      </Button>
      <Button
        sx={{
          width: "255px",
        }}
        onClick={handleAddToCalendar(`/dashboard/sessions/${id}`)}
        variant="secondary"
        data-testid="add-to-calendar-button"
      >
        Add to calendar
      </Button>
    </Fragment>
  );
};

export default SpotReserved;
