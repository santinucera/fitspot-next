/*
 *
 *
 * TrainerBroadcast
 *
 *
 */

/** @jsx jsx */
import { Box, Flex, jsx } from "theme-ui";
import { FunctionComponent } from "react";
import { useQueryTrainersSessionGetService } from "services/SessionService";
import { useParams } from "react-router-dom";
import Spinner from "components/Spinner";
import Chat from "containers/Chat/Chat";

const TrainerBroadcast: FunctionComponent = () => {
  const { sessionId } = useParams();
  const {
    data: session,
    isError,
    isFetchedAfterMount,
  } = useQueryTrainersSessionGetService({
    urlParams: { sessionId: parseInt(sessionId, 10) },
  });

  if (!isFetchedAfterMount) return <Spinner />;

  if (isError || !session) {
    return <div>Error! Failed to retrieve the session.</div>;
  }

  const { streamUrlBroadcast } = session;

  return (
    <Flex
      sx={{
        flexDirection: ["column", "row", "row"],
      }}
    >
      {streamUrlBroadcast && (
        <Box
          sx={{
            width: ["100%", "calc(100% - 320px)"],
            mr: ["0", 8, 8],
            maxWidth: 1280,
            minWidth: 280,
          }}
        >
          <iframe
            title="TenSpot Virtual Session"
            allowFullScreen
            allow="camera; microphone"
            src={streamUrlBroadcast}
            sx={{ width: "100%", height: 900 }}
            sandbox="allow-scripts allow-same-origin"
            data-testid="broadcast-iframe"
          />
        </Box>
      )}
      <Box
        sx={{
          width: ["60%", 320],
          minHeight: 400,
          maxHeight: 900,
          minWidth: 280,
        }}
      >
        <Chat />
      </Box>
    </Flex>
  );
};

export default TrainerBroadcast;
