/**
 *
 *
 * Tests for <TrainerBroadcast />
 *
 *
 */

import React from "react";
import TrainerBroadcast from "./TrainerBroadcast";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import {
  render,
  waitFor,
  waitForElementToBeRemoved,
  screen,
} from "@testing-library/react";
import {
  setTrainerSessionResponse,
  createTrainerSessionDetails,
} from "test-utils/session-service-test-utils";

const { getByTestId, getByText, getByRole, queryByTestId } = screen;

describe("<TrainerBroadcast />", () => {
  beforeEach(async () => {
    queryCache.setQueryData("user", createUser());
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerBroadcast />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render error if session fails to load", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());
    setTrainerSessionResponse(500);
    render(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <TrainerBroadcast />
      </ReactQueryConfigProvider>
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() =>
      expect(
        getByText("Error! Failed to retrieve the session.")
      ).toBeInTheDocument()
    );
    expect(spy).toHaveBeenCalled();
  });

  it("should render broadcast iFrame for on-platform sessions", async () => {
    const session = {
      ...createTrainerSessionDetails(),
      streamUrlBroadcast: "some url",
    };
    setTrainerSessionResponse(200, session);

    render(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <TrainerBroadcast />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("broadcast-iframe")).toBeInTheDocument();
    expect(getByRole("log")).toBeInTheDocument();
  });

  it("should not render broadcast iFrame for off-platform sessions", async () => {
    const session = {
      ...createTrainerSessionDetails(),
      streamUrlBroadcast: null,
    };
    setTrainerSessionResponse(200, session);

    render(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <TrainerBroadcast />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(queryByTestId("broadcast-iframe")).toBeNull();
    expect(getByRole("log")).toBeInTheDocument();
  });
});
