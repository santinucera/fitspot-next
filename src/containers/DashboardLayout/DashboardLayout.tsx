/**
 *
 *
 * <DashboardLayout />
 *
 *
 */
/** @jsx jsx */
import { jsx, Box, Flex, Text } from "theme-ui";
import { FunctionComponent } from "react";
import { alpha } from "@theme-ui/color";
import IconHome from "components/IconHome";
import IconSession from "components/IconSession";
import { Dropdown } from "components/Dropdown/Dropdown";
import IconArrow from "components/IconArrow";
import IconUserPlus from "components/IconUserPlus";
import IconBell from "components/IconBell";
import {
  useQueryUserGetData,
  isAdmin,
  hasAppsAccess,
} from "services/UserService";
import IconOnDemand from "components/IconOnDemand";
import IconRibbon from "components/IconRibbon";
import IconApps from "components/IconApps";
import ActivityFeed from "components/ActivityFeed";
import { useQueryActivityFeedGetService } from "services/ActivityFeedService";
import IconTenSpot from "components/IconTenSpot";
import Layout from "containers/Layout";

interface DashboardLayoutProps {}

const DashboardLayout: FunctionComponent<DashboardLayoutProps> = () => {
  const user = useQueryUserGetData();

  const { data: activityFeed = [] } = useQueryActivityFeedGetService({
    urlParams: { limit: 3 },
  });
  const company = user.companyList[0];
  return (
    <Layout
      links={[
        { label: "Home", end: true, icon: IconHome, to: "/dashboard/" },
        { label: "Sessions", icon: IconSession, to: "sessions" },
        { label: "On Demand", icon: IconOnDemand, to: "on-demand" },
        { label: "Challenges", icon: IconRibbon, to: "challenges" },
        {
          label: "Member Blog",
          icon: IconTenSpot,
          target: "_blank",
          as: "a",
          href: "https://tenspot.com/blog/category/members-content/",
        },
        {
          label: "Apps",
          icon: IconApps,
          to: "apps",
          condition: () => hasAppsAccess(user),
        },
        {
          label: "Admin",
          icon: IconArrow,
          as: "a",
          href: "/membersadmin/",
          condition: () => isAdmin(user),
        },
      ]}
      headerComponent={
        <Box sx={{ display: ["none", "none", "block"] }}>
          <Dropdown
            width={390}
            handle={
              <Flex
                sx={{
                  bg: "white",
                  height: 40,
                  width: 40,
                  borderRadius: 20,
                  alignItems: "center",
                  justifyContent: "center",
                  mr: 5,
                }}
              >
                <IconBell />
              </Flex>
            }
          >
            {activityFeed.length > 0 ? (
              <ActivityFeed feed={activityFeed} showDate={false} />
            ) : (
              <Flex
                sx={{
                  flexDirection: "column",
                  alignItems: "center",
                  p: 7,
                }}
              >
                <Text
                  sx={{
                    color: alpha("primary", 0.6),
                    fontWeight: "bold",
                    fontSize: 5,
                    mb: 1,
                  }}
                >
                  No Recent Activity
                </Text>
                <Text
                  sx={{
                    color: alpha("primary", 0.6),
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  Here you will see the recent activity for sessions and active
                  Challenges
                </Text>
              </Flex>
            )}
          </Dropdown>
        </Box>
      }
      bottomLinks={[
        {
          label: "Invite a Colleague",
          icon: IconUserPlus,
          as: "a",
          href: `mailto:?subject=Join%20Me%20on%20Ten%20Spot&body=Did%20you%20know%20that%20because%20we%20work%20at%20${company.name}%2C%20we%20can%20join%20free%20interactive%20workshops%20and%20fitness%20classes%20together%20virtually%20with%20Ten%20Spot%3F!%20Use%20your%20work%20email%20address%20to%20register.%20https%3A%2F%2Ftenspot.co%2Fuser%2Flogin%2F%3FpublicCode%3D${company.publicCode}&utm=inviteafriend`,
        },
      ]}
    />
  );
};

export default DashboardLayout;
