/**
 *
 *
 * Tests for <DashboardLayout />
 *
 *
 */
import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { queryCache } from "react-query";
import DashboardLayout from "./DashboardLayout";
import { createUser } from "test-utils/user-service-test-utils";
import { RolesWithinCompany } from "services/UserService";

describe("<DashboardLayout />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    queryCache.setQueryData("user", createUser());
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        reload: jest.fn(),
      },
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<DashboardLayout />, { wrapper: MemoryRouter });
    expect(spy).not.toHaveBeenCalled();
  });

  it("should show Admin link if user is admin", async () => {
    const user = createUser();
    const company = user.companyList[0];
    // queryCache gets cleared after each test.
    queryCache.setQueryData("user", {
      ...user,
      companyList: [
        { ...company, roleWithinCompany: RolesWithinCompany.admin },
      ],
    });
    render(<DashboardLayout />, {
      wrapper: MemoryRouter,
    });
    await waitFor(
      // Finding elements by the "navigation" aria landmark role is not yet supported
      // by React Testing Library. Looks like "header" is the only supported landmark
      // role as of now.
      () =>
        expect(screen.getByTestId("nav")).toContainElement(
          // Framer motion seems to create multiple copies of child elements
          screen.getAllByText(/admin/i)[0]
        )
    );
  });

  it("should show Apps link if user has access", async () => {
    const user = createUser();
    const company = user.companyList[0];
    // queryCache gets cleared after each test.
    queryCache.setQueryData("user", {
      ...user,
      companyList: [{ ...company, servicesTabVisible: true }],
    });
    render(<DashboardLayout />, {
      wrapper: MemoryRouter,
    });
    await waitFor(() =>
      expect(screen.getByTestId("nav")).toContainElement(
        screen.getAllByText(/apps/i)[0]
      )
    );
  });
});
