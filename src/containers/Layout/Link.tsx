/**
 *
 *
 * <Link />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import { NavLink as NavLinkInterface } from "./SideBar";
import NavLink from "components/NavLink";

const Link: FunctionComponent<NavLinkInterface> = ({
  condition,
  dark,
  label,
  ...linkProps
}) => {
  return !condition || condition() ? (
    <NavLink dark={dark} {...linkProps}>
      {label}
    </NavLink>
  ) : null;
};

export default Link;
