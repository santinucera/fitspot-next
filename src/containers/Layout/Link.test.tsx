/**
 *
 *
 * Tests for <Link />
 *
 *
 */

import React from "react";
import { renderWithRouter } from "test-utils/render-with-router";
import Link from "./Link";

describe("<Link />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<Link icon="div" label="Home" to="/dashboard" />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render link", () => {
    const HOME = "Home";
    const { getByText } = renderWithRouter(
      <Link icon="div" label={HOME} to="/dashboard" />
    );
    expect(getByText(HOME)).toBeInTheDocument();
  });
  it("should not render link", () => {
    const HOME = "Home";
    const { queryByText } = renderWithRouter(
      <Link icon="div" label={HOME} to="/dashboard" condition={() => false} />
    );
    expect(queryByText(HOME)).toBeNull();
  });
});
