/*
 *
 *
 * Layout
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, MenuButton } from "theme-ui";
import {
  FunctionComponent,
  useState,
  useEffect,
  useRef,
  ReactNode,
} from "react";
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import Avatar from "components/Avatar";
import { Dropdown, DropdownButton } from "components/Dropdown/Dropdown";
import Logo from "components/Logo";
import { useQueryUserGetData } from "services/UserService";
import { useMutationAuthPostLogoutService } from "services/AuthService";
import SideBar, { NavLink } from "./SideBar";

interface LayoutProps {
  links: NavLink[];
  bottomLinks?: NavLink[];
  headerComponent?: ReactNode;
  dark?: boolean;
}

const Layout: FunctionComponent<LayoutProps> = ({
  links,
  headerComponent,
  dark,
  bottomLinks,
}) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const user = useQueryUserGetData();
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  const [handleLogout] = useMutationAuthPostLogoutService({
    mutationConfig: {
      onSuccess() {
        window.localStorage.clear();
        window.location.reload();
      },
    },
  });

  const handleMenuToggle: (
    event: React.MouseEvent<HTMLButtonElement | HTMLDivElement>
  ) => void = () => {
    setMobileMenuOpen(!mobileMenuOpen);
  };
  useEffect(() => {
    if (mobileMenuOpen) setMobileMenuOpen(!mobileMenuOpen);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const layoutRef = useRef<HTMLDivElement>(null);

  /**
   * Scrolls to top when the route changes
   */
  useEffect(() => {
    if (!layoutRef.current) return;
    layoutRef.current.scrollTo(0, 0);
  }, [pathname]);

  return (
    <Flex>
      <SideBar
        mobileMenuOpen={mobileMenuOpen}
        setMobileMenuOpen={setMobileMenuOpen}
        links={links}
        bottomLinks={bottomLinks}
        dark={dark}
      />
      <Box
        ref={layoutRef}
        id="layout"
        sx={{
          bg: "light-gray",
          width: "100%",
          minHeight: "100vh",
        }}
      >
        <Flex sx={{ position: "relative" }}>
          <Flex
            sx={{
              alignItems: "center",
              bg: ["white", "white", "transparent"],
              position: ["block", "block", "absolute"],
              right: 0,
              top: 0,
              width: ["100%", "100%", "auto"],
              px: [5, 6],
              py: [3, 6],
              justifyContent: ["space-between", "space-between", "normal"],
            }}
          >
            <MenuButton
              sx={{ display: ["block", "block", "none"], p: 0 }}
              onClick={handleMenuToggle}
            />
            <Box
              sx={{
                alignItems: "center",
                display: ["flex", "flex", "none"],
              }}
            >
              <Logo isMobile />
            </Box>
            <Flex
              data-testid="user-header-actions"
              sx={{ ml: ["none", "none", "auto"] }}
            >
              {headerComponent}
              <Dropdown
                handle={
                  <Box data-testid="user-avatar">
                    <Avatar size={40} url={user?.avatar?.url} />
                  </Box>
                }
              >
                <DropdownButton
                  onClick={() => navigate("/dashboard/settings/profile")}
                >
                  Settings
                </DropdownButton>
                <DropdownButton onClick={handleLogout}>Logout</DropdownButton>
              </Dropdown>
            </Flex>
          </Flex>
        </Flex>
        <Box
          sx={{
            p: [5, 7],
          }}
        >
          <Outlet />
        </Box>
      </Box>
    </Flex>
  );
};

export default Layout;
