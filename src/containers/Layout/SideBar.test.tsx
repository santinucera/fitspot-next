/**
 *
 *
 * Tests for <SideBar />
 *
 *
 */

import { waitFor } from "@testing-library/react";
import React from "react";
import { renderWithRouter } from "test-utils/render-with-router";
import SideBar from "./SideBar";

describe("<SideBar />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <SideBar
        mobileMenuOpen={false}
        setMobileMenuOpen={() => {}}
        links={[
          { label: "Home", icon: "div", to: "/dashboard/" },
          { label: "Sessions", icon: "div", to: "sessions" },
        ]}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render labels and icons", async () => {
    const HOME = "Home";
    const { getAllByText, getAllByTestId } = renderWithRouter(
      <SideBar
        mobileMenuOpen={false}
        setMobileMenuOpen={() => {}}
        links={[
          {
            label: HOME,
            icon: () => <div data-testid="icon" />,
            to: "/dashboard/",
          },
        ]}
      />
    );
    await waitFor(() => {
      expect(getAllByText(HOME)).toHaveLength(2);
      expect(getAllByTestId("icon")).toHaveLength(2);
    });
  });
  it("should not render link with condition", async () => {
    const HOME = "Home";
    const SESSIONS = "Sessions";
    const { getAllByText, queryByText } = renderWithRouter(
      <SideBar
        mobileMenuOpen={false}
        setMobileMenuOpen={() => {}}
        links={[
          {
            label: HOME,
            icon: "div",
            to: "/dashboard/",
            condition: () => true,
          },
          {
            label: SESSIONS,
            icon: "div",
            to: "sessions",
            condition: () => false,
          },
        ]}
      />
    );
    await waitFor(() => {
      expect(getAllByText(HOME)).toHaveLength(2);
      expect(queryByText(SESSIONS)).toBeNull();
    });
  });
  it("should render labels and icons from bottomLinks", async () => {
    const SESSIONS = "Sessions";
    const { getAllByText, getAllByTestId } = renderWithRouter(
      <SideBar
        mobileMenuOpen={false}
        setMobileMenuOpen={() => {}}
        links={[
          {
            label: "Home",
            icon: "div",
            to: "/dashboard/",
          },
        ]}
        bottomLinks={[
          {
            label: SESSIONS,
            icon: () => <div data-testid="icon" />,
            to: "sessions",
          },
        ]}
      />
    );
    await waitFor(() => {
      expect(getAllByText(SESSIONS)).toHaveLength(2);
      expect(getAllByTestId("icon")).toHaveLength(2);
    });
  });
});
