/**
 *
 *
 * <SideBar />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Divider } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import Nav from "components/Nav";
import NavLink from "components/NavLink";
import Logo from "components/Logo";
import { NavLinkProps } from "components/NavLink/NavLink";
import IconX from "components/IconX";
import Link from "./Link";

export declare type NavLink = NavLinkProps & {
  label: string;
  condition?: () => boolean;
};

interface SideBarProps {
  links: NavLink[];
  bottomLinks?: NavLink[];
  mobileMenuOpen: boolean;
  setMobileMenuOpen: (mobileMenuOpen: boolean) => void;
  dark?: boolean;
}

const SideBar: FunctionComponent<SideBarProps> = ({
  links,
  mobileMenuOpen,
  setMobileMenuOpen,
  dark,
  bottomLinks,
}) => {
  return (
    <Nav isOpen={mobileMenuOpen} dark={dark} sx={{ position: "relative" }}>
      <Box
        sx={{
          position: "absolute",
          top: 0,
          right: 0,
          p: 3,
          display: ["block", "block", "none"],
        }}
        onClick={() => setMobileMenuOpen(!mobileMenuOpen)}
      >
        <IconX />
      </Box>
      <Box
        sx={{
          textAlign: "center",
          mb: 7,
          display: ["none", "none", "block"],
        }}
      >
        <Logo sx={{ color: dark ? "white" : "primary" }} />
      </Box>
      {links.map((link) => (
        <Link key={link.label} {...link} dark={dark} />
      ))}
      {bottomLinks && (
        <Fragment>
          <Divider />
          {bottomLinks.map((link) => (
            <Link key={link.label} {...link} />
          ))}
        </Fragment>
      )}
    </Nav>
  );
};

export default SideBar;
