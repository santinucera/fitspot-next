/**
 *
 *
 * Tests for <Layout />
 *
 *
 */

import React from "react";
import {
  render,
  waitFor,
  screen,
  fireEvent,
  within,
} from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import Layout from "./Layout";

describe("<Layout />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    queryCache.setQueryData("user", createUser());
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        reload: jest.fn(),
      },
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Layout links={[]} />, {
      wrapper: MemoryRouter,
    });
    expect(spy).not.toHaveBeenCalled();
  });

  it("should log user out and reload the browser", async () => {
    render(<Layout links={[]} />, {
      wrapper: MemoryRouter,
    });
    await waitFor(() =>
      expect(screen.getByTestId("user-header-actions")).toBeInTheDocument()
    );
    fireEvent.click(screen.getByTestId("user-avatar"));
    fireEvent.click(
      within(screen.getByTestId("user-header-actions")).getByText(/logout/i)
    );
    await waitFor(() => expect(window.location.reload).toHaveBeenCalled());
  });
});
