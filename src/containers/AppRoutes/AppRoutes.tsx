/**
 *
 *
 * <AppRoutes />
 *
 *
 */
/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent, useEffect } from "react";
import {
  Routes,
  Route,
  Outlet,
  useNavigate,
  useLocation,
  useParams,
  Navigate,
} from "react-router-dom";
import Dashboard from "containers/Dashboard";
import DashboardLayout from "containers/DashboardLayout";
import Session from "containers/Session";
import Page from "components/Page";
import NavTabs from "components/NavTabs";
import SettingsProfile from "containers/SettingsProfile";
import SettingsAccount from "containers/SettingsAccount";
import SettingsNotifications from "containers/SettingsNotifications";
import SettingsCalendar from "containers/SettingsCalendar";
import SettingsWearables from "containers/SettingsWearables";
import OnDemandVideos from "containers/OnDemandVideos";
import Apps from "containers/Apps";
import Challenges from "containers/Challenges";
import Challenge from "containers/Challenge";
import OnDemandVideo from "containers/OnDemandVideo";
import ExpertProfile from "containers/ExpertProfile";
import StandaloneChat from "containers/StandaloneChat";
import LiveSessions from "containers/LiveSessions";
import SessionsReserved from "containers/SessionsReserved";
import TrainerSessions from "containers/TrainerSessions";
import TrainerSettingsProfile from "containers/TrainerSettingsProfile";
import TrainerLayout from "containers/TrainerLayout";
import TrainerSettingsAccount from "containers/TrainerSettingsAccount";
import ProtectedRoute from "components/ProtectedRoute";
import { UserType } from "services/UserService";
import TrainerBroadcast from "containers/TrainerBroadcast";

const SessionsRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page title="Sessions">
          <NavTabs
            items={[
              { to: "/dashboard/sessions", label: "Schedule" },
              { to: "/dashboard/sessions/reserved", label: "Reserved" },
            ]}
          />
          <Outlet />
        </Page>
      }
    >
      <Route path="/" element={<LiveSessions />} />
      <Route path="reserved" element={<SessionsReserved />} />
    </Route>
    <Route path=":sessionId" element={<Session chatType="chat" />} />
    <Route
      path=":sessionId/private/:userPublicId"
      element={<Session chatType="private-group" />}
    />
    <Route
      path=":sessionId/company"
      element={<Session chatType="company-group" />}
    />
  </Routes>
);

const SettingsRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page backTo="/dashboard">
          <NavTabs
            items={[
              {
                to: "/dashboard/settings/profile",
                label: "Profile",
              },
              {
                to: "/dashboard/settings/account",
                label: "Account Settings",
              },
              {
                to: "/dashboard/settings/notifications",
                label: "Notifications",
              },
              {
                to: "/dashboard/settings/calendar",
                label: "Calendar",
              },
              {
                to: "/dashboard/settings/wearables",
                label: "Wearables",
              },
            ]}
          />
          <Outlet />
        </Page>
      }
    >
      <Route path="profile" element={<SettingsProfile />} />
      <Route path="account" element={<SettingsAccount />} />
      <Route path="notifications" element={<SettingsNotifications />} />
      <Route path="calendar" element={<SettingsCalendar />} />
      <Route path="wearables" element={<SettingsWearables />} />
    </Route>
  </Routes>
);

const OnDemandRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page title="On Demand Videos">
          <Outlet />
        </Page>
      }
    >
      <Route path="/" element={<OnDemandVideos />} />
    </Route>
    <Route
      path=":videoId"
      element={
        <Page backTo="/dashboard/on-demand">
          <OnDemandVideo />
        </Page>
      }
    />
    <Route
      path=":videoId/private/:userPublicId"
      element={
        <Page backTo="/dashboard/on-demand">
          <OnDemandVideo privateGroup />
        </Page>
      }
    />
  </Routes>
);

const ExpertProfileRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page title="Expert Profile">
          <Outlet />
        </Page>
      }
    ></Route>
    <Route
      path=":trainerId"
      element={
        <Page backTo="/dashboard">
          <ExpertProfile />
        </Page>
      }
    />
  </Routes>
);

const ChallengesRoutes: FunctionComponent = () => {
  return (
    <Routes>
      <Route
        path="*"
        element={
          <Page title="Challenges">
            <Outlet />
          </Page>
        }
      >
        <Route path="/" element={<Challenges />} />
      </Route>

      <Route
        path=":challengeId"
        element={
          <Page backTo="/dashboard/challenges">
            <Challenge />
          </Page>
        }
      />
    </Routes>
  );
};

const AppsRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page title="Apps">
          <Outlet />
        </Page>
      }
    >
      <Route path="/" element={<Apps />} />
    </Route>
  </Routes>
);

const TrainerSessionsRoutes: FunctionComponent = () => {
  // Configure the Go Back button to return to the selected session.
  const TrainerBroadcastPage: FunctionComponent = () => {
    const { sessionId } = useParams();

    return (
      <Page backTo={`/dashboard/trainer/sessions?session_id=${sessionId}`}>
        <TrainerBroadcast />
      </Page>
    );
  };

  return (
    <Routes>
      <Route
        path="*"
        element={
          <Page title="Sessions">
            <Outlet />
          </Page>
        }
      >
        <Route path="/" element={<TrainerSessions />} />
      </Route>
      <Route path=":sessionId" element={<TrainerBroadcastPage />} />
    </Routes>
  );
};

const TrainerSettingsRoutes: FunctionComponent = () => (
  <Routes>
    <Route
      path="*"
      element={
        <Page title="Settings">
          <NavTabs
            items={[
              { to: "/dashboard/trainer/settings/profile", label: "Profile" },
              { to: "/dashboard/trainer/settings/account", label: "Account" },
            ]}
          />
          <Outlet />
        </Page>
      }
    >
      <Route path="profile" element={<TrainerSettingsProfile />} />
      <Route path="account" element={<TrainerSettingsAccount />} />
      <Navigate to="/dashboard/trainer/settings/profile" />
    </Route>
  </Routes>
);

const AppRoutes: FunctionComponent = () => {
  const location = useLocation();
  const navigate = useNavigate();

  // Trailing slash is required to make navigation active states work
  useEffect(() => {
    if (location.pathname === "/dashboard") {
      navigate("dashboard/");
    }
  }, [location, navigate]);

  return (
    <Routes>
      <ProtectedRoute
        path="dashboard/chat/:sessionId"
        element={<StandaloneChat />}
        guards={[UserType.customer, UserType.admin]}
      />
      <ProtectedRoute
        path="dashboard/trainer/*"
        guards={[UserType.trainer]}
        element={<TrainerLayout />}
      >
        <Route path="/sessions/*" element={<TrainerSessionsRoutes />} />
        <Route path="/settings/*" element={<TrainerSettingsRoutes />} />
        <Navigate to="/dashboard/trainer/sessions" />
      </ProtectedRoute>
      <ProtectedRoute
        path="dashboard/*"
        element={<DashboardLayout />}
        guards={[UserType.customer, UserType.admin]}
      >
        <Route path="/" element={<Dashboard />} />
        <Route path="sessions/*" element={<SessionsRoutes />} />
        <Route path="settings/*" element={<SettingsRoutes />} />
        <Route path="on-demand/*" element={<OnDemandRoutes />} />
        <Route path="challenges/*" element={<ChallengesRoutes />} />
        <Route path="apps/*" element={<AppsRoutes />} />
        <Route path="trainers/*" element={<ExpertProfileRoutes />} />
        <Route
          path="*"
          element={
            <Page title="Not Found">
              Oops! We can't find a page at this location. Please try one of the
              links to the left.
            </Page>
          }
        />
      </ProtectedRoute>
    </Routes>
  );
};

export default AppRoutes;
