/**
 *
 *
 * Tests for <AppRoutes />
 *
 *
 */
import React, { ReactNode } from "react";
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { MemoryRouter, useLocation } from "react-router-dom";
import { LocalizationProvider } from "@material-ui/pickers";
import DateFnsAdapter from "@material-ui/pickers/adapter/date-fns";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import AppRoutes from "./AppRoutes";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { User } from "services/types";
import {
  createTrainerSession,
  setTrainerSessionsResponse,
  createTrainerSessionDetails,
  setTrainerSessionResponse,
} from "test-utils/session-service-test-utils";
import { UserType } from "services/UserService";

jest.mock("services/CometChatService", () => ({
  __esModule: true,
  useCometChatService: jest.fn().mockImplementation(() => ({
    fetchPreviousMessages: jest.fn(),
    messages: [],
    connect: jest.fn(),
    handleDismount: jest.fn(),
    isConnected: true,
  })),
  default: ({ children }: { children: ReactNode }) => <div>{children}</div>,
}));

describe("<AppRoutes />", () => {
  let user: User;
  const originalWindow = global.window;
  beforeEach(async () => {
    jest.clearAllMocks();
    jest.clearAllTimers();
    user = createUser();
    queryCache.setQueryData("user", () => user);
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        href: "",
      },
      writable: true,
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<AppRoutes />, { wrapper: MemoryRouter });
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render Chat without page chrome", async () => {
    const {
      getByText,
      getByRole,
      queryByTestId,
      getByLabelText,
    } = renderWithMemoryRouter("/*", ["/dashboard/chat/1234"], <AppRoutes />);
    await waitFor(() => {
      expect(getByText("Live Chat")).toBeInTheDocument();
      expect(getByRole("log")).toBeInTheDocument();
      expect(queryByTestId("page-title")).toBeNull();
    });
    await waitFor(() => expect(getByLabelText("Enter comment")).toBeEnabled());
  });

  it("should render Sessions route", async () => {
    const { getByText, getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/sessions"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByText("Schedule")).toBeInTheDocument();
  });

  it("should render Reserved Sessions route", async () => {
    const { getByText, getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/sessions/reserved"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByText("Reserved")).toBeInTheDocument();
  });

  it("should render Session route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/sessions/1234"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("session-title")).toBeInTheDocument();
  });

  it("should render Profile route", async () => {
    const { getByText } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/settings/profile"],
      <LocalizationProvider dateAdapter={DateFnsAdapter}>
        <AppRoutes />
      </LocalizationProvider>
    );
    await waitFor(() => {
      expect(getByText("Edit Profile")).toBeInTheDocument();
    });
  });

  it("should render Account route", async () => {
    const { getByText } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/settings/account"],
      <AppRoutes />
    );
    await waitFor(() => {
      expect(getByText("Change Password")).toBeInTheDocument();
    });
  });

  it("should render Notifications route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/settings/notifications"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("notifications-container")).toBeInTheDocument();
  });

  it("should render Calendar route", async () => {
    const { getByText, getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/settings/calendar"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByText("Calendar Settings")).toBeInTheDocument();
  });

  it("should render Wearable route", async () => {
    const { getByText } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/settings/wearables"],
      <AppRoutes />
    );
    await waitFor(() => {
      expect(getByText("Manage Your Wearable Devices")).toBeInTheDocument();
    });
  });

  it("should render On Demand Videos route", async () => {
    const { getByText } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/on-demand"],
      <AppRoutes />
    );
    await waitFor(() => {
      expect(getByText("On Demand Videos")).toBeInTheDocument();
    });
  });

  it("should render On Demand Video route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/on-demand/10452"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("on-demand-container")).toBeInTheDocument();
  });

  it("should render Challenges route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/challenges"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("challenges")).toBeInTheDocument();
  });

  it("should render Challenge route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/challenges/1234"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("challengeHeader")).toBeInTheDocument();
  });

  it("should render App route", async () => {
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/apps"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("category")).toBeInTheDocument();
  });

  it("should forward non-trainer user to /auth/login", async () => {
    queryCache.setQueryData("user", () => createUser(UserType.customer));
    const El = () => {
      return (
        <div>
          <AppRoutes />
        </div>
      );
    };
    renderWithMemoryRouter("/*", ["/dashboard/trainer"], <El />);
    await waitFor(() => {
      expect(window.location.href).toBe("/user/auth?next=/");
    });
  });

  it("should render Trainers sessions route", async () => {
    queryCache.setQueryData("user", () => createUser(UserType.trainer));
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/trainer/sessions/"],
      <AppRoutes />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("trainer-sessions-lists")).toBeInTheDocument();
  });

  it("should forward to Trainers sessions route", async () => {
    queryCache.setQueryData("user", () => createUser(UserType.trainer));
    const El = () => {
      const location = useLocation();
      if (!user) return null;
      return (
        <div>
          <div data-testid="location">{location.pathname}</div>
          <AppRoutes />
        </div>
      );
    };
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      ["/dashboard/trainer"],
      <El />
    );
    await waitFor(() => getByTestId("spinner"));
    expect(getByTestId("location")).toHaveTextContent(
      "/dashboard/trainer/sessions"
    );
  });

  it("should configure the Go Back button the Trainers Broadcast page to return to the selected session", async () => {
    queryCache.setQueryData("user", () => createUser(UserType.trainer));
    const sessionId = 1;
    const name = "Some Session Name";
    setTrainerSessionsResponse(200, [
      { ...createTrainerSession(), id: sessionId, name },
    ]);
    setTrainerSessionResponse(200, {
      ...createTrainerSessionDetails(),
      id: sessionId,
      name,
    });
    const El = () => {
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.search}</div>
          <AppRoutes />
        </div>
      );
    };
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      [`/dashboard/trainer/sessions/${sessionId}`],
      <El />
    );
    fireEvent.click(getByTestId("go-back-button"));
    expect(getByTestId("location")).toHaveTextContent(
      `?session_id=${sessionId}`
    );
  });
});
