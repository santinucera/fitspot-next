/**
 *
 *
 * <App />
 *
 *
 */
/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent, useEffect, Fragment } from "react";
import smoothscroll from 'smoothscroll-polyfill';
import AppRoutes from "containers/AppRoutes";
import { useLocation } from "react-router-dom";
import { Helmet } from "react-helmet";
import { useQueryUserGetService } from "services/UserService";
import { useQueryUserAppSettingsGetService } from "services/UserService/useQueryUserAppSettingsGetService";
import isProduction from "utils/is-production";
import useAnalytics from "hooks/useAnalytics";

interface AppProps {}

const App: FunctionComponent<AppProps> = () => {
  const location = useLocation();
  const loginUrl = `/user/auth?next=${location.pathname}${location.search}`;
  const { reportPageView } = useAnalytics();
  smoothscroll.polyfill();

  const { isSuccess: isUserServiceSuccess } = useQueryUserGetService({
    queryConfig: {
      retry: false,
      onError: () => {
        window.location.href = loginUrl;
      },
    },
  });

  const { isSuccess: isAppSettingsSuccess } = useQueryUserAppSettingsGetService(
    {
      queryConfig: {
        retry: false,
        onError: () => {
          window.location.href = loginUrl;
        },
      },
    }
  );

  /**
   * Track page views
   */
  useEffect(() => {
    reportPageView(location.pathname);
  }, [location, reportPageView]);

  return isUserServiceSuccess && isAppSettingsSuccess ? (
    <Fragment>
      {isProduction() && (
        <Helmet>
          <script>
            {/* Hotjar Tracking Code */}
            {`
                (function(h,o,t,j,a,r){
                    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                    h._hjSettings={hjid:1906780,hjsv:6};
                    a=o.getElementsByTagName('head')[0];
                    r=o.createElement('script');r.async=1;
                    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                    a.appendChild(r);
                })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            `}
          </script>
          <script>
            {/* Google Tag Manager */}
            {`
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-WR3SLXJ');
            `}
          </script>
        </Helmet>
      )}
      <AppRoutes />
    </Fragment>
  ) : null;
};

export default App;
