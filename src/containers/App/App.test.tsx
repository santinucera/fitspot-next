/**
 *
 *
 * Tests for <App />
 *
 *
 */
import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { MemoryRouter, useLocation, useNavigate } from "react-router-dom";
import { rest } from "msw";
import server from "services/_mocks/server";
import {
  useQueryUserGetService,
  useQueryUserAppSettingsGetService,
  userGetUrl,
  userAppSettingsGetUrl,
} from "services/UserService";
import App from "./App";

const mockReportPageView = jest.fn();
const mockReportHumanApiDeviceConnected = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportPageView: mockReportPageView,
    reportHumanApiDeviceConnected: mockReportHumanApiDeviceConnected,
  }))
);

describe("<App />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        href: "",
      },
      writable: true,
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<App />, { wrapper: MemoryRouter });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should forward to "/user/auth" if user is not logged in', async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    server.use(
      rest.get(userGetUrl, (req, res, ctx) => {
        return res(ctx.status(403));
      })
    );
    const El = () => {
      const { isError: isUserServiceError } = useQueryUserGetService();
      const {
        isSuccess: isAppSettingsSuccess,
      } = useQueryUserAppSettingsGetService();
      return (
        <div>
          <div data-testid="location">{window.location.href}</div>
          <div data-testid="user-service-error">
            {isUserServiceError && isUserServiceError.toString()}
          </div>
          <div data-testid="settings-service-success">
            {isAppSettingsSuccess.toString()}
          </div>
          <App />
        </div>
      );
    };
    const { getByTestId } = render(<El />, { wrapper: MemoryRouter });
    await waitFor(() => {
      expect(getByTestId("user-service-error")).toHaveTextContent("true");
      expect(getByTestId("settings-service-success")).toHaveTextContent("true");
    });
    expect(getByTestId("location")).toHaveTextContent("/user/auth?next=/");
  });

  it('should forward to "/user/auth" if app settings request fails', async () => {
    // Swallow error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    server.use(
      rest.get(userAppSettingsGetUrl, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );
    const El = () => {
      const { isSuccess: isUserServiceSuccess } = useQueryUserGetService();
      const {
        isError: isAppSettingsError,
      } = useQueryUserAppSettingsGetService();
      return (
        <div>
          <div data-testid="location">{global.window.location.href}</div>
          <div data-testid="user-service-success">
            {isUserServiceSuccess.toString()}
          </div>
          <div data-testid="settings-service-error">
            {isAppSettingsError.toString()}
          </div>
          <App />
        </div>
      );
    };
    const { getByTestId } = render(<El />, { wrapper: MemoryRouter });
    await waitFor(() => {
      expect(getByTestId("settings-service-error")).toHaveTextContent("true");
      expect(getByTestId("user-service-success")).toHaveTextContent("true");
    });
    expect(getByTestId("location")).toHaveTextContent("/user/auth?next=/");
  });

  it("should render the dashboard", async () => {
    const El = () => {
      const navigate = useNavigate();
      return (
        <div>
          <button
            type="button"
            onClick={() => navigate("/dashboard")}
            data-testid="navigate"
          >
            go
          </button>
          <App />
        </div>
      );
    };
    const { getByTestId } = render(<El />, {
      wrapper: MemoryRouter,
    });
    fireEvent.click(getByTestId("navigate"));
    await waitFor(() =>
      expect(getByTestId("page-title")).toHaveTextContent("Home")
    );
  });

  it("should track page views", async () => {
    const El = () => {
      const navigate = useNavigate();
      const location = useLocation();
      return (
        <div>
          <button
            type="button"
            onClick={() => navigate("/dashboard")}
            data-testid="navigate"
          >
            go
          </button>
          <div test-id="location">{location.pathname}</div>
          <App />
        </div>
      );
    };
    const { getByTestId } = render(<El />, {
      wrapper: MemoryRouter,
    });
    fireEvent.click(getByTestId("navigate"));
    await waitFor(() => expect(getByTestId("page-title")));

    expect(mockReportPageView).toHaveBeenCalledWith("/");
    expect(mockReportPageView).toHaveBeenCalledWith("/dashboard");
  });
});
