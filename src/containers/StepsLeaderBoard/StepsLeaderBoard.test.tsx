/**
 *
 *
 * Tests for <StepsLeaderBoard />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import StepsLeaderBoard from "./StepsLeaderBoard";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

describe("<StepsLeaderBoard />", () => {
  beforeEach(() => {
    queryCache.setQueryData("user", createUser());
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<StepsLeaderBoard />);
    expect(spy).not.toHaveBeenCalled();
  });
});
