/*
 *
 *
 * StepsLeaderBoard
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box, Text, Divider, Button } from "theme-ui";
import { FC, useState } from "react";
import { useQueryWearablesGetStepLeadersService } from "services/WearablesService";
import Avatar from "components/Avatar";
import thousands from "utils/thousands";
import H3 from "components/H3";
import { useQueryUserGetData } from "services/UserService";
import dayjs from "utils/days";
import { Dropdown, DropdownButton } from "components/Dropdown";
import IconChevron from "components/IconChevron";
import { useHumanApiModal } from "hooks";

interface StepFilterOptions {
  title: string;
  key: string;
}

interface DateParams {
  dateStart: string;
  dateEnd: string;
}

interface StepsByOptionsDates {
  thisWeek: DateParams;
  lastWeek: DateParams;
  thisMonth: DateParams;
  lastMonth: DateParams;
}

const stepsByOptions: StepFilterOptions[] = [
  {
    title: "This Week",
    key: "thisWeek",
  },
  {
    title: "Last Week",
    key: "lastWeek",
  },
  {
    title: "This Month",
    key: "thisMonth",
  },
  {
    title: "Last Month",
    key: "lastMonth",
  },
];

const stepsByOptionsDates: StepsByOptionsDates = {
  thisWeek: {
    dateStart: dayjs().startOf("week").subtract(1, "day").format("YYYY-MM-DD"), // Saturday
    dateEnd: dayjs().endOf("week").subtract(1, "day").format("YYYY-MM-DD"), // Friday
  },
  lastWeek: {
    dateStart: dayjs()
      .subtract(1, "week")
      .startOf("week")
      .subtract(1, "day")
      .format("YYYY-MM-DD"),
    dateEnd: dayjs()
      .subtract(1, "week")
      .endOf("week")
      .subtract(1, "day")
      .format("YYYY-MM-DD"),
  },
  thisMonth: {
    dateStart: dayjs().startOf("month").format("YYYY-MM-DD"),
    dateEnd: dayjs().format("YYYY-MM-DD"),
  },
  lastMonth: {
    dateStart: dayjs()
      .subtract(1, "month")
      .startOf("month")
      .format("YYYY-MM-DD"),
    dateEnd: dayjs().subtract(1, "month").endOf("month").format("YYYY-MM-DD"),
  },
};

interface StepsLeaderBoardProps {
  limit?: number;
}

const StepsLeaderBoard: FC<StepsLeaderBoardProps> = ({ limit = 4 }) => {
  const user = useQueryUserGetData();

  const { token } = useHumanApiModal();

  const [state, setState] = useState<StepFilterOptions>(stepsByOptions[0]);
  const { data } = useQueryWearablesGetStepLeadersService({
    queryConfig: {
      keepPreviousData: true,
      staleTime: Infinity,
    },
    urlParams: {
      companyId: user?.companyList?.[0].id || 0,
      type: 0,
      limit,
      ...stepsByOptionsDates[state.key as keyof StepsByOptionsDates],
    },
  });

  return (
    <Flex
      data-testid="steps-leaderboard"
      sx={{
        flexDirection: "column",
        bg: "white",
        py: 5,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "gray",
        height: 400,
        width: 330,
      }}
    >
      <Flex
        sx={{
          flexDirection: "column",
          px: 5,
        }}
      >
        <H3
          sx={{
            fontWeight: "300",
            fontSize: 4,
          }}
        >
          Steps Leaderboard
        </H3>
        <Flex sx={{ justifyContent: "space-between" }}>
          <Flex>
            <Dropdown
              handle={
                <Flex sx={{ alignItems: "center", fontSize: 1 }}>
                  <Box>{state.title}</Box>
                  <IconChevron sx={{ transform: "rotate(90deg)", ml: 1 }} />
                </Flex>
              }
              align="left"
            >
              {stepsByOptions.map(({ title, key }) => (
                <DropdownButton
                  key={key}
                  onClick={() => setState({ title, key })}
                >
                  {title}
                </DropdownButton>
              ))}
            </Dropdown>
          </Flex>
          <Button data-hapi-token={token} variant="underlined" sx={{ p: 0 }}>
            Manage device
          </Button>
        </Flex>

        <Divider />
        <Flex
          sx={{
            mt: 2,
            color: "medium-gray",
            fontSize: 0,
            justifyContent: "space-between",
          }}
        >
          <Text>Ranking</Text>
          <Text>Steps</Text>
        </Flex>
      </Flex>
      <Box
        sx={{
          pt: 1,
        }}
      >
        {data?.map(({ rank, id, name, avatarUrl, total }) => (
          <Flex
            sx={{
              py: 2,
              px: 5,
              mb: "11px",
              alignItems: "center",
              bg: id === user?.id ? "#f8f8f8" : "white",
            }}
            key={id}
          >
            <Text sx={{ width: 20, mr: 3 }}>{rank}.</Text>
            <Avatar size={38} url={avatarUrl} sx={{ mr: 3 }} />
            <Text>{name}</Text>
            <Text sx={{ ml: "auto", fontWeight: "bold" }}>
              {thousands(total)}
            </Text>
          </Flex>
        ))}
      </Box>
    </Flex>
  );
};

export default StepsLeaderBoard;
