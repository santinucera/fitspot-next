/**
 *
 *
 * <NonRatedSessionsNotifications />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { queryCache } from "react-query";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Alert from "components/Alert";
import {
  SessionsGetQueryParams,
  SessionsModel,
  useMutationDismissSessionRatingPostService,
} from "services/SessionService";
import { useRateSession } from "hooks";
import useToast from "hooks/useToast";

interface NonRatedSessionsAlertsProps {
  attendedSessions?: SessionsModel;
  sessionsQueryParams?: SessionsGetQueryParams;
}

const NonRatedSessionsAlerts: FunctionComponent<NonRatedSessionsAlertsProps> = ({
  attendedSessions,
  sessionsQueryParams,
}) => {
  const { nonRatedSessions, rateSession } = useRateSession({
    attendedSessions,
    sessionsQueryParams,
  });
  const { addToast } = useToast();
  const [dismissSession] = useMutationDismissSessionRatingPostService({
    mutationConfig: {
      onMutate: ({ sessionId }) => {
        // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
        queryCache.cancelQueries(["non-rated-sessions"]);

        // Snapshot the previous value
        const previousSessions = queryCache.getQueryData([
          "non-rated-sessions",
        ]);

        // Optimistically update to the new value
        queryCache.setQueryData(
          ["non-rated-sessions"],
          nonRatedSessions?.filter((session) => session.id !== sessionId)
        );

        // Return a rollback function
        return () =>
          queryCache.setQueryData(["non-rated-sessions"], previousSessions);
      },
      onError: (_, __, rollback) => {
        rollback();
        addToast({
          title: "Error!",
          description: "Failed to dismiss this session, please try again.",
          type: "error",
        });
      },
    },
  });

  const handleRateSession = (sessionId: number, overall: number) =>
    rateSession({ sessionId, rating: { overall } });

  const handleDismissRating = (sessionId: number) => () =>
    dismissSession({ sessionId });

  return (
    <Fragment>
      {nonRatedSessions?.map(({ name, id }) => (
        <Alert
          title={name}
          key={id}
          icon={
            <StarBorderIcon
              sx={{
                color: "yellow",
                fontSize: 4,
              }}
            />
          }
          onClose={handleDismissRating(id)}
          backgroundColor="light-yellow"
          borderColor="yellow"
          secondaryTitle="Rate Session"
        >
          <Rating
            name={name}
            size="large"
            onChange={(_, newValue) => {
              handleRateSession(id, newValue!);
            }}
            sx={{
              "& .MuiRating-label": {
                color: "yellow",
              },
            }}
            emptyIcon={
              <StarBorderIcon sx={{ color: "primary" }} fontSize="inherit" />
            }
          />
        </Alert>
      ))}
    </Fragment>
  );
};

export default NonRatedSessionsAlerts;
