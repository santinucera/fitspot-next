/**
 *
 *
 * Tests for <NonRatedSessionsAlerts />
 *
 *
 */

import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import NonRatedSessionsAlerts from "./NonRatedSessionsAlerts";

import {
  createNonRatedSessionModel,
  setDismissSessionResponse,
  setNonRatedSessionsResponse,
} from "test-utils/session-service-test-utils";
import { ToastProvider } from "hooks/useToast";

describe("<NonRatedSessionsAlerts />", () => {
  it("should not log errors in console", () => {
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const spy = jest.spyOn(global.console, "error");
    render(<NonRatedSessionsAlerts />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render NonRatedSessionsAlerts", async () => {
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const { getByText } = render(<NonRatedSessionsAlerts />);
    await waitFor(() => {
      expect(getByText(sessions[0].name)).toBeInTheDocument();
    });
  });
  it("should remove session after rating session", async () => {
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const name = sessions[0].name;
    const { findAllByRole, queryByText } = render(<NonRatedSessionsAlerts />);
    await waitFor(() => {
      expect(queryByText(name)).toBeInTheDocument();
    });
    const radioButtons = await findAllByRole("radio");
    await waitFor(() => {
      fireEvent.click(radioButtons[0]);
    });
    expect(queryByText(name)).toBeNull();
  });
  it("should remove session dimissing session", async () => {
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const name = sessions[0].name;
    const { getByRole, queryByText } = render(<NonRatedSessionsAlerts />);
    await waitFor(() => {
      expect(queryByText(name)).toBeInTheDocument();
      fireEvent.click(getByRole("button"));
    });
    await waitFor(() => expect(queryByText(name)).toBeNull());
  });
  it("should not remove session dimissing session fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const sessions = [createNonRatedSessionModel()];
    setNonRatedSessionsResponse(sessions);
    const { getByRole, queryByText } = render(
      <ToastProvider>
        <NonRatedSessionsAlerts />
      </ToastProvider>
    );
    await waitFor(() => {
      setDismissSessionResponse(500);
      fireEvent.click(getByRole("button"));
    });
    await waitFor(() => {
      expect(
        queryByText("Failed to dismiss this session, please try again.")
      ).toBeInTheDocument();
    });
  });
});
