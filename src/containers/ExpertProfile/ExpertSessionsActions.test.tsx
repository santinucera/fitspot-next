/**
 *
 *
 * Tests for <ExpertSessionsActions />
 *
 *
 */

import React from "react";
import { renderWithRouter } from "test-utils/render-with-router";

import ExpertSessionsActions from "./ExpertSessionsActions";
import { waitFor } from "@testing-library/react";
import { MutateFunction } from "react-query";
import {
  SessionRsvpPostParams,
  SessionRsvpPostResponseData,
} from "services/SessionService";
import { AxiosError } from "axios";
import { createExpert } from "test-utils/expert-service-test-utils";
import { expertModel } from "services/ExpertService/models";

describe("<ExpertSessionsActions />", () => {
  it("should not log errors in console", () => {
    const expert = expertModel(createExpert());
    const fn: MutateFunction<
      SessionRsvpPostResponseData,
      AxiosError<any>,
      SessionRsvpPostParams,
      () => void
    > = (): any => {};
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <ExpertSessionsActions
        sessionId={2}
        sessions={expert.sessions}
        handleRSVP={fn}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render See Details of sessions", async () => {
    const expert = expertModel(createExpert());
    const fn: MutateFunction<
      SessionRsvpPostResponseData,
      AxiosError<any>,
      SessionRsvpPostParams,
      () => void
    > = (): any => {};

    const { getByRole } = renderWithRouter(
      <ExpertSessionsActions
        sessionId={2}
        sessions={expert.sessions}
        handleRSVP={fn}
      />
    );
    await waitFor(() =>
      expect(getByRole("link")).toHaveTextContent("See Details")
    );
  });
});
