/**
 *
 *
 * <ExpertSessionsActions />
 *
 *
 */

/** @jsx jsx */
import { jsx, Button } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import { Link } from "react-router-dom";
import { SessionsModel } from "services/SessionService/types";

interface ExpertSessionsActionsProps {
  sessionId: number;
  sessions: SessionsModel;
  handleRSVP: (params: { sessionId: number; isRSVP: boolean }) => void;
}

const ExpertSessionsActions: FunctionComponent<ExpertSessionsActionsProps> = ({
  sessionId,
  handleRSVP,
  sessions,
}) => {
  const session = sessions.find((s) => s.id === sessionId);
  return (
    <Fragment>
      <Button
        variant={session?.currentUserIsRSVP! ? "success" : "rsvp-button"}
        onClick={() =>
          handleRSVP({
            sessionId,
            isRSVP:
              session?.currentUserIsRSVP === null ||
              session?.currentUserIsRSVP === false
                ? true
                : false,
          })
        }
        data-testid={
          session?.currentUserIsRSVP! ? "cancel-rsvp-button" : "rsvp-button"
        }
      >
        {session?.currentUserIsRSVP ? "Registered" : "Reserve a spot"}
      </Button>
      <Link
        sx={{ variant: "buttons.secondary" }}
        to={`/dashboard/sessions/${sessionId}`}
      >
        See Details
      </Link>
    </Fragment>
  );
};

export default ExpertSessionsActions;
