/**
 *
 *
 * Tests for <ExpertProfile />
 *
 *
 */

import React from "react";
import {
  fireEvent,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import { User } from "services/types";
import ExpertProfile from "./ExpertProfile";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import {
  setExpertResponse,
  createExpert,
} from "test-utils/expert-service-test-utils";
import {
  setEmptyOnDemandResponse,
  setOnDemandResponse,
} from "test-utils/on-demand-service-test-utils";

import {
  setSessionRsvpResponse,
  createCustomerDashboardSession,
  setSessionsResponse,
} from "test-utils/session-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { ToastProvider } from "hooks/useToast";
import ModalProvider from "containers/Modal";

const mockReportRSVP = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportRSVP: mockReportRSVP,
  }))
);

describe("<ExpertProfile />", () => {
  let user: User;
  beforeEach(async () => {
    jest.clearAllMocks();
    jest.clearAllTimers();
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", async () => {
    const expert = createExpert();
    setExpertResponse(200, expert);
    const spy = jest.spyOn(global.console, "error");
    const { getByText, getByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ExpertProfile />
    );

    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitFor(() =>
      expect(getByText(expert.firstName!)).toBeInTheDocument()
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render no sessions if session are empty", async () => {
    const expert = { ...createExpert(), sessions: [] };

    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());
    setExpertResponse(200, expert);

    const { getByText, getByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ExpertProfile />
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitFor(() =>
      expect(getByText("No Upcoming Sessions")).toBeInTheDocument()
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render no videos if videos are empty", async () => {
    const expert = createExpert();
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());
    setExpertResponse(200, expert);

    setEmptyOnDemandResponse();

    const { getByText, getByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ExpertProfile />
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitFor(() =>
      expect(getByText("No On-Demand Videos")).toBeInTheDocument()
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should report RSVP to analytics", async () => {
    const expert = createExpert();
    const session = {
      ...expert.sessions[0],
      isRSVP: false,
    };
    setSessionsResponse([session]);
    setSessionRsvpResponse(200);
    const { getByTestId, getAllByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ModalProvider>
        <ExpertProfile />
      </ModalProvider>
    );
    const rsvpButton = await waitFor(() => getAllByTestId("rsvp-button"));
    fireEvent.click(rsvpButton[0]);
    await waitFor(() => getByTestId("modal"));
    expect(mockReportRSVP).toHaveBeenCalledWith({
      category: session.activityCategory,
      companyId: session.companyId,
      sessionId: session.id,
      isRSVP: true,
      userId: user.id,
    });
  });

  it("should not report RSVP on failure", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const session = {
      ...createCustomerDashboardSession(),
      currentUserIsRSVP: false,
    };
    setSessionsResponse([session]);
    setSessionRsvpResponse(500);
    const { getByTestId, getAllByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ToastProvider>
        <ExpertProfile />
      </ToastProvider>
    );
    const rsvpButton = await waitFor(() => getAllByTestId("rsvp-button"));
    fireEvent.click(rsvpButton[0]);
    await waitFor(() => getByTestId(/toast-item/));
    expect(mockReportRSVP).not.toHaveBeenCalled();
  });

  it("should render error on Expert failure", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());

    setExpertResponse(500);

    const { getByText, getByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <ExpertProfile />
      </ReactQueryConfigProvider>
    );

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(getByText("Error!")).toBeInTheDocument();
    });

    expect(spy).toHaveBeenCalled();
  });

  it("should render error on On-Demand failure", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());

    setOnDemandResponse(500);

    const { getByText, getByTestId } = renderWithMemoryRouter(
      ":trainerId",
      ["/patricks969"],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <ExpertProfile />
      </ReactQueryConfigProvider>
    );

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(getByText("Error!")).toBeInTheDocument();
    });

    expect(spy).toHaveBeenCalled();
  });
});
