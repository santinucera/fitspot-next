/**
 *
 *
 * Tests for <ExpertTitle />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import ExpertTitle from "./ExpertTitle";

describe("<ExpertTitle />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ExpertTitle expertName="some string" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render name", () => {
    const value = "some string";
    const { getByRole } = render(<ExpertTitle expertName={value} />);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
