/*
 *
 *
 * ExpertProfile
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Grid, Divider, Heading, Text } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import Spinner from "components/Spinner";
import { useParams } from "react-router-dom";
import ExpertInfo from "components/ExpertInfo";
import ExpertTitle from "./ExpertTitle";
import { useQueryExpertGetInfoService } from "services/ExpertService";
import SessionCardGrid from "components/SessionCardGrid";
import { useQueryOnDemandVideosGetService } from "services/OnDemandVideoService";
import { queryCache } from "react-query";
import useToast from "hooks/useToast";
import { useMutationSessionRsvpPostService } from "services/SessionService";
import { ExpertModel } from "services/ExpertService";
import { useQueryUserGetData } from "services/UserService";
import useAnalytics from "hooks/useAnalytics";
import { useModal } from "containers/Modal/Modal";
import OnDemandItem from "components/OnDemandItem";
import ExpertSessionsActions from "./ExpertSessionsActions";
import WatchTogetherModal from "containers/WatchTogetherModal";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface ExpertProfileProps {}

const ExpertProfile: FunctionComponent<ExpertProfileProps> = () => {
  const { trainerId } = useParams();
  const { reportRSVP } = useAnalytics();

  const urlParams = { expertId: trainerId };
  const {
    isFetching,
    data: expertInfo,
    isError,
  } = useQueryExpertGetInfoService({ urlParams });

  const {
    isError: isErrorOnDemand,
    isFetching: isFetchingOnDemand,
    data: onDemandVideos,
  } = useQueryOnDemandVideosGetService({
    queryParams: {
      trainerId: expertInfo?.id,
      limit: 3,
    },
  });

  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  /**
   * Gets the user from the cache.
   */
  const user = useQueryUserGetData();

  /**
   * `handleRSVP` is the mutation function reserves a session
   */
  const [handleRSVP] = useMutationSessionRsvpPostService({
    mutationConfig: {
      onMutate: (variables) => {
        queryCache.cancelQueries(["expert", urlParams]);
        const previousTrainer = queryCache.getQueryData<ExpertModel>([
          "expert",
          urlParams,
        ]);
        queryCache.setQueryData<ExpertModel>(
          ["expert", urlParams],
          (oldTrainer): ExpertModel => {
            return {
              ...oldTrainer!,
              sessions: oldTrainer!.sessions.map((session) => {
                if (session.id === variables.sessionId) {
                  return {
                    ...session,
                    currentUserIsRSVP: Boolean(variables.isRSVP),
                  };
                }
                return session;
              }),
            };
          }
        );
        // Rollback - set sessions back to previous value
        return () =>
          queryCache.setQueryData(["expert", urlParams], previousTrainer);
      },
      onSuccess: (__, { sessionId, isRSVP }) => {
        const trainer = queryCache.getQueryData<ExpertModel>([
          "expert",
          urlParams,
        ]);

        const session = trainer?.sessions?.find(
          (session) => session.id === sessionId
        );

        if (session && isRSVP) {
          setModal(
            <WatchTogetherModal
              handleClose={removeModal}
              id={session.id}
              title={session.name}
              description={session.description}
              startDate={session.startDate}
              endDate={session.endDate}
              type="session"
            />
          );
        } else {
          addToast({
            title: "Reservation Canceled",
            description: `${session?.name} session canceled`,
            type: "success",
          });
        }

        if (user && session) {
          reportRSVP({
            isRSVP,
            userId: user.id,
            sessionId: sessionId,
            companyId: session?.companyId || null,
            category: session?.activityCategory || null,
          });
        }
      },
      onError: (_, __, rollback) => {
        rollback();
        addToast({
          title: "Error!",
          description: "An error occurred, please try again.",
          type: "error",
        });
      },
      onSettled: () => {
        queryCache.invalidateQueries(["expert", urlParams]);
      },
    },
  });

  if (isFetching || isFetchingOnDemand) return <Spinner />;

  if (isError || isErrorOnDemand || !expertInfo) {
    return <div>Error!</div>;
  }

  return (
    <Flex sx={{ flexDirection: "column" }} data-testid="trainer">
      <ExpertTitle expertName={expertInfo?.firstName || ""} />
      <Flex sx={{ flexDirection: ["column", "row", "row"] }}>
        <Box sx={{ width: ["100%", "100%", "30%"], pr: 8, mt: 5, pb: 2 }}>
          <ExpertInfo
            avatar={expertInfo?.avatar?.url || ""}
            firstName=""
            lastName=""
            bio=""
            activityName=""
          />
        </Box>
        <Box sx={{ width: ["100%", "100%", "30%"], mt: 5, pb: 2 }}>
          <Text>{expertInfo?.bio}</Text>
        </Box>
      </Flex>
      <Divider sx={{ mb: 7, mt: 5 }} />
      {expertInfo?.sessions!.length > 0 ? (
        <Fragment>
          <Heading
            sx={{ fontSize: 0, mb: 3 }}
          >{`${expertInfo?.firstName}'s Next Sessions`}</Heading>
          <SessionCardGrid
            sessions={expertInfo?.sessions!}
            cardActions={({ sessionId }: { sessionId: number }) => (
              <ExpertSessionsActions
                handleRSVP={handleRSVP}
                sessions={expertInfo?.sessions!}
                sessionId={sessionId}
              />
            )}
          />
        </Fragment>
      ) : (
        <Flex sx={{ alignItems: "center", flexDirection: "column" }}>
          <Heading
            sx={{
              fontWeight: 800,
              fontSize: 6,
            }}
          >
            No Upcoming Sessions
          </Heading>
          <Text>Check later for new sessions</Text>
        </Flex>
      )}
      <Divider sx={{ mb: 7, mt: 5 }} />
      <Heading sx={{ fontSize: 0, mb: 3 }}>On-Demand Videos</Heading>
      {onDemandVideos?.data.length ? (
        <Grid gap={6} columns={[1, "repeat(3, minmax(0,1fr))"]}>
          {onDemandVideos.data.map(
            ({ id, name, trainer, startDate, thumbnailUrl }) => (
              <OnDemandItem
                key={id}
                id={id}
                name={name}
                trainer={trainer}
                startDate={startDate}
                thumbnailUrl={thumbnailUrl}
              />
            )
          )}
        </Grid>
      ) : (
        <Flex sx={{ alignItems: "center", flexDirection: "column" }}>
          <Heading
            sx={{
              fontWeight: 800,
              fontSize: 6,
            }}
          >
            No On-Demand Videos
          </Heading>
          <Text>Check later for new videos</Text>
        </Flex>
      )}
    </Flex>
  );
};

export default ExpertProfile;
