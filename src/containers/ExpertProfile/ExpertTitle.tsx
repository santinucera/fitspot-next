/**
 *
 *
 * <ExpertTitle />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Heading, Text } from "theme-ui";
import { FunctionComponent } from "react";

interface ExpertTitleProps {
  expertName: string;
}

const ExpertTitle: FunctionComponent<ExpertTitleProps> = ({ expertName }) => {
  return (
    <Flex sx={{flexDirection: 'column', pb: 20, borderBottom: '1px solid #EDEDED'}}>
      <Text sx={{color: 'dark-gray', fontSize: 0, mb:1}}>Expert</Text>
      <Heading sx={{fontSize: 5}} as="h1">{expertName}</Heading>
    </Flex>
  );
};

export default ExpertTitle;
