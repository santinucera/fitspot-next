/**
 *
 *
 * Tests for <Challenge />
 *
 *
 */

import React from "react";
import {
  render,
  waitForElementToBeRemoved,
  fireEvent,
  waitFor,
} from "@testing-library/react";
import Challenge from "./Challenge";
import { renderWithRouter } from "test-utils/render-with-router";
import { queryCache } from "react-query";
import {
  setChallengeResponse,
  createChallenge,
  setChallengeErrorResponse,
  createChallengeLeaderBoardItem,
  setChallengeReportPostResponse,
} from "test-utils/challenge-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import ModalProvider from "containers/Modal";
import { ReactQueryConfigProvider } from "react-query";
import { User } from "services/types";
import dayjs from "utils/days";
import { ToastProvider } from "hooks/useToast";

const mockReportChallengeRSVP = jest.fn();
const mockReportAddPoints = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportChallengeRSVP: mockReportChallengeRSVP,
    reportAddPoints: mockReportAddPoints,
  }))
);

jest.mock("react-router-dom", () => ({
  useParams: () => ({
    challengeId: 1,
  }),
}));

describe("<Challenge />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Challenge />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should fetch and render a challenge with a title", async () => {
    const name = "Some Title";
    setChallengeResponse({ ...createChallenge(), name });
    const { getByTestId, getByText } = renderWithRouter(<Challenge />);
    expect(getByTestId("spinner")).toBeInTheDocument();
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByText(name)).toBeInTheDocument();
  });

  it("should RSVP to a challenge and report to analytics", async () => {
    const challenge = createChallenge();
    // Set response to a non-joined challenge with no participants.
    setChallengeResponse({ ...challenge, leaderboard: [] });
    const { getByTestId, getAllByText } = renderWithRouter(
      <ToastProvider>
        <Challenge />
      </ToastProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    // Set to a joined challenge
    const joinedChallenge = {
      ...challenge,
      leaderboard: [createChallengeLeaderBoardItem(1, 1, true)],
      individualPointsSummary: {
        ...challenge.individualPointsSummary,
        userId: 1,
      },
    };
    setChallengeResponse(joinedChallenge);
    // There is a Join Challenge button in the header and the Leaderboard in challenges
    // with no participants.
    const joinChallengeButton = getAllByText("Join challenge")[0];
    fireEvent.click(joinChallengeButton);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        "You have entered the challenge"
      )
    );
    await waitFor(() =>
      expect(mockReportChallengeRSVP).toHaveBeenCalledWith({
        userId: user.id,
        challengeId: joinedChallenge.id,
        isRSVP: true,
      })
    );
  });

  it("should un-RSVP to a challenge and report to analytics", async () => {
    const challenge = createChallenge();
    const joinedChallenge = {
      ...challenge,
      leaderboard: [createChallengeLeaderBoardItem(1, 1, true)],
      individualPointsSummary: {
        ...challenge.individualPointsSummary,
        userId: 1,
      },
    };
    setChallengeResponse(joinedChallenge);
    const { getByTestId, getByText } = renderWithRouter(
      <ModalProvider>
        <ToastProvider>
          <Challenge />
        </ToastProvider>
      </ModalProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    setChallengeResponse(createChallenge());
    const joinedButton = await waitFor(() => getByText("Joined"));
    fireEvent.click(joinedButton);
    const leaveButton = await waitFor(() => getByTestId("submit-button"));
    fireEvent.click(leaveButton);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Sad to see you go!")
    );
    await waitFor(() =>
      expect(mockReportChallengeRSVP).toHaveBeenCalledWith({
        userId: user.id,
        challengeId: challenge.id,
        isRSVP: false,
      })
    );
  });

  it("should add points and report to analytics", async () => {
    const challenge = { ...createChallenge(), id: 1, isJoined: true };
    setChallengeResponse(challenge);
    const { getByTestId, getAllByText } = renderWithRouter(
      <ModalProvider>
        <Challenge />
      </ModalProvider>
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    fireEvent.click(getAllByText("ADD")[0]);
    const modal = await waitFor(() => getByTestId("modal"));
    const addPointsButton = await waitFor(() =>
      getByTestId("add-points-button")
    );
    const activity = challenge.activities[0];
    const challengeRecord = {
      activityRuleId: activity.id,
      date: dayjs().format("YYYY-DD-MM"),
      id: 1,
      points: 1,
    };
    setChallengeReportPostResponse(200, [challengeRecord]);
    fireEvent.click(addPointsButton);
    await waitForElementToBeRemoved(modal);
    await waitFor(() =>
      expect(mockReportAddPoints).toHaveBeenCalledWith({
        userId: user.id,
        challengeId: challenge.id,
      })
    );
  });

  it("should show an error message when fetch fails", async () => {
    const consoleError = jest.fn();
    jest.spyOn(console, "error").mockImplementation(consoleError);
    setChallengeErrorResponse();
    const { getAllByText, getByTestId } = renderWithRouter(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <Challenge />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(
        getAllByText(
          "An error ocurred. Please refresh the browser and try again."
        )
      ).toHaveLength(1);
    });
    await waitFor(() => expect(consoleError).toBeCalled());
  });
});
