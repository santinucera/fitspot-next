/**
 *
 *
 * Tests for <Points />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Points from "./Points";

describe("<Points />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Points
        isJoined={true}
        isActive={true}
        activities={[]}
        openPointsModal={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
