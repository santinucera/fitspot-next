/**
 *
 *
 * <Prizes />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text, Divider, Card } from "theme-ui";
import { FunctionComponent } from "react";

import ribbon1 from "images/ribbon-1.png";
import ribbon2 from "images/ribbon-2.png";
import ribbon3 from "images/ribbon-3.png";
import ordinalSuffix from "utils/ordinal-suffix";
import H2 from "components/H2";
import { PrizeModel } from "services/ChallengeService";

interface Ribbons {
  [x: number]: string;
}

const ribbons: Ribbons = {
  1: ribbon1,
  2: ribbon2,
  3: ribbon3,
};

interface PrizesProps {
  prizes: PrizeModel[];
}

const Prizes: FunctionComponent<PrizesProps> = ({ prizes }) => {
  return prizes?.length ? (
    <Card
      sx={{
        bg: "white",
        mt: 5,
        p: 6,
      }}
    >
      <H2>Prizes</H2>
      <Divider sx={{ my: 5 }} />
      <Flex
        sx={{
          width: "100%",
          flexDirection: "row",
          flexWrap: "wrap",
        }}
      >
        {prizes.map(({ id, title, rank, description, url, hostname }) => {
          return (
            <Flex
              key={id}
              sx={{
                mb: 3,
                flexDirection: "column",
                alignItems: "center",
                width: [
                  "100%",
                  `${Math.round(100 / prizes?.length)}%`,
                  `${Math.round(100 / prizes?.length)}%`,
                ],
              }}
            >
              <img
                sx={{ width: "42px", height: "47px", my: 3 }}
                src={ribbons?.[rank]}
                alt={`ranking ${rank}`}
              />
              <Text sx={{ color: "dark-gray" }}>
                {rank}
                {ordinalSuffix(rank)} place prize
              </Text>
              <Box sx={{ my: 3, textAlign: "center" }}>
                <Text sx={{ fontWeight: "heading" }}>{title}</Text>
                <Text sx={{ color: "dark-gray" }}>{description}</Text>
              </Box>
              <a href={url} sx={{ color: "blue" }}>
                {hostname}
              </a>
            </Flex>
          );
        })}
      </Flex>
    </Card>
  ) : null;
};

export default Prizes;
