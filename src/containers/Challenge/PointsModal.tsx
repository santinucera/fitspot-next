/**
 *
 *
 * <PointsModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button, Text, Flex } from "theme-ui";
import { FunctionComponent, useMemo, useState } from "react";
import H2 from "components/H2";
import { Rule } from "services/ChallengeService";
import Counter from "components/Counter";

interface RuleDescriptionProps {
  rule: Rule;
}

const RuleDescription: FunctionComponent<RuleDescriptionProps> = ({ rule }) => {
  let description: string;
  switch (rule) {
    case "Per Day":
      description =
        "This is a daily activity, come back tomorrow to record more points!";
      break;
    case "Per Challenge":
      description = "This activity may only be completed once per challenge.";
      break;
    default:
      description = "";
  }
  return (
    <Text sx={{ mt: 5 }} data-testid="rule-description">
      {description}
    </Text>
  );
};

interface PointsModalProps {
  title: string;
  points: number;
  canAddPoints: boolean;
  activityRuleId: number;
  maxPointCanAdd: number;
  canAddIndividualPoints: boolean;
  rule: Rule;
  handleAddPoints: (activityRuleId: number, points: number) => void;
}

const PointsModal: FunctionComponent<PointsModalProps> = ({
  title,
  points,
  activityRuleId,
  maxPointCanAdd,
  canAddIndividualPoints,
  rule,
  handleAddPoints,
}) => {
  const [pointsInc, setPoints] = useState<number>(1);

  const pointsToAdd = pointsInc * points;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const pointsEqualMax = useMemo(() => pointsToAdd === maxPointCanAdd, []);

  return (
    <Flex
      sx={{
        p: 5,
        alignItems: "center",
        flexDirection: "column",
        flex: " 0 0 100%",
      }}
    >
      <Box
        sx={{
          mb: 7,
          textAlign: "center",
        }}
      >
        <H2>{title}</H2>
        {canAddIndividualPoints && !pointsEqualMax && (
          <Flex sx={{ flexDirection: "column", alignItems: "center" }}>
            <Text>How many times did you complete this activity?</Text>
            <Box sx={{ my: 6 }}>
              <Counter
                value={pointsInc}
                step={1}
                min={1}
                max={maxPointCanAdd / points}
                onChange={(value) => setPoints(value)}
              />
            </Box>
          </Flex>
        )}
        <Text>
          {pointsToAdd} point{pointsToAdd > 1 && "s"} will be added
        </Text>
      </Box>
      <Button
        onClick={() => handleAddPoints(activityRuleId, pointsToAdd)}
        data-testid="add-points-button"
      >
        Add Points
      </Button>
      <RuleDescription rule={rule} />
    </Flex>
  );
};

export default PointsModal;
