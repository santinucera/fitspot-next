/**
 *
 *
 * Tests for <PointsModal />
 *
 *
 */

import React from "react";
import {
  fireEvent,
  render,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import PointsModal from "./PointsModal";

const mockedRemoveModal = jest.fn();

jest.mock("containers/Modal/Modal", () => ({
  useModal: () => {
    return {
      removeModal: mockedRemoveModal,
    };
  },
}));

describe("<PointsModal />", () => {
  let handleAddPoints: jest.Mock;
  beforeEach(async () => {
    handleAddPoints = jest.fn();
  });

  afterEach(() => jest.unmock("containers/Modal/Modal"));

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <PointsModal
        title={"points modal"}
        points={10}
        canAddPoints={true}
        activityRuleId={2}
        maxPointCanAdd={20}
        canAddIndividualPoints={true}
        rule="Per Day"
        handleAddPoints={handleAddPoints}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should handle adding points", async () => {
    const { findAllByRole, findByText } = render(
      <PointsModal
        title={"points modal"}
        points={10}
        canAddPoints={true}
        activityRuleId={2}
        maxPointCanAdd={20}
        canAddIndividualPoints={true}
        rule="Per Day"
        handleAddPoints={handleAddPoints}
      />
    );
    let pointsText = await findByText("10 points will be added");
    const buttons = await findAllByRole("button");
    const increase = buttons[1];
    fireEvent.click(increase);
    waitForElementToBeRemoved(() => pointsText);
    pointsText = await findByText("20 points will be added");
    expect(pointsText).toBeInTheDocument();
  });

  it("should show the Per Day rule description", () => {
    const { getByTestId } = render(
      <PointsModal
        title={"points modal"}
        points={10}
        canAddPoints={true}
        activityRuleId={2}
        maxPointCanAdd={20}
        canAddIndividualPoints={true}
        rule="Per Day"
        handleAddPoints={handleAddPoints}
      />
    );

    expect(getByTestId("rule-description")).toHaveTextContent(
      "This is a daily activity, come back tomorrow to record more points!"
    );
  });

  it("should show the Per Challenge rule description", () => {
    const { getByTestId } = render(
      <PointsModal
        title={"points modal"}
        points={10}
        canAddPoints={true}
        activityRuleId={2}
        maxPointCanAdd={20}
        canAddIndividualPoints={true}
        rule="Per Challenge"
        handleAddPoints={handleAddPoints}
      />
    );

    expect(getByTestId("rule-description")).toHaveTextContent(
      "This activity may only be completed once per challenge."
    );
  });
});
