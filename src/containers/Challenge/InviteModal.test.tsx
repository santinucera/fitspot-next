/**
 *
 *
 * Tests for <InviteModal />
 *
 *
 */

import React from "react";
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import ModalProvider, { useModal } from "containers/Modal/Modal";
import InviteModal from "./InviteModal";

describe("<InviteModal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<InviteModal challengeId={1} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render link with id", () => {
    const id = 1;
    const { getByDisplayValue } = render(<InviteModal challengeId={id} />);
    expect(
      getByDisplayValue(`https://tenspot.co/dashboard/challenges/${id}`)
    ).toBeInTheDocument();
  });

  it("should close modal with submit button", async () => {
    const El = () => {
      const { setModal } = useModal();
      return (
        <button
          data-testid="button"
          onClick={() => setModal(<InviteModal challengeId={1} />)}
        >
          Click
        </button>
      );
    };
    const { getByTestId } = render(
      <ModalProvider>
        <El />
      </ModalProvider>
    );
    fireEvent.click(getByTestId("button"));
    await waitFor(() => {
      fireEvent.click(getByTestId("submit-button"));
    });
    await waitForElementToBeRemoved(() => getByTestId("modal"));
  });
});
