/**
 *
 *
 * <LeaderBoard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box, Card, Button, Text, Divider } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { alpha } from "@theme-ui/color";
import { ChallengeLeaderBoardItem } from "services/ChallengeService/types";
import H2 from "components/H2";
import thousands from "utils/thousands";
import Avatar from "components/Avatar";
import { useQueryUserGetData } from "services/UserService";
import Tooltip from "components/Tooltip";
import IconInfo from "components/IconInfo";

interface LeaderBoardProps {
  hasParticipants: boolean;
  isActive: boolean;
  isJoined: boolean;
  hasEnded: boolean;
  leaderboard: ChallengeLeaderBoardItem[];
  handleRSVP: () => void;
}

const LeaderBoard: FunctionComponent<LeaderBoardProps> = ({
  hasParticipants,
  isActive,
  leaderboard,
  isJoined,
  hasEnded,
  handleRSVP,
}) => {
  const user = useQueryUserGetData();
  return (
    <Card
      sx={{
        bg: "white",
        p: 6,
        mt: 4,
      }}
    >
      <Flex sx={{ alignItems: "center" }}>
        <H2 sx={{ mr: 4, mb: 0 }}>Leaderboard</H2>
        <Tooltip text="Make sure to log activities that require manual tracking. Once the challenge is has concluded, participants with the top 3 point values will be the winners.">
          <IconInfo size={20} />
        </Tooltip>
      </Flex>
      <Divider sx={{ my: 5 }} />
      {!isActive && isJoined && !hasEnded && (
        <Flex sx={{ alignItems: "center", flexDirection: "column", py: 7 }}>
          <H2>This challenge hasn’t started</H2>
          <Text sx={{ color: "dark-gray", mb: 7 }}>
            We'll send you a reminder when the challenge begins!
          </Text>
        </Flex>
      )}
      {!hasParticipants && (
        <Flex sx={{ alignItems: "center", flexDirection: "column", py: 7 }}>
          <H2>No participants yet</H2>
          <Text sx={{ color: "dark-gray", mb: 7 }}>
            This challenge seems to be empty.
            <br />
            Join first and then invite friends!
          </Text>
          <Button onClick={handleRSVP}>Join challenge</Button>
        </Flex>
      )}
      {hasParticipants && (isActive || hasEnded) && (
        <Fragment>
          <Flex sx={{ justifyContent: "space-between" }}>
            <Box>Participants</Box>
            <Box>Points</Box>
          </Flex>
          <Flex sx={{ my: 5, flexDirection: "column" }}>
            {leaderboard?.map(
              ({ firstName, lastName, points, avatar, rank, userId }, idx) => (
                <Flex
                  key={idx}
                  sx={{
                    my: 2,
                    width: "100%",
                    alignItems: "center",
                    bg: user.id === userId ? alpha("gray", 0.5) : "white",
                  }}
                >
                  <Box sx={{ width: "30px" }}>{rank}.</Box>
                  <Avatar url={avatar && avatar.url} size={40} />
                  <Box ml={4}>
                    {firstName} {lastName}
                  </Box>
                  <Box ml="auto">{thousands(points)}</Box>
                </Flex>
              )
            )}
          </Flex>
        </Fragment>
      )}
    </Card>
  );
};

export default LeaderBoard;
