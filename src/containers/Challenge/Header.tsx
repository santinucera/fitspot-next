/**
 *
 *
 * <Header />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Divider, Text, Button } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import H2 from "components/H2";
import IconCheckMark from "components/IconCheckMark";
import IconCalendar from "components/IconCalendar";
import { Dayjs } from "dayjs";
import { useModal } from "containers/Modal/Modal";
import InviteModal from "./InviteModal";

interface HeaderProps {
  isJoined: boolean;
  hasEnded: boolean;
  name: string;
  handleRSVP: () => void;
  startDate: Dayjs;
  endDate: Dayjs;
  challengeId: number;
}

const Header: FunctionComponent<HeaderProps> = ({
  isJoined,
  hasEnded,
  name,
  handleRSVP,
  startDate,
  endDate,
  challengeId,
}) => {
  const { setModal } = useModal();
  const openShareModal = () =>
    setModal(<InviteModal challengeId={challengeId} />);
  return (
    <Box>
      <Flex
        data-testid="challengeHeader"
        sx={{ flexDirection: ["column", "row"] }}
      >
        <Flex
          sx={{
            flexDirection: "column",
            flexWrap: "wrap",
            maxWidth: ["100%", 400],
          }}
        >
          <H2>{name}</H2>
        </Flex>
        {!hasEnded && (
          <Flex sx={{ ml: [0, 3, "auto"], my: [4, 0, 0] }}>
            {isJoined ? (
              <Fragment>
                <Button sx={{ mr: 6, width: 140 }} onClick={openShareModal}>
                  Share
                </Button>
                <Button
                  sx={{ width: 140 }}
                  variant="secondary"
                  onClick={handleRSVP}
                >
                  <IconCheckMark sx={{ mr: 2 }} /> Joined
                </Button>
              </Fragment>
            ) : (
              <Button onClick={handleRSVP}>Join</Button>
            )}
          </Flex>
        )}
      </Flex>
      <Flex sx={{ alignItems: "center" }}>
        <IconCalendar sx={{ color: "dark-gray", mr: 3 }} />
        <Text sx={{ color: "dark-gray", lineHeight: "24px" }}>
          {startDate.format("MMM DD")} - {endDate.format("MMM DD")}
        </Text>
      </Flex>
      <Divider sx={{ my: 7 }} />
    </Box>
  );
};

export default Header;
