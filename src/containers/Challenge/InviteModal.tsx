/**
 *
 *
 * <InviteModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Button, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import CopyToClipBoard from "components/CopyToClipBoard";
import H2 from "components/H2";
import { useModal } from "containers/Modal/Modal";

interface InviteModalProps {
  challengeId: number;
}

const InviteModal: FunctionComponent<InviteModalProps> = ({ challengeId }) => {
  const { removeModal } = useModal();
  return (
    <Box sx={{ p: 5, width: "100%" }}>
      <Flex
        sx={{
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          py: 6,
        }}
      >
        <H2 sx={{ fontSize: 6 }}>Invite a friend</H2>
        <Text sx={{ mb: 7, color: "medium-gray", textAlign: "center" }}>
          Challenges are better when your friends join in.
        </Text>
        <Box sx={{ mb: 7, width: "100%" }}>
          <CopyToClipBoard
            label="Share this challenge URL with your friends"
            value={`https://tenspot.co/dashboard/challenges/${challengeId}`}
          />
        </Box>
        <Button
          sx={{
            width: "255px",
          }}
          variant="secondary"
          data-testid="submit-button"
          onClick={removeModal}
        >
          Done
        </Button>
      </Flex>
    </Box>
  );
};

export default InviteModal;
