/*
 *
 *
 * Challenge
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Divider } from "theme-ui";
import { Fragment, FunctionComponent, SyntheticEvent } from "react";
import days from "utils/days";
import {
  useQueryChallengeGetService,
  useMutationChallengeRsvpPostService,
} from "services/ChallengeService";
import { useQueryUserGetData } from "services/UserService";
import { useParams } from "react-router-dom";
import Spinner from "components/Spinner";
import ActivityFeed from "components/ActivityFeed";
import H2 from "components/H2";
import { useModal } from "containers/Modal/Modal";
import useToast from "hooks/useToast";
import useAnalytics from "hooks/useAnalytics";
import { useMutationChallengeReportPostService } from "services/ChallengeService";
import { useQueryActivityFeedGetService } from "services/ActivityFeedService";
import ChallengeWinnerAvatars from "components/ChallengeWinnerAvatars";
import PointsModal from "./PointsModal";
import Header from "./Header";
import LeaderBoard from "./LeaderBoard";
import Prizes from "./Prizes";
import Points, { ChallengePointsModal } from "./Points";
import LeaveModal from "./LeaveModal";
import About from "./About";

interface ChallengeProps {}

const Challenge: FunctionComponent<ChallengeProps> = () => {
  const { challengeId } = useParams();
  const urlParams = { challengeId: Number(challengeId) };
  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const { reportChallengeRSVP, reportAddPoints } = useAnalytics();
  // User is fetched by <App />
  const user = useQueryUserGetData();

  const {
    isFetchedAfterMount,
    data: challenge,
    refetch,
  } = useQueryChallengeGetService({
    urlParams,
  });

  const { data: activityFeed = [] } = useQueryActivityFeedGetService({
    urlParams: {
      challengeId: urlParams.challengeId,
    },
  });

  const [rsvpMutation] = useMutationChallengeRsvpPostService({
    mutationConfig: {
      onSuccess(response) {
        refetch();
        // If leaving challenge...
        if (challenge?.isJoined) {
          addToast({
            title: "Success!",
            description: "Sad to see you go!",
            type: "success",
          });
          removeModal();
        } else {
          // If joining challenge...
          addToast({
            title: "Success!",
            description: "You have entered the challenge",
            type: "success",
          });
        }

        if (user && challenge) {
          reportChallengeRSVP({
            userId: user.id,
            challengeId: challenge.id,
            isRSVP: !challenge.isJoined,
          });
        }
      },
    },
  });

  const [makeAddPointsRequest] = useMutationChallengeReportPostService({
    mutationConfig: {
      onSuccess(response) {
        refetch();
        removeModal();
        if (user && challenge) {
          reportAddPoints({
            userId: user.id,
            challengeId: challenge.id,
          });
        }
      },
    },
  });
  const handleAddPoints = (activityId: number, points: number): void => {
    if (challenge) {
      makeAddPointsRequest({
        challengeId: challenge.id,
        points,
        activityRuleId: activityId,
        date: days().format("YYYY-MM-DD"),
      });
    }
  };

  const openPointsModal = ({
    name,
    activityRuleId,
    points,
    currentPoints,
    canAddPoints,
    maxPoints,
    canAddIndividualPoints,
    rule,
  }: ChallengePointsModal) => (event: SyntheticEvent) => {
    setModal(
      <PointsModal
        title={name}
        points={points}
        canAddPoints={canAddPoints}
        activityRuleId={activityRuleId}
        maxPointCanAdd={maxPoints - currentPoints}
        canAddIndividualPoints={canAddIndividualPoints}
        rule={rule}
        handleAddPoints={handleAddPoints}
      />
    );
  };

  if (!isFetchedAfterMount) return <Spinner />;
  if (!challenge)
    return (
      <div>An error ocurred. Please refresh the browser and try again.</div>
    );

  const handleRSVP = () => {
    const mutation = () => rsvpMutation({ challengeId: id, isRSVP: !isJoined });
    if (isJoined) setModal(<LeaveModal handleLeave={mutation} />);
    else mutation();
  };

  const {
    hasParticipants,
    id,
    isJoined,
    activities,
    leaderboard,
    prizes,
    isActive,
    hasEnded,
    name,
    description,
    endDate,
    startDate,
    rules,
  } = challenge;

  return (
    <Box>
      <Header
        hasEnded={hasEnded}
        isJoined={isJoined}
        name={name}
        handleRSVP={handleRSVP}
        startDate={startDate}
        endDate={endDate}
        challengeId={id}
      />
      {hasEnded && (
        <Box
          sx={{
            bg: "white",
            p: 6,
            mb: 4,
          }}
        >
          <H2 sx={{ textAlign: "center" }}>Challenge winners</H2>
          <Divider sx={{ my: 4 }} />
          <Flex sx={{ justifyContent: "center" }}>
            <ChallengeWinnerAvatars leaderboard={leaderboard} />
          </Flex>
        </Box>
      )}
      <Flex
        sx={{
          width: "100%",
          flexDirection: "row",
          flexWrap: "wrap",
        }}
      >
        <Flex
          sx={{
            width: ["100%", "100%", "50%"],
            pr: ["0px", "0px", "12px"],
            flexDirection: "column",
          }}
        >
          <About description={description} rules={rules} />
          <LeaderBoard
            hasParticipants={hasParticipants}
            leaderboard={leaderboard}
            isActive={isActive}
            isJoined={isJoined}
            hasEnded={hasEnded}
            handleRSVP={() => {
              rsvpMutation({ challengeId: id, isRSVP: !isJoined });
            }}
          />
          <Prizes prizes={prizes} />
        </Flex>
        <Flex
          sx={{
            pl: ["0px", "0px", "12px"],
            mt: ["18px", "18px", "0px"],
            width: ["100%", "100%", "50%"],
            flexDirection: "column",
          }}
        >
          <Points
            isJoined={isJoined}
            isActive={isActive}
            activities={activities}
            openPointsModal={openPointsModal}
            hasEnded={hasEnded}
          />
          {!hasEnded && (
            <Box sx={{ mt: 4 }}>
              <ActivityFeed
                feed={activityFeed}
                showActivityName={false}
                header={
                  <Fragment>
                    <H2 role="heading">Activity feed</H2>
                    <Divider sx={{ my: 5 }} />
                  </Fragment>
                }
              />
            </Box>
          )}
        </Flex>
      </Flex>
    </Box>
  );
};

export default Challenge;
