/**
 *
 *
 * <Points />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Grid, Flex, Text, Card, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import { ActivityModel, Rule } from "services/ChallengeService";

import H2 from "components/H2";
import IconCircleCheck from "components/IconCircleCheck";
import Tooltip from "components/Tooltip";
import IconInfo from "components/IconInfo";

export interface ChallengePointsModal {
  name: string;
  activityRuleId: number;
  points: number;
  currentPoints: number;
  canAddPoints: boolean;
  maxPoints: number;
  canAddIndividualPoints: boolean;
  rule: Rule;
}

interface PointsProps {
  isJoined: boolean;
  isActive: boolean;
  hasEnded: boolean;
  activities: ActivityModel[];
  openPointsModal: (
    props: ChallengePointsModal
  ) => (event: React.SyntheticEvent) => void;
}

const Points: FunctionComponent<PointsProps> = ({
  isJoined,
  isActive,
  activities,
  openPointsModal,
  hasEnded,
}) => {
  return (
    <Card
      sx={{
        bg: "white",
        p: 6,
      }}
    >
      <Flex sx={{ alignItems: "center" }}>
        <H2 sx={{ mr: 4, mb: 0 }}>Earn Points</H2>
        <Tooltip text="Points are only accrued once you join the challenge. Activities have a maximum per challenge or per day. Once that maximum is reached, points can no longer be added. Some activities will be tracked automatically.">
          <IconInfo size={20} />
        </Tooltip>
      </Flex>
      <Divider sx={{ my: 5 }} />
      <Grid
        sx={{
          mt: 5,
          flexDirection: "column",
          gridTemplateColumns: `repeat(${
            isJoined && isActive && !hasEnded ? 3 : 2
          }, auto)`,
        }}
      >
        <Box>Activity</Box>
        {isJoined && (isActive || hasEnded) && (
          <Box sx={{ textAlign: "right" }}>Points Earned</Box>
        )}
        {!hasEnded && <Box />}
        {activities.map(
          ({
            id,
            name,
            maxPoints,
            currentPoints,
            maxPointsReached,
            canAddPoints,
            points,
            canAddIndividualPoints,
            pointType,
            rule,
          }) => (
            <Grid
              key={id}
              sx={{
                display: "contents",
              }}
            >
              <Box>
                <Text sx={{ fontWeight: "heading" }}>{name}</Text>
                <Text sx={{ color: "dark-gray" }}>
                  {points} Point{points > 1 && "s"}
                </Text>
              </Box>
              {isJoined && (isActive || hasEnded) && (
                <Flex sx={{ ml: "auto", alignItems: "center" }}>
                  {currentPoints}/{maxPoints}
                </Flex>
              )}
              {(isActive || !hasEnded) && (
                <Flex sx={{ justifyContent: "flex-end", alignItems: "center" }}>
                  {pointType !== "Manual" && isJoined && isActive && (
                    <Text sx={{ color: "dark-gray", fontWeight: 700 }}>
                      AUTO
                    </Text>
                  )}
                  {pointType === "Manual" &&
                    !maxPointsReached &&
                    isJoined &&
                    isActive && (
                      <Text
                        as="button"
                        onClick={openPointsModal({
                          name,
                          activityRuleId: id,
                          points,
                          currentPoints,
                          canAddPoints,
                          maxPoints,
                          canAddIndividualPoints,
                          rule,
                        })}
                        sx={{
                          color: "blue",
                          textDecoration: "underline",
                        }}
                      >
                        ADD
                      </Text>
                    )}
                  {maxPointsReached && isJoined && (
                    <IconCircleCheck sx={{ mr: 2, color: "dark-green" }} />
                  )}
                </Flex>
              )}
            </Grid>
          )
        )}
      </Grid>
    </Card>
  );
};

export default Points;
