/**
 *
 *
 * <About />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Card, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import H2 from "components/H2";
import H5 from "components/H5";
import { RuleElement } from "services/ChallengeService";

interface AboutProps {
  rules: RuleElement[];
  description: string;
}

const About: FunctionComponent<AboutProps> = ({ description, rules }) => {
  return (
    <Card
      sx={{
        bg: "white",
        p: 6,
      }}
    >
      <H2>About</H2>
      <Divider sx={{ my: 5 }} />
      <Text sx={{ color: "dark-gray", mb: 5 }}>{description}</Text>
      {rules.length > 0 && (
        <Box>
          <H5>Rules and Regulations</H5>
          {rules.map(({ title, id }, index) => (
            <Text key={id} sx={{ color: "dark-gray" }}>{`${
              index + 1
            }. ${title}`}</Text>
          ))}
        </Box>
      )}
    </Card>
  );
};

export default About;
