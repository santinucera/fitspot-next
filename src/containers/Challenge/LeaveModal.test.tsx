/**
 *
 *
 * Tests for <LeaveModal />
 *
 *
 */

import React from "react";
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import LeaveModal from "./LeaveModal";
import ModalProvider, { useModal } from "containers/Modal/Modal";

describe("<LeaveModal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<LeaveModal handleLeave={() => {}} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should hide modal if user clicks cancel button", async () => {
    const El = () => {
      const { setModal } = useModal();
      return (
        <button
          data-testid="open-modal-button"
          onClick={() => setModal(<LeaveModal handleLeave={() => {}} />)}
        >
          Click
        </button>
      );
    };
    const { getByTestId } = render(
      <ModalProvider>
        <El />
      </ModalProvider>
    );
    await waitFor(() => fireEvent.click(getByTestId("open-modal-button")));
    await waitFor(() => getByTestId("modal"));
    await waitFor(() => fireEvent.click(getByTestId("cancel-button")));
    await waitForElementToBeRemoved(() => getByTestId("modal"));
  });
  it("should hide modal if user clicks submit button", async () => {
    const El = () => {
      const { setModal, removeModal } = useModal();
      return (
        <button
          data-testid="open-modal-button"
          onClick={() => setModal(<LeaveModal handleLeave={removeModal} />)}
        >
          Click
        </button>
      );
    };
    const { getByTestId } = render(
      <ModalProvider>
        <El />
      </ModalProvider>
    );
    await waitFor(() => fireEvent.click(getByTestId("open-modal-button")));
    await waitFor(() => getByTestId("modal"));
    await waitFor(() => fireEvent.click(getByTestId("submit-button")));
    await waitForElementToBeRemoved(() => getByTestId("modal"));
  });
});
