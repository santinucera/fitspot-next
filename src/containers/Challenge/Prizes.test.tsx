/**
 *
 *
 * Tests for <Prizes />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Prizes from "./Prizes";

describe("<Prizes />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Prizes prizes={[]} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should not render nothing if empty prices", () => {
    const { queryByText} = render(<Prizes prizes={[]} />);
    expect(queryByText('Prizes')).toBeNull()
  });
});
