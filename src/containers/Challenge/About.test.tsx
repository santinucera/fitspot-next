/**
 *
 *
 * Tests for <About />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import About from "./About";
import { createChallenge } from "test-utils/challenge-service-test-utils";

describe("<About />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<About description={""} rules={[]} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render description", () => {
    const description = "Some description";
    const { getByText } = render(
      <About description={description} rules={[]} />
    );
    expect(getByText(description)).toBeInTheDocument();
  });
  it("should render rules", () => {
    const { rules } = createChallenge();
    const title = rules[0].title;
    const { getByText } = render(<About description={""} rules={rules} />);
    expect(getByText(`1. ${title}`)).toBeInTheDocument();
  });
});
