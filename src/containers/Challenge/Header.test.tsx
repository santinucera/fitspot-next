/**
 *
 *
 * Tests for <Header />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Header from "./Header";
import dayjs from "dayjs";

describe("<Header />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Header
        isJoined={true}
        name="Name"
        description="Silence is golden"
        handleRSVP={() => {}}
        startDate={dayjs()}
        endDate={dayjs()}
        hasEnded
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
