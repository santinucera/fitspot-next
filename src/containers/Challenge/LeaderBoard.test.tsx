/**
 *
 *
 * Tests for <LeaderBoard />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import LeaderBoard from "./LeaderBoard";
import { User } from "services/types";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

describe("<LeaderBoard />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <LeaderBoard
        hasParticipants
        isActive
        isJoined
        leaderboard={[]}
        hasEnded
        handleRSVP={jest.fn()}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
