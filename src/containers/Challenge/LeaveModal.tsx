/**
 *
 *
 * <LeaveModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Text, Heading, Flex, Button } from "theme-ui";
import { FunctionComponent } from "react";
import { useModal } from "containers/Modal/Modal";

interface LeaveModalProps {
  handleLeave: () => void;
}

const LeaveModal: FunctionComponent<LeaveModalProps> = ({ handleLeave }) => {
  const { removeModal } = useModal();
  return (
    <Flex
      sx={{
        flexDirection: "column",
        alignItems: "center",
        height: 300,
        justifyContent: "center",
      }}
    >
      <Heading sx={{ fontSize: 6, mb: 2 }}>Leave challenge</Heading>
      <Text sx={{ mb: 7, textAlign: "center" }}>
        Are you sure you want to leave this challenge? All your points and
        ranking will be permanently deleted.
      </Text>
      <Flex sx={{ width: "100%", justifyContent: "center" }}>
        <Button
          variant="danger"
          data-testid="submit-button"
          sx={{ width: 150, mr: 2 }}
          onClick={handleLeave}
        >
          Leave challenge
        </Button>
        <Button
          variant="underlined"
          data-testid="cancel-button"
          sx={{ width: 150 }}
          onClick={removeModal}
        >
          Cancel
        </Button>
      </Flex>
    </Flex>
  );
};

export default LeaveModal;
