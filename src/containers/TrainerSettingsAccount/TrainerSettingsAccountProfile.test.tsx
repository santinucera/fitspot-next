/**
 *
 *
 * Tests for <TrainerSettingsAccountProfile />
 *
 *
 */

import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import TrainerSettingsAccountProfile from "./TrainerSettingsAccountProfile";
import { ToastProvider } from "hooks/useToast";
import {
  createUser,
  setUserProfileErrorResponse,
} from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { UserType } from "services/UserService";

describe("<TrainerSettingsAccountProfile />", () => {
  let firstName: HTMLInputElement | null;
  let lastName: HTMLInputElement | null;
  let phone: HTMLInputElement | null;
  let address: HTMLInputElement | null;
  let city: HTMLInputElement | null;
  let zipcode: HTMLInputElement | null;
  const trainerUser = createUser(UserType.trainer);

  beforeEach(() => {
    queryCache.setQueryData("user", {
      ...trainerUser,
      firstName: null,
      lastName: null,
    });
    render(
      <ToastProvider>
        <TrainerSettingsAccountProfile />
      </ToastProvider>
    );
    firstName = screen.getByTestId("firstName").querySelector("input");
    lastName = screen.getByTestId("lastName").querySelector("input");
    phone = screen.getByTestId("phone").querySelector("input");
    address = screen.getByTestId("address").querySelector("input");
    city = screen.getByTestId("city").querySelector("input");
    zipcode = screen.getByTestId("zipcode").querySelector("input");
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSettingsAccountProfile />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should change a profile info", async () => {
    fireEvent.change(firstName!, { target: { value: "asdfasdf" } });
    fireEvent.change(lastName!, { target: { value: "asdfasdf" } });
    fireEvent.change(phone!, { target: { value: "1234567890" } });
    fireEvent.change(address!, { target: { value: "asdfasdf" } });
    fireEvent.change(city!, { target: { value: "asdfasdf" } });
    fireEvent.change(zipcode!, { target: { value: "33055" } });

    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      screen.getByTestId(/toast-item/i);
    });
    expect(screen.getByTestId(/toast-item/i)).toHaveTextContent("Success!");
  });

  it("should show form errors", async () => {
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your first name."
      );
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your last name."
      );
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your phone number."
      );
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your address."
      );
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your city."
      );
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Please enter your zip code."
      );
    });
    fireEvent.change(firstName!, { target: { value: "asdfasdf" } });
    fireEvent.change(lastName!, { target: { value: "asdfasdf" } });
    fireEvent.change(phone!, { target: { value: "1234567" } });
    fireEvent.change(address!, { target: { value: "asdfasdf" } });
    fireEvent.change(city!, { target: { value: "asdfasdf" } });
    fireEvent.change(zipcode!, { target: { value: "3355" } });
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Phone must be at least 10 numbers."
      );
    });
    await waitFor(() => {
      expect(screen.getByTestId("change-profile-form")).toHaveTextContent(
        "Zip code must be at least 5 numbers."
      );
    });
  });

  it("should show server errors", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserProfileErrorResponse();
    fireEvent.change(firstName!, { target: { value: "asdfasdf" } });
    fireEvent.change(lastName!, { target: { value: "asdfasdf" } });
    fireEvent.change(phone!, { target: { value: "1234567890" } });
    fireEvent.change(address!, { target: { value: "asdfasdf" } });
    fireEvent.change(city!, { target: { value: "asdfasdf" } });
    fireEvent.change(zipcode!, { target: { value: "33055" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent("Error!");
    });
    expect(global.console.error).toHaveBeenCalled();
  });
});
