/*
 *
 *
 * TrainerSettingsAccount
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import TrainerSettingsAccountProfile from "./TrainerSettingsAccountProfile";
import TrainerSettingsAccountPassword from "./TrainerSettingsAccountPassword";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */

const TrainerSettingsAccount: FunctionComponent = () => {
  return (
    <Box sx={{ width: ["100%", "40%"] }}>
      <TrainerSettingsAccountProfile />
      <TrainerSettingsAccountPassword />
    </Box>
  );
};

export default TrainerSettingsAccount;
