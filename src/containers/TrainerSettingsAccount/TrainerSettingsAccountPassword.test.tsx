/**
 *
 *
 * Tests for <TrainerSettingsAccountPassword />
 *
 *
 */

import React from "react";
import TrainerSettingsAccountPassword from "./TrainerSettingsAccountPassword";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { ToastProvider } from "hooks/useToast";
import {
  createUser,
  setUserPasswordResetResponse,
} from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { UserType } from "services/UserService";

describe("<TrainerSettingsAccountPassword />", () => {
  let oldPasswordInput: HTMLElement;
  let newPasswordInput: HTMLElement;

  beforeEach(() => {
    queryCache.setQueryData("user", createUser(UserType.trainer));
    render(
      <ToastProvider>
        <TrainerSettingsAccountPassword />
      </ToastProvider>
    );
    oldPasswordInput = screen.getByPlaceholderText("Old Password");
    newPasswordInput = screen.getByPlaceholderText("New Password");
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSettingsAccountPassword />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should change a users password", async () => {
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      screen.getByTestId(/toast-item/i);
    });
    expect(screen.getByTestId(/toast-item/i)).toHaveTextContent("Success!");
  });

  it("should show form errors", async () => {
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Please enter your old password."
      );
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Please enter a new password."
      );
    });
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasd" } });
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Password must be at least 8 characters."
      );
    });
  });

  it("should show toast when update password fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserPasswordResetResponse(500);
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent(
        "Error!Failed to change password."
      );
    });
    expect(global.console.error).toHaveBeenCalled();
  });

  it("should show specific message when old password does not match", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserPasswordResetResponse(400, undefined, "old password mismatch");
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent(
        "Error!Old password does not match."
      );
    });
    expect(global.console.error).toHaveBeenCalled();
  });
});
