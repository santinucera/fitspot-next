/**
 *
 *
 * <TrainerSettingsAccountPassword />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button } from "theme-ui";
import { FunctionComponent } from "react";
import { useForm } from "react-hook-form";
import TrainerInput from "components/TrainerInput";
import { useQueryUserGetData } from "services/UserService";
import { PasswordPatchRequestData } from "services/UserService";
import useUpdatePassword from "hooks/useUpdatePassword";

const defaultValues = {
  oldPassword: "",
  newPassword: "",
};

const TrainerSettingsAccountPassword: FunctionComponent = () => {
  const trainer = useQueryUserGetData();
  const email = trainer?.email || "";

  const { handleSubmit, control, errors, reset } = useForm<
    PasswordPatchRequestData
  >({
    defaultValues: {
      email,
      ...defaultValues,
    },
  });
  const [updatePassword, { isLoading }] = useUpdatePassword(() =>
    reset({
      email,
      ...defaultValues,
    })
  );

  const onSubmit = handleSubmit((data) => {
    updatePassword(data);
  });

  return (
    <Box as="form" onSubmit={onSubmit} data-testid="change-password-form">
      <TrainerInput
        control={control}
        label="Email"
        name="email"
        disabled={true}
        defaultValue=""
        helperText="You cannot edit your email"
      />
      <TrainerInput
        control={control}
        label="Old Password"
        name="oldPassword"
        type="password"
        placeholder="Old Password"
        errors={errors}
        defaultValue=""
        rules={{
          minLength: {
            value: 8,
            message: "Password must be at least 8 characters.",
          },
          required: {
            value: true,
            message: "Please enter your old password.",
          },
        }}
      />
      <TrainerInput
        control={control}
        label="New Password"
        name="newPassword"
        type="password"
        defaultValue=""
        placeholder="New Password"
        errors={errors}
        rules={{
          minLength: {
            value: 8,
            message: "Password must be at least 8 characters.",
          },
          required: {
            value: true,
            message: "Please enter a new password.",
          },
        }}
      />
      <Button type="submit" mb={9} disabled={isLoading}>
        {isLoading ? "Saving..." : "Save Changes"}
      </Button>
    </Box>
  );
};

export default TrainerSettingsAccountPassword;
