/**
 *
 *
 * <TrainerSettingsAccountProfile />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { useForm } from "react-hook-form";
import TrainerInput from "components/TrainerInput";
import Select from "components/Select";
import { useQueryUserGetData } from "services/UserService";
import { useMutationUserPatchService } from "services/UserService";
import useUsStates from "hooks/useUsStates";
import { queryCache } from "react-query";
import useToast from "hooks/useToast";

const TrainerSettingsAccountProfile: FunctionComponent = () => {
  const trainer = useQueryUserGetData();
  const states = useUsStates();

  const { handleSubmit, control, errors } = useForm({
    defaultValues: {
      firstName: trainer?.firstName || "",
      lastName: trainer?.lastName || "",
      phone: trainer?.phone || "",
      address: trainer?.trainer?.address || "",
      city: trainer?.trainer?.city || "",
      state: trainer?.trainer?.state || "",
      zipcode: trainer?.trainer?.zipcode || "",
    },
  });

  const { addToast } = useToast();

  const [handleUserPatch, { isLoading }] = useMutationUserPatchService({
    mutationConfig: {
      onSuccess(data) {
        addToast({
          title: "Success!",
          description: "Your profile has been updated.",
          type: "success",
        });
        queryCache.setQueryData("user", data);
      },
      onError(error) {
        addToast({
          title: "Error!",
          description: error?.response?.data?.message,
          type: "error",
        });
      },
    },
  });

  const onSubmit = handleSubmit((data) => {
    handleUserPatch(data);
  });

  return (
    <Box as="form" onSubmit={onSubmit} data-testid="change-profile-form">
      <TrainerInput
        data-testid="firstName"
        control={control}
        errors={errors}
        label="First Name"
        name="firstName"
        rules={{
          required: {
            value: true,
            message: "Please enter your first name.",
          },
        }}
      />
      <TrainerInput
        data-testid="lastName"
        control={control}
        errors={errors}
        label="Last Name"
        name="lastName"
        rules={{
          required: {
            value: true,
            message: "Please enter your last name.",
          },
        }}
      />
      <TrainerInput
        data-testid="phone"
        control={control}
        errors={errors}
        label="Phone"
        name="phone"
        type="number"
        rules={{
          minLength: {
            value: 10,
            message: "Phone must be at least 10 numbers.",
          },
          required: {
            value: true,
            message: "Please enter your phone number.",
          },
        }}
      />
      <TrainerInput
        data-testid="address"
        control={control}
        errors={errors}
        label="Address"
        name="address"
        rules={{
          required: {
            value: true,
            message: "Please enter your address.",
          },
        }}
      />
      <Flex sx={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Box sx={{ width: "40%" }}>
          <TrainerInput
            data-testid="city"
            errors={errors}
            control={control}
            label="City"
            name="city"
            rules={{
              required: {
                value: true,
                message: "Please enter your city.",
              },
            }}
          />
        </Box>
        <Box sx={{ width: "20%" }}>
          <Select
            data-testid="state"
            control={control}
            options={states.map((s) => ({
              value: s.abbreviation,
              label: s.abbreviation,
            }))}
            label="State"
            name="state"
          />
        </Box>
        <Box sx={{ width: "30%" }}>
          <TrainerInput
            data-testid="zipcode"
            control={control}
            label="Zip Code"
            name="zipcode"
            type="number"
            errors={errors}
            rules={{
              minLength: {
                value: 5,
                message: "Zip code must be at least 5 numbers.",
              },
              required: {
                value: true,
                message: "Please enter your zip code.",
              },
            }}
          />
        </Box>
      </Flex>
      <Button type="submit" mb={9} disabled={isLoading}>
        {isLoading ? "Saving..." : "Save Changes"}
      </Button>
    </Box>
  );
};

export default TrainerSettingsAccountProfile;
