/**
 *
 *
 * Tests for <TrainerSettingsAccount />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import TrainerSettingsAccount from "./TrainerSettingsAccount";
import { createUser } from "test-utils/user-service-test-utils";
import { User } from "services/types";
import { queryCache } from "react-query";
import { UserType } from "services/UserService";

describe("<TrainerSettingsAccount />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser(UserType.trainer);
    queryCache.setQueryData("user", () => user);
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSettingsAccount />);
    expect(spy).not.toHaveBeenCalled();
  });
});
