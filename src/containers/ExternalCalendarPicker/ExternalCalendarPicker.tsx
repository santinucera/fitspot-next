/**
 *
 *
 * <ExternalCalendarPicker />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text, Button } from "theme-ui";
import { FunctionComponent, useState } from "react";
import { queryCache } from "react-query";
import CalendarOptions from "components/CalendarOptions";
import H2 from "components/H2";
import IconCircleCheck from "components/IconCircleCheck";
import { useQueryUserGetData } from "services/UserService";
import { useMutationCalendarPostSettingsService } from "services/CalendarService";
import useExternalCalendar, {
  CalendarEventOptions,
} from "hooks/useExternalCalendar";

interface ExternalCalendarPickerProps {
  handleClose: () => void;
  eventOptions: Omit<CalendarEventOptions, "calendarPreference">;
}

const ExternalCalendarPicker: FunctionComponent<ExternalCalendarPickerProps> = ({
  handleClose,
  eventOptions,
}) => {
  const user = useQueryUserGetData();
  const [calendarState, setCalendarState] = useState({
    calendarPreference: user.calendarPreference,
    makeDefaultCalendar: true,
  });

  const { calendarPreference, makeDefaultCalendar } = calendarState;

  const handleCalendarSelection = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { value } = event.target;
    setCalendarState({
      calendarPreference: Number(value),
      makeDefaultCalendar,
    });
  };

  const [mutateCalendarPreference] = useMutationCalendarPostSettingsService({
    onSuccess() {
      // Refetch user
      queryCache.invalidateQueries("user");
    },
  });
  const { createExternalCalendarEvent } = useExternalCalendar();

  const handleCalendarState = () => {
    setCalendarState({
      calendarPreference,
      makeDefaultCalendar: !makeDefaultCalendar,
    });
  };
  const handleAddToCalendar = () => {
    if (makeDefaultCalendar)
      mutateCalendarPreference({ preference: calendarPreference });
    createExternalCalendarEvent({
      ...eventOptions,
      calendarPreference,
    });
    handleClose();
  };

  return (
    <Flex
      sx={{
        p: 5,
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <H2 sx={{ fontSize: 6, mt: 4 }}>Select a calendar</H2>
      <Text>
        Please select the calendar application you would like to export this to.
      </Text>
      <Flex
        sx={{
          my: 3,
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        <CalendarOptions
          handleCalendarSelection={handleCalendarSelection}
          calendarPreference={calendarPreference}
        />
      </Flex>
      <Flex
        onClick={handleCalendarState}
        sx={{
          cursor: "pointer",
          alignItems: "center",
          my: 2,
        }}
      >
        {makeDefaultCalendar ? (
          <IconCircleCheck />
        ) : (
          <Box
            sx={{
              borderRadius: "100%",
              width: 24,
              height: 24,
              border: "1px solid",
            }}
          ></Box>
        )}
        <Text sx={{ ml: 2 }}>Make this selection my default calendar </Text>
      </Flex>
      <Button
        sx={{
          my: 3,
        }}
        onClick={handleAddToCalendar}
        data-testid="add-to-calendar-button"
        disabled={calendarPreference === 0}
      >
        Add to calendar
      </Button>
    </Flex>
  );
};

export default ExternalCalendarPicker;
