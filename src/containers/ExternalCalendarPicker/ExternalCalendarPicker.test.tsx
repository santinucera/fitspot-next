/**
 *
 *
 * Tests for <ExternalCalendarPicker />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import ExternalCalendarPicker from "./ExternalCalendarPicker";
import dayjs from "dayjs";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

describe("<ExternalCalendarPicker />", () => {
  beforeEach(async () => {
    queryCache.setQueryData("user", () => createUser());
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <ExternalCalendarPicker
        eventOptions={{
          id: 1,
          title: "name",
          description: "description",
          startDate: dayjs(),
          endDate: dayjs(),
          path: `/dashboard/sessions/1`,
        }}
        handleClose={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
