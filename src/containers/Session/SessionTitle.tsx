/**
 *
 *
 * <SessionTitle />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Flex, Text, Button } from "theme-ui";
import { Fragment, FunctionComponent, useMemo } from "react";
import IconCalendar from "components/IconCalendar";
import IconUsers from "components/IconUsers";
import PillarChip from "components/PillarChip";
import IconBolt from "components/IconBolt";
import IconCheckMark from "components/IconCheckMark";
import IconX from "components/IconX";
import IconPlus from "components/IconPlus";
import { useNavigate, useParams } from "react-router-dom";
import { useQuerySessionGetData } from "services/SessionService";
import useSessionStatus from "hooks/useSessionStatus";
import { useQueryUserGetData } from "services/UserService";
import { useModal } from "containers/Modal/Modal";
import InviteColleague from "components/InviteColleague";
import WatchTogetherModal from "containers/WatchTogetherModal";
import useSessionRsvp from "hooks/useSessionRSVP";
import useToast from "hooks/useToast";
import { queryCache } from "react-query";
import SessionRatingButton from "components/SessionRatingButton";
import IconReplay from "components/IconReplay";
import { CalendarPreference } from "services/CalendarService";
import { createEvent } from "hooks/useExternalCalendar";
import ExternalCalendarPicker from "containers/ExternalCalendarPicker";
interface SessionTitleProps {
  isGroupChat: boolean;
}

const SessionTitle: FunctionComponent<SessionTitleProps> = ({
  isGroupChat,
}) => {
  const navigate = useNavigate();
  const { sessionId } = useParams();
  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const user = useQueryUserGetData();
  const session = useQuerySessionGetData({ sessionId });

  const hasDefaultCalendar =
    user.calendarPreference !==
    CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED;

  if (!session) {
    throw new Error(
      `There is no session cache for a session with id: ${sessionId}`
    );
  }

  const {
    name,
    activityCategory,
    participants,
    startDate,
    endDate,
    description,
    trainer,
    isLiveSession,
    id,
    isReplay,
    onDemandActive,
  } = session;

  const openWatchTogetherModal = () => {
    setModal(
      <WatchTogetherModal
        isLive={isSessionStartingSoon || isSessionLive}
        showInvite={true}
        handleClose={removeModal}
        id={id}
        title={name}
        description={description}
        startDate={startDate}
        endDate={endDate}
        type="session"
      />
    );
  };

  const openInviteColleagueModal = () => {
    setModal(
      <InviteColleague
        title="Add a Colleague to this Session"
        message="Together is always better! Copy and share the link below to invite
          colleagues to watch this session together in a private group. Only
          colleagues with the unique link below will be able to join you."
        url={window.location.href}
      />
    );
  };

  const handleAddToCalendar = () => {
    if (hasDefaultCalendar) {
      createEvent({
        id,
        title: name,
        description,
        startDate,
        endDate,
        calendarPreference: user.calendarPreference,
        path: `/dashboard/sessions/${session.id}`,
      });
    } else {
      setModal(
        <ExternalCalendarPicker
          eventOptions={{
            id,
            title: name,
            description,
            startDate,
            endDate,
            path: `/dashboard/sessions/${session.id}`,
          }}
          handleClose={removeModal}
        />
      );
    }
  };

  const {
    isSessionLive,
    isSessionStartingSoon,
    isSessionCompleted,
    isSessionUpcoming,
  } = useSessionStatus({
    startDate: session?.startDate,
    endDate: session?.endDate,
  });

  /**
   * Return the current user if a participant.
   */
  const viewerParticipant = useMemo(() => {
    return session.participants.find(
      (participant) => participant?.user.id === user?.id
    );
  }, [session, user]);

  /**
   * If the user is a participant and has is reserved.
   */
  const isRSVP = viewerParticipant?.isRSVP || false;
  const wasAttended = viewerParticipant?.wasAttended || false;

  const { handleSessionRSVP, handleSessionCancelRSVP } = useSessionRsvp({
    urlParams: {
      sessionId,
    },
    isRSVP,
  });

  const ratingPoints = viewerParticipant?.ratingPoints || null;
  const ratingStreamQuality = viewerParticipant?.ratingStreamQuality || null;
  const ratingTrainer = viewerParticipant?.ratingTrainer || null;
  const ratingLocation = viewerParticipant?.ratingLocation || null;

  const participantCount = participants.length;

  return (
    <Flex
      data-testid="session-title"
      sx={{
        pb: 7,
        borderBottom: 1,
        borderColor: "gray",
        borderStyle: "solid",
        flexDirection: ["column", "row"],
      }}
    >
      <Flex sx={{ flexDirection: "column" }}>
        <Flex sx={{ flexDirection: ["column", "column", "row"] }}>
          <Flex
            sx={{
              borderStyle: "solid",
              flexDirection: "column",
              justifyContent: "center",
              pl: 0,
              pr: 7,
              mr: 8,
            }}
          >
            <PillarChip
              pillar={activityCategory}
              sx={{ position: "relative", top: -1, left: 0 }}
            />
            <Heading as="h2" sx={{ fontSize: 4 }}>
              {name}
            </Heading>
          </Flex>
        </Flex>

        <Flex
          sx={{
            mt: 5,
            flexDirection: ["column", "row"],
            color: "darker-gray",
          }}
        >
          <Flex sx={{ mr: 6, alignItems: "center" }}>
            {isSessionLive || isSessionStartingSoon ? (
              <Flex
                data-testid="session-status"
                sx={{
                  color: "forest-green",
                  fontWeight: 700,
                  alignItems: "center",
                  lineHeight: 0,
                }}
              >
                <IconBolt size={"14px"} />
                {isSessionLive && "LIVE"}
                {isSessionStartingSoon && "STARTING SOON"}
              </Flex>
            ) : (
              <Fragment>
                <Flex sx={{ mr: 2, flex: "none" }}>
                  <IconCalendar />
                </Flex>
                <Text data-testid="session-date">
                  <span sx={{ fontWeight: 700 }}>
                    {startDate.format("MMMM DD")}
                  </span>
                  {" | "}
                  {`${startDate.format("h:mm")} - ${endDate.format("h:mma")}`}
                </Text>
              </Fragment>
            )}
          </Flex>
          <Flex sx={{ mr: 6, mt: [2, 0, 0], alignItems: "center" }}>
            <Flex sx={{ mr: 2, flex: "none" }}>
              <IconUsers />
            </Flex>
            <Box>
              <Text>
                <span sx={{ fontWeight: 700 }}>
                  {participantCount} Member
                  {(participantCount === 0 || participantCount > 1) && "s"}
                </span>{" "}
                {isSessionCompleted ? "Attended" : "Attending"}
              </Text>
            </Box>
          </Flex>
          {isReplay && (
            <Flex sx={{ mr: 8, mt: [2, 0, 0], alignItems: "center" }}>
              <Flex sx={{ mr: 2, flex: "none" }}>
                <IconReplay />
              </Flex>
              <Text sx={{ fontWeight: 600 }}>REPLAY</Text>
            </Flex>
          )}
        </Flex>
      </Flex>

      {isSessionUpcoming && (
        <Flex
          sx={{
            ml: [0, "auto"],
            minWidth: "auto",
            mt: [4, "auto"],
            flexDirection: ["column", "row"],
          }}
        >
          {isRSVP ? (
            <Flex
              sx={{
                flexDirection: "column",
                alignItems: "flex-end",
              }}
            >
              <Flex
                sx={{
                  flexDirection: ["column", "row"],
                  width: "100%",
                }}
              >
                <Button
                  data-testid="watch-together-button"
                  sx={{
                    mr: 2,
                    mb: [2],
                    width: ["100%"],
                  }}
                  onClick={openWatchTogetherModal}
                >
                  Watch together
                </Button>
                <Button
                  sx={{
                    width: ["100%"],
                  }}
                  data-testid="cancel-rsvp-button"
                  variant="secondary"
                  onClick={() => handleSessionCancelRSVP(session.id)}
                >
                  <IconCheckMark sx={{ mr: 2 }} />
                  Reserved
                </Button>
              </Flex>
              <Text
                onClick={handleAddToCalendar}
                sx={{
                  textDecoration: "underline",
                  cursor: "pointer",
                }}
              >
                add to calendar
              </Text>
            </Flex>
          ) : (
            <Button
              data-testid="rsvp-button"
              onClick={() =>
                handleSessionRSVP({ sessionId: session.id, isRSVP: true })
              }
              sx={{ width: ["100%"] }}
            >
              Reserve a spot
            </Button>
          )}
        </Flex>
      )}

      {isSessionCompleted && (
        <Flex
          sx={{
            ml: [0, "auto"],
            minWidth: "auto",
            mt: [4, "auto"],
            flexDirection: ["column", "row"],
          }}
        >
          {onDemandActive && (
            <Button
              data-testid="on-demand-button"
              onClick={() => navigate(`/dashboard/on-demand/${sessionId}`)}
              variant="secondary"
              sx={{
                width: ["100%"],
                mb: [2],
              }}
            >
              Watch on-demand
            </Button>
          )}
          {wasAttended && (
            <Box sx={{ ml: [0, 3], width: ["100%"] }}>
              <SessionRatingButton
                ratings={{
                  ratingPoints,
                  ratingStreamQuality,
                  ratingTrainer,
                  ratingLocation,
                }}
                sessionName={name || ""}
                avatar={trainer?.avatar || ""}
                startDate={startDate}
                endDate={endDate}
                isLiveSession={isLiveSession}
                sessionId={id}
                onSuccess={() => {
                  queryCache.invalidateQueries(["session"]);
                  addToast({
                    title: "Success!",
                    description: "You successfully rated the session.",
                    type: "success",
                  });
                  removeModal();
                }}
                onError={() => {
                  addToast({
                    title: "Error!",
                    description: "An error occurred, please try again.",
                    type: "error",
                  });
                }}
              />
            </Box>
          )}
        </Flex>
      )}

      {(isSessionStartingSoon || isSessionLive) && (
        <Flex
          sx={{
            ml: [0, "auto"],
            minWidth: "auto",
            mt: [4, "auto"],
            flexDirection: ["column", "row"],
          }}
        >
          {isGroupChat ? (
            <Fragment>
              <Button
                onClick={openInviteColleagueModal}
                sx={{ mr: 3, width: ["100%"], mb: [2] }}
              >
                <IconPlus
                  sx={{
                    mr: 2,
                  }}
                />
                Invite colleague
              </Button>
              <Button
                onClick={() => navigate(`/dashboard/sessions/${sessionId}`)}
                variant="secondary"
                sx={{ mr: 3, width: ["100%"] }}
              >
                <IconX
                  sx={{
                    mr: 2,
                  }}
                />
                LEAVE GROUP
              </Button>
            </Fragment>
          ) : (
            <Button
              data-testid="watch-together-button"
              onClick={openWatchTogetherModal}
              sx={{ width: ["100%"] }}
            >
              Watch together
            </Button>
          )}
        </Flex>
      )}
    </Flex>
  );
};

export default SessionTitle;
