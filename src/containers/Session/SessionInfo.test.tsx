/**
 *
 *
 * Tests for <SessionInfo />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SessionInfo from "./SessionInfo";

describe("<SessionInfo />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SessionInfo description="foo bar" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
