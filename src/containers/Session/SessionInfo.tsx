/**
 *
 *
 * <SessionInfo />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Text } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import IconDownload from "components/IconDownload";
import { FileUrl } from "services/types";

interface SessionInfoProps {
  description: string | null;
  additionalInfo: string | null;
  attachment: FileUrl | null;
}

const SessionInfo: FunctionComponent<SessionInfoProps> = ({
  description,
  additionalInfo,
  attachment,
}) => {
  return (
    <Box>
      <Heading
        as="h2"
        sx={{
          pb: 5,
          fontSize: 1,
          fontWeight: 400,
        }}
      >
        Description
      </Heading>
      <Text as="p" sx={{ color: "medium-gray" }}>
        {description}
      </Text>
      {additionalInfo && (
        <Fragment>
          <Heading
            as="h2"
            sx={{
              pb: 5,
              pt: 7,
              fontSize: 1,
              fontWeight: 400,
            }}
          >
            Additional Info
          </Heading>
          <Text as="p" sx={{ color: "medium-gray" }}>
            {additionalInfo}
          </Text>
        </Fragment>
      )}
      {attachment && (
        <Fragment>
          <Heading
            as="h2"
            sx={{
              pb: 5,
              pt: 7,
              fontSize: 1,
              fontWeight: 400,
            }}
          >
            Get more information:
          </Heading>
          <a
            href={attachment.url || undefined}
            target="_blank"
            rel="noopener noreferrer"
            sx={{
              variant: "buttons.secondary",
            }}
          >
            <IconDownload size={15} sx={{ mr: 2 }} />
            Download
          </a>
        </Fragment>
      )}
    </Box>
  );
};

export default SessionInfo;
