/**
 *
 *
 * Tests for Session
 *
 *
 */

import React from "react";
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
  screen,
} from "@testing-library/react";
import dayjs from "dayjs";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import Session from "./Session";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import {
  createSession,
  setSessionResponse,
  setSessionRsvpResponse,
} from "test-utils/session-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { ToastProvider } from "hooks/useToast";
import { User } from "services/types";
import ModalProvider from "containers/Modal";
import { MemoryRouter, Route, Routes } from "react-router-dom";

const { getByTestId, getByText, queryByTestId } = screen;
const mockReportRSVP = jest.fn();
const mockReportAttended = jest.fn();
const mockReportJoinPrivateGroup = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportRSVP: mockReportRSVP,
    reportAttended: mockReportAttended,
    reportJoinPrivateGroup: mockReportJoinPrivateGroup,
  }))
);

describe("<Session />", () => {
  let user: User;
  beforeEach(async () => {
    jest.clearAllMocks();
    jest.clearAllTimers();
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", async () => {
    const session = createSession();
    setSessionResponse(200, session);
    const spy = jest.spyOn(global.console, "error");
    renderWithMemoryRouter(
      "/:sessionId",
      ["/123"],
      <Session chatType="chat" />
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitFor(() => expect(getByText(session.name)).toBeInTheDocument());
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render error if session fails to load", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());
    setSessionResponse(500);
    renderWithMemoryRouter(
      "/:sessionId",
      ["/123"],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <Session chatType="chat" />
      </ReactQueryConfigProvider>
    );
    await waitFor(() => expect(getByTestId("spinner")).toBeInTheDocument());
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => expect(getByText("Error!")).toBeInTheDocument());
    expect(spy).toHaveBeenCalled();
  });

  it("should show the video when the session is in progress", async () => {
    let session = createSession();
    session = {
      ...session,
      date: dayjs().subtract(1, "m").toISOString(),
      dt_end: dayjs().add(1, "m").toISOString(),
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      ["/123"],
      <Session chatType="chat" />
    );
    await waitFor(() => {
      expect(getByTestId("video")).toBeInTheDocument();
    });
  });

  it("should open a modal before a session starts in group chat", async () => {
    let session = createSession();
    session = {
      ...session,
      date: dayjs().add(24, "h").toISOString(),
      dt_end: dayjs().add(25, "h").toISOString(),
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId/private/:userPublicId",
      ["/123/private/publicId123"],
      <ModalProvider>
        <Session chatType="private-group" />
      </ModalProvider>
    );
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(
        getByText("This session is not yet available")
      ).toBeInTheDocument();
    });
  });

  it("should open a model to choose group viewing/video chat", async () => {
    let session = createSession();
    session = {
      ...session,
      date: dayjs().subtract(1, "m").toISOString(),
      dt_end: dayjs().add(1, "m").toISOString(),
    };
    setSessionResponse(200, session);
    render(
      <MemoryRouter initialEntries={["/dashboard/sessions/123"]}>
        <ModalProvider>
          <Routes>
            <Route
              path={"/dashboard/sessions/:sessionId"}
              element={<Session chatType="chat" />}
            />
            <Route
              path="/dashboard/sessions/:sessionId/private/:userPublicId"
              element={<Session chatType="private-group" />}
            />
          </Routes>
        </ModalProvider>
      </MemoryRouter>
    );
    const watchTogetherBtn = await waitFor(() =>
      getByTestId("watch-together-button")
    );
    expect(watchTogetherBtn).toBeInTheDocument();
    fireEvent.click(watchTogetherBtn);
    await waitFor(() => expect(getByTestId("modal")).toBeInTheDocument());
    const joinPrivateGroupBtn = await waitFor(() =>
      getByTestId("join-private-group-button")
    );
    expect(joinPrivateGroupBtn).toBeInTheDocument();
    fireEvent.click(joinPrivateGroupBtn);
    const joinGroupBtn = await waitFor(() => getByTestId("join-group-button"));
    expect(joinGroupBtn).toBeInTheDocument();
    fireEvent.click(joinGroupBtn);
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(mockReportJoinPrivateGroup).toHaveBeenCalledWith({
      sessionId: session.id,
      userId: user.id,
      companyId: user.companyList[0].id,
    });
    await waitFor(() => {
      expect(getByTestId("group-video")).toBeInTheDocument();
    });
  });

  it("should show a toast and revert back to un-RSVP-d when RSVP request fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    // RSVP buttons only show if session is not in progress
    const date = dayjs().add(1, "day").toISOString();
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      date,
      dt_end: date,
      participants: [],
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <ToastProvider>
        <Session chatType="chat" />
      </ToastProvider>
    );
    const rsvpButton = await waitFor(() => getByTestId("rsvp-button"));
    setSessionRsvpResponse(500);
    fireEvent.click(rsvpButton);
    await waitFor(() =>
      expect(getByTestId("cancel-rsvp-button")).toBeInTheDocument()
    );
    await waitFor(() => expect(getByTestId("rsvp-button")).toBeInTheDocument());
    await waitFor(() => expect(getByTestId(/toast-item/)).toBeInTheDocument());
    // Do not report RSVP event
    expect(mockReportRSVP).not.toHaveBeenCalled();
  });

  it("should RSVP for the session and report to analytics", async () => {
    // RSVP buttons only show if session is not in progress
    const date = dayjs().add(1, "day").toISOString();
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      date,
      dt_end: date,
      participants: [],
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <ModalProvider>
        <Session chatType="chat" />
      </ModalProvider>
    );
    const rsvpButton = await waitFor(() => getByTestId("rsvp-button"));
    setSessionRsvpResponse(200);
    fireEvent.click(rsvpButton);
    await waitFor(() => getByTestId("modal"));
    await waitFor(() =>
      expect(mockReportRSVP).toHaveBeenCalledWith({
        isRSVP: true,
        userId: user.id,
        sessionId: session.id,
        companyId: session.companyId,
        category: "Play",
      })
    );
  });

  it("should mark the session as attended and report to analytics on load if the session is in progress", async () => {
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      // Session is in progress 15 minutes before `date` to 15 minutes after `dt_end`
      date: dayjs().subtract(1, "m").toISOString(),
      dt_end: dayjs().add(1, "m").toISOString(),
      liveSession: true,
      participants: [],
    };

    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <Session chatType="chat" />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() =>
      expect(mockReportAttended).toHaveBeenCalledWith({
        userId: user.id,
        sessionId: session.id,
        companyId: session.companyId,
        category: "Play",
      })
    );
  });

  it.skip("should check if the session is in progress every minute", async () => {
    // "modern" enables jest.setSystemTime but is not available in Jest 24.
    // CRA is somehow stalled on upgrade, but intends to ship v4 with Jest 26.
    jest.useFakeTimers("modern");
    const setIntervalSpy = jest.spyOn(global, "setInterval");
    let session = createSession();
    // Session is in progress 15 minutes before `date` to 15 minutes after `dt_end`
    // Set state date to 16 minutes in the future
    const date = dayjs().add(16, "minute");
    const dateISO = date.toISOString();
    session = {
      ...session,
      id: 1234,
      date: dateISO,
      dt_end: dateISO,
      liveSession: true,
      participants: [],
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <Session chatType="chat" />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    // Advance time forward one minute
    // This doesn't work in the current Jest version 24 that ships with CRA.
    jest.setSystemTime(dayjs().add(1, "minute").toDate().valueOf());
    jest.advanceTimersToNextTimer();
    // Not called, probably because system time hasn't advanced
    expect(setIntervalSpy).toHaveBeenCalledTimes(1);
    await waitFor(() => expect(mockReportAttended).toHaveBeenCalledTimes(1));
  });

  it("should show link if it's a live session with zoom url", async () => {
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      liveSession: true,
      liveSessionUrl: "https://zoom.us",
      date: dayjs().subtract(1, "m").toISOString(),
      dt_end: dayjs().add(1, "m").toISOString(),
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <Session chatType="chat" />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() =>
      expect(getByTestId("live-session-link")).toBeInTheDocument()
    );
  });

  it("should show video if it's a live session with no zoom url", async () => {
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      liveSession: true,
      date: dayjs().subtract(1, "m").toISOString(),
      dt_end: dayjs().add(1, "m").toISOString(),
    };
    setSessionResponse(200, session);
    renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <Session chatType="chat" />
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => expect(getByTestId("video")).toBeInTheDocument());
  });

  it("should show a modal notifying that the session has ended", async () => {
    let session = createSession();
    session = {
      ...session,
      id: 1234,
      liveSession: true,
      date: dayjs().subtract(24, "h").toISOString(),
      dt_end: dayjs().subtract(25, "h").toISOString(),
    };
    setSessionResponse(200, session);
    const { getByTestId } = renderWithMemoryRouter(
      "/:sessionId",
      [`/${session.id.toString()}`],
      <ModalProvider>
        <Session chatType="company-group" />
      </ModalProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(getByText("This session has ended")).toBeInTheDocument();
    });
  });
});
