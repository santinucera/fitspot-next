/**
 *
 *
 * Tests for <SessionTitle />
 *
 *
 */

import React from "react";
import SessionTitle from "./SessionTitle";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { createSessionModel } from "test-utils/session-service-test-utils";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import dayjs from "dayjs";
import { createParticipant, Session } from "services/SessionService";
import MockDate from "mockdate";
import ModalProvider from "containers/Modal";
import ErrorBoundary from "components/ErrorBoundary";

const { getByTestId, getByText } = screen;

const mockedNavigate = jest.fn();
jest.mock("react-router-dom", () => {
  const router = jest.requireActual("react-router-dom");
  return {
    ...router,
    useNavigate: () => mockedNavigate,
  };
});

const renderComponent = (
  data?: Partial<Session>,
  setSessionData: boolean = true,
  hasDefaultCalendar: boolean = false
) => {
  queryCache.setQueryData("user", {
    ...createUser(),
    calendarPreference: hasDefaultCalendar ? 2 : 0,
  });

  if (setSessionData)
    queryCache.setQueryData(
      ["session", { sessionId: "1" }],
      createSessionModel(data)
    );

  renderWithMemoryRouter(
    "/:sessionId",
    ["/1"],
    <ErrorBoundary>
      <ModalProvider>
        <SessionTitle isGroupChat={false} />
      </ModalProvider>
    </ErrorBoundary>
  );
};

beforeEach(() => {
  MockDate.set("2010-12-01T11:30:00.000Z");
  window.open = jest.fn();
});

afterAll(() => {
  MockDate.reset();
  jest.restoreAllMocks();
});

describe("<SessionTitle />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent();
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a pillar", () => {
    renderComponent({
      date: dayjs().add(24, "h").toISOString(),
      dt_end: dayjs().add(25, "h").toISOString(),
    });
    expect(getByTestId("pillar")).toHaveTextContent("Play");
  });

  it("should render a session date", () => {
    renderComponent({
      date: dayjs().add(24, "h").toISOString(),
      dt_end: dayjs().add(25, "h").toISOString(),
    });
    expect(getByTestId("session-date")).toHaveTextContent(
      "December 02 | 11:30 - 12:30pm"
    );
  });

  it("should render a live session status", () => {
    renderComponent({
      date: dayjs().subtract(10, "m").toISOString(),
      dt_end: dayjs().add(1, "h").toISOString(),
    });
    expect(getByTestId("session-status")).toHaveTextContent("LIVE");
  });

  it("should render a coming soon session status", () => {
    renderComponent({
      date: dayjs().add(10, "m").toISOString(),
      dt_end: dayjs().add(1, "h").toISOString(),
    });
    expect(getByTestId("session-status")).toHaveTextContent("STARTING SOON");
  });

  it("should render a button to reserve session and reserve a session", async () => {
    renderComponent({
      date: dayjs().add(24, "h").toISOString(),
      dt_end: dayjs().add(25, "h").toISOString(),
    });
    const rsvpBtn = getByTestId("rsvp-button");
    expect(rsvpBtn).toBeInTheDocument();
    fireEvent.click(rsvpBtn);
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(getByTestId("watch-together-button")).toBeInTheDocument();
      expect(getByTestId("cancel-rsvp-button")).toBeInTheDocument();
    });
  });

  it("should render a button to view an on-demand video if the session is completed", async () => {
    renderComponent({
      date: dayjs().subtract(24, "h").toISOString(),
      dt_end: dayjs().subtract(25, "h").toISOString(),
      onDemandActive: true,
      videoUrl: "/path",
    });
    const onDemandBtn = getByTestId("on-demand-button");
    expect(onDemandBtn).toBeInTheDocument();
    fireEvent.click(onDemandBtn);
    await waitFor(() => {
      expect(mockedNavigate).toHaveBeenLastCalledWith("/dashboard/on-demand/1");
    });
  });

  it("should throw an error if session data is not in query cache", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(() => {});
    renderComponent({}, false);
    await waitFor(() => {
      expect(spy).toHaveBeenCalled();
      expect(
        getByText("There is no session cache for a session with id: 1")
      ).toBeInTheDocument();
    });
  });

  it("should render a button to add session to calendar and open a modal to select default calendar when session is RSVP'd and upcoming", async () => {
    renderComponent({
      date: dayjs().add(24, "h").toISOString(),
      dt_end: dayjs().add(25, "h").toISOString(),
      participants: [createParticipant(createUser())],
    });
    const buttonText = "add to calendar";
    await waitFor(() => {
      expect(getByText(buttonText)).toBeInTheDocument();
    });
    fireEvent.click(getByText(buttonText));
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
    });
    fireEvent.click(getByTestId("calendar-option-input-google"));
    fireEvent.click(getByTestId("add-to-calendar-button"));
    expect(window.open).toHaveBeenCalled();
  });

  it("should render a button to add session to a users default calendar when session is RSVP'd and upcoming", async () => {
    renderComponent(
      {
        date: dayjs().add(24, "h").toISOString(),
        dt_end: dayjs().add(25, "h").toISOString(),
        participants: [createParticipant(createUser())],
      },
      true,
      true
    );
    const buttonText = "add to calendar";
    await waitFor(() => {
      expect(getByText(buttonText)).toBeInTheDocument();
    });
    fireEvent.click(getByText(buttonText));
    expect(window.open).toHaveBeenCalled();
  });
});
