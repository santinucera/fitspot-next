/**
 *
 *
 * <EmbedContainer />
 *
 *
 */

/** @jsx jsx */
import { jsx, Embed } from "theme-ui";
import { FunctionComponent } from "react";

interface VideoProps {
  src: string | null;
  fullHeight: boolean;
}

const EmbedContainer: FunctionComponent<VideoProps> = ({ src, fullHeight }) => {
  return (
    <Embed
      src={src || ""}
      data-testid="video"
      ratio={fullHeight ? 1 : 16 / 9}
    />
  );
};

export default EmbedContainer;
