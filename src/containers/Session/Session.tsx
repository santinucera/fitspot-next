/*
 *
 *
 * Session
 *
 *
 */

/** @jsx jsx */
import { useEffect, useMemo, Fragment } from "react";
import {
  jsx,
  Box,
  Flex,
  Heading,
  Link as ThemeLink,
  AspectRatio,
  Button,
  Text,
} from "theme-ui";
import { FunctionComponent } from "react";
import Page from "components/Page";
import SessionTitle from "./SessionTitle";
import { useParams } from "react-router-dom";
import SessionInfo from "./SessionInfo";
import EmbedContainer from "./EmbedContainer";
import Chat from "containers/Chat";
import Spinner from "components/Spinner";
import {
  useQuerySessionGetService,
  useMutationSessionAttendedPostService,
} from "services/SessionService";
import { useQueryUserGetData } from "services/UserService";
import ExpertInfoCard from "components/ExpertInfoCard";
import useSessionStatus from "hooks/useSessionStatus";
import H2 from "components/H2";
import H4 from "components/H4";
import OnDemandCarousel from "containers/OnDemandCarousel";
import useAnalytics from "hooks/useAnalytics";
import { useModal } from "containers/Modal/Modal";
import SessionTime from "components/SessionTime";
import { CalendarPreference } from "services/CalendarService";
import { createEvent } from "hooks/useExternalCalendar";

interface SessionProps {
  chatType: "chat" | "private-group" | "company-group";
}

const Session: FunctionComponent<SessionProps> = ({ chatType }) => {
  const isGroupChat =
    chatType === "company-group" || chatType === "private-group";
  const { sessionId, userPublicId } = useParams();
  const urlParams = { sessionId };

  // User is fetched by <App />
  const user = useQueryUserGetData();
  const companyName = user?.companyList?.[0].name || "";
  const { reportAttended, reportJoinPrivateGroup } = useAnalytics();
  const { setModal, removeModal } = useModal();
  const hasDefaultCalendar =
    user.calendarPreference !==
    CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED;

  /**
   * Session request
   */
  const {
    isFetchedAfterMount,
    isError,
    data: session,
  } = useQuerySessionGetService({
    urlParams,
  });

  /**
   * Attend session request
   */
  const [makeAttendedRequest] = useMutationSessionAttendedPostService({
    mutationConfig: {
      onSuccess: () => {
        if (session && user) {
          reportAttended({
            userId: user.id,
            sessionId: session.id,
            companyId: session.companyId,
            category: session.activityCategory,
          });
        }
      },
    },
  });

  /**
   * Gets the current status of a given session.
   */
  const {
    isSessionLive,
    isSessionCompleted,
    isSessionStartingSoon,
    isSessionUpcoming,
  } = useSessionStatus({
    endDate: session?.endDate,
    startDate: session?.startDate,
  });

  useEffect(() => {
    if (isSessionLive) {
      makeAttendedRequest({
        sessionId: Number(sessionId),
        wasAttended: true,
      });
    }
  }, [isSessionLive, sessionId, makeAttendedRequest]);

  useEffect(() => {
    if (isGroupChat && session && user) {
      reportJoinPrivateGroup({
        sessionId: session.id,
        userId: user.id,
        companyId: user.companyList[0].id || null,
      });
    }
  }, [isGroupChat, session, user, reportJoinPrivateGroup]);

  const groupVideoEmbedURL = useMemo(() => {
    if (userPublicId) {
      return `https://trainer.fitspotapp.com/Group_${userPublicId}`;
    }
    if (session)
      return `https://trainer.fitspotapp.com/${companyName.replace(
        /\s/gi,
        "_"
      )}_Session_${session.id}`;
  }, [companyName, userPublicId, session]);

  /**
   * If it's a group chat and is the session has ended
   * this opens a modal to let them know the session has ended.
   */
  useEffect(() => {
    if (isSessionCompleted && isGroupChat && session) {
      setModal(
        <Flex
          sx={{
            flexDirection: "column",
            alignItems: "center",
            py: 7,
            px: 3,
            width: "100%",
          }}
        >
          <H2 sx={{ fontSize: 6 }}>This session has ended</H2>
          <Text sx={{ my: 6, color: "medium-gray", textAlign: "center" }}>
            {session.name}
            <br />
            <SessionTime
              startDate={session.startDate}
              endDate={session.endDate}
            />
          </Text>
          <Button variant="secondary" onClick={removeModal}>
            Done
          </Button>
        </Flex>
      );
    }
  }, [session, isSessionCompleted, isGroupChat, setModal, removeModal]);

  /**
   * If it's a group chat and is the session hasn't started
   * this opens a modal for a user that is joining another users group chat
   * informing them the session hasn't started. It's confusing 🤦
   */
  useEffect(() => {
    if (isSessionUpcoming && isGroupChat && session) {
      setModal(
        <Flex
          sx={{
            flexDirection: "column",
            alignItems: "center",
            py: 7,
            px: 3,
            width: "100%",
          }}
        >
          <H2 sx={{ fontSize: 6 }}>This session is not yet available</H2>
          <Text sx={{ my: 6, textAlign: "center" }}>
            {session.name}
            <br />
            <SessionTime
              startDate={session.startDate}
              endDate={session.endDate}
            />
          </Text>
          <Text sx={{ mb: 8, color: "medium-gray", textAlign: "center" }}>
            Your session has not started yet. Please come back 15mins before the
            session to join the private viewing experience. Until then, feel
            free to add this session to your calendar for an extra reminder.
          </Text>
          <Flex
            sx={{
              width: "100%",
            }}
          >
            {hasDefaultCalendar && (
              <Button
                sx={{
                  width: "100%",
                  mr: 4,
                }}
                onClick={() => {
                  if (session && hasDefaultCalendar) {
                    createEvent({
                      id: session.id,
                      title: session.name,
                      description: session.description,
                      startDate: session.startDate,
                      endDate: session.endDate,
                      calendarPreference: user.calendarPreference,
                      path: `/dashboard/sessions/${session.id}`,
                    });
                    removeModal();
                  }
                }}
              >
                Add to calendar
              </Button>
            )}
            <Button
              sx={{
                width: "100%",
              }}
              variant="secondary"
              onClick={removeModal}
            >
              Done
            </Button>
          </Flex>
        </Flex>
      );
    }
  }, [
    session,
    isSessionUpcoming,
    isGroupChat,
    setModal,
    removeModal,
    user,
    hasDefaultCalendar,
  ]);

  /**
   * View
   */
  if (!isFetchedAfterMount)
    return (
      <Page backTo="/dashboard/sessions">
        <Spinner />
      </Page>
    );

  if (isError || !session) {
    return <div>Error!</div>;
  }

  const {
    trainer,
    trainerId,
    description,
    liveSessionUrl,
    isOnPlatform,
    additionalInfo,
    attachment,
    trainerRating,
  } = session;

  return (
    <Page backTo="/dashboard/sessions">
      <SessionTitle isGroupChat={isGroupChat} />
      {(isSessionLive || isSessionStartingSoon) && (
        <Flex
          mt={6}
          mb={8}
          sx={{
            flexDirection: ["column", "row", "row"],
          }}
        >
          <Box
            sx={{
              width: ["100%", "calc(100% - 320px)"],
              mr: ["0", 8, 8],
              mb: [7, "0", "0"],
              maxWidth: 1280,
              minWidth: 280,
            }}
          >
            {liveSessionUrl?.includes("zoom.us") ? (
              <Flex
                sx={{
                  height: 400,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ThemeLink
                  href={liveSessionUrl}
                  target="_blank"
                  variant="cta"
                  data-testid="live-session-link"
                >
                  Click here to join session
                </ThemeLink>
              </Flex>
            ) : (
              <Fragment>
                {isSessionLive && (
                  <EmbedContainer
                    src={liveSessionUrl}
                    fullHeight={!isOnPlatform}
                  />
                )}
                {isSessionStartingSoon && (
                  <AspectRatio ratio={16 / 9}>
                    <Flex
                      sx={{
                        bg: "gray",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      <H2>Starting soon!</H2>
                      <H4>
                        Stream will start playing automatically when it's live
                      </H4>
                    </Flex>
                  </AspectRatio>
                )}
              </Fragment>
            )}
          </Box>
          <Box
            sx={{
              flex: "none",
              width: [280, isGroupChat ? 400 : 330],
            }}
          >
            {chatType === "chat" && (
              <Fragment>
                <Heading sx={{ mb: 2, fontSize: 3 }}>Public Live Chat</Heading>
                <Chat />
              </Fragment>
            )}
            {isGroupChat && (
              <iframe
                data-testid="group-video"
                title="TenSpot Virtual Session"
                allowFullScreen={true}
                allow="microphone; camera"
                src={groupVideoEmbedURL}
                width="400"
                height="600"
                sandbox="allow-scripts allow-same-origin"
              />
            )}
          </Box>
        </Flex>
      )}
      <Flex sx={{ flexWrap: "wrap", mt: 6 }}>
        {trainer && (
          <Box
            sx={{
              width: ["100%", "100%", "206px"],
              mr: [0, 0, 8],
            }}
          >
            <ExpertInfoCard
              avatar={trainer.avatar}
              firstName={trainer.firstName}
              lastName={trainer.lastName}
              publicId={trainer.publicId}
              rating={trainerRating}
            />
          </Box>
        )}
        <Box
          sx={{ maxWidth: 690, width: ["100%", "100%", "70%"], mt: [6, 6, 0] }}
        >
          <SessionInfo
            description={description}
            additionalInfo={additionalInfo}
            attachment={attachment}
          />
        </Box>
        {trainerId && trainer && (
          <Box sx={{ mt: 8, width: "100%" }}>
            <OnDemandCarousel
              title={`Watch now from ${
                trainer.firstName
              } ${trainer.lastName?.charAt(0)}.`}
              trainerId={trainerId}
              limit={5}
              hasSeeMoreLink={false}
            />
          </Box>
        )}
      </Flex>
    </Page>
  );
};

export default Session;
