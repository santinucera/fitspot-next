/**
 *
 *
 * Tests for <EmbedContainer />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import EmbedContainer from "./EmbedContainer";

describe("<EmbedContainer />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<EmbedContainer src="some url" fullHeight={false} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
