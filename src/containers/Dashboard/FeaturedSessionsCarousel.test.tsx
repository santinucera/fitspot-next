/**
 *
 *
 * Tests for <FeaturedSessionsCarousel />
 *
 *
 */

import React from "react";
import { screen } from "@testing-library/react";
import { renderWithRouter } from "test-utils/render-with-router";
import FeaturedSessionsCarousel from "./FeaturedSessionsCarousel";
import { createStandardSessionModel } from "test-utils/session-service-test-utils";

const { getByText } = screen;
const handleRSVP = jest.fn();
const handleCancelRSVP = jest.fn();

describe("<FeaturedSessionsCarousel />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <FeaturedSessionsCarousel
        sessions={[createStandardSessionModel()]}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render sessions", () => {
    const session = createStandardSessionModel();
    renderWithRouter(
      <FeaturedSessionsCarousel
        sessions={[session]}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(getByText(session.name)).toBeInTheDocument();
  });
});
