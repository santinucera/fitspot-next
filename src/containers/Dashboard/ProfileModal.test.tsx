/**
 *
 *
 * Tests for <ProfileModal />
 *
 *
 */

import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import ProfileModal from "./ProfileModal";
import { LocalizationProvider } from "@material-ui/pickers";
import DateFnsAdapter from "@material-ui/pickers/adapter/date-fns";
import ModalProvider from "containers/Modal";
import { useModal } from "containers/Modal/Modal";

describe("<ProfileModal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <LocalizationProvider dateAdapter={DateFnsAdapter}>
        <ProfileModal />
      </LocalizationProvider>
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should not remove modal when user submits with errors", async () => {
    const El = () => {
      const { setModal } = useModal();
      return (
        <button data-testid="button" onClick={() => setModal(<ProfileModal />)}>
          Click
        </button>
      );
    };
    const { getByTestId, queryByTestId } = render(
      <LocalizationProvider dateAdapter={DateFnsAdapter}>
        <ModalProvider>
          <El />
        </ModalProvider>
      </LocalizationProvider>
    );
    fireEvent.click(getByTestId("button"));
    await waitFor(() => {
      fireEvent.click(getByTestId("submit-button"));
    });
    await waitFor(() => expect(queryByTestId("modal")).toBeInTheDocument());
  });
});
