/**
 *
 *
 * <OnDemandCarousel />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import Carousel from "components/Carousel";
import { useQueryOnDemandVideosGetService } from "services/OnDemandVideoService";
import { Pillar } from "services/types";
import OnDemandCard from "./OnDemandCard";
import SeeMoreLink from "./SeeMoreLink";

interface OnDemandCarouselProps {
  categoryId: number;
  title: Pillar;
}

const OnDemandCarousel: FunctionComponent<OnDemandCarouselProps> = ({
  categoryId,
  title,
}) => {
  const { isSuccess, data: OnDemandVideos } = useQueryOnDemandVideosGetService({
    queryParams: {
      categoryId,
      limit: 11,
      startDate: "2020-06-01T00:00:00Z",
    },
  });

  return (
    <Carousel
      seeAllLink={`/dashboard/on-demand?category=${categoryId}`}
      count={OnDemandVideos?.data.length}
      title={title}
      sx={{ pl: 5, py: 5 }}
    >
      {isSuccess &&
        OnDemandVideos?.data.map(
          ({ id, name, thumbnailUrl, trainer, duration }) => (
            <OnDemandCard
              key={id}
              id={id}
              name={name}
              thumbnailUrl={thumbnailUrl}
              trainer={trainer}
              duration={duration}
              activityCategory={title}
            />
          )
        )}
      {isSuccess && (
        <SeeMoreLink to={`/dashboard/on-demand?category=${categoryId}`} />
      )}
    </Carousel>
  );
};

export default OnDemandCarousel;
