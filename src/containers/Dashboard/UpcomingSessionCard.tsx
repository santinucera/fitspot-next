/**
 *
 *
 * <UpcomingSessionCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, AspectRatio, Button, Text, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import H4 from "components/H4";
import IconCheckMark from "components/IconCheckMark";
import { CustomerDashboardSessionModel } from "services/SessionService";
import dayjs from "utils/days";
import PillarChip from "components/PillarChip";
import IconPlayCircle from "components/IconPlayCircle";
import Pipe from "components/Pipe";

interface UpcomingSessionCardProps
  extends Pick<
    CustomerDashboardSessionModel,
    | "id"
    | "name"
    | "startDate"
    | "endDate"
    | "isRSVP"
    | "activityCategory"
    | "trainerAvatar"
    | "trainerName"
    | "thumbnailImage"
  > {
  handleRSVP: (id: number) => void;
  handleCancelRSVP: (id: number) => void;
}

const UpcomingSessionCard: FunctionComponent<UpcomingSessionCardProps> = ({
  id,
  name,
  startDate,
  endDate,
  activityCategory,
  trainerAvatar,
  trainerName,
  thumbnailImage,
  isRSVP,
  handleRSVP,
  handleCancelRSVP,
}) => {
  const isActiveSession = dayjs().isBetween(startDate, endDate);
  return (
    <Flex
      key={id}
      sx={{ width: 330, flexDirection: "column" }}
      data-testid={`upcoming-session-card-${id}`}
    >
      <Link
        to={`/dashboard/sessions/${id}`}
        sx={{ position: "relative", display: "block", mb: 3 }}
      >
        <AspectRatio
          ratio={16 / 11}
          sx={{
            backgroundImage: `url(${thumbnailImage?.url || trainerAvatar})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {isActiveSession && <IconPlayCircle />}
        </AspectRatio>
        <PillarChip pillar={activityCategory} isLive={isActiveSession} />
      </Link>
      <Box sx={{ mb: 8 }}>
        <H4>{name}</H4>
        <Flex sx={{ color: "dark-gray", flexDirection: "row" }}>
          <Text>
            {startDate.isToday()
              ? `Today ${startDate.format("h:mma")}`
              : startDate.format("MMM DD, h:mma")}
            - {endDate.format("h:mma")}
          </Text>
          <Pipe />
          <Text>{trainerName}</Text>
        </Flex>
      </Box>
      <Box sx={{ mt: "auto" }}>
        {isActiveSession ? (
          <Link
            sx={{ variant: "buttons.primary" }}
            to={`/dashboard/sessions/${id}`}
            data-testid="join-session-button"
          >
            Join Session
          </Link>
        ) : isRSVP ? (
          <Button
            variant="secondary"
            onClick={() => handleCancelRSVP(id)}
            data-testid={`upcoming-session-cancel-rsvp-button-${id}`}
          >
            <IconCheckMark sx={{ mr: 2 }} />
            Reserved
          </Button>
        ) : (
          <Button
            onClick={() => handleRSVP(id)}
            data-testid={`upcoming-session-rsvp-button-${id}`}
          >
            Reserve
          </Button>
        )}
      </Box>
    </Flex>
  );
};

export default UpcomingSessionCard;
