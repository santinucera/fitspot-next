/**
 *
 *
 * Tests for <Dashboard />
 *
 *
 */
import React from "react";
import Dashboard from "./Dashboard";
import {
  waitFor,
  within,
  screen,
  waitForElementToBeRemoved,
  fireEvent,
} from "@testing-library/react";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  createCustomerDashboardSession,
  setSessionsResponse,
  createStandardSession,
  setFeaturedSessionsResponse,
} from "test-utils/session-service-test-utils";
import {
  StandardSession,
  CustomerDashboardSession,
} from "services/SessionService";
import {
  createChallenge,
  setChallengesResponse,
} from "test-utils/challenge-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { User } from "services/types";
import ModalProvider from "containers/Modal";
import { ToastProvider } from "hooks/useToast";
import dayjs from "utils/days";

const { getByTestId, getAllByText, getByText } = screen;

describe("<Dashboard />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", async () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<Dashboard />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render sessions", async () => {
    const session = {
      ...createCustomerDashboardSession(),
      name: "Karate Session",
      isRSVP: false,
      wasAttended: false,
    };
    setSessionsResponse([session]);
    renderWithRouter(<Dashboard />);
    await waitFor(() => {
      expect(getAllByText(session.name)).toHaveLength(1);
    });
  });

  it("should rsvp for upcoming session", async () => {
    const id = 1;
    const tomorrow = dayjs().add(1, "day");
    const session = {
      ...createCustomerDashboardSession(),
      id,
      name: "Karate Session",
      startDate: tomorrow,
      endDate: tomorrow,
      isRSVP: false,
      wasAttended: false,
    };
    setSessionsResponse([session]);
    renderWithRouter(
      <ModalProvider>
        <ToastProvider>
          <Dashboard />
        </ToastProvider>
      </ModalProvider>
    );
    const rsvpButton = await waitFor(() =>
      getByTestId(`upcoming-session-rsvp-button-${id}`)
    );
    setSessionsResponse([{ ...session, isRSVP: true }]);
    fireEvent.click(rsvpButton);
    const closeModalButton = await waitFor(() =>
      getByTestId("close-modal-button")
    );
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(closeModalButton);
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(
      getByTestId(`upcoming-session-cancel-rsvp-button-${id}`)
    ).toBeInTheDocument();
  });

  it("should cancel rsvp for upcoming session", async () => {
    const id = 1;
    const tomorrow = dayjs().add(1, "day").format();
    const session: CustomerDashboardSession = {
      ...createCustomerDashboardSession(),
      id,
      name: "Karate Session",
      date: tomorrow,
      dt_end: tomorrow,
      isRSVP: true,
      wasAttended: false,
    };
    setSessionsResponse([session]);
    setFeaturedSessionsResponse(200, []);
    renderWithRouter(
      <ModalProvider>
        <ToastProvider>
          <Dashboard />
        </ToastProvider>
      </ModalProvider>
    );
    const cancelRsvpButton = await waitFor(() =>
      getByTestId(`upcoming-session-cancel-rsvp-button-${id}`)
    );
    setSessionsResponse([{ ...session, isRSVP: false }]);
    fireEvent.click(cancelRsvpButton);
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("confirm-button"));
    const toast = await waitFor(() => getByTestId(/toast-item/));
    expect(toast).toHaveTextContent(/^Reservation Canceled/);
    expect(
      getByTestId(`upcoming-session-rsvp-button-${id}`)
    ).toBeInTheDocument();
  });

  it("should render featured sessions", async () => {
    const name = "some featured session";
    const session = { ...createStandardSession(), name, isFeatured: true };
    setFeaturedSessionsResponse(200, [session]);
    renderWithRouter(<Dashboard />);
    await waitFor(() => {
      expect(getByText(name)).toBeInTheDocument();
    });
  });

  it("should rsvp for featured session", async () => {
    const id = 1;
    const tomorrow = dayjs().add(1, "day").format();
    const session: StandardSession = {
      ...createStandardSession(),
      id,
      name: "Karate Session",
      dates: {
        start: tomorrow,
        startLocal: tomorrow,
        end: tomorrow,
        endLocal: tomorrow,
      },
      isRSVP: false,
      wasAttended: false,
      isFeatured: true,
    };
    setFeaturedSessionsResponse(200, [session]);
    setSessionsResponse([]);

    renderWithRouter(
      <ModalProvider>
        <ToastProvider>
          <Dashboard />
        </ToastProvider>
      </ModalProvider>
    );
    const rsvpButton = await waitFor(() =>
      getByTestId(`featured-session-rsvp-button-${id}`)
    );
    setFeaturedSessionsResponse(200, [{ ...session, isRSVP: true }]);
    fireEvent.click(rsvpButton);
    const closeModalButton = await waitFor(() =>
      getByTestId("close-modal-button")
    );
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(closeModalButton);
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(
      getByTestId(`featured-session-cancel-rsvp-button-${id}`)
    ).toBeInTheDocument();
  });

  it("should cancel rsvp for featured session", async () => {
    const id = 1;
    const tomorrow = dayjs().add(1, "day").format();
    const session: StandardSession = {
      ...createStandardSession(),
      id,
      name: "Karate Session",
      dates: {
        start: tomorrow,
        startLocal: tomorrow,
        end: tomorrow,
        endLocal: tomorrow,
      },
      isRSVP: true,
      wasAttended: false,
      isFeatured: true,
    };
    setFeaturedSessionsResponse(200, [session]);
    setSessionsResponse([]);

    renderWithRouter(
      <ModalProvider>
        <ToastProvider>
          <Dashboard />
        </ToastProvider>
      </ModalProvider>
    );
    const cancelRsvpButton = await waitFor(() =>
      getByTestId(`featured-session-cancel-rsvp-button-${id}`)
    );
    setFeaturedSessionsResponse(200, [{ ...session, isRSVP: false }]);
    fireEvent.click(cancelRsvpButton);
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("confirm-button"));
    const toast = await waitFor(() => getByTestId(/toast-item/));
    expect(toast).toHaveTextContent(/^Reservation Canceled/);
    expect(
      getByTestId(`featured-session-rsvp-button-${id}`)
    ).toBeInTheDocument();
  });

  it("should render challenges", async () => {
    setChallengesResponse([
      { ...createChallenge(), id: 2, name: "Challenge One" },
    ]);
    renderWithRouter(<Dashboard />);
    await waitFor(() => {
      expect(screen.getByText("Challenge One")).toBeInTheDocument();
    });
  });

  it("should render the steps leaderboard", async () => {
    renderWithRouter(<Dashboard />);
    await waitFor(() => {
      expect(
        within(screen.getByTestId("steps-leaderboard")).getByText("bob")
      ).toBeInTheDocument();
    });
  });

  it("should render on-demand pillars ", async () => {
    renderWithRouter(<Dashboard />);
    await waitFor(() => {
      expect(screen.getByText("Move")).toBeInTheDocument();
      expect(screen.getByText("Chill")).toBeInTheDocument();
      expect(screen.getByText("Eat")).toBeInTheDocument();
      expect(screen.getByText("Thrive")).toBeInTheDocument();
      expect(screen.getByText("Play")).toBeInTheDocument();
    });
  });
});
