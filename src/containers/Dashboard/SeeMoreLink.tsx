/**
 *
 *
 * <SeeMoreLink />
 *
 *
 */

/** @jsx jsx */
import { jsx, Text } from "theme-ui";
import { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import IconArrow from "components/IconArrow";

interface SeeMoreLinkProps {
  to: string;
}

const SeeMoreLink: FunctionComponent<SeeMoreLinkProps> = ({ to }) => {
  return (
    <Link
      sx={{
        width: 300,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        color: "primary",
        textDecoration: "none",
        fontWeight: 700,
      }}
      to={to}
    >
      <Text sx={{ lineHeight: "24px" }}>SEE MORE</Text>
      <IconArrow direction="right" />
    </Link>
  );
};

export default SeeMoreLink;
