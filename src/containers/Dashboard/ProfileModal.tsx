/**
 *
 *
 * <ProfileModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Button, Flex, Box } from "theme-ui";
import { FunctionComponent, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import Avatar from "components/Avatar";
import { useModal } from "containers/Modal/Modal";
import DatePicker from "components/DatePicker";
import {
  useMutationUserPatchService,
  useQueryUserGetService,
} from "services/UserService";

import daysjs from "utils/days";
import AvatarCropper from "containers/AvatarCropper";

interface ProfileModalProps {}

type Fields = {
  gender: number | null;
  birthday: Date | null;
};

const GENDERS = {
  FEMALE: { id: 1, label: "Female" },
  MALE: { id: 0, label: "Male" },
  UNSPECIFIED: { id: 2, label: "Unspecified" },
};

const ProfileModal: FunctionComponent<ProfileModalProps> = () => {
  const { removeModal } = useModal();
  const [image, setImage] = useState<any>();

  const [handleUpdateUser, { data, isLoading }] = useMutationUserPatchService({
    mutationConfig: {
      onSuccess() {
        removeModal();
      },
    },
  });

  useQueryUserGetService({
    queryConfig: {
      enabled: data,
    },
  });

  const [avatarId, setAvatarId] = useState<number | null>(null);
  const { handleSubmit, control } = useForm<Fields>({
    defaultValues: {
      gender: null,
      birthday: null,
    },
  });

  const handleFinishUpload = ({
    avatarId,
    avatarUrl,
  }: {
    avatarId: number;
    avatarUrl: string;
  }) => {
    setAvatarId(avatarId);
    setImage(avatarUrl);
  };

  const onSubmit = ({ birthday, gender }: Fields) => {
    handleUpdateUser({
      avatarId,
      ...(gender && { gender }),
      ...(birthday && {
        customer: {
          birthday: daysjs(birthday).format("YYYY-MM-DD"),
        },
      }),
    });
  };

  return (
    <AvatarCropper
      onFinishUpload={handleFinishUpload}
      initialComponent={({ onClick }) => (
        <Flex
          sx={{
            overflow: "hidden",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Box mb={6}>
            <Avatar url={image} size={140} />
          </Box>
          <Button sx={{ mb: 6 }} variant="underlined" onClick={onClick}>
            Select Profile Picture
          </Button>
          <DatePicker control={control} label="Your Birthday" name="birthday" />
          <Controller
            name="gender"
            control={control}
            render={({ onChange, value }) => (
              <Flex
                sx={{
                  mb: 5,
                  justifyContent: "space-evenly",
                  width: "100%",
                  px: [0, 4],
                }}
              >
                {Object.values(GENDERS).map(({ id, label }) => (
                  <Button
                    key={id}
                    data-testid={`gender-${id}-button`}
                    variant={value === id ? "primary" : "secondary"}
                    onClick={() => onChange(id)}
                  >
                    {label}
                  </Button>
                ))}
              </Flex>
            )}
          />
          <Button
            data-testid="submit-button"
            onClick={handleSubmit(onSubmit)}
            disabled={isLoading || !avatarId}
          >
            {isLoading ? "Saving..." : "Save Changes"}
          </Button>
        </Flex>
      )}
    />
  );
};

export default ProfileModal;
