/**
 *
 *
 * Tests for <ChallengesCarousel />
 *
 *
 */

import React from "react";
import { screen, waitFor } from "@testing-library/react";
import ChallengesCarousel from "./ChallengesCarousel";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  createChallenge,
  createNonJoinedChallenge,
  setChallengesResponse,
} from "test-utils/challenge-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";

describe("<ChallengesCarousel />", () => {
  beforeEach(() => {
    queryCache.setQueryData("user", createUser());
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<ChallengesCarousel />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render steps leaderboard", async () => {
    renderWithRouter(<ChallengesCarousel />);
    await waitFor(() => {
      expect(screen.getByText("bob")).toBeInTheDocument();
    });
    expect(screen.getByText("bob")).toMatchInlineSnapshot(`
      <div
        class="css-vurnku"
      >
        bob
      </div>
    `);
  });

  it("should render challenges", async () => {
    setChallengesResponse([
      { ...createNonJoinedChallenge(), id: 1, name: "Challenge To Join!" },
      { ...createChallenge(), id: 2, name: "Challenge One" },
    ]);
    renderWithRouter(<ChallengesCarousel />);
    await waitFor(() => {
      expect(screen.getByText("Challenge To Join!")).toBeInTheDocument();
      expect(screen.getByText("Challenge One")).toBeInTheDocument();
    });
    expect(screen.getByText("Challenge To Join!")).toMatchSnapshot();
    expect(screen.getByText("Challenge One")).toMatchSnapshot();
    await waitFor(() => {
      expect(screen.getByTestId("challenge-winner-card")).toBeInTheDocument();
    });
  });
});
