/**
 *
 *
 * Tests for <FeaturedSessionCard />
 *
 *
 */

import React from "react";
import { screen, fireEvent } from "@testing-library/react";
import { useLocation } from "react-router-dom";
import {
  renderWithRouter,
  renderWithMemoryRouter,
} from "test-utils/render-with-router";
import FeaturedSessionCard from "./FeaturedSessionCard";
import dayjs from "utils/days";

const { getByTestId, queryByTestId, getByText } = screen;

describe("<FeaturedSessionCard />", () => {
  const handleRSVP = jest.fn();
  const handleCancelRSVP = jest.fn();

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <FeaturedSessionCard
        id={1}
        name="some name"
        startDate={dayjs()}
        endDate={dayjs()}
        thumbnailUrl="https://example.com/image.png"
        trainerName="Some Trainer"
        pillar="Move"
        isRSVP={true}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render pillar", () => {
    const pillar = "Move";
    renderWithRouter(
      <FeaturedSessionCard
        id={1}
        name="some name"
        startDate={dayjs()}
        endDate={dayjs()}
        thumbnailUrl="https://example.com/image.png"
        trainerName="Some Trainer"
        pillar={pillar}
        isRSVP={true}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(getByTestId("pillar")).toHaveTextContent(pillar);
  });

  it("should not render pillar", () => {
    renderWithRouter(
      <FeaturedSessionCard
        id={1}
        name="some name"
        startDate={dayjs()}
        endDate={dayjs()}
        thumbnailUrl="https://example.com/image.png"
        trainerName="Some Trainer"
        isRSVP={true}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(queryByTestId("pillar")).toBeNull();
  });

  it("should trigger handleRSVP", () => {
    const id = 1;
    renderWithRouter(
      <FeaturedSessionCard
        id={id}
        name="some name"
        startDate={dayjs()}
        endDate={dayjs()}
        thumbnailUrl="https://example.com/image.png"
        trainerName="Some Trainer"
        isRSVP={false}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    fireEvent.click(getByText(/^Reserve$/));
    expect(handleRSVP).toHaveBeenCalledWith(id);
  });

  it("should trigger handleCancelRSVP", () => {
    const id = 1;
    renderWithRouter(
      <FeaturedSessionCard
        id={id}
        name="some name"
        startDate={dayjs()}
        endDate={dayjs()}
        thumbnailUrl="https://example.com/image.png"
        trainerName="Some Trainer"
        isRSVP={true}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    fireEvent.click(getByText(/^Reserved$/));
    expect(handleCancelRSVP).toHaveBeenCalledWith(id);
  });

  it("should forward the user to the detail page", () => {
    const id = 1;
    const name = "some name";
    const El = () => {
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.pathname}</div>
          <FeaturedSessionCard
            id={id}
            name={name}
            startDate={dayjs()}
            endDate={dayjs()}
            thumbnailUrl="https://example.com/image.png"
            trainerName="Some Trainer"
            isRSVP={true}
            handleRSVP={handleRSVP}
            handleCancelRSVP={handleCancelRSVP}
          />
        </div>
      );
    };
    renderWithMemoryRouter("/*", ["/"], <El />);
    fireEvent.click(getByText(name));
    expect(getByTestId("location")).toHaveTextContent(
      `/dashboard/sessions/${id}`
    );
  });
});
