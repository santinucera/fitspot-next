/**
 *
 *
 * Tests for <OnDemandCarousel />
 *
 *
 */

import React from "react";
import { renderWithRouter } from "test-utils/render-with-router";

import OnDemandCarousel from "./OnDemandCarousel";

describe("<OnDemandCarousel />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<OnDemandCarousel categoryId={2} title="Move" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
