/**
 *
 *
 * <Dashboard />
 *
 *
 */
/** @jsx jsx */
import { jsx, Flex, Text, Box, Divider } from "theme-ui";
import { Fragment, FunctionComponent, useEffect } from "react";
import daysjs from "utils/days";
import Page from "components/Page";
import H1 from "components/H1";
import { useQueryUserGetData } from "services/UserService";
import ActivityFeed from "components/ActivityFeed";
import H2 from "components/H2";
import H5 from "components/H5";
import { useQueryActivityFeedGetService } from "services/ActivityFeedService";
import ChallengesCarousel from "./ChallengesCarousel";
import UpcomingSessionsCarousel from "./UpcomingSessionsCarousel";
import OnDemandCarousel from "./OnDemandCarousel";
import { useModal } from "containers/Modal/Modal";
import ProfileModal from "./ProfileModal";
import useLocalStorage from "hooks/useLocalStorage";
import FeaturedSessionsCarousel from "./FeaturedSessionsCarousel";
import {
  useQueryFeaturedSessionsGetService,
  useQuerySessionsGetService,
} from "services/SessionService";
import useSessionsRSVP from "hooks/useSessionsRSVP";

interface DashboardProps {}

type userFirstLogin = {
  firstLogin: boolean;
};

const Dashboard: FunctionComponent<DashboardProps> = () => {
  const user = useQueryUserGetData();
  const { setModal } = useModal();
  const { getItem, setItem } = useLocalStorage();

  useEffect(() => {
    const item = getItem<userFirstLogin>(user?.publicId!);
    if (item?.firstLogin) {
      setModal(<ProfileModal />);
      setItem(user?.publicId!, { firstLogin: false });
    }
  }, [getItem, setItem, setModal, user]);

  const sessionsQueryParams = {
    limit: 5,
    short: false,
    attended: false,
  };
  const { data: upcomingSessions } = useQuerySessionsGetService({
    urlParams: sessionsQueryParams,
  });
  const { handleRSVP: rsvpHandler, handleCancelRSVP } = useSessionsRSVP({
    sessionsQueryParams,
  });
  const handleRSVP = (id: number) =>
    rsvpHandler({ sessionId: id, isRSVP: true });

  const { data: activityFeed = [] } = useQueryActivityFeedGetService();
  const { data: featuredSessions = [] } = useQueryFeaturedSessionsGetService();

  return (
    <Page title="Home">
      <Flex sx={{ mb: 2, flexDirection: "column" }}>
        <Text sx={{ color: "darker-gray" }}>
          {daysjs().format("MMMM DD, YYYY")}
        </Text>
        <H1 sx={{ fontWeight: 300 }}>Welcome back, {user?.firstName}!</H1>
      </Flex>
      <Box
        sx={{
          mx: ["-18px", 0, 0],
        }}
      >
        <Box sx={{ mb: 3 }}>
          <UpcomingSessionsCarousel
            sessions={upcomingSessions}
            handleRSVP={handleRSVP}
            handleCancelRSVP={handleCancelRSVP}
          />
        </Box>
        <Box sx={{ mb: 3 }}>
          <FeaturedSessionsCarousel
            sessions={featuredSessions}
            handleRSVP={handleRSVP}
            handleCancelRSVP={handleCancelRSVP}
          />
        </Box>
        <Flex sx={{ mb: 3, flexDirection: ["column", "row"] }}>
          <Box
            sx={{
              bg: "white",
              width: "100%",
            }}
          >
            <ChallengesCarousel />
          </Box>
          <Box
            sx={{
              mt: [3, 0],
              ml: [0, 3],
              bg: "white",
              overflow: "hidden",
              flex: "none",
              maxWidth: ["100%", 400],
            }}
          >
            <ActivityFeed
              feed={activityFeed}
              header={
                <Fragment>
                  <H5 light>{user?.companyList[0].name}</H5>
                  <H2 role="heading">Activity feed</H2>
                  <Divider sx={{ my: 5 }} />
                </Fragment>
              }
            />
          </Box>
        </Flex>
        <Box sx={{ mb: 3 }}>
          <OnDemandCarousel title="Thrive" categoryId={8} />
        </Box>
        <Box sx={{ mb: 3 }}>
          <OnDemandCarousel title="Chill" categoryId={4} />
        </Box>
        <Box sx={{ mb: 3 }}>
          <OnDemandCarousel title="Eat" categoryId={5} />
        </Box>
        <Box sx={{ mb: 3 }}>
          <OnDemandCarousel title="Move" categoryId={6} />
        </Box>
        <Box sx={{ mb: 3 }}>
          <OnDemandCarousel title="Play" categoryId={7} />
        </Box>
      </Box>
    </Page>
  );
};

export default Dashboard;
