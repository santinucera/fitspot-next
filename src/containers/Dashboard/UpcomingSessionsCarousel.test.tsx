/**
 *
 *
 * Tests for <UpcomingSessionsCarousel />
 *
 *
 */

import React from "react";
import { fireEvent, screen } from "@testing-library/react";
import UpcomingSessionsCarousel from "./UpcomingSessionsCarousel";
import { createCustomerDashboardSessionModel } from "test-utils/session-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { User } from "services/types";
import ModalProvider from "containers/Modal";
import { ToastProvider } from "hooks/useToast";

const { getByText } = screen;
const handleRSVP = jest.fn();
const handleCancelRSVP = jest.fn();

describe("<UpcomingSessionsCarousel />", () => {
  let user: User;
  beforeEach(async () => {
    jest.clearAllMocks();
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <UpcomingSessionsCarousel
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should trigger handleRSVP", async () => {
    const session = {
      ...createCustomerDashboardSessionModel(),
      isRSVP: false,
    };
    renderWithRouter(
      <ModalProvider>
        <UpcomingSessionsCarousel
          sessions={[session]}
          handleRSVP={handleRSVP}
          handleCancelRSVP={handleCancelRSVP}
        />
      </ModalProvider>
    );
    fireEvent.click(getByText(/^Reserve$/));
    expect(handleRSVP).toHaveBeenCalled();
  });

  it("should trigger handleCancelRSVP", async () => {
    const session = {
      ...createCustomerDashboardSessionModel(),
      isRSVP: true,
    };

    renderWithRouter(
      <ToastProvider>
        <UpcomingSessionsCarousel
          sessions={[session]}
          handleRSVP={handleRSVP}
          handleCancelRSVP={handleCancelRSVP}
        />
      </ToastProvider>
    );

    fireEvent.click(getByText(/^Reserved$/i));
    expect(handleCancelRSVP).toHaveBeenCalled();
  });
});
