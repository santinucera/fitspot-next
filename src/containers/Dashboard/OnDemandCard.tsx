/**
 *
 *
 * <OnDemandCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, AspectRatio, Text } from "theme-ui";
import { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import H4 from "components/H4";
import IconPlayCircle from "components/IconPlayCircle";
import { Pillar } from "services/types";
import PillarChip from "components/PillarChip";

interface OnDemandCardProps {
  id: number;
  name: string;
  duration: string;
  thumbnailUrl: string;
  trainer: string;
  activityCategory: Pillar;
}

const OnDemandCard: FunctionComponent<OnDemandCardProps> = ({
  id,
  name,
  thumbnailUrl,
  trainer,
  duration,
  activityCategory,
}) => {
  return (
    <Box key={id} sx={{ width: 330 }}>
      <Link
        to={`/dashboard/on-demand/${id}`}
        sx={{ position: "relative", display: "block", mb: 3 }}
      >
        <AspectRatio
          ratio={16 / 11}
          sx={{
            backgroundImage: `url(${thumbnailUrl})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <IconPlayCircle />
        </AspectRatio>
        <PillarChip pillar={activityCategory} />
      </Link>
      <Box sx={{ mb: 3 }}>
        <H4>{name}</H4>
        <Text sx={{ color: "dark-gray" }}>
          {trainer} <span sx={{ mx: 3 }}>|</span> {duration}
        </Text>
      </Box>
    </Box>
  );
};

export default OnDemandCard;
