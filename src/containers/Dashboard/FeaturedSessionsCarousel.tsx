/**
 *
 *
 * <FeaturedSessionsCarousel />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FC } from "react";
import { StandardSessionModel } from "services/SessionService";
import Carousel from "components/Carousel";
import FeaturedSessionCard from "./FeaturedSessionCard";

interface FeaturedSessionsCarouselProps {
  sessions: StandardSessionModel[];
  handleRSVP: (id: number) => void;
  handleCancelRSVP: (id: number) => void;
}

const FeaturedSessionsCarousel: FC<FeaturedSessionsCarouselProps> = ({
  sessions,
  handleRSVP,
  handleCancelRSVP,
}) => {
  return (
    <Carousel
      count={sessions.length}
      title="Featured upcoming sessions"
      sx={{ pl: 5, py: 5 }}
    >
      {sessions.map(
        ({ id, name, dates, thumbnailImage, trainer, activity, isRSVP }) => (
          <FeaturedSessionCard
            key={id}
            id={id}
            name={name}
            startDate={dates.start}
            endDate={dates.end}
            thumbnailUrl={thumbnailImage?.url || trainer?.avatar.url || ""}
            trainerName={`${trainer?.fullName}`}
            pillar={activity?.category}
            handleRSVP={handleRSVP}
            handleCancelRSVP={handleCancelRSVP}
            isRSVP={isRSVP}
          />
        )
      )}
    </Carousel>
  );
};

export default FeaturedSessionsCarousel;
