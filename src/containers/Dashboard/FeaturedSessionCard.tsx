/**
 *
 *
 * <FeaturedSessionCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Button, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { Dayjs } from "dayjs";
import { Pillar } from "services/types";
import { Link } from "react-router-dom";
import PillarChip from "components/PillarChip";
import IconCheckMark from "components/IconCheckMark";
import Pipe from "components/Pipe";

interface FeaturedSessionCardProps {
  id: number;
  name: string;
  startDate: Dayjs;
  endDate: Dayjs;
  thumbnailUrl: string;
  trainerName: string;
  pillar?: Pillar;
  isRSVP: boolean;
  handleRSVP: (id: number) => void;
  handleCancelRSVP: (id: number) => void;
}

const FeaturedSessionCard: FunctionComponent<FeaturedSessionCardProps> = ({
  id,
  name,
  startDate,
  endDate,
  thumbnailUrl,
  trainerName,
  pillar,
  isRSVP,
  handleRSVP,
  handleCancelRSVP,
}) => {
  return (
    <Box
      sx={{
        position: "relative",
        display: "flex",
        alignItems: "flex-end",
        backgroundImage: `url(${thumbnailUrl})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        width: [330, 604],
        height: [340],
      }}
      data-testid={`featured-session-card-${id}`}
    >
      {pillar && (
        <PillarChip
          pillar={pillar}
          sx={{
            position: "absolute",
            top: "16px",
            left: "16px",
          }}
        />
      )}

      <Flex
        sx={{
          background:
            "linear-gradient(4.04deg, #141418 -94.49%, rgba(20, 20, 24, 0) 127.95%)",
          width: "100%",
          padding: [4, "16px 16px 24px 16px"],
          flex: "1 1 auto",
          flexDirection: ["column", "row"],
        }}
      >
        <Box
          sx={{
            flex: "1 1 auto",
            mb: [4, "0"],
          }}
        >
          <Link
            to={`/dashboard/sessions/${id}`}
            sx={{ color: "white", textDecoration: "none" }}
          >
            <Heading
              as="h4"
              sx={{
                color: "white",
                fontSize: ["15px", "24px"],
                lineHeight: ["inherit", "40px"],
                fontWeight: "heading",
                mb: [2, "10px"],
              }}
            >
              {name}
            </Heading>
            {startDate.format("ddd, MMM D")}
            <Pipe />
            {`${startDate.format("h:mm")} - ${endDate.format("h:mma")}`}
            <Pipe />
            {trainerName}
          </Link>
        </Box>
        <Flex
          sx={{
            flex: ["1 1 auto", "1 0 149px"],
            justifyContent: ["flex-start", "flex-end"],
            alignItems: ["flex-start", "flex-end"],
          }}
        >
          {isRSVP ? (
            <Button
              variant="secondary"
              onClick={() => handleCancelRSVP(id)}
              data-testid={`featured-session-cancel-rsvp-button-${id}`}
            >
              <IconCheckMark sx={{ mr: 2 }} />
              Reserved
            </Button>
          ) : (
            <Button
              onClick={() => handleRSVP(id)}
              data-testid={`featured-session-rsvp-button-${id}`}
            >
              Reserve
            </Button>
          )}
        </Flex>
      </Flex>
    </Box>
  );
};

export default FeaturedSessionCard;
