/**
 *
 *
 * <ChallengesCarousel />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import Carousel from "components/Carousel";
import StepsLeaderBoard from "containers/StepsLeaderBoard";
import {
  useQueryChallengesGetCompletedService,
  useQueryChallengesGetCurrentService,
} from "services/ChallengeService";
import ChallengeWinnerCard from "components/ChallengeWinnerCard";
import ChallengeCard from "components/ChallengeCard";
import useChallengeRSVP from "hooks/useChallengeRSVP";
import SeeMoreLink from "./SeeMoreLink";

interface ChallengesCarouselProps {}

const ChallengesCarousel: FunctionComponent<ChallengesCarouselProps> = () => {
  const {
    isSuccess: currentChallengesIsSuccess,
    data: currentChallenges = [],
  } = useQueryChallengesGetCurrentService();

  const {
    isSuccess: completeChallengesIsSuccess,
    data: completeChallenges,
  } = useQueryChallengesGetCompletedService();
  const { handleChallengeRSVP } = useChallengeRSVP("current-challenges");

  return (
    <Carousel
      seeAllLink={`/dashboard/challenges`}
      count={currentChallenges.length + completeChallenges?.length!}
      title="Challenges"
      sx={{ pl: 5, py: 5 }}
    >
      <StepsLeaderBoard limit={4} data-testid="carousel-steps-leaderboard" />
      {currentChallengesIsSuccess &&
        currentChallenges.map(
          ({
            id,
            isJoined,
            isActive,
            name,
            description,
            leaderboard,
            hasParticipants,
            participantsCount,
            startDate,
            endDate,
            daysLeft,
            totalDays,
            totalPossiblePoints,
            myRank,
            individualPointsSummary: { totalPoints },
          }) => {
            return (
              <ChallengeCard
                key={id}
                id={id}
                isJoined={isJoined}
                isActive={isActive}
                name={name}
                description={description}
                leaderboard={leaderboard}
                hasParticipants={hasParticipants}
                participantsCount={participantsCount}
                startDate={startDate}
                endDate={endDate}
                daysLeft={daysLeft}
                totalDays={totalDays}
                totalPoints={totalPoints}
                totalPossiblePoints={totalPossiblePoints}
                rank={myRank}
                handleRSVP={() => {
                  handleChallengeRSVP({ challengeId: id, isRSVP: true });
                }}
              />
            );
          }
        )}

      {completeChallengesIsSuccess &&
        completeChallenges?.map(
          ({
            id,
            name,
            leaderboard,
            startDate,
            endDate,
            myRank,
            individualPointsSummary: { totalPoints },
          }) => (
            <ChallengeWinnerCard
              sx={{
                px: 3,
              }}
              key={id}
              id={id}
              name={name}
              leaderboard={leaderboard}
              startDate={startDate}
              endDate={endDate}
              myRank={myRank}
              myPoints={totalPoints}
            />
          )
        )}

      {currentChallengesIsSuccess && (
        <SeeMoreLink to={`/dashboard/challenges`} />
      )}
    </Carousel>
  );
};

export default ChallengesCarousel;
