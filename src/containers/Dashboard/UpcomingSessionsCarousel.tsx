/**
 *
 *
 * <UpcomingSessionsCarousel />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FC } from "react";
import Carousel from "components/Carousel";
import UpcomingSessionCard from "./UpcomingSessionCard";
import { CustomerDashboardSessionModel } from "services/SessionService";
import SeeMoreLink from "./SeeMoreLink";

export interface UpcomingSessionCarouselParams {
  sessions?: CustomerDashboardSessionModel[];
  handleRSVP: (id: number) => void;
  handleCancelRSVP: (id: number) => void;
}

const UpcomingSessionsCarousel: FC<UpcomingSessionCarouselParams> = ({
  sessions,
  handleRSVP,
  handleCancelRSVP,
}) => {
  return (
    <Carousel
      seeAllLink={`/dashboard/sessions`}
      count={sessions?.length}
      title="Upcoming live sessions"
      sx={{ pl: 5, py: 5 }}
    >
      {sessions &&
        sessions.map(
          ({
            id,
            name,
            isRSVP,
            startDate,
            endDate,
            activityCategory,
            trainerAvatar,
            trainerName,
            thumbnailImage,
          }) => (
            <UpcomingSessionCard
              key={id}
              id={id}
              name={name}
              startDate={startDate}
              endDate={endDate}
              activityCategory={activityCategory}
              trainerAvatar={trainerAvatar}
              trainerName={trainerName}
              thumbnailImage={thumbnailImage}
              isRSVP={isRSVP}
              handleRSVP={handleRSVP}
              handleCancelRSVP={handleCancelRSVP}
            />
          )
        )}
      {sessions && <SeeMoreLink to={`/dashboard/sessions`} />}
    </Carousel>
  );
};

export default UpcomingSessionsCarousel;
