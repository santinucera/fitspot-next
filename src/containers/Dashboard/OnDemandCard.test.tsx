/**
 *
 *
 * Tests for <OnDemandCard />
 *
 *
 */

import React from "react";
import OnDemandCard from "./OnDemandCard";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<OnDemandCard />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <OnDemandCard
        id={1}
        name="Foo"
        thumbnailUrl="/abc"
        trainer="Jane"
        duration="1 hour"
        activityCategory="Move"
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
