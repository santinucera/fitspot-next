/**
 *
 *
 * Tests for <UpcomingSessionCard />
 *
 *
 */

import React from "react";
import { screen, fireEvent } from "@testing-library/react";
import { useLocation } from "react-router-dom";
import UpcomingSessionCard from "./UpcomingSessionCard";
import dayjs from "utils/days";
import {
  renderWithRouter,
  renderWithMemoryRouter,
} from "test-utils/render-with-router";

const { getByText, getByTestId } = screen;

const mockedNavigate = jest.fn();
jest.mock("react-router-dom", () => {
  const router = jest.requireActual("react-router-dom");
  return {
    ...router,
    useNavigate: () => mockedNavigate,
  };
});

describe("<UpcomingSessionCard />", () => {
  const handleRSVP = jest.fn();
  const handleCancelRSVP = jest.fn();

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");

    renderWithRouter(
      <UpcomingSessionCard
        id={1}
        name="Foo"
        startDate={dayjs()}
        endDate={dayjs()}
        activityCategory="Move"
        trainerAvatar={"foo"}
        trainerName="Name"
        thumbnailImage={{ url: "https://example.com/image.png" }}
        isRSVP={false}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    expect(spy).not.toHaveBeenCalled();
    spy.mockRestore();
  });

  it("should forward user to session page", () => {
    const id = 1;
    const El = () => {
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.pathname}</div>
          <UpcomingSessionCard
            id={id}
            name="Foo"
            startDate={dayjs().subtract(1, "minute")}
            endDate={dayjs().add(1, "minute")}
            activityCategory="Move"
            trainerAvatar={"foo"}
            trainerName="Name"
            thumbnailImage={{ url: "https://example.com/image.png" }}
            isRSVP={false}
            handleRSVP={handleRSVP}
            handleCancelRSVP={handleCancelRSVP}
          />
        </div>
      );
    };
    renderWithMemoryRouter("/*", ["/"], <El />);
    fireEvent.click(getByText(/^Join Session$/));
    expect(getByTestId("location")).toHaveTextContent("/");
  });

  it("should trigger handleRSVP", () => {
    renderWithRouter(
      <UpcomingSessionCard
        id={1}
        name="Foo"
        startDate={dayjs()}
        endDate={dayjs()}
        activityCategory="Move"
        trainerAvatar={"foo"}
        trainerName="Name"
        thumbnailImage={{ url: "https://example.com/image.png" }}
        isRSVP={false}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    fireEvent.click(getByText(/^Reserve$/));
    expect(handleRSVP).toHaveBeenCalled();
  });

  it("should trigger handleCancelRSVP", () => {
    renderWithRouter(
      <UpcomingSessionCard
        id={1}
        name="Foo"
        startDate={dayjs()}
        endDate={dayjs()}
        activityCategory="Move"
        trainerAvatar={"foo"}
        trainerName="Name"
        thumbnailImage={{ url: "https://example.com/image.png" }}
        isRSVP={true}
        handleRSVP={handleRSVP}
        handleCancelRSVP={handleCancelRSVP}
      />
    );
    fireEvent.click(getByText(/^Reserved$/));
    expect(handleCancelRSVP).toHaveBeenCalled();
  });
});
