/**
 *
 *
 * Tests for <SeeMoreLink />
 *
 *
 */

import React from "react";
import SeeMoreLink from "./SeeMoreLink";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<SeeMoreLink />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<SeeMoreLink to="/" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
