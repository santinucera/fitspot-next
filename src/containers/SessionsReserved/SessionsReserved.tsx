/*
 *
 *
 * SessionsReserved
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Flex } from "theme-ui";
import { FC, Fragment, useMemo, useState } from "react";
import dayjs from "utils/days";
import {
  SessionsGetQueryParams,
  sessionsGroupedByDayRangesModel,
  useQuerySessionsGetService,
  WeekRange,
} from "services/SessionService";
import range from "utils/range";
import chunk from "utils/chunk";
import Spinner from "components/Spinner";
import H3 from "components/H3";
import LiveSessionCard from "containers/LiveSessions/LiveSessionCard";
import useSessionsRSVP from "hooks/useSessionsRSVP";
import { Link } from "react-router-dom";
import IconBoxArrow from "components/IconBoxArrow";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */

const SessionsReserved: FC = () => {
  /**
   * The current selected week, default is 1. 1 === This week.
   */
  const [currentWeek, setCurrentWeek] = useState(1);

  const [currentWeeks, setCurrentWeeks] = useState(1);

  /**
   * Creates the weeks for sessions navigation and dates to be populated
   * with sessions grouped by weeks.
   */
  const { weeks, navigation, queryParams } = useMemo((): {
    navigation: string[];
    weeks: WeekRange;
    queryParams: SessionsGetQueryParams;
  } => {
    const start = dayjs()
      .startOf("week")
      .add(1, "day")
      .subtract(currentWeeks, "week");
    const end = start.clone().endOf("week").add(2, "week");
    const days = range(0, 21).map((day) => start.clone().add(day, "day"));
    const weeks = chunk(days, 7);

    return {
      navigation: weeks.map((week) => {
        if (dayjs().isBetween(week[0], week[week.length - 1]))
          return "THIS WEEK";
        return `${week[0].format("MMM D")} - ${week[week.length - 1].format(
          "D"
        )}`;
      }),
      weeks,
      queryParams: {
        short: false,
        isRsvp: true,
        attended: true,
        limit: 9999,
        dtStart: start.toISOString(),
        dtEnd: end.toISOString(),
      },
    };
  }, [currentWeeks]);

  const { isFetchedAfterMount, data: sessions } = useQuerySessionsGetService({
    urlParams: queryParams,
  });

  /**
   * Groups the sessions by weeks.
   */
  const groupedSessions = useMemo(() => {
    if (!sessions) return [];
    return sessionsGroupedByDayRangesModel(sessions, weeks);
  }, [sessions, weeks]);

  const hasReservedSessions = useMemo(() => {
    if (groupedSessions.length === 0) return false;
    return Object.entries(groupedSessions[currentWeek]).some(
      ([_, a]) => a.length
    );
  }, [groupedSessions, currentWeek]);

  const { handleRSVP, handleCancelRSVP } = useSessionsRSVP({
    sessionsQueryParams: queryParams,
  });

  /**
   * Sets the current week.
   */
  const setActiveWeek = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const date = event.currentTarget?.dataset.day;
    if (!date) return;
    const index = navigation.findIndex((d) => d === date);
    setCurrentWeek(index);
  };

  const changeWeeks = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const direction = event.currentTarget.dataset.direction;
    if (!direction) return;
    setCurrentWeeks((prevState) =>
      direction === "prev" ? prevState + 1 : prevState - 1
    );
  };

  return (
    <Box>
      <Flex
        sx={{
          alignItems: "center",
          bg: "light-gray",
          py: 3,
          mb: 7,
        }}
      >
        <Flex
          data-direction="prev"
          onClick={changeWeeks}
          sx={{
            cursor: "pointer",
            width: "50px",
          }}
        >
          <IconBoxArrow direction="left" />
        </Flex>
        {navigation.map((date, idx) => (
          <Flex
            key={date}
            data-day={date}
            onClick={setActiveWeek}
            className={currentWeek === idx ? "active" : ""}
            sx={{
              alignItems: "center",
              flexDirection: "column",
              color: "dark-gray",
              cursor: "pointer",
              px: 6,
              transition: "color 75ms ease",
              "&.active": {
                color: "primary",
              },
            }}
          >
            <Text
              sx={{
                textTransform: "uppercase",
                fontSize: 3,
                fontWeight: 700,
              }}
            >
              {date}
            </Text>
          </Flex>
        ))}
        {currentWeeks !== 0 && (
          <Flex
            data-direction="next"
            onClick={changeWeeks}
            sx={{
              cursor: "pointer",
              width: "50px",
            }}
          >
            <IconBoxArrow direction="right" />
          </Flex>
        )}
      </Flex>
      {!isFetchedAfterMount ? (
        <Spinner />
      ) : (
        <Fragment>
          {!hasReservedSessions && (
            <Box
              sx={{
                color: "medium-gray",
                mt: 8,
              }}
            >
              <H3>You have no sessions reserved for this week.</H3>
              <Text>When you reserve a session, it will appear here.</Text>
              <Link
                to="/dashboard/sessions"
                sx={{
                  variant: "buttons.primary",
                  mt: 8,
                }}
              >
                Browse upcoming sessions
              </Link>
            </Box>
          )}
          {hasReservedSessions &&
            Object.entries(groupedSessions[currentWeek]).map(
              ([key, sessions]) => {
                if (sessions.length === 0) return null;
                return (
                  <Box key={key} data-session-card-day={key}>
                    <Text
                      sx={{
                        color: "primary",
                        fontWeight: 700,
                        textTransform: "uppercase",
                        position: "sticky",
                        bg: "light-gray",
                        top: "0px",
                        p: 3,
                        zIndex: 1,
                      }}
                    >
                      {dayjs(key).format("dddd, MMM DD")}
                    </Text>
                    {sessions.length === 0 && (
                      <Text>
                        Check back tomorrow for more upcoming live sessions!
                      </Text>
                    )}
                    {sessions.map((session) => {
                      return (
                        <LiveSessionCard
                          key={session.id}
                          session={session}
                          handleRSVP={handleRSVP}
                          handleCancelRSVP={handleCancelRSVP}
                        />
                      );
                    })}
                  </Box>
                );
              }
            )}
        </Fragment>
      )}
    </Box>
  );
};

export default SessionsReserved;
