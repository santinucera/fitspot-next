/**
 *
 *
 * Tests for <SessionsReserved />
 *
 *
 */

import React from "react";
import {
  fireEvent,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import SessionsReserved from "./SessionsReserved";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { renderWithRouter } from "test-utils/render-with-router";
import MockDate from "mockdate";

describe("<SessionsReserved />", () => {
  beforeEach(async () => {
    queryCache.setQueryData("user", () => createUser());
    MockDate.set("09/11/2020");
  });
  afterAll(() => {
    MockDate.reset();
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<SessionsReserved />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render reserved sessions", async () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<SessionsReserved />);
    expect(spy).not.toHaveBeenCalled();
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    await waitFor(() => {
      expect(screen.getByTestId("session-live")).toBeInTheDocument();
    });
  });

  it("should render a message to navigate to sessions schedule when no sessions are reserved", async () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<SessionsReserved />);
    expect(spy).not.toHaveBeenCalled();
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    fireEvent.click(screen.getByText("Sep 14 - 20"));
    await waitFor(() => {
      expect(
        screen.getByText("You have no sessions reserved for this week.")
      ).toBeInTheDocument();
    });
  });

  it.todo(
    "should request more past reserved sessions when clicking prev button"
  );

  it.todo(
    "should request more future reserved sessions when clicking next button"
  );
});
