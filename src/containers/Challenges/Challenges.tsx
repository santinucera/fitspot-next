/*
 *
 *
 * Challenges
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Grid } from "theme-ui";
import { FunctionComponent, useMemo, useState } from "react";
import { useQueryChallengesGetCurrentService } from "services/ChallengeService/useQueryChallengesGetCurrentService";
import Spinner from "components/Spinner";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "components/Tabs";
import { Dropdown, DropdownButton } from "components/Dropdown";
import IconChevron from "components/IconChevron";
import {
  CategorizedChallengeModel,
  useQueryChallengesGetCompletedService,
} from "services/ChallengeService";
import ChallengeCard from "components/ChallengeCard";
import ChallengeWinnerCard from "components/ChallengeWinnerCard";
import StepsLeaderBoard from "containers/StepsLeaderBoard";
import { filteredChallengesModel } from "services/ChallengeService/models";
import useChallengeRSVP from "hooks/useChallengeRSVP";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface ChallengeFilterOption {
  title: string;
  key: string;
}

interface ChallengesProps {}

const challengeFilterOptions: ChallengeFilterOption[] = [
  {
    title: "All",
    key: "all",
  },
  {
    title: "Active",
    key: "active",
  },
  {
    title: "Completed",
    key: "completed",
  },
  {
    title: "Upcoming",
    key: "upcoming",
  },
];

const Challenges: FunctionComponent<ChallengesProps> = () => {
  const [filterState, setFilterState] = useState<ChallengeFilterOption>(
    challengeFilterOptions[0]
  );

  const {
    isFetchedAfterMount,
    error,
    data = [],
  } = useQueryChallengesGetCurrentService();

  const {
    isFetchedAfterMount: isFetchedAfterMountCompletedChallenges,
    error: errorCompletedChallenges,
    data: completedChallenges = [],
  } = useQueryChallengesGetCompletedService();

  const { handleChallengeRSVP } = useChallengeRSVP("current-challenges");

  const joinedSessions = useMemo(
    () => data?.filter(({ isJoined }) => isJoined),
    [data]
  );

  const filteredChallenges = useMemo(
    () => filteredChallengesModel([...data, ...completedChallenges]),
    [data, completedChallenges]
  );

  const handlersFiltersChange = (filterState: ChallengeFilterOption) => () =>
    setFilterState(filterState);

  if (!isFetchedAfterMount || !isFetchedAfterMountCompletedChallenges)
    return <Spinner />;
  if (error !== null || errorCompletedChallenges !== null)
    return (
      <div>An error ocurred. Please refresh the browser and try again.</div>
    );

  return (
    <Box>
      <Tabs>
        <TabList>
          <Tab>All ({filteredChallenges.all.length})</Tab>
          <Tab>Mine ({joinedSessions.length})</Tab>
        </TabList>
        <Flex sx={{ my: 6 }} data-testid="challenges">
          <Dropdown
            handle={
              <Flex sx={{ alignItems: "center" }}>
                <Box>Show {filterState.title}</Box>
                <IconChevron sx={{ transform: "rotate(90deg)", ml: 1 }} />
              </Flex>
            }
            align="left"
          >
            {challengeFilterOptions.map(({ title, key }) => (
              <DropdownButton
                key={key}
                onClick={handlersFiltersChange({ title, key })}
              >
                {title}
              </DropdownButton>
            ))}
          </Dropdown>
        </Flex>
        <TabPanels>
          <TabPanel>
            <Grid
              sx={{
                gap: 6,
                gridTemplateColumns: "repeat(auto-fit, 330px)",
                justifyContent: ["center", "center", "normal"],
              }}
            >
              <StepsLeaderBoard />
              {filteredChallenges[
                filterState.key as keyof CategorizedChallengeModel
              ].map(
                ({
                  id,
                  isJoined,
                  isActive,
                  hasEnded,
                  name,
                  description,
                  leaderboard,
                  hasParticipants,
                  participantsCount,
                  startDate,
                  endDate,
                  daysLeft,
                  totalDays,
                  totalPossiblePoints,
                  myRank,
                  individualPointsSummary: { totalPoints },
                }) => {
                  if (hasEnded) {
                    return (
                      <ChallengeWinnerCard
                        key={id}
                        id={id}
                        name={name}
                        leaderboard={leaderboard}
                        startDate={startDate}
                        endDate={endDate}
                        myRank={myRank}
                        myPoints={totalPoints}
                      />
                    );
                  }
                  return (
                    <ChallengeCard
                      key={id}
                      id={id}
                      isJoined={isJoined}
                      isActive={isActive}
                      name={name}
                      description={description}
                      leaderboard={leaderboard}
                      hasParticipants={hasParticipants}
                      participantsCount={participantsCount}
                      startDate={startDate}
                      endDate={endDate}
                      daysLeft={daysLeft}
                      totalDays={totalDays}
                      totalPoints={totalPoints}
                      totalPossiblePoints={totalPossiblePoints}
                      rank={myRank}
                      handleRSVP={() => {
                        handleChallengeRSVP({ challengeId: id, isRSVP: true });
                      }}
                    />
                  );
                }
              )}
            </Grid>
          </TabPanel>
          <TabPanel>
            <Grid
              sx={{
                gap: 6,
                gridTemplateColumns: "repeat(auto-fit, 330px)",
                justifyContent: ["center", "center", "normal"],
              }}
            >
              {joinedSessions.map(
                ({
                  id,
                  isJoined,
                  isActive,
                  name,
                  description,
                  leaderboard,
                  hasParticipants,
                  participantsCount,
                  startDate,
                  endDate,
                  daysLeft,
                  totalDays,
                  totalPossiblePoints,
                  myRank,
                  individualPointsSummary: { totalPoints },
                }) => {
                  return (
                    <ChallengeCard
                      key={id}
                      id={id}
                      isJoined={isJoined}
                      isActive={isActive}
                      name={name}
                      description={description}
                      leaderboard={leaderboard}
                      hasParticipants={hasParticipants}
                      participantsCount={participantsCount}
                      startDate={startDate}
                      endDate={endDate}
                      daysLeft={daysLeft}
                      totalDays={totalDays}
                      totalPoints={totalPoints}
                      totalPossiblePoints={totalPossiblePoints}
                      rank={myRank}
                    />
                  );
                }
              )}
            </Grid>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default Challenges;
