/**
 *
 *
 * Tests for <Challenges />
 *
 *
 */

import React from "react";
import { ReactQueryConfigProvider, queryCache } from "react-query";
import {
  fireEvent,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import Challenges from "./Challenges";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  setChallengesResponse,
  createChallenge,
  setChallengesErrorResponse,
  setChallengesCompletedResponse,
  createCompletedChallenge,
} from "test-utils/challenge-service-test-utils";
import { User } from "services/types";
import { createUser } from "test-utils/user-service-test-utils";

const mockReportChallengeRSVP = jest.fn();
const mockReportHumanApiDeviceConnected = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportHumanApiDeviceConnected: mockReportHumanApiDeviceConnected,
    reportChallengeRSVP: mockReportChallengeRSVP,
  }))
);

describe("<Challenges />", () => {
  let user: User;
  const seeDetailsText = /see details/i;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", user);
  });

  afterAll(() => jest.unmock("services/UserService"));

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<Challenges />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should fetch challenges on mount", async () => {
    setChallengesCompletedResponse([createCompletedChallenge()]);
    const { getByTestId, getAllByText } = renderWithRouter(<Challenges />);
    expect(getByTestId("spinner")).toBeInTheDocument();
    await waitFor(() => {
      // One ChallengeCard, onChallengeWinnerCard
      expect(getAllByText(seeDetailsText)).toHaveLength(2);
    });
  });

  it("should display correct tab counts", async () => {
    setChallengesResponse([
      { ...createChallenge(), id: 2 },
      { ...createChallenge(), id: 3 },
    ]);
    const { getAllByText } = renderWithRouter(<Challenges />);
    await waitFor(() => {
      expect(getAllByText(seeDetailsText)).toHaveLength(5);
      expect(getAllByText("All (3)")).toHaveLength(1);
      expect(getAllByText("Mine (2)")).toHaveLength(1);
    });
  });

  it("should join a challenge and report to analytics", async () => {
    const challengeId = 2;
    setChallengesResponse([
      { ...createChallenge(), id: challengeId, leaderboard: [] },
    ]);
    const { getAllByText, getByTestId } = renderWithRouter(<Challenges />);
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(getAllByText("Join")).toHaveLength(1);
    });
    expect(getAllByText("All (2)")).toHaveLength(1);
    expect(getAllByText("Mine (0)")).toHaveLength(1);
    setChallengesResponse([{ ...createChallenge(), id: 2 }]);
    fireEvent.click(getAllByText("Join")[0]);
    await waitFor(() => getAllByText("Joined")[0]);
    expect(getAllByText("All (2)")).toHaveLength(1);
    expect(getAllByText("Mine (1)")).toHaveLength(1);
    await waitFor(() =>
      expect(mockReportChallengeRSVP).toHaveBeenCalledWith({
        userId: user.id,
        challengeId,
        isRSVP: true,
      })
    );
  });

  it("should show an error message when fetch fails", async () => {
    const consoleError = jest.fn();
    jest.spyOn(console, "error").mockImplementation(consoleError);
    setChallengesErrorResponse();
    const { getAllByText, getByTestId } = renderWithRouter(
      // TODO... create a renderWith helper
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <Challenges />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(
        getAllByText(
          "An error ocurred. Please refresh the browser and try again."
        )
      ).toHaveLength(1);
    });
    await waitFor(() => expect(consoleError).toBeCalled());
  });
});
