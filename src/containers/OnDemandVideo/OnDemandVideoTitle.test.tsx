/**
 *
 *
 * Tests for <OnDemandVideoTitle />
 *
 *
 */

import React from "react";
import OnDemandVideoTitle from "./OnDemandVideoTitle";
import dayjs from "dayjs";
import { renderWithRouter } from "test-utils/render-with-router";
import { createOnDemandVideoModel } from "test-utils/on-demand-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";

describe("<OnDemandVideoTitle />", () => {
  beforeEach(async () => {
    queryCache.setQueryData("user", () => createUser());
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <OnDemandVideoTitle onDemandVideo={createOnDemandVideoModel()} />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render activityCategory, name, duration, trainer, and date", () => {
    const activityCategory = "Chill";
    const name = "Example name";
    const startDate = dayjs();
    const endDate = dayjs().add(1, "hour");
    const trainer = "Trainer";
    const { getByText } = renderWithRouter(
      <OnDemandVideoTitle
        onDemandVideo={{
          data: {
            ...createOnDemandVideoModel().data,
            name,
            activityCategory,
            startDate,
            endDate,
            trainer,
          },
        }}
      />
    );
    expect(getByText(activityCategory)).toBeInTheDocument();
    expect(getByText(name)).toBeInTheDocument();
    expect(getByText(trainer)).toBeInTheDocument();
    expect(
      getByText(`${dayjs.duration(endDate.diff(startDate)).asMinutes()} mins`)
    ).toBeInTheDocument();
    expect(getByText(startDate.format("M/D/YY"))).toBeInTheDocument();
  });
});
