/**
 *
 *
 * <OnDemandVideoTitle />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Text, Button } from "theme-ui";
import { FunctionComponent } from "react";
import H2 from "components/H2";
import H5 from "components/H5";
import Avatar from "components/Avatar";
import PillarChip from "components/PillarChip";
import { Link } from "react-router-dom";
import IconClock from "components/IconClock";
import IconCalendar from "components/IconCalendar";
import dayjs from "utils/days";
import { useModal } from "containers/Modal/Modal";
import { OnDemandVideoModel } from "services/OnDemandVideoService";
import WatchTogetherModal from "containers/WatchTogetherModal";
import { CalendarPreference } from "services/CalendarService";
import { useQueryUserGetData } from "services/UserService";
import useExternalCalendar from "hooks/useExternalCalendar";
import ExternalCalendarPicker from "containers/ExternalCalendarPicker";

interface OnDemandVideoTitleProps {
  onDemandVideo: OnDemandVideoModel;
}

const OnDemandVideoTitle: FunctionComponent<OnDemandVideoTitleProps> = ({
  onDemandVideo: {
    data: {
      name,
      trainerAvatar,
      activityCategory,
      trainer,
      trainerPublicCode,
      startDate,
      endDate,
      id,
      description,
    },
  },
}) => {
  const { setModal, removeModal } = useModal();
  const { calendarPreference } = useQueryUserGetData();
  const { createExternalCalendarEvent } = useExternalCalendar();
  const today = dayjs();
  const todayAfterDuration = dayjs().add(
    endDate.diff(startDate, "minute"),
    "minute"
  );
  const openWatchTogetherModal = () => {
    setModal(
      <WatchTogetherModal
        isLive
        showInvite
        handleClose={removeModal}
        id={id}
        title={name}
        description={description}
        startDate={today}
        endDate={todayAfterDuration}
        type="video"
      />
    );
  };
  const handleAddToCalendar = () => {
    const eventOptions = {
      id,
      title: name,
      description,
      startDate: today,
      endDate: todayAfterDuration,
      path: `/dashboard/on-demand/${id}`,
    };
    if (
      calendarPreference !==
      CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED
    ) {
      createExternalCalendarEvent({
        ...eventOptions,
        calendarPreference,
      });
    } else {
      setModal(
        <ExternalCalendarPicker
          eventOptions={eventOptions}
          handleClose={removeModal}
        />
      );
    }
  };
  return (
    <Flex
      sx={{
        mb: [6, 0],
        justifyContent: "space-between",
        flexDirection: ["column", "row", "row"],
      }}
    >
      <Flex>
        <Avatar url={trainerAvatar} size={55} sx={{ mr: 4 }} />
        <Flex sx={{ flexDirection: "column", width: "100%" }}>
          <PillarChip
            pillar={activityCategory}
            sx={{ position: "relative", top: -1, left: 0 }}
          />
          <H2 sx={{ fontWeight: "normal", mr: [0, 0, 2] }}>{name}</H2>
          <Flex sx={{ color: "darker-gray" }}>
            <Flex sx={{ mr: [0, 8], alignItems: "center" }}>
              <H5 sx={{ fontWeight: "normal" }}>{trainer}</H5>
              <H5
                sx={{
                  fontWeight: "normal",
                  mx: 2,
                  display: ["none", "block", "block"],
                }}
              >
                |
              </H5>
              <Link
                to={`/dashboard/trainers/${trainerPublicCode}`}
                sx={{
                  textDecoration: "underline",
                  fontWeight: "bold",
                  alignItems: "center",
                  color: "darker-gray",
                  display: ["none", "flex", "flex"],
                }}
              >
                {"More about me >"}
              </Link>
            </Flex>
            <Text sx={{ mx: 2, display: ["block", "none", "none"] }}>|</Text>
            <Flex sx={{ mr: [0, 8], alignItems: "center" }}>
              <IconClock
                size={22}
                sx={{ mr: 2, display: ["none", "block", "block"] }}
              />
              <Text>
                {`${dayjs.duration(endDate.diff(startDate)).asMinutes()} mins`}
              </Text>
            </Flex>
            <Text sx={{ mx: 2, display: ["block", "none", "none"] }}>|</Text>
            <Flex sx={{ alignItems: "center" }}>
              <IconCalendar
                size={22}
                sx={{ mr: 2, display: ["none", "block", "block"] }}
              />
              <Text>{startDate.format("M/D/YY")}</Text>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Flex
        sx={{
          flexDirection: ["column", "row", "row"],
          mt: [2, 0, 0],
        }}
      >
        <Button
          sx={{ mr: [0, 2, 2], mb: [2, 0, 0], width: ["100%", "auto", "auto"] }}
          onClick={openWatchTogetherModal}
        >
          Watch together
        </Button>
        <Button
          sx={{ width: ["100%", "auto", "auto"] }}
          variant="secondary"
          onClick={handleAddToCalendar}
        >
          Add to calendar
        </Button>
      </Flex>
    </Flex>
  );
};

export default OnDemandVideoTitle;
