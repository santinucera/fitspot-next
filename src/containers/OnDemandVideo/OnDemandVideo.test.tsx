/**
 *
 *
 * Tests for <OnDemandVideo />
 *
 *
 */

import React from "react";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import {
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { rest } from "msw";
import server from "services/_mocks/server";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { createUser } from "test-utils/user-service-test-utils";
import { User } from "services/types";
import OnDemandVideo from "./OnDemandVideo";
import { onDemandVideoGetUrl } from "services/OnDemandVideoService";

const mockReportOnDemandView = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportOnDemandView: mockReportOnDemandView,
  }))
);

const videoId = 1234;
const userPublicId = "user_1234";

describe("<OnDemandVideo />", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<OnDemandVideo />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should load a on-demand video", async () => {
    renderWithMemoryRouter("/:videoId", [`/${videoId}`], <OnDemandVideo />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.getByTestId(`video`)).toBeInTheDocument();
  });

  it("should show an error message if on-demand video does not exist", async () => {
    const consoleError = jest.fn();
    jest.spyOn(console, "error").mockImplementation(consoleError);
    renderWithMemoryRouter(
      "/:videoId",
      [`/${videoId}`],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <OnDemandVideo />
      </ReactQueryConfigProvider>
    );
    server.use(
      rest.get(`${onDemandVideoGetUrl}:videoId`, (req, res, ctx) => {
        return res(ctx.status(404), ctx.json({ error: "not found" }));
      })
    );
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    await waitFor(() => expect(consoleError).toBeCalled());
    await waitFor(() =>
      expect(screen.getByTestId("on-demand-error")).toMatchInlineSnapshot(`
        <div
          data-testid="on-demand-error"
        >
          A video doesn't exist with id: 
          1234
        </div>
      `)
    );
  });

  it("should report view to analytics on load", () => {
    renderWithMemoryRouter("/:videoId", [`/${videoId}`], <OnDemandVideo />);
    expect(mockReportOnDemandView).toHaveBeenCalledWith({
      userId: user.id,
      videoId,
    });
  });

  it("should not show group video if url doesn't contain public user id", async () => {
    renderWithMemoryRouter("/:videoId", [`/${videoId}`], <OnDemandVideo />);
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.queryByTestId(`group-video`)).toBeNull();
  });

  it("should show group video video if url doesn't contain public user id", async () => {
    renderWithMemoryRouter(
      "/:videoId/private/:userPublicId",
      [`/${videoId}/private/${userPublicId}`],
      <OnDemandVideo privateGroup />
    );
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.queryByTestId(`group-video`)).toBeInTheDocument();
  });

  /**
   * I don't know how to hook into the `play` event on the video element.
   * I'll need to dig into how to test the event.
   */
  it.todo("should report play to analytics on play click");
});
