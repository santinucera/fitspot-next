/**
 *
 *
 * Tests for <VideoInformation />
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import VideoInformation from "./VideoInformation";

describe("<VideoInformation />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <VideoInformation
        additionalInfo="some info"
        description="some descriptionyarn te"
        attachment={{ url: "https://google.com" }}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render description, additionalInfo and attachment", async () => {
    const additionalInfo = "some info";
    const description = "some description";
    const { getByText } = render(
      <VideoInformation
        additionalInfo={additionalInfo}
        description={description}
        attachment={{ url: "https://google.com" }}
      />
    );
    await waitFor(() => {
      expect(getByText(additionalInfo)).toBeInTheDocument();
      expect(getByText(description)).toBeInTheDocument();
      expect(getByText("Download")).toBeInTheDocument();
    });
  });
});
