/*
 *
 *
 * OnDemandVideo
 *
 *
 */

/** @jsx jsx */
import { useCallback, useEffect } from "react";
import { jsx, Box, Flex, AspectRatio, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import { useQueryUserGetData } from "services/UserService";
import Spinner from "components/Spinner";
import { useParams } from "react-router-dom";
import useAnalytics from "hooks/useAnalytics";
import { useQueryOnDemandVideoGetService } from "services/OnDemandVideoService";
import VideoInformation from "./VideoInformation";
import OnDemandCarousel from "containers/OnDemandCarousel";
import OnDemandVideoTitle from "./OnDemandVideoTitle";

interface OnDemandVideoProps {
  privateGroup?: boolean;
}

const OnDemandVideo: FunctionComponent<OnDemandVideoProps> = ({
  privateGroup,
}) => {
  const urlParams = useParams();
  const { userPublicId } = urlParams;
  const videoId = Number(urlParams.videoId);
  const { reportOnDemandPlay, reportOnDemandView } = useAnalytics();
  const {
    isFetchedAfterMount,
    data: onDemandVideo,
  } = useQueryOnDemandVideoGetService({
    urlParams: { videoId },
  });

  // User is fetched by <App />
  const user = useQueryUserGetData();

  /**
   * On load
   */
  useEffect(() => {
    reportOnDemandView({ userId: user.id, videoId });
  }, [reportOnDemandView, user.id, videoId]);

  const videoRef = useCallback(
    (node: HTMLVideoElement) => {
      if (!node) return;
      node.addEventListener("play", () => {
        if (user) {
          reportOnDemandPlay({
            userId: user.id,
            videoId,
          });
        }
      });
    },
    [user, videoId, reportOnDemandPlay]
  );

  if (!isFetchedAfterMount) return <Spinner />;

  if (!onDemandVideo)
    return (
      <div data-testid="on-demand-error">
        A video doesn't exist with id: {videoId}
      </div>
    );

  const {
    data: {
      attachment,
      videoUrl,
      additionalInfo,
      description,
      trainer,
      trainerId,
      id,
    },
  } = onDemandVideo;

  return (
    <Box data-testid="on-demand-container">
      <OnDemandVideoTitle onDemandVideo={onDemandVideo} />
      <Divider />
      <Flex
        sx={{
          mt: 6,
          mb: 8,
          maxWidth: 1400,
          flexWrap: ["wrap", "wrap", "nowrap"],
        }}
      >
        <Box
          sx={{
            width: ["100%", "100%", "calc(100% - 320px)"],
            pr: [0, 0, 8],
          }}
        >
          <AspectRatio ratio={16 / 9}>
            <video
              controls={true}
              width="100%"
              height="100%"
              ref={videoRef}
              data-testid="video"
            >
              <source src={videoUrl} type="video/mp4" />
            </video>
          </AspectRatio>
        </Box>
        <Box sx={{ width: ["100%", "100%", 320], mt: [5, 5, 0] }}>
          {privateGroup ? (
            <iframe
              data-testid="group-video"
              title="TenSpot Virtual Session"
              allowFullScreen={true}
              allow="microphone; camera"
              src={`https://trainer.fitspotapp.com/Group_${userPublicId}_Video_${id}`}
              width="400"
              height="600"
              sandbox="allow-scripts allow-same-origin"
            />
          ) : (
            <VideoInformation
              attachment={attachment}
              additionalInfo={additionalInfo}
              description={description}
            />
          )}
        </Box>
      </Flex>
      <Box sx={{ mt: 8, width: "100%" }}>
        {privateGroup ? (
          <VideoInformation
            attachment={attachment}
            additionalInfo={additionalInfo}
            description={description}
          />
        ) : (
          <OnDemandCarousel
            title={`Watch now from ${trainer}`}
            trainerId={trainerId}
            limit={5}
            hasSeeMoreLink={false}
          />
        )}
      </Box>
    </Box>
  );
};

export default OnDemandVideo;
