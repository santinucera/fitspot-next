/**
 *
 *
 * <VideoInformation />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, Text } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import IconDownload from "components/IconDownload";
import { FileUrl } from "services/types";

interface VideoInformationProps {
  attachment: FileUrl | null;
  additionalInfo: string | null;
  description: string;
}

const VideoInformation: FunctionComponent<VideoInformationProps> = ({
  description,
  additionalInfo,
  attachment,
}) => {
  return (
    <Fragment>
      <Heading sx={{ mb: 2 }}>Description</Heading>
      <Text>{description}</Text>
      {additionalInfo && (
        <Fragment>
          <Heading sx={{ mb: 2, mt: 4 }}>Additional Info</Heading>
          <Text>{additionalInfo}</Text>
        </Fragment>
      )}
      {attachment?.url && (
        <Fragment>
          <Heading sx={{ mb: 2, mt: 4 }}>Get more information:</Heading>
          <a
            href={attachment.url}
            target="_blank"
            rel="noopener noreferrer"
            sx={{
              variant: "buttons.secondary",
            }}
          >
            <IconDownload size={15} sx={{ mr: 2 }} />
            Download
          </a>
        </Fragment>
      )}
    </Fragment>
  );
};

export default VideoInformation;
