/*
 *
 *
 * SettingsNotifications
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent, useState, useEffect } from "react";
import H2 from "components/H2";
import Switch from "components/Switch";
import FormRow from "components/FormRow";
import Spinner from "components/Spinner";
import useToast from "hooks/useToast";
import {
  useQueryNotificationsGetSettingsService,
  useMutationNotificationsPostSettingsService,
  NotificationsGetSettingsResponseData,
} from "services/NotificationService";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface SettingsNotificationsProps {}

const SettingsNotifications: FunctionComponent<SettingsNotificationsProps> = () => {
  const [state, setState] = useState<any>(null);

  const {
    isFetching,
    data = [],
  }: {
    data: any;
    isFetching: boolean;
  } = useQueryNotificationsGetSettingsService();

  const { addToast } = useToast();

  const [mutate] = useMutationNotificationsPostSettingsService({
    mutationConfig: {
      onSuccess() {
        addToast({
          title: "Success!",
          description: "Your notifications have been updated.",
          type: "success",
        });
      },
    },
  });

  useEffect(() => {
    if (isFetching) return;
    setState(
      data.reduce(
        (acc: any, { id, email }: NotificationsGetSettingsResponseData) => {
          acc[id] = email;
          return acc;
        },
        {}
      )
    );
  }, [isFetching, data]);

  const handleToggle = (event: any) => {
    const { name, checked } = event.target;
    setState(() => {
      const newState = {
        ...state,
        [name]: checked,
      };
      mutate(
        Object.entries(newState).map(([id, value]) => ({
          email: value,
          notification: value,
          id: Number(id),
        }))
      );
      return newState;
    });
  };

  if (state === null) return <Spinner />;

  return (
    <Box data-testid="notifications-container">
      <Box mb={7}>
        <H2>Newsletter</H2>
        <FormRow
          title="New Publication"
          content="Receive an email when a new Newsletter is being published"
          action={
            <Switch onChange={handleToggle} checked={state["60"]} name="60" />
          }
        />
      </Box>
      <Box mb={7}>
        <H2>Sessions</H2>
        <FormRow
          title="Upcoming Reminder"
          content="Receive an email when the session is about to start"
          action={
            <Switch onChange={handleToggle} checked={state["818"]} name="818" />
          }
        />
        <FormRow
          title="RSVP Reminder"
          content="Receive an email to remind RSVP to sessions"
          action={
            <Switch
              onChange={handleToggle}
              checked={state["8180"]}
              name="8180"
            />
          }
        />
      </Box>
      <Box mb={7}>
        <H2>Challenges</H2>
        <FormRow
          title="Starting Reminder"
          content="Receive an email to remember the starting of a Challenge"
          action={
            <Switch onChange={handleToggle} checked={state["44"]} name="44" />
          }
        />
      </Box>
    </Box>
  );
};

export default SettingsNotifications;
