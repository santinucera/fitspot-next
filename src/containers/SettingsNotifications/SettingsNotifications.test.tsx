/**
 *
 *
 * Tests for <SettingsNotifications />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SettingsNotifications from "./SettingsNotifications";

describe("<SettingsNotifications />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SettingsNotifications />);
    expect(spy).not.toHaveBeenCalled();
  });
});
