/**
 *
 *
 * Tests for <SettingsWearables />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SettingsWearables from "./SettingsWearables";

describe.skip("<SettingsWearables />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SettingsWearables />);
    expect(spy).not.toHaveBeenCalled();
  });
});
