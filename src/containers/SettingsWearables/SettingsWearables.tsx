/*
 *
 *
 * SettingsWearables
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button } from "theme-ui";
import { FunctionComponent } from "react";
import H2 from "components/H2";
import FormRow from "components/FormRow";
import { useHumanApiModal } from "hooks";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface SettingsWearablesProps {}

const SettingsWearables: FunctionComponent<SettingsWearablesProps> = () => {
  const { token } = useHumanApiModal();

  return (
    <Box mb={7}>
      <H2>Manage Your Wearable Devices</H2>
      <FormRow
        title="Wearable Data"
        content="By connecting your wearable device, you authorize Ten Spot to store data from your device, including steps for the Steps Leaderboard."
        action={<Button data-hapi-token={token}>Manage devices</Button>}
      />
    </Box>
  );
};

export default SettingsWearables;
