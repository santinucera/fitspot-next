/**
 *
 *
 * Tests for <TrainerLayout />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import TrainerLayout from "./TrainerLayout";
import { MemoryRouter } from "react-router-dom";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

describe("<TrainerLayout />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    queryCache.setQueryData("user", createUser());
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        reload: jest.fn(),
      },
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerLayout />, {
      wrapper: MemoryRouter,
    });
    expect(spy).not.toHaveBeenCalled();
  });
});
