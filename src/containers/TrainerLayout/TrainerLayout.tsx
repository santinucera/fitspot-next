/*
 *
 *
 * TrainerLayout
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import Layout from "containers/Layout";
import IconSession from "components/IconSession";
import IconGear from "components/IconGear";

const TrainerLayout: FunctionComponent = () => {
  return (
    <Layout
      dark
      links={[
        { label: "Sessions", icon: IconSession, to: "sessions" },
        { label: "Settings", icon: IconGear, to: "settings" },
      ]}
    />
  );
};

export default TrainerLayout;
