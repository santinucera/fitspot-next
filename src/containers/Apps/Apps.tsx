/*
 *
 *
 * Apps
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Image } from "theme-ui";
import { FunctionComponent } from "react";
import { useQueryAppsGetService } from "services/AppsService";
import { useQueryUserGetData } from "services/UserService";
import Spinner from "components/Spinner";
import H3 from "components/H3";
import H5 from "components/H5";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface AppsProps {}

const Apps: FunctionComponent<AppsProps> = () => {
  const user = useQueryUserGetData();

  const { isFetching, data, error } = useQueryAppsGetService({
    urlParams: {
      companyId: user?.companyList?.[0]?.id,
    },
  });

  if (isFetching) return <Spinner />;

  if (error) return <Text>An error occurred, please try again.</Text>;

  return (
    <Box>
      {data?.map(({ logo, id, content, category, url }) => (
        <a
          href={url}
          key={id}
          target="_blank"
          rel="noopener noreferrer"
          sx={{
            display: "flex",
            alignItems: "center",
            bg: "white",
            mb: 3,
            py: 5,
            color: "primary",
            textDecoration: "none",
          }}
        >
          <Box sx={{ mx: 5 }}>
            <Image src={logo} sx={{ width: 120 }} />
          </Box>
          <Box sx={{ ml: 5 }}>
            <H3>{content[1]}</H3>
            <H5 sx={{ mb: 3 }} data-testid="category">
              {category}
            </H5>
            <Text>{content[0]}</Text>
          </Box>
        </a>
      ))}
    </Box>
  );
};

export default Apps;
