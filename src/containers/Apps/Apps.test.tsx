/**
 *
 *
 * Tests for <Apps />
 *
 *
 */

import React from "react";
import {
  render,
  screen,
  waitForElementToBeRemoved,
  waitFor,
} from "@testing-library/react";
import Apps from "./Apps";
import server from "services/_mocks/server";
import { rest } from "msw";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import { appsGetUrl } from "services/AppsService";

describe("<Apps />", () => {
  beforeEach(() => {
    queryCache.setQueryData("user", createUser());
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Apps />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should show a loading spinner then load data", async () => {
    render(<Apps />);
    expect(screen.getByTestId("spinner")).toBeInTheDocument();
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(screen.getByText("Mental Wellness")).toBeInTheDocument();
  });
  it("should show an error message if the request fails", async () => {
    const consoleError = jest.fn();
    jest.spyOn(console, "error").mockImplementation(consoleError);
    server.use(
      rest.get(`${appsGetUrl}:id`, (req, res, ctx) => {
        return res(ctx.status(403), ctx.text("An error occurred"));
      })
    );
    render(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <Apps />
      </ReactQueryConfigProvider>
    );
    expect(screen.getByTestId("spinner")).toBeInTheDocument();
    await waitForElementToBeRemoved(() => screen.getByTestId("spinner"));
    expect(
      screen.getByText("An error occurred, please try again.")
    ).toBeInTheDocument();
    await waitFor(() => expect(consoleError).toBeCalled());
  });
});
