/**
 *
 *
 * <OnDemandCarousel />
 *
 *
 */

/** @jsx jsx */
import { jsx, Text } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { Link } from "react-router-dom";
import Carousel from "components/Carousel";
import { useQueryOnDemandVideosGetService } from "services/OnDemandVideoService";
import OnDemandCard from "components/OnDemandCard";
import IconArrow from "components/IconArrow";

interface OnDemandCarouselProps {
  categoryId?: number;
  trainerId?: number | null;
  limit?: number;
  startDate?: string;
  title: string;
  hasSeeMoreLink?: boolean;
}

const OnDemandCarousel: FunctionComponent<OnDemandCarouselProps> = ({
  categoryId,
  trainerId,
  title,
  limit = 11,
  startDate = "2020-06-01T00:00:00Z",
  hasSeeMoreLink = true,
}) => {
  const {
    isSuccess,
    isError,
    data,
    isFetchedAfterMount,
  } = useQueryOnDemandVideosGetService({
    queryParams: {
      categoryId,
      trainerId,
      limit,
      startDate,
    },
  });

  if (!isFetchedAfterMount) return null;
  if (data?.data.length === 0) return null;

  return (
    <Carousel
      seeAllLink={
        hasSeeMoreLink ? `/dashboard/on-demand?category=${categoryId}` : ""
      }
      count={data?.data.length}
      title={title}
      sx={{ p: 5 }}
    >
      {isSuccess && (
        <Fragment>
          {data?.data.map(
            ({
              id,
              name,
              thumbnailUrl,
              trainer,
              duration,
              activityCategory,
            }) => (
              <OnDemandCard
                key={id}
                id={id}
                name={name}
                thumbnailUrl={thumbnailUrl}
                trainer={trainer}
                duration={duration}
                activityCategory={activityCategory}
              />
            )
          )}
          {hasSeeMoreLink && (
            <Link
              data-testid="see-more-button"
              sx={{
                width: 300,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color: "primary",
                textDecoration: "none",
                fontWeight: 700,
              }}
              to={`/dashboard/on-demand?category=${categoryId}`}
            >
              <Text sx={{ lineHeight: "24px" }}>SEE MORE</Text>
              <IconArrow direction="right" />
            </Link>
          )}
        </Fragment>
      )}
      {isError && <Text>There was an issue with loading the videos</Text>}
    </Carousel>
  );
};

export default OnDemandCarousel;
