/**
 *
 *
 * Tests for <OnDemandCarousel />
 *
 *
 */

import { waitFor, screen } from "@testing-library/react";
import React from "react";
import { ReactQueryConfigProvider } from "react-query";
import { setOnDemandResponse } from "test-utils/on-demand-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";

import OnDemandCarousel from "./OnDemandCarousel";

describe("<OnDemandCarousel />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<OnDemandCarousel categoryId={2} title="Move" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render on demand videos", async () => {
    renderWithRouter(<OnDemandCarousel categoryId={2} title="Move" />);
    await waitFor(() => {
      expect(screen.getByTestId("on-demand-card-5838")).toBeInTheDocument();
    });
  });

  it("should render a see more link", async () => {
    renderWithRouter(
      <OnDemandCarousel categoryId={2} title="Move" hasMore={true} />
    );
    await waitFor(() => {
      expect(screen.getByTestId("see-more-button")).toBeInTheDocument();
    });
  });

  it("should not render a see more link", async () => {
    renderWithRouter(
      <OnDemandCarousel categoryId={2} title="Move" hasMore={false} />
    );
    await waitFor(() => {
      expect(screen.queryByTestId("see-more-button")).not.toBeInTheDocument();
    });
  });

  it("should render an error message when request fails", async () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();
    setOnDemandResponse(500);
    renderWithRouter(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <OnDemandCarousel categoryId={2} title="Move" />
      </ReactQueryConfigProvider>
    );
    await waitFor(() => {
      expect(
        screen.getByText("There was an issue with loading the videos")
      ).toBeInTheDocument();
    });
    expect(spy).toHaveBeenCalled();
  });
});
