/**
 *
 *
 * Tests for <StandaloneChat />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import StandaloneChat from "./StandaloneChat";

describe("<StandaloneChat />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<StandaloneChat />);
    expect(spy).not.toHaveBeenCalled();
  });
});
