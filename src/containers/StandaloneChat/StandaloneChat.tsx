/*
 *
 *
 * StandaloneChat
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading } from "theme-ui";
import { FunctionComponent } from "react";
import Chat from "containers/Chat";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface StandaloneChatProps {}

const StandaloneChat: FunctionComponent<StandaloneChatProps> = () => {
  return (
    <Box
      sx={{
        width: "100vw",
        minWidth: "100%",
        height: "100vh",
        minHeight: "100%",
        backgroundColor: "light-gray",
      }}
    >
      <Box
        sx={{
          width: 300,
          height: 450,
          padding: 3,
        }}
      >
        <Heading sx={{ mb: 2, fontSize: 3 }}>Live Chat</Heading>
        <Chat />
      </Box>
    </Box>
  );
};

export default StandaloneChat;
