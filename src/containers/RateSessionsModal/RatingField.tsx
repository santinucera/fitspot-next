/**
 *
 *
 * <RatingField />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Text } from "theme-ui";
import { FunctionComponent } from "react";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Control, Controller } from "react-hook-form";

interface RatingFieldProps {
  title?: string;
  subtitle: string;
  name: string;
  control: Control;
  trigger: () => void;
}

const RatingField: FunctionComponent<RatingFieldProps> = ({
  title,
  subtitle,
  name,
  control,
  trigger,
}) => {
  return (
    <Flex
      sx={{
        width: "100%",
        flexDirection: "column",
        mb: 7,
      }}
    >
      {title && <Text sx={{ mb: 2, wordBreak: "break-all" }}>{title}</Text>}
      <Flex
        sx={{
          flexDirection: ["column", "row"],
          justifyContent: "space-between",
          alignItems: "baseline",
        }}
      >
        <Text sx={{ fontSize: 2, fontWeight: "bold" }}>{subtitle}</Text>
        <Controller
          control={control}
          name={name}
          defaultValue={0}
          render={({ value, onChange }) => (
            <Rating
              name={name}
              size="large"
              value={value}
              onChange={(_, newValue) => {
                onChange(newValue);
                trigger();
              }}
              sx={{
                "& .MuiRating-label": {
                  color: "yellow",
                },
              }}
              emptyIcon={
                <StarBorderIcon sx={{ color: "primary" }} fontSize="inherit" />
              }
            />
          )}
        />
      </Flex>
    </Flex>
  );
};

export default RatingField;
