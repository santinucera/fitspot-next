/**
 *
 *
 * Tests for <RatingField />
 *
 *
 */

import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import { useForm } from "react-hook-form";
import RatingField from "./RatingField";

describe("<RatingField />", () => {
  it("should not log errors in console", () => {
    const El = () => {
      const { control, trigger } = useForm({ defaultValues: { rating: 4 } });
      return (
        <div>
          <RatingField
            control={control}
            trigger={trigger}
            name="rating"
            title="Rating"
            subtitle="Field"
          />
        </div>
      );
    };
    const spy = jest.spyOn(global.console, "error");
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render title and subtitle", () => {
    const title = "Title";
    const subtitle = "Subtitle";
    const El = () => {
      const { control, trigger } = useForm({ defaultValues: { rating: 4 } });
      return (
        <div>
          <RatingField
            control={control}
            trigger={trigger}
            name="rating"
            title={title}
            subtitle={subtitle}
          />
        </div>
      );
    };
    const { queryByText } = render(<El />);
    expect(queryByText(title)).toBeInTheDocument();
    expect(queryByText(subtitle)).toBeInTheDocument();
  });

  it("should modify values from form", async () => {
    const El = () => {
      const name = "rating";
      const { control, watch, trigger } = useForm({
        defaultValues: {
          [name]: 4,
        },
      });
      const rating = watch(name);
      return (
        <div>
          <RatingField
            control={control}
            trigger={trigger}
            name={name}
            title="Title"
            subtitle="Subtitle"
          />
          <span>{rating}</span>
        </div>
      );
    };
    const { findAllByRole, queryByText } = render(<El />);
    await waitFor(() => {
      expect(queryByText("4")).toBeInTheDocument();
    });
    const radioButtons = await findAllByRole("radio");
    await waitFor(() => {
      fireEvent.click(radioButtons[0]);
    });
    expect(queryByText("1")).toBeInTheDocument();
  });
});
