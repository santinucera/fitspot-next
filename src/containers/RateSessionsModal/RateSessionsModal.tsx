/*
 *
 *
 * RateSessionsModal
 *
 *
 */

/** @jsx jsx */
import { jsx, Text, Flex, Heading, Button } from "theme-ui";
import { FunctionComponent, useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import {
  CustomerDashboardSessionModel,
  NonRatedSessionModel,
  useMutationRateSessionPatchService,
  useMutationRateSessionPostService,
} from "services/SessionService";
import SessionTime from "components/SessionTime";
import Input from "components/Input";
import RatingField from "./RatingField";
import { useModal } from "containers/Modal/Modal";
import Avatar from "components/Avatar";

interface RateSessionsModalProps {
  ratings: {
    ratingPoints?: number | null;
    ratingStreamQuality?: number | null;
    ratingTrainer?: number | null;
    ratingLocation?: number | null;
  };
  session: Pick<
    CustomerDashboardSessionModel | NonRatedSessionModel,
    "name" | "trainerAvatar" | "startDate" | "endDate" | "liveSession" | "id"
  >;
  mutationConfig: {
    onSuccess: () => void;
    onError: () => void;
  };
}

type FIELDS = {
  session: number;
  streamQuality: number;
  location: number;
  trainer: number;
  text: string;
};

const RateSessionsModal: FunctionComponent<RateSessionsModalProps> = ({
  ratings: { ratingPoints, ratingStreamQuality, ratingTrainer, ratingLocation },
  session: { name, trainerAvatar, startDate, endDate, liveSession, id },
  mutationConfig,
}) => {
  const { removeModal } = useModal();
  const { handleSubmit, control, watch, trigger } = useForm<FIELDS>({
    defaultValues: {
      session: ratingPoints || 0,
      streamQuality: ratingStreamQuality || 0,
      location: ratingLocation || 0,
      trainer: ratingTrainer || 0,
      text: "",
    },
  });

  const hasRatedSession =
    ratingPoints !== null ||
    ratingStreamQuality !== null ||
    ratingTrainer !== null ||
    ratingLocation !== null;

  const disabled =
    watch("text").length === 0 &&
    watch("trainer") === 0 &&
    watch(liveSession ? "streamQuality" : "location") === 0;

  useEffect(() => {
    trigger();
  }, [trigger]);

  const trainerImage =
    typeof trainerAvatar === "string"
      ? trainerAvatar
      : trainerAvatar?.url || "";

  const [rateSessionPost] = useMutationRateSessionPostService({
    mutationConfig,
  });
  const [rateSessionPatch] = useMutationRateSessionPatchService({
    mutationConfig,
  });
  const rateSession = hasRatedSession ? rateSessionPatch : rateSessionPost;

  const onSubmit: SubmitHandler<FIELDS> = (rating) => {
    rateSession({
      sessionId: id,
      rating: { ...rating, overall: rating.session },
    });
  };

  return (
    <Flex
      sx={{
        alignItems: "center",
        flexDirection: "column",
        width: "100%",
        p: 2,
      }}
    >
      <Text sx={{ mb: 4 }}>Feedback</Text>
      <Flex
        sx={{
          backgroundColor: "light-yellow",
          flexDirection: "row",
          padding: 4,
          width: "100%",
          mb: 7,
        }}
      >
        <Avatar size={80} url={trainerImage} />
        <Flex sx={{ flexDirection: "column", ml: 4 }}>
          <Heading sx={{ fontSize: 5 }}>{name}</Heading>
          <SessionTime startDate={startDate} endDate={endDate} />
        </Flex>
      </Flex>
      <RatingField
        name="session"
        control={control}
        trigger={trigger}
        subtitle="How was the session?"
      />
      <RatingField
        name="trainer"
        control={control}
        trigger={trigger}
        subtitle="How was the instructor?"
      />
      {liveSession ? (
        <RatingField
          name="streamQuality"
          control={control}
          trigger={trigger}
          subtitle="How was the audio and video?"
        />
      ) : (
        <RatingField
          name="location"
          control={control}
          trigger={trigger}
          subtitle="How was the location?"
        />
      )}
      <Flex sx={{ flexDirection: "column", width: "100%", mb: 3 }}>
        <Text sx={{ mb: 2 }}>Suggestions for improving your experience?</Text>
        <Input
          control={control}
          name="text"
          variant="standard"
          placeholder="Write here any suggestions you may have..."
        />
      </Flex>
      <Flex sx={{ flexDirection: ["column-reverse", "row"] }}>
        <Button
          onClick={removeModal}
          variant="secondary"
          data-testid="skip-button"
          sx={{ mr: [0, 4], width: "100%" }}
        >
          Skip for Now
        </Button>
        <Button
          onClick={handleSubmit(onSubmit)}
          variant="primary"
          data-testid="submit-button"
          disabled={disabled}
          sx={{ mb: [4, 0], width: "100%" }}
        >
          Submit Feedback
        </Button>
      </Flex>
    </Flex>
  );
};

export default RateSessionsModal;
