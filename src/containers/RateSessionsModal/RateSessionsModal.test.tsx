/**
 *
 *
 * Tests for <RateSessionsModal />
 *
 *
 */

import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import RateSessionsModal from "./RateSessionsModal";
import {
  createCustomerDashboardSessionModel,
  createNonRatedSessionModel,
  setRateSessionPatchResponse,
} from "test-utils/session-service-test-utils";

const { queryByText, findAllByRole, getByTestId } = screen;

describe("<RateSessionsModal />", () => {
  it("should not log errors in console with CustomDashboardSessionModel", async () => {
    const spy = jest.spyOn(global.console, "error");
    const {
      name,
      trainerAvatar,
      startDate,
      endDate,
      liveSession,
      id,
    } = createCustomerDashboardSessionModel();
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: null,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError: () => {},
          onSuccess: () => {},
        }}
      />
    );
    expect(spy).not.toHaveBeenCalled();
    await waitFor(() => expect(queryByText(name)).toBeInTheDocument());
  });

  it("should not log errors in console with CustomDashboardSession", async () => {
    const spy = jest.spyOn(global.console, "error");
    const {
      name,
      trainerAvatar,
      startDate,
      endDate,
      liveSession,
      id,
    } = createNonRatedSessionModel();
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: null,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError: () => {},
          onSuccess: () => {},
        }}
      />
    );
    expect(spy).not.toHaveBeenCalled();
    await waitFor(() => expect(queryByText(name)).toBeInTheDocument());
  });

  it("should show session ratings", async () => {
    const {
      name,
      trainerAvatar,
      startDate,
      endDate,
      liveSession,
      id,
    } = createNonRatedSessionModel();
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: null,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError: () => {},
          onSuccess: () => {},
        }}
      />
    );
    await waitFor(() => {
      expect(queryByText("How was the session?")).toBeInTheDocument();
      expect(queryByText("How was the instructor?")).toBeInTheDocument();
      expect(queryByText("How was the audio and video?")).toBeInTheDocument();
    });
  });

  it("should show location rating", async () => {
    const { name, trainerAvatar, startDate, endDate, liveSession, id } = {
      ...createNonRatedSessionModel(),
      liveSession: false,
    };
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: null,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError: () => {},
          onSuccess: () => {},
        }}
      />
    );
    await waitFor(() =>
      expect(queryByText("How was the location?")).toBeInTheDocument()
    );
  });

  it("should rate a session and call the success function", async () => {
    const onSuccess = jest.fn();
    const {
      name,
      trainerAvatar,
      startDate,
      endDate,
      liveSession,
      id,
    } = createCustomerDashboardSessionModel();
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: null,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError: () => {},
          onSuccess,
        }}
      />
    );
    const radioButtons = await findAllByRole("radio");
    const btn = await getByTestId("submit-button");
    await waitFor(() => fireEvent.click(radioButtons[2]));
    await waitFor(() => fireEvent.click(radioButtons[7]));
    await waitFor(() => fireEvent.click(btn));
    await waitFor(() => {
      expect(onSuccess).toHaveBeenCalled();
    });
  });

  it("should handle error when rating patch request fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const onError = jest.fn();
    const onSuccess = jest.fn();
    const {
      name,
      trainerAvatar,
      startDate,
      endDate,
      liveSession,
      id,
    } = createCustomerDashboardSessionModel();
    render(
      <RateSessionsModal
        ratings={{
          ratingPoints: 5,
          ratingStreamQuality: null,
          ratingTrainer: null,
          ratingLocation: null,
        }}
        session={{ name, trainerAvatar, startDate, endDate, liveSession, id }}
        mutationConfig={{
          onError,
          onSuccess,
        }}
      />
    );
    setRateSessionPatchResponse(500);
    const radioButtons = await findAllByRole("radio");
    const btn = await getByTestId("submit-button");
    await waitFor(() => fireEvent.click(radioButtons[2]));
    await waitFor(() => fireEvent.click(radioButtons[7]));
    await waitFor(() => fireEvent.click(btn));
    await waitFor(() => {
      expect(onError).toHaveBeenCalled();
      expect(onSuccess).toBeCalledTimes(0);
    });
  });
});
