/*
 *
 *
 * SettingsProfile
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Button, Grid, Flex, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Avatar from "components/Avatar";
import Select from "components/Select";
import Input from "components/Input";
import DatePicker from "components/DatePicker";
import { useModal } from "containers/Modal/Modal";
import AvatarModal from "./AvatarModal";
import { useQueryUserGetData } from "services/UserService";
import useToast from "hooks/useToast";
import H2 from "components/H2";
import { useMutationUserPatchService } from "services/UserService";
import { queryCache } from "react-query";
import daysjs from "utils/days";

interface SettingsProfileProps {}

const genderOptions = [
  { value: 0, label: "Male" },
  { value: 1, label: "Female" },
  { value: 2, label: "Unspecified" },
];

type Fields = {
  firstName: string;
  gender: number;
  customer: {
    birthday: Date;
  };
};

const SettingsProfile: FunctionComponent<SettingsProfileProps> = () => {
  const user = useQueryUserGetData();
  const { setModal } = useModal();

  const { handleSubmit, control } = useForm({
    defaultValues: {
      firstName: user?.firstName,
      lastName: user?.lastName,
      gender: user?.gender,
      customer: {
        birthday: user?.customer?.birthday,
      },
    },
  });

  const { addToast } = useToast();

  const [handleUserPatch, { isLoading }] = useMutationUserPatchService({
    mutationConfig: {
      onSuccess(data) {
        addToast({
          title: "Success!",
          description: "Your profile has been updated.",
          type: "success",
        });
        queryCache.setQueryData("user", data);
      },
    },
  });

  // Todo: type this handler
  const onSubmit: SubmitHandler<any> = (data) => {
    handleUserPatch({
      ...data,
      customer: {
        birthday: daysjs(data.customer.birthday).format("YYYY-MM-DD"),
      },
    });
  };

  const handleOpenImageEditor = () => setModal(<AvatarModal />);

  return (
    <Box>
      <H2>Edit Profile</H2>
      <Flex sx={{ my: 7, alignItems: "center" }}>
        <Avatar url={user?.avatar?.url} size={80} sx={{ mr: 7 }} />
        <Button onClick={handleOpenImageEditor}>Update Picture</Button>
      </Flex>
      <Box as="form" onSubmit={handleSubmit(onSubmit)}>
        <Grid gap={5} columns={[0, 2]} mb={2} sx={{ maxWidth: 780 }}>
          <Input control={control} label="First Name" name="firstName" />
          <Input control={control} label="Last Name" name="lastName" />
        </Grid>
        <Grid gap={5} columns={[0, 2]} sx={{ maxWidth: 780 }}>
          <DatePicker
            control={control}
            label="Your Birthday"
            name="customer.birthday"
          />
          <Select
            control={control}
            options={genderOptions}
            label="Gender"
            name="gender"
          />
        </Grid>
        <Divider />
        <Button type="submit" mt={8} disabled={isLoading}>
          {isLoading ? "Saving..." : "Save Changes"}
        </Button>
      </Box>
    </Box>
  );
};

export default SettingsProfile;
