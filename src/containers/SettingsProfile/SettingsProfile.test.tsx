/**
 *
 *
 * Tests for SettingsProfile
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SettingsProfile from "./SettingsProfile";

describe.skip("<SettingsProfile />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SettingsProfile profile={{ avatar: { url: "/path" } }} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
