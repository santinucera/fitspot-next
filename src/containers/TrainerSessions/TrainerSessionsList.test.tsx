/**
 *
 *
 * Tests for <TrainerSessionsList />
 *
 *
 */

import React from "react";
import { screen } from "@testing-library/react";
import { queryCache } from "react-query";
import TrainerSessionsList from "./TrainerSessionsList";
import { createTrainerSessionModel } from "test-utils/session-service-test-utils";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { SessionStatus } from "services/SessionService";
import dayjs from "utils/days";

const { getByTestId, getByText } = screen;

describe("<TrainerSessionsList />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=1`],
      <TrainerSessionsList title="some title" type="inProgress" />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render sessions", () => {
    const title = "Some Title";
    const session = {
      ...createTrainerSessionModel(),
      status: SessionStatus.inProgress,
      startDate: dayjs(),
      endDate: dayjs(),
    };
    const session1 = { ...session, name: "First Session", id: 1 };
    const session2 = { ...session, name: "Second Session", id: 2 };
    const sessions = [
      { ...session, name: "First Session", id: 1 },
      { ...session, name: "Second Session", id: 2 },
    ];
    queryCache.setQueryData("trainers-sessions", sessions);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=1`],
      <TrainerSessionsList title={title} type="inProgress" />
    );
    expect(getByTestId("trainer-sessions-list-header")).toHaveTextContent(
      title
    );
    expect(
      getByTestId(`trainer-sessions-list-item-${sessions[0].id}`)
    ).toHaveTextContent(session1.name);
    expect(
      getByTestId(`trainer-sessions-list-item-detail-link-${sessions[0].id}`)
    ).toHaveClass("active");
    expect(
      getByTestId(`trainer-sessions-list-item-${sessions[1].id}`)
    ).toHaveTextContent(session2.name);
  });

  it("should render message if there are no sessions", () => {
    queryCache.setQueryData("trainers-sessions", []);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=1`],
      <TrainerSessionsList title="some title" type="inProgress" />
    );
    expect(getByText("No some title sessions found.")).toBeInTheDocument();
  });
});
