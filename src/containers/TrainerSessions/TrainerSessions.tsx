/*
 *
 *
 * TrainerSessions
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui";
import { FunctionComponent, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { useQueryTrainersSessionsGetService } from "services/SessionService";
import dayjs from "utils/days";
import Spinner from "components/Spinner";
import TrainerSessionsList from "./TrainerSessionsList";
import TrainerSessionDetails from "containers/TrainerSessionDetails";
import useSelectedSession from "hooks/useSelectedSession";
import useCategorizedTrainerSessions from "hooks/useCategorizedTrainerSessions";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
const TrainerSessions: FunctionComponent = () => {
  const scrollTopRef = useRef<HTMLDivElement | null>(null);
  const {
    isLoading,
    isError,
    data: sessions,
    error,
  } = useQueryTrainersSessionsGetService({
    urlParams: {
      dtStart: dayjs().subtract(1, "month").startOf("month").utc().format(),
      dtEnd: dayjs().add(2, "month").endOf("month").utc().format(),
    },
  });

  const navigate = useNavigate();
  const { inProgress, pending, upcoming } = useCategorizedTrainerSessions();
  const { selectedSession, selectedSessionId } = useSelectedSession();

  // Show the first available session on load
  useEffect(() => {
    if (sessions && !selectedSessionId) {
      // Find the first session on the categorized lists and navigate to it.
      const firstAvailableSession = inProgress.length
        ? inProgress[0]
        : pending.length
        ? pending[0]
        : upcoming.length
        ? upcoming[0]
        : null;

      if (firstAvailableSession) {
        navigate(`?session_id=${firstAvailableSession.id}`);
      }
    }

    if (scrollTopRef.current) {
      scrollTopRef.current.scrollIntoView();
    }
  }, [
    selectedSession,
    selectedSessionId,
    inProgress,
    navigate,
    pending,
    sessions,
    upcoming,
  ]);

  if (isLoading) {
    return <Spinner />;
  }

  if (isError) {
    return (
      <div>
        {/trainer profile is not complete/.test(error?.response?.data.message)
          ? "Your account is under review, please check back later."
          : "Error: unable to fetch sessions."}
      </div>
    );
  }

  return (
    <Flex
      sx={{
        flex: "0 1 auto",
        flexDirection: ["column", "row"],
      }}
      data-testid="trainer-sessions"
    >
      <Flex
        sx={{
          height: ["360px", "611px"],
          overflow: "hidden",
          mr: ["0", "30px"],
          mb: [7, "0"], // 24px
          flex: ["1 0 auto", "0 1 333px"],
        }}
        data-testid="inner-scroll-container"
      >
        <Flex
          sx={{
            backgroundColor: "white",
            flexDirection: "column",
            overflowY: "scroll",
            height: "100%",
            flex: "1 1 auto",
            minHeight: 0,
          }}
          data-testid="trainer-sessions-lists"
        >
          <TrainerSessionsList title="In Progress" type="inProgress" />

          <TrainerSessionsList title="Pending" type="pending" />

          <TrainerSessionsList title="Upcoming" type="upcoming" />
        </Flex>
      </Flex>

      {selectedSessionId && !selectedSession && (
        <Box>Session with id {selectedSessionId} could not be found.</Box>
      )}

      {selectedSession && <TrainerSessionDetails />}
    </Flex>
  );
};

export default TrainerSessions;
