/**
 *
 *
 * <TrainerSessionsListItem />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Heading, Text } from "theme-ui";
import { forwardRef } from "react";
import { Link } from "react-router-dom";
import TrainerSessionTimes from "./TrainerSessionTimes";
import { Dayjs } from "dayjs";
import Avatar from "components/Avatar";
import useSelectedSession from "hooks/useSelectedSession";

interface TrainerSessionsListItemProps {
  id: number;
  name: string;
  rsvpCount: number;
  startDate: Dayjs;
  endDate: Dayjs;
  avatarUrl?: string;
}

const TrainerSessionsListItem = forwardRef<
  HTMLLIElement,
  TrainerSessionsListItemProps
>(({ id, name, rsvpCount, startDate, endDate, avatarUrl }, ref) => {
  const { selectedSessionId } = useSelectedSession();

  return (
    <li ref={ref} data-testid={`trainer-sessions-list-item-${id}`}>
      <Link
        className={selectedSessionId === id ? "active" : undefined}
        to={`?session_id=${id}`}
        sx={{
          display: "block",
          backgroundColor: "white",
          color: "dark-gray",
          fontSize: 0,
          fontWeight: "body",
          textDecoration: "none",
          lineHeight: "21px",
          borderColor: "gray",
          borderStyle: "solid",
          borderWidth: "0 0 1px 0",
          padding: "12px 15px",
          "&.active, &:hover": {
            backgroundColor: "medium-light-gray",
            color: "darker-gray",
          },
        }}
        data-testid={`trainer-sessions-list-item-detail-link-${id}`}
      >
        <Flex
          sx={{
            flexDirection: "row",
          }}
        >
          <Avatar
            url={avatarUrl}
            size={34}
            sx={{
              mr: "18px",
            }}
          />

          <Flex
            sx={{
              flexDirection: "column",
            }}
          >
            <Heading
              as="h4"
              sx={{
                fontWeight: "body",
                fontSize: 0,
                lineHeight: "21px",
                margin: 0,
                color: "primary",
              }}
              data-testid="trainer-session-title"
            >
              {name}
            </Heading>
            <Text data-testid="trainer-session-times">
              <TrainerSessionTimes startDate={startDate} endDate={endDate} />
              {rsvpCount} {rsvpCount === 1 ? "RSVP" : "RSVPs"}
            </Text>
          </Flex>
        </Flex>
      </Link>
    </li>
  );
});

export default TrainerSessionsListItem;
