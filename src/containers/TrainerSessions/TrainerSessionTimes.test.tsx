/**
 *
 *
 * Tests for <TrainerSessionTimes />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import TrainerSessionTimes from "./TrainerSessionTimes";
import dayjs from "utils/days";

describe("<TrainerSessionTimes />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSessionTimes startDate={dayjs()} endDate={dayjs()} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
