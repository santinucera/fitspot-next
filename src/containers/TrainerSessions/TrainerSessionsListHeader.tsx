/**
 *
 *
 * <TrainerSessionsListHeader />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading } from "theme-ui";
import { FunctionComponent } from "react";

const TrainerSessionsListHeader: FunctionComponent = ({ children }) => {
  return (
    <Heading
      as="h3"
      sx={{
        lineHeight: "54px",
        fontWeight: "heading",
        textTransform: "uppercase",
        fontSize: 0,
        padding: "0 16px",
        borderWidth: "0 0 1px 0",
        borderColor: "gray",
        borderStyle: "solid",
      }}
    >
      {children}
    </Heading>
  );
};

export default TrainerSessionsListHeader;
