/**
 *
 *
 * <NoSessionsFoundMessage />
 *
 *
 */

/** @jsx jsx */
import { jsx, Text } from "theme-ui";
import { FunctionComponent } from "react";

interface NoSessionsFoundMessageProps {
  categoryTitle: string;
}

const NoSessionsFoundMessage: FunctionComponent<NoSessionsFoundMessageProps> = ({
  categoryTitle,
}) => {
  return (
    <Text
      sx={{
        backgroundColor: "white",
        color: "dark-gray",
        fontSize: 0,
        fontWeight: "body",
        textDecoration: "none",
        lineHeight: "21px",
        borderColor: "gray",
        borderStyle: "solid",
        borderWidth: "0 0 1px 0",
        padding: "12px 15px",
      }}
    >
      No {categoryTitle.toLowerCase()} sessions found.
    </Text>
  );
};

export default NoSessionsFoundMessage;
