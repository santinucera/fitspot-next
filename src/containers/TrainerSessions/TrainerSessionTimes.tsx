/**
 *
 *
 * <TrainerSessionTimes />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import { Dayjs } from "dayjs";

interface TrainerSessionTimesProps {
  startDate: Dayjs;
  endDate: Dayjs;
}

const TrainerSessionTimes: FunctionComponent<TrainerSessionTimesProps> = ({
  startDate,
  endDate,
}) => {
  return (
    <Box>
      {startDate.format("ddd, MMM D")}
      {" | "}
      {startDate.format("h:mm")}
      {" - "}
      {endDate.format("h:mma")}
    </Box>
  );
};

export default TrainerSessionTimes;
