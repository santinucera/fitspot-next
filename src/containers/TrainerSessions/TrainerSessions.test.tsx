/**
 *
 *
 * Tests for <TrainerSessions />
 *
 *
 */

import React from "react";
import { waitForElementToBeRemoved, screen } from "@testing-library/react";
import { ReactQueryConfigProvider } from "react-query";
import { useLocation } from "react-router-dom";
import TrainerSessions from "./TrainerSessions";
import {
  renderWithRouter,
  renderWithMemoryRouter,
} from "test-utils/render-with-router";
import { mockTrainersSessionsGetResponseData } from "services/SessionService";
import { setTrainerSessionsResponse } from "test-utils/session-service-test-utils";

const { getByTestId, queryByTestId, getByText } = screen;

describe("<TrainerSessions />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<TrainerSessions />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should select the session corresponding to the session_id url search param", async () => {
    const { id, name } = mockTrainersSessionsGetResponseData[0];
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <TrainerSessions />);
    await waitForElementToBeRemoved(() => getByTestId("spinner"));

    expect(
      getByTestId(`trainer-sessions-list-item-detail-link-${id}`)
    ).toHaveClass("active");
    expect(getByTestId("trainer-session-details")).toHaveTextContent(name);
  });

  it("should forward the user to the first available session", async () => {
    const El = () => {
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.search}</div>
          <TrainerSessions />
        </div>
      );
    };
    renderWithMemoryRouter(
      "/*",
      [`/`],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <El />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("location")).toHaveTextContent(
      `?session_id=${mockTrainersSessionsGetResponseData[0].id}`
    );
    const { id, name } = mockTrainersSessionsGetResponseData[0];
    expect(getByTestId("trainer-session-details")).toHaveTextContent(name);
    expect(
      getByTestId(`trainer-sessions-list-item-detail-link-${id}`)
    ).toHaveClass("active");
  });

  it("should display a message if the selected session does not exist", async () => {
    const sessionId = 123;
    const El = () => {
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.search}</div>
          <TrainerSessions />
        </div>
      );
    };
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${sessionId}`],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <El />
      </ReactQueryConfigProvider>
    );
    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(queryByTestId("trainer-session-details")).not.toBeInTheDocument();
    const pattern = RegExp(`Session with id ${sessionId} could not be found.`);
    expect(getByText(pattern));
  });

  it("should not render details if there are no sessions", async () => {
    setTrainerSessionsResponse(200, []);
    renderWithMemoryRouter("/*", [`/`], <TrainerSessions />);

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(getByTestId("trainer-sessions-lists")).toBeInTheDocument();
    expect(getByText(/No in progress sessions found\./)).toBeInTheDocument();
    expect(getByText(/No pending sessions found\./)).toBeInTheDocument();
    expect(getByText(/No upcoming sessions found\./)).toBeInTheDocument();
    expect(queryByTestId("trainer-session-details")).not.toBeInTheDocument();
  });

  it("should show error message if the sessions request fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setTrainerSessionsResponse(500);
    renderWithMemoryRouter(
      "/*",
      [`/`],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <TrainerSessions />
      </ReactQueryConfigProvider>
    );

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(queryByTestId("trainer-sessions-lists")).not.toBeInTheDocument();
    expect(queryByTestId("trainer-session-details")).not.toBeInTheDocument();
    expect(getByText(/Error: unable to fetch sessions\./)).toBeInTheDocument();
  });

  it("should show error specific message if the sessions request fails with 'trainer profile is not complete'", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setTrainerSessionsResponse(
      403,
      undefined,
      "trainer profile is not complete"
    );
    renderWithMemoryRouter(
      "/*",
      [`/`],
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <TrainerSessions />
      </ReactQueryConfigProvider>
    );

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    expect(queryByTestId("trainer-sessions-lists")).not.toBeInTheDocument();
    expect(queryByTestId("trainer-session-details")).not.toBeInTheDocument();
    expect(
      getByText(/Your account is under review, please check back later\./)
    ).toBeInTheDocument();
  });
});
