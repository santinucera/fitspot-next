/**
 *
 *
 * <TrainerSessionsList />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, Flex } from "theme-ui";
import { FunctionComponent, useRef, useEffect } from "react";
import scrollIntoView from "scroll-into-view-if-needed";
import NoSessionsFoundMessage from "./NoSessionsFoundMessage";
import TrainerSessionsListItem from "./TrainerSessionsListItem";
import useSelectedSession from "hooks/useSelectedSession";
import useCategorizedTrainerSessions from "hooks/useCategorizedTrainerSessions";

interface TrainerSessionsListProps {
  title: string;
  type: "inProgress" | "pending" | "upcoming";
}

const TrainerSessionsList: FunctionComponent<TrainerSessionsListProps> = ({
  title,
  type,
}) => {
  const sessions = useCategorizedTrainerSessions()[type];
  const { selectedSessionId } = useSelectedSession();
  const activeSessionRef = useRef<HTMLLIElement | null>(null);

  /**
   * Scroll the active session into view on load if this list
   * contains the active session.
   */
  useEffect(() => {
    // True if the active session is part of this list.
    const hasActiveSession = sessions?.some(
      ({ id }) => id === Number(selectedSessionId)
    );

    if (hasActiveSession && activeSessionRef.current) {
      scrollIntoView(activeSessionRef.current, {
        scrollMode: "if-needed",
        behavior: "smooth",
        block: "nearest",
      });
    }
  }, [selectedSessionId, sessions]);

  return (
    <Flex sx={{ flexDirection: "column", flex: "1 0 auto" }}>
      <Heading
        as="h3"
        sx={{
          lineHeight: "54px",
          fontWeight: "heading",
          textTransform: "uppercase",
          fontSize: 0,
          padding: "0 16px",
          borderWidth: "0 0 1px 0",
          borderColor: "gray",
          borderStyle: "solid",
        }}
        data-testid="trainer-sessions-list-header"
      >
        {title}
      </Heading>

      <ul data-testid="trainer-sessions-list">
        {sessions?.length ? (
          sessions.map(
            ({ id, name, participants, startDate, endDate, trainer }) => (
              <TrainerSessionsListItem
                key={id}
                id={id}
                name={name}
                startDate={startDate}
                endDate={endDate}
                rsvpCount={participants.filter(({ isRSVP }) => isRSVP).length}
                avatarUrl={trainer?.avatar?.url}
                ref={
                  Number(selectedSessionId) === id
                    ? activeSessionRef
                    : undefined
                }
              />
            )
          )
        ) : (
          <li key="not-found">
            <NoSessionsFoundMessage categoryTitle={title} />
          </li>
        )}
      </ul>
    </Flex>
  );
};

export default TrainerSessionsList;
