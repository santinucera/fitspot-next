/**
 *
 *
 * Tests for <TrainerSessionsListItem />
 *
 *
 */

import React from "react";
import { screen } from "@testing-library/react";
import TrainerSessionsListItem from "./TrainerSessionsListItem";
import dayjs from "utils/days";
import { renderWithMemoryRouter } from "test-utils/render-with-router";

const { getByTestId } = screen;

describe("<TrainerSessionsListItem />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=1`],
      <TrainerSessionsListItem
        id={1}
        name="Some name"
        startDate={dayjs()}
        endDate={dayjs()}
        rsvpCount={0}
        avatarUrl="https://example.com/x.png"
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should have active class", () => {
    const id = 1;
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <TrainerSessionsListItem
        id={1}
        name="Some name"
        startDate={dayjs()}
        endDate={dayjs()}
        rsvpCount={0}
        avatarUrl="https://example.com/x.png"
      />
    );
    expect(
      getByTestId(`trainer-sessions-list-item-detail-link-${id}`)
    ).toHaveClass("active");
  });

  it("should display singular RSVP when there is one RSVP", () => {
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=$1`],
      <TrainerSessionsListItem
        id={1}
        name="Some name"
        startDate={dayjs()}
        endDate={dayjs()}
        rsvpCount={1}
        avatarUrl="https://example.com/x.png"
      />
    );
    expect(getByTestId(`trainer-session-times`)).toHaveTextContent(/1 RSVP$/);
  });

  it.each([[0], [2]])(
    "should display plural RSVPs when there are %d RSVPs",
    (count) => {
      renderWithMemoryRouter(
        "/*",
        [`/?session_id=1`],
        <TrainerSessionsListItem
          id={1}
          name="Some name"
          startDate={dayjs()}
          endDate={dayjs()}
          rsvpCount={count}
          avatarUrl="https://example.com/x.png"
        />
      );
      expect(getByTestId(`trainer-session-times`)).toHaveTextContent(/RSVPs$/);
    }
  );
});
