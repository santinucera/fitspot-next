/**
 *
 *
 * Tests for <TrainerSessionsListHeader />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import TrainerSessionsListHeader from "./TrainerSessionsListHeader";

describe("<TrainerSessionsListHeader />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<TrainerSessionsListHeader />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render children", () => {
    const value = "some string";
    const { getByRole } = render(
      <TrainerSessionsListHeader>{value}</TrainerSessionsListHeader>
    );
    expect(getByRole("heading")).toHaveTextContent(value);
  });
});
