/**
 *
 *
 * Tests for <NoSessionsFoundMessage />
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import NoSessionsFoundMessage from "./NoSessionsFoundMessage";

const { getByText } = screen;

describe("<NoSessionsFoundMessage />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<NoSessionsFoundMessage categoryTitle="Some Category" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render message", () => {
    render(<NoSessionsFoundMessage categoryTitle="Some Category" />);
    expect(getByText("No some category sessions found.")).toBeInTheDocument();
  });
});
