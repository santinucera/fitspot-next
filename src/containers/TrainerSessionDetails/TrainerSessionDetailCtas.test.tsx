/**
 *
 *
 * Tests for <TrainerSessionDetailCtas />
 *
 *
 */

import React, { useState } from "react";
import { screen, fireEvent } from "@testing-library/react";
import { queryCache } from "react-query";
import dayjs from "utils/days";
import { Dayjs } from "dayjs";
import TrainerSessionDetailCtas from "./TrainerSessionDetailCtas";
import { SessionStatus } from "services/SessionService";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { createTrainerSessionModel } from "test-utils/session-service-test-utils";

const {
  getByText,
  queryByRole,
  queryByText,
  getByTestId,
  queryByTestId,
} = screen;

describe("<TrainerSessionDetailCtas />", () => {
  const handleCheckInClick = jest.fn();
  const handleAcceptClick = jest.fn();
  const handleDeclineClick = jest.fn();
  const handleCancelClick = jest.fn();
  const handleCompleteClick = jest.fn();

  beforeEach(async () => {
    jest.clearAllMocks();
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const id = 1;
    const session = { ...createTrainerSessionModel(), id };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <TrainerSessionDetailCtas
        handleCheckInClick={handleCheckInClick}
        handleAcceptClick={handleAcceptClick}
        handleDeclineClick={handleDeclineClick}
        handleCancelClick={handleCancelClick}
        handleCompleteClick={handleCompleteClick}
        showLoaderFor={null}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it.each<[SessionStatus]>([[SessionStatus.new], [SessionStatus.pending]])(
    "should render accept and decline buttons when status is %d and startDate is at least 48 hours from now",
    (status) => {
      const id = 1;
      const twoDaysFromNow = dayjs().add(48, "hour").add(1, "second");
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate: twoDaysFromNow,
        endDate: twoDaysFromNow,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);

        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick}
            handleAcceptClick={handleAcceptClick.mockImplementation(() => {
              setIsLoading(true);
            })}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick}
            showLoaderFor={isLoading ? "accept" : null}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      fireEvent.click(getByTestId("decline-button"));
      expect(handleDeclineClick).toHaveBeenCalled();
      fireEvent.click(getByTestId("accept-button"));
      expect(handleAcceptClick).toHaveBeenCalled();
      expect(getByTestId("decline-button")).toBeDisabled();
      expect(getByTestId("accept-button")).toContainElement(
        getByTestId("loading-indicator")
      );
    }
  );

  it.each<[SessionStatus]>([[SessionStatus.new], [SessionStatus.pending]])(
    "should render accept but not decline when status is %d and startDate is less than 48 hours from now",
    (status) => {
      const id = 1;
      const now = dayjs();
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate: now,
        endDate: now,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);

        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick}
            handleAcceptClick={handleAcceptClick.mockImplementation(() => {
              setIsLoading(true);
            })}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick}
            showLoaderFor={isLoading ? "accept" : null}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      expect(getByTestId("accept-button")).toBeInTheDocument();
      expect(queryByTestId("decline-button")).toBeNull();
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs]>([
    // Accepted during session
    [SessionStatus.accepted, dayjs(), dayjs()],
    // Accepted during broadcast window
    [
      SessionStatus.accepted,
      dayjs().add(14, "minute"),
      dayjs().add(14, "minute"),
    ],
    // Accepted during broadcast window
    [
      SessionStatus.accepted,
      dayjs().subtract(14, "minute"),
      dayjs().subtract(14, "minute"),
    ],
    // inProgress during session
    [SessionStatus.inProgress, dayjs(), dayjs()],
    // inProgress during broadcast window
    [
      SessionStatus.inProgress,
      dayjs().add(14, "minute"),
      dayjs().add(14, "minute"),
    ],
    // inProgress during broadcast window
    [
      SessionStatus.inProgress,
      dayjs().subtract(14, "minute"),
      dayjs().subtract(14, "minute"),
    ],
  ])(
    "should render get started button when status is %d, startDate is %o, and endDate is %o",
    (status, startDate, endDate) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        startDate,
        endDate,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);
        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick.mockImplementation(() => {
              setIsLoading(true);
            })}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick}
            showLoaderFor={isLoading ? "checkIn" : null}
            key={status}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      const getStartedButton = getByText("Get Started");
      fireEvent.click(getStartedButton);
      expect(getByText("Get Started")).toContainElement(
        getByTestId("loading-indicator")
      );
      expect(handleCheckInClick).toHaveBeenCalled();
      expect(queryByText("Cancel")).toBe(null);
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs, boolean]>([
    // inProgress off-platform during broadcast window during session
    [SessionStatus.inProgress, dayjs(), dayjs(), false],
  ])(
    "should render Join Chat button when status is %d, startDate is %o, and endDate is %o",
    (status, startDate, endDate, isOnPlatform) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate,
        endDate,
        streamUrlBroadcast: isOnPlatform ? "some url" : null,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);
        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick.mockImplementation(() => {
              setIsLoading(true);
            })}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick}
            showLoaderFor={isLoading ? "checkIn" : null}
            key={status}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      const getStartedButton = getByText("Join Chat");
      fireEvent.click(getStartedButton);
      expect(getByText("Join Chat")).toContainElement(
        getByTestId("loading-indicator")
      );
      expect(handleCheckInClick).toHaveBeenCalled();
      expect(queryByText("Cancel")).toBe(null);
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs]>([
    // Accepted 48 hours before session start
    [SessionStatus.accepted, dayjs().add(49, "hour"), dayjs().add(49, "hour")],
  ])(
    "should render cancel button when status is %d, startDate is %o, and endDate is %o",
    (status, startDate, endDate) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate,
        endDate,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      renderWithMemoryRouter(
        "/*",
        [`/?session_id=${id}`],
        <TrainerSessionDetailCtas
          handleCheckInClick={handleCheckInClick}
          handleAcceptClick={handleAcceptClick}
          handleDeclineClick={handleDeclineClick}
          handleCancelClick={handleCancelClick}
          handleCompleteClick={handleCompleteClick}
          showLoaderFor={null}
          key={status}
        />
      );
      fireEvent.click(getByText("Cancel"));
      expect(handleCancelClick).toHaveBeenCalled();
      expect(queryByText("Get Started")).toBe(null);
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs, boolean]>([
    // On-platform session past broadcast time. Too late for you!
    [
      SessionStatus.accepted,
      dayjs().subtract(16, "minute"),
      dayjs().subtract(16, "minute"),
      true,
    ],
  ])(
    "should not render buttons when status is %d, startDate is %o, and endDate is %o",
    (status, startDate, endDate, isOnPlatform) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate,
        endDate,
        // streamUrlBroadcast value === on platform session
        streamUrlBroadcast: isOnPlatform ? "https://stream.com" : null,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      renderWithMemoryRouter(
        "/*",
        [`/?session_id=${id}`],
        <TrainerSessionDetailCtas
          handleCheckInClick={handleCheckInClick}
          handleAcceptClick={handleAcceptClick}
          handleDeclineClick={handleDeclineClick}
          handleCancelClick={handleCancelClick}
          handleCompleteClick={handleCompleteClick}
          showLoaderFor={null}
          key={status}
        />
      );
      expect(queryByRole("button")).toBe(null);
    }
  );

  it.each<[Dayjs, Dayjs, string | null]>([
    // Off-platform session during broadcast window (15m before to 15m after)
    [dayjs(), dayjs(), null],
    // Off-platform session during broadcast window (15m before to 15m after)
    [dayjs().add(14, "minute"), dayjs().add(14, "minute"), null],
    // Off-platform session during broadcast window (15m before to 15m after)
    [dayjs().subtract(14, "minute"), dayjs().subtract(14, "minute"), null],
    // Off-platform session forever (trainer forgot to check in)
    [dayjs().subtract(16, "day"), dayjs().subtract(16, "day"), null],
  ])(
    "should render Arrived button when startDate is %o, endDate is %o, and streamUrlBroadcast is %s",
    (startDate, endDate, streamUrlBroadcast) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status: SessionStatus.accepted,
        startDate,
        endDate,
        streamUrlBroadcast: streamUrlBroadcast || null,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);
        return (
          <TrainerSessionDetailCtas
            // Check in button is sometimes Arrived and sometimes Get Started.
            // They both set status to `inProgress`.
            handleCheckInClick={handleCheckInClick.mockImplementation(() => {
              setIsLoading(true);
            })}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick}
            showLoaderFor={isLoading ? "checkIn" : null}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      fireEvent.click(getByTestId("check-in-button"));
      expect(handleCheckInClick).toHaveBeenCalled();
      expect(getByTestId("check-in-button")).toContainElement(
        getByTestId("loading-indicator")
      );
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs, boolean]>([
    // inProgress session after endDate
    [
      SessionStatus.inProgress,
      dayjs().subtract(1, "minute"),
      dayjs().subtract(1, "minute"),
      true,
    ],
    [
      SessionStatus.inProgress,
      dayjs().subtract(1, "minute"),
      dayjs().subtract(1, "minute"),
      false,
    ],
  ])(
    "should render Complete and check in buttons if the session status is %d, startDate is %o, endDate is %o, isOnPlatform is %s",
    async (status, startDate, endDate, isOnPlatform) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate,
        endDate, // session is over
        streamUrlBroadcast: isOnPlatform ? "some url" : null,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);

        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick.mockImplementation(() =>
              setIsLoading(true)
            )}
            showLoaderFor={isLoading ? "complete" : null}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      expect(getByTestId("check-in-button")).toHaveTextContent(
        isOnPlatform ? "Get Started" : "Join Chat"
      );
      fireEvent.click(getByTestId("complete-button"));
      expect(handleCompleteClick).toHaveBeenCalled();
      expect(getByTestId("complete-button")).toContainElement(
        getByTestId("loading-indicator")
      );
    }
  );

  it.each<[SessionStatus, Dayjs, Dayjs, boolean]>([
    // inProgress on-platform after broadcast window
    [
      SessionStatus.inProgress,
      dayjs().subtract(16, "minute"),
      dayjs().subtract(16, "minute"),
      true,
    ],
    // inProgress off-platform after broadcast window
    [
      SessionStatus.inProgress,
      dayjs().subtract(16, "minute"),
      dayjs().subtract(16, "minute"),
      false,
    ],
  ])(
    "should render complete button but not get started if status is %d, startDate is %o, endDate is %o",
    (status, startDate, endDate, isOnPlatform) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status,
        startDate,
        endDate,
        streamUrlBroadcast: isOnPlatform ? "some url" : null,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      const El = () => {
        const [isLoading, setIsLoading] = useState<boolean>(false);

        return (
          <TrainerSessionDetailCtas
            handleCheckInClick={handleCheckInClick}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
            handleCancelClick={handleCancelClick}
            handleCompleteClick={handleCompleteClick.mockImplementation(() =>
              setIsLoading(true)
            )}
            showLoaderFor={isLoading ? "complete" : null}
          />
        );
      };
      renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
      expect(queryByTestId("check-in-button")).toBeNull();
      fireEvent.click(getByTestId("complete-button"));
      expect(handleCompleteClick).toHaveBeenCalled();
      expect(getByTestId("complete-button")).toContainElement(
        getByTestId("loading-indicator")
      );
    }
  );
});
