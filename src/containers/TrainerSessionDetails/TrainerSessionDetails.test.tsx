/**
 *
 *
 * Tests for <TrainerSessionDetails />
 *
 *
 */

import React from "react";
import { queryCache } from "react-query";
import {
  fireEvent,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import TrainerSessionDetails from "./TrainerSessionDetails";
import {
  createSession,
  createTrainerSessionModel,
  setSessionStatusUpdateResponse,
  setSessionCancelResponse,
  setTrainerSessionsResponse,
  createTrainerSessionDetails,
} from "test-utils/session-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { SessionStatus, createParticipant } from "services/SessionService";
import { ToastProvider } from "hooks/useToast";
import dayjs from "utils/days";
import ModalProvider from "containers/Modal";

const mockedNavigate = jest.fn();

jest.mock("react-router-dom", () => {
  const router = jest.requireActual("react-router-dom");
  return {
    ...router,
    useNavigate: () => mockedNavigate,
  };
});

const { getByTestId, getByText, queryByTestId } = screen;
// With extra hour to account for test run time
const twoDaysFromNow = dayjs().add(49, "hour");
const oneMinuteAgo = dayjs().subtract(1, "minute");

describe("<TrainerSessionDetails />", () => {
  afterEach(() => {
    jest.restoreAllMocks();
    queryCache.clear();
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <TrainerSessionDetails />
    );

    expect(spy).not.toHaveBeenCalled();
  });

  it("should accept and optimistically update sessions", async () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <TrainerSessionDetails />
      </ToastProvider>
    );
    setTrainerSessionsResponse(200, [
      { ...session, status: SessionStatus.accepted },
    ]);
    fireEvent.click(getByTestId("accept-button"));
    expect(getByTestId("accept-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    expect(getByTestId("decline-button")).toBeDisabled();
    await waitForElementToBeRemoved(() => getByTestId("loading-indicator"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Session accepted.")
    );
    // Cancel button is present because the session is now accepted
    expect(getByTestId("cancel-button")).toBeInTheDocument();
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it("should show a toast error when accept fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <TrainerSessionDetails />
      </ToastProvider>
    );

    const reason = "Some reason.";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("accept-button"));
    await waitForElementToBeRemoved(() => getByTestId("loading-indicator"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        `Failed to accept session. ${reason}`
      )
    );
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it("should add a period to the end of status update error messages", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <TrainerSessionDetails />
      </ToastProvider>
    );

    const reason = "Some reason";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("accept-button"));
    await waitForElementToBeRemoved(() => getByTestId("loading-indicator"));
    const message = new RegExp(`^Error!Failed to accept session. ${reason}.$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
  });

  it("should not add a period to the end of status update error messages that already have one", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <TrainerSessionDetails />
      </ToastProvider>
    );

    const reason = "Some reason.";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("accept-button"));
    await waitForElementToBeRemoved(() => getByTestId("loading-indicator"));
    const message = new RegExp(`^Error!Failed to accept session. ${reason}$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
  });

  /**
   * The following skipped tests pass but throw `Warning: Can't perform a
   * React state update on an unmounted component.` Disabling until I find
   * a fix.
   */
  it.skip("should decline and optimistically remove session", async () => {
    const id = 1;
    // Session that start in under 48 hours cannot be declined.
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <ModalProvider>
          <TrainerSessionDetails />
        </ModalProvider>
      </ToastProvider>
    );
    fireEvent.click(getByTestId("decline-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    setTrainerSessionsResponse(200, []);
    const confirmButton = getByTestId("confirm-button");
    fireEvent.click(confirmButton);
    expect(confirmButton).toContainElement(getByTestId("loading-indicator"));
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Session declined.")
    );
    expect(mockedNavigate).toHaveBeenCalledWith("/dashboard/trainer/sessions");
  });

  it.skip("should show a toast error when decline fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ModalProvider>
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      </ModalProvider>
    );

    const reason = "Some reason.";
    setSessionCancelResponse(500, undefined, reason);
    fireEvent.click(getByTestId("decline-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        `Failed to decline session. ${reason}`
      )
    );
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it.skip("should add a period to cancel request error messages", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ModalProvider>
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      </ModalProvider>
    );

    const reason = "Some reason";
    setSessionCancelResponse(500, undefined, reason);
    fireEvent.click(getByTestId("decline-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    const message = new RegExp(`^Error!Failed to decline session. ${reason}.$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
  });

  it.skip("should not add a period to cancel request error messages if it already has one", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.pending,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ModalProvider>
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      </ModalProvider>
    );

    const reason = "Some reason.";
    setSessionCancelResponse(500, undefined, reason);
    fireEvent.click(getByTestId("decline-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    const message = new RegExp(`^Error!Failed to decline session. ${reason}$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
  });

  // End test failures

  it("should check in and forward user to broadcast page if this is an on-platform session", async () => {
    const id = 1;
    const now = dayjs();
    const formattedNow = now.format();
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.accepted,
      startDate: now,
      endDate: now,
      // On-platform session
      streamUrlBroadcast: "some stream url",
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      return (
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      );
    };
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
    setSessionStatusUpdateResponse(200, {
      ...createSession(),
      id,
      status: SessionStatus.inProgress,
      date: formattedNow,
      dt_end: formattedNow,
    });
    fireEvent.click(getByTestId("check-in-button"));
    expect(getByTestId("check-in-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    await waitFor(() =>
      expect(mockedNavigate).toHaveBeenCalledWith(id.toString())
    );
  });

  it("should show a toast error when check in fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.accepted,
      startDate: dayjs(),
      endDate: dayjs(),
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      return (
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      );
    };
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);

    const reason = "Some reason";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("check-in-button"));
    expect(getByTestId("check-in-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    await waitForElementToBeRemoved(() => getByTestId("loading-indicator"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        `Failed to check in. ${reason}`
      )
    );
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it("should forward user to broadcast without checking in if already checked in during on-platform session", async () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.inProgress,
      startDate: dayjs(),
      endDate: dayjs(),
      // On-platform session
      streamUrlBroadcast: "some stream url",
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      return (
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      );
    };
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
    const checkInButton = getByTestId("check-in-button");
    expect(checkInButton).toHaveTextContent("Get Started");
    fireEvent.click(getByTestId("check-in-button"));
    expect(queryByTestId("loading-indicator")).toBeNull();
    await waitFor(() =>
      expect(mockedNavigate).toHaveBeenCalledWith(id.toString())
    );
  });

  it("should check in and forward user to broadcast page if this is an off-platform session during the broadcast window", async () => {
    const id = 1;
    const startDate = dayjs();
    const endDate = dayjs();
    // Off-platform session
    const streamUrlBroadcast = null;
    const session = {
      ...createTrainerSessionModel(),
      status: SessionStatus.accepted,
      id,
      startDate,
      endDate,
      streamUrlBroadcast,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      return (
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      );
    };
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
    setSessionStatusUpdateResponse(200, {
      ...createSession(),
      status: SessionStatus.inProgress,
      id,
      date: startDate.format(),
      dt_end: endDate.format(),
      streamKey: streamUrlBroadcast,
    });
    const checkInButton = getByTestId("check-in-button");
    expect(checkInButton).toHaveTextContent("Arrived");
    fireEvent.click(getByTestId("check-in-button"));
    expect(getByTestId("check-in-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        "You have checked in."
      )
    );
    expect(mockedNavigate).toHaveBeenCalledWith(id.toString());
  });

  it("should forward user to broadcast without checking in if already checked in during off-platform session", async () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.inProgress,
      startDate: dayjs(),
      endDate: dayjs(),
      // Off-platform session
      streamUrlBroadcast: null,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      return (
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      );
    };
    renderWithMemoryRouter("/*", [`/?session_id=${id}`], <El />);
    const checkInButton = getByTestId("check-in-button");
    expect(checkInButton).toHaveTextContent("Join Chat");
    fireEvent.click(getByTestId("check-in-button"));
    expect(queryByTestId("loading-indicator")).toBeNull();
    await waitFor(() =>
      expect(mockedNavigate).toHaveBeenCalledWith(id.toString())
    );
  });

  it("should cancel and remove session", async () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.accepted,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <ModalProvider>
          <TrainerSessionDetails />
        </ModalProvider>
      </ToastProvider>
    );
    fireEvent.click(getByTestId("cancel-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    setTrainerSessionsResponse(200, []);
    fireEvent.click(getByTestId("confirm-button"));
    expect(getByTestId("confirm-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Session canceled.")
    );
    await waitFor(() =>
      expect(mockedNavigate).toHaveBeenCalledWith("/dashboard/trainer/sessions")
    );
  });

  it("should show a toast error when cancel fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.accepted,
      startDate: twoDaysFromNow,
      endDate: twoDaysFromNow,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ModalProvider>
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      </ModalProvider>
    );

    const reason = "Some reason.";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("cancel-button"));
    await waitFor(() => getByTestId("confirmation-modal"));
    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("confirmation-modal"));
    const message = new RegExp(`^Error!Failed to cancel session. ${reason}$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it("should complete session", async () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.inProgress,
      startDate: dayjs().subtract(1, "minute"),
      endDate: dayjs().subtract(1, "minute"),
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ToastProvider>
        <ModalProvider>
          <TrainerSessionDetails />
        </ModalProvider>
      </ToastProvider>
    );
    setTrainerSessionsResponse(200, [
      { ...session, status: SessionStatus.completed },
    ]);
    fireEvent.click(getByTestId("complete-button"));
    expect(getByTestId("complete-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Session completed.")
    );
    await waitFor(() =>
      expect(mockedNavigate).toHaveBeenCalledWith("/dashboard/trainer/sessions")
    );
  });

  it("should show a toast error when completed fails", async () => {
    // Swallow request error
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.inProgress,
      startDate: oneMinuteAgo,
      endDate: oneMinuteAgo,
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <ModalProvider>
        <ToastProvider>
          <TrainerSessionDetails />
        </ToastProvider>
      </ModalProvider>
    );

    const reason = "Some reason.";
    setSessionStatusUpdateResponse(500, undefined, reason);
    fireEvent.click(getByTestId("complete-button"));
    expect(getByTestId("complete-button")).toContainElement(
      getByTestId("loading-indicator")
    );
    const message = new RegExp(`^Error!Failed to complete session. ${reason}$`);
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(message)
    );
    expect(getByTestId("complete-button")).not.toContainElement(
      queryByTestId("loading-indicator")
    );
    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it("should display singular RSVP when there is one RSVP", () => {
    const id = 1;
    const session = {
      ...createTrainerSessionModel(),
      id,
      status: SessionStatus.accepted,
      startDate: dayjs().add(16, "minute"),
      endDate: dayjs().add(16, "minute"),
      participants: [createParticipant(createUser(), true, false)],
    };
    queryCache.setQueryData("trainers-sessions", [session]);
    renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <TrainerSessionDetails />
    );
    expect(getByText(/1 RSVP$/)).toBeInTheDocument();
  });

  it.each([
    [[]],
    [
      [
        createParticipant(createUser(), true, false),
        createParticipant(createUser(), true, false),
      ],
    ],
  ])(
    "should display plural RSVPs when there is more than one one RSVP",
    (participants) => {
      const id = 1;
      const session = {
        ...createTrainerSessionModel(),
        id,
        status: SessionStatus.accepted,
        startDate: dayjs().add(16, "minute"),
        endDate: dayjs().add(16, "minute"),
        participants,
      };
      queryCache.setQueryData("trainers-sessions", [session]);
      renderWithMemoryRouter(
        "/*",
        [`/?session_id=${id}`],
        <TrainerSessionDetails />
      );
      expect(getByText(/RSVPs$/)).toBeInTheDocument();
    }
  );
});
