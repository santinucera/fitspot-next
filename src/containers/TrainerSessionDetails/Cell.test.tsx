/**
 *
 *
 * Tests for <Cell />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Cell from "./Cell";

describe("<Cell />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Cell textAlign="left" header />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render children", () => {
    const expected = "some string";
    const { getByText } = render(<Cell>{expected}</Cell>);
    expect(getByText(expected)).toBeInTheDocument();
  });
});
