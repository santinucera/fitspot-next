/**
 *
 *
 * <TrainerSessionDetailCtas />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Button } from "theme-ui";
import { Fragment, FC } from "react";
import dayjs from "utils/days";
import { SessionStatus } from "services/SessionService";
import useSelectedSession from "hooks/useSelectedSession";
import LoadingButton from "components/LoadingButton";

interface TrainerSessionDetailCtasProps {
  handleCheckInClick: () => void;
  handleAcceptClick: () => void;
  handleDeclineClick: () => void;
  handleCancelClick: () => void;
  handleCompleteClick: () => void;
  showLoaderFor: "checkIn" | "accept" | "complete" | null;
}

const TrainerSessionDetailCtas: FC<TrainerSessionDetailCtasProps> = ({
  handleCheckInClick, // I.e. "Get Started"
  handleAcceptClick,
  handleDeclineClick,
  handleCancelClick,
  handleCompleteClick,
  showLoaderFor,
}) => {
  const { selectedSession } = useSelectedSession();

  if (!selectedSession) return null;

  const { startDate, endDate, status } = selectedSession;
  const now = dayjs();
  const isDuringBroadcast = now.isBetween(
    startDate.subtract(15, "minute"),
    endDate.add(15, "minute")
  );
  const is48HoursBeforeSession = startDate.isAfter(now.add(48, "hour"));
  const isAfterBroadcastStart = now.isAfter(startDate.subtract(15, "minute"));
  const isOnPlatform = Boolean(selectedSession.streamUrlBroadcast);

  // Decline and cancel are handled in a confirm modal and don't show loaders
  const showCheckInLoader = showLoaderFor === "checkIn";
  const showAcceptLoader = showLoaderFor === "accept";
  const showCompleteLoader = showLoaderFor === "complete";

  const CheckInButton: FC<{ title: string }> = ({ title }) => (
    <LoadingButton
      variant="primary"
      onClick={handleCheckInClick}
      data-testid="check-in-button"
      isLoading={showCheckInLoader}
      sx={{ mr: 6 }}
    >
      {title}
    </LoadingButton>
  );

  const Buttons: FC = () => {
    switch (status) {
      case SessionStatus.new: // Same as pending.
      case SessionStatus.pending:
        return (
          <Fragment>
            <LoadingButton
              variant="primary"
              onClick={handleAcceptClick}
              sx={{ mr: 6 }}
              data-testid="accept-button"
              isLoading={showAcceptLoader}
            >
              Accept
            </LoadingButton>
            {is48HoursBeforeSession && (
              <Button
                variant="danger"
                onClick={handleDeclineClick}
                data-testid="decline-button"
                disabled={showAcceptLoader}
              >
                Decline
              </Button>
            )}
          </Fragment>
        );
      case SessionStatus.accepted:
        return (
          <Fragment>
            {is48HoursBeforeSession ? (
              <Button
                variant="danger"
                onClick={handleCancelClick}
                data-testid="cancel-button"
              >
                Cancel
              </Button>
            ) : isOnPlatform && isDuringBroadcast ? (
              <CheckInButton title="Get Started" />
            ) : !isOnPlatform && isAfterBroadcastStart ? (
              <CheckInButton title="Arrived" />
            ) : null}
          </Fragment>
        );
      case SessionStatus.inProgress:
        // In progress sessions have been "checked in",
        // i.e. They have be accepted, cannot be canceled,
        // and the user has visited the broadcast page before.
        return (
          <Fragment>
            {isDuringBroadcast ? (
              isOnPlatform ? (
                <CheckInButton title="Get Started" />
              ) : (
                <CheckInButton title="Join Chat" />
              )
            ) : null}

            {now.isAfter(endDate) && (
              <LoadingButton
                variant="primary"
                onClick={handleCompleteClick}
                data-testid="complete-button"
                isLoading={showCompleteLoader}
              >
                Completed
              </LoadingButton>
            )}
          </Fragment>
        );
      default:
        return null;
    }
  };

  return (
    <Flex
      sx={{
        flexDirection: "row",
      }}
    >
      <Buttons />
    </Flex>
  );
};

export default TrainerSessionDetailCtas;
