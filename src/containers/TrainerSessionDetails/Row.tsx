/**
 *
 *
 * <Row />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { FunctionComponent } from "react";

interface RowProps {
  withBorder?: boolean;
}

const Row: FunctionComponent<RowProps> = ({ withBorder, children }) => {
  return (
    <Flex
      sx={{
        flexDirection: "row",
        padding: "24px",
        borderStyle: "solid",
        borderColor: "gray",
        borderWidth: withBorder ? "0 0 1px 0" : "0",
      }}
    >
      {children}
    </Flex>
  );
};

export default Row;
