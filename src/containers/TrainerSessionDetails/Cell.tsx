/**
 *
 *
 * <Cell />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";

interface CellProps {
  textAlign?: "left" | "right" | "center";
  header?: boolean;
}

const Cell: FunctionComponent<CellProps> = ({
  textAlign,
  header,
  children,
}) => {
  return (
    <Box sx={{ textAlign, color: header ? "primary" : "darker-gray" }}>
      {children}
    </Box>
  );
};

export default Cell;
