/**
 *
 *
 * <TrainerSessionDetails />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Heading, AspectRatio } from "theme-ui";
import { FunctionComponent, useState } from "react";
import { useNavigate } from "react-router-dom";
import { queryCache } from "react-query";
import { AxiosResponse } from "axios";
import IconUsers from "components/IconUsers";
import IconDollarSign from "components/IconDollarSign";
import Cell from "./Cell";
import Row from "./Row";
import GridRow from "./GridRow";
import TrainerSessionDetailCtas from "./TrainerSessionDetailCtas";
import useSelectedSession from "hooks/useSelectedSession";
import {
  useMutationSessionStatusPatchService,
  useMutationCancelSessionPatchService,
  SessionStatus,
  useQueryTrainersSessionsGetData,
  TrainerSessionModel,
} from "services/SessionService";
import useToast from "hooks/useToast";
import { useModal } from "containers/Modal/Modal";
import ConfirmationModal from "components/ConfirmationModal";

const TrainerSessionDetails: FunctionComponent = () => {
  const { selectedSession, selectedSessionId } = useSelectedSession();
  const { addToast } = useToast();
  const navigate = useNavigate();
  const navigateToBroadcastPage = () => navigate(`${selectedSessionId}`);
  // TrainerSessions will forward the user to the first available session
  // when the session_id url param is removed.
  const navigateToFirstAvailableSession = () =>
    navigate("/dashboard/trainer/sessions");
  const [showLoaderFor, setShowLoaderFor] = useState<
    "checkIn" | "accept" | "complete" | null
  >(null);
  const { setModal, removeModal } = useModal();

  const successToast = (description: string) =>
    addToast({
      title: "Success!",
      description,
      type: "success",
    });
  const errorToast = (description: string) =>
    addToast({
      title: "Error!",
      description,
      type: "error",
    });
  const sessions = useQueryTrainersSessionsGetData();
  const trainersSessionsQueryKey = "trainers-sessions";
  const optimisticallyUpdateSessionStatus = (status: SessionStatus): void => {
    queryCache.cancelQueries(trainersSessionsQueryKey);
    queryCache.setQueryData(
      trainersSessionsQueryKey,
      // Sessions is guaranteed to exist because the component won't render without it.
      sessions?.reduce(
        (
          acc: TrainerSessionModel[],
          session: TrainerSessionModel
        ): TrainerSessionModel[] => {
          if (session.id === selectedSessionId) {
            return [...acc, { ...session, status }];
          }
          return [...acc, session];
        },
        []
      )
    );
    // Reload sessions
    queryCache.invalidateQueries(trainersSessionsQueryKey);
  };
  const optimisticallyRemoveSelectedSession = () => {
    queryCache.cancelQueries(trainersSessionsQueryKey);
    queryCache.setQueryData(
      trainersSessionsQueryKey,
      sessions!.filter((session) => session.id !== selectedSessionId)
    );
    // Reload sessions
    queryCache.invalidateQueries(trainersSessionsQueryKey);
  };
  /**
   * Extract the message string from error response data.
   * Pad with a single leading space and add a trailing period if necessary.
   */
  const getMessageFromErrorResponse = (
    response: AxiosResponse<{ message: string }> | undefined
  ): string => {
    const message = response?.data?.message && ` ${response.data.message}`;
    if (message) {
      return /\.$/.test(message) ? message : `${message}.`;
    } else {
      return "";
    }
  };

  const [updateStatus] = useMutationSessionStatusPatchService({
    mutationConfig: {
      onSuccess: ({ streamKey }, { status }) => {
        switch (status) {
          case SessionStatus.accepted:
            setShowLoaderFor(null);
            optimisticallyUpdateSessionStatus(status);
            successToast("Session accepted.");
            break;
          case SessionStatus.canceled:
            removeModal();
            optimisticallyRemoveSelectedSession();
            successToast("Session canceled.");
            navigateToFirstAvailableSession();
            break;
          case SessionStatus.inProgress:
            // Forward user to broadcast page if this is on-platform
            if (Boolean(streamKey)) {
              navigateToBroadcastPage();
            } else {
              setShowLoaderFor(null);
              optimisticallyUpdateSessionStatus(status);
              successToast("You have checked in.");
              navigateToBroadcastPage();
            }
            break;
          case SessionStatus.completed:
            setShowLoaderFor(null);
            optimisticallyUpdateSessionStatus(status);
            successToast("Session completed.");
            navigateToFirstAvailableSession();
            break;
          default:
            break;
        }
      },
      onError: ({ response }, { status }) => {
        const message = getMessageFromErrorResponse(response);

        switch (status) {
          case SessionStatus.accepted:
            setShowLoaderFor(null);
            errorToast(`Failed to accept session.${message}`);
            break;
          case SessionStatus.canceled:
            removeModal();
            errorToast(`Failed to cancel session.${message}`);
            break;
          case SessionStatus.inProgress:
            setShowLoaderFor(null);
            errorToast(`Failed to check in.${message}`);
            break;
          case SessionStatus.completed:
            setShowLoaderFor(null);
            errorToast(`Failed to complete session.${message}`);
            break;
          default:
            break;
        }
      },
    },
  });

  // Confusing, but cancelSessionMutation is used to decline the session (as opposed to accept).
  const [cancelSessionMutation] = useMutationCancelSessionPatchService({
    mutationConfig: {
      onSuccess: () => {
        removeModal();
        optimisticallyRemoveSelectedSession();
        successToast("Session declined.");
        navigateToFirstAvailableSession();
      },
      onError: ({ response }) => {
        removeModal();
        errorToast(
          `Failed to decline session.${getMessageFromErrorResponse(response)}`
        );
      },
    },
  });

  if (!selectedSession) return null;

  const {
    name,
    startDate,
    endDate,
    trainerPayout,
    participants,
    officeLocation,
  } = selectedSession;
  const rsvpParticipants = participants.filter(({ isRSVP }) => isRSVP);

  const acceptSession = () => {
    setShowLoaderFor("accept");
    updateStatus({
      sessionId: selectedSessionId!,
      status: SessionStatus.accepted,
    });
  };

  const declineSession = () => {
    // Yes, this is on purpose. Decline === failed, status 4, which can
    // only be done via the /workouts/cancel/:sessionId endpoint.
    cancelSessionMutation({ sessionId: selectedSessionId! });
  };
  const handleDeclineClick = () => {
    setModal(
      <ConfirmationModal
        title="Are you sure you want to decline this session?"
        confirmationText="Yes, decline session"
        handleConfirmation={declineSession}
        handleCancel={removeModal}
        cancelText="Close"
      />
    );
  };

  const checkIn = () => {
    // Don't check in if already checked in
    if (selectedSession.status === SessionStatus.inProgress) {
      navigateToBroadcastPage();
    } else {
      setShowLoaderFor("checkIn");
      updateStatus({
        sessionId: selectedSessionId!,
        status: SessionStatus.inProgress,
      });
    }
  };

  const cancelSession = () => {
    updateStatus({
      sessionId: selectedSessionId!,
      status: SessionStatus.canceled,
    });
  };
  const handleCancelClick = () => {
    setModal(
      <ConfirmationModal
        title="Are you sure you want to cancel this session?"
        confirmationText="Yes, cancel session"
        handleConfirmation={cancelSession}
        handleCancel={removeModal}
        cancelText="Close"
      />
    );
  };

  const completeSession = () => {
    setShowLoaderFor("complete");
    updateStatus({
      sessionId: selectedSessionId!,
      status: SessionStatus.completed,
    });
  };

  return (
    <Flex
      sx={{
        flexDirection: "column",
        flex: ["1 0 auto", "0 1 496px"],
        fontSize: 0,
        lineHeight: "21px",
        backgroundColor: "white",
      }}
      data-testid="trainer-session-details"
    >
      <AspectRatio
        ratio={16 / 9}
        sx={{
          backgroundColor: "medium-light-gray",
          padding: "24px",
          display: "flex",
          alignItems: "center",
          justifyContent: "left",
        }}
      >
        <Heading
          as="h1"
          sx={{
            fontSize: 5,
            fontWeight: "heading",
            lineHeight: "40px",
            color: "primary",
          }}
        >
          {name}
        </Heading>
      </AspectRatio>

      <GridRow>
        <Cell header>Date</Cell>
        <Cell header textAlign="center">
          Time
        </Cell>
        <Cell header textAlign="right">
          Location
        </Cell>
        <Cell>{startDate.format("ddd, MMM D")}</Cell>
        <Cell textAlign="center">
          {startDate.format("h:mma")} - {endDate.format("h:mma")}
        </Cell>
        <Cell textAlign="right">
          {officeLocation?.name || "Virtual session"}
        </Cell>
      </GridRow>

      <Row withBorder>
        <IconUsers size={22} sx={{ mr: "25px" }} />{" "}
        <Cell>
          {rsvpParticipants.length}{" "}
          {rsvpParticipants.length === 1 ? "RSVP" : "RSVPs"}
        </Cell>
      </Row>

      <Row>
        <IconDollarSign size={22} sx={{ mr: "25px" }} />{" "}
        <Cell>
          You will earn ${trainerPayout ? trainerPayout.toFixed(2) : "0.00"}
        </Cell>
      </Row>
      <Row>
        <TrainerSessionDetailCtas
          handleCheckInClick={checkIn}
          handleAcceptClick={acceptSession}
          handleDeclineClick={handleDeclineClick}
          handleCancelClick={handleCancelClick}
          handleCompleteClick={completeSession}
          showLoaderFor={showLoaderFor} // Only for accept
        />
      </Row>
    </Flex>
  );
};

export default TrainerSessionDetails;
