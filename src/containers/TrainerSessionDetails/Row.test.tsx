/**
 *
 *
 * Tests for <Row />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Row from "./Row";

describe("<Row />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Row withBorder />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render children", () => {
    const expected = "some string";
    const { getByText } = render(<Row>{expected}</Row>);
    expect(getByText(expected)).toBeInTheDocument();
  });
});
