/**
 *
 *
 * <GridRow />
 *
 *
 */

/** @jsx jsx */
import { jsx, Grid } from "theme-ui";
import { FunctionComponent } from "react";

const GridRow: FunctionComponent = ({ children }) => {
  return (
    <Grid
      columns={[3, "1fr 1fr 1fr"]}
      gap={0}
      sx={{
        borderStyle: "solid",
        borderColor: "gray",
        borderWidth: "0 0 1px 0",
        padding: "24px",
      }}
    >
      {children}
    </Grid>
  );
};

export default GridRow;
