/**
 *
 *
 * Tests for <GridRow />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import GridRow from "./GridRow";

describe("<GridRow />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<GridRow />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render children", () => {
    const expected = "some string";
    const { getByText } = render(<GridRow>{expected}</GridRow>);
    expect(getByText(expected)).toBeInTheDocument();
  });
});
