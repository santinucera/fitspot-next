/*
 *
 *
 * Chat
 *
 *
 */

/** @jsx jsx */
import { useEffect, useReducer, useRef, Fragment } from "react";
import { jsx, Flex, Input } from "theme-ui";
import { FunctionComponent } from "react";
import { useParams } from "react-router";
import dayjs from "utils/days";
import Message from "./Message";
import SystemMessage from "./SystemMessage";
import CometChatService, {
  useCometChatService,
  isSystemMessage,
  Message as MessageType,
} from "services/CometChatService";
import {
  State,
  ActionTypes,
  Action,
  UpdateCommentAction,
  ClearCommentAction,
  IsReadyAction,
} from "./types";

/**
 * ############################################################################
 *
 * State
 *
 * ############################################################################
 */
const initialState: State = {
  comment: "",
  isReady: false,
};

/**
 * ############################################################################
 *
 * Actions
 *
 * ############################################################################
 */
export const updateCommentAction = (comment: string): UpdateCommentAction => ({
  type: ActionTypes.UPDATE_COMMENT,
  comment,
});

export const clearCommentAction = (): ClearCommentAction => ({
  type: ActionTypes.CLEAR_COMMENT,
});

export const isReadyAction = (): IsReadyAction => ({
  type: ActionTypes.IS_READY,
});

/**
 * ############################################################################
 *
 * Reducer
 *
 * ############################################################################
 */
export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.UPDATE_COMMENT:
      return { ...state, comment: action.comment };
    case ActionTypes.CLEAR_COMMENT:
      return { ...state, comment: "" };
    case ActionTypes.IS_READY:
      return { ...state, isReady: true };
  }
};

/**
 * ############################################################################
 *
 * Utils
 *
 * ############################################################################
 */

/**
 * Return true if the previous message was sent longer than minutesAgo
 * or if there is no previous message.
 */
export const previousMessageWasSentMinutesAgo = (
  minutesAgo: number,
  message: MessageType,
  previousMessage?: MessageType
): boolean => {
  return previousMessage
    ? dayjs(previousMessage.sentAt).isBefore(
        dayjs(message.sentAt).subtract(minutesAgo, "minute")
      )
    : true;
};

/**
 * ############################################################################
 *
 * Component
 *
 * ############################################################################
 */
interface ChatProps {}

export const Chat: FunctionComponent<ChatProps> = () => {
  const { sessionId } = useParams();
  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    connect,
    handleDismount,
    error,
    user,
    messages,
    addSystemMessage,
    sendMessage,
    fetchPreviousMessages,
    isConnected,
    isFailure,
  } = useCometChatService();
  const messageListRef = useRef<HTMLOListElement>(null);

  /**
   * On load
   */
  useEffect(() => {
    // Connect to chat group
    (async () => {
      if (sessionId) {
        // addSystemMessage("info", "Connecting...");
        await connect(sessionId.toString());
        // addSystemMessage("info", "Connected.");
      }
    })();
    return () => {
      handleDismount();
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sessionId]);

  /**
   * On connect
   */
  useEffect(() => {
    let isDismounting = false;
    // Fetch previous messages on connect.
    if (isConnected) {
      (async () => {
        await fetchPreviousMessages();
        !isDismounting && dispatch(isReadyAction());
      })();
    }
    return () => {
      isDismounting = true;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isConnected]);

  /**
   * On error
   */
  useEffect(() => {
    // Add errors to the messages list.
    if (isFailure && error) {
      addSystemMessage("error", error.message);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFailure]);

  /**
   * On message received
   */
  useEffect(() => {
    // Scroll to bottom when messages are updated
    if (messageListRef.current) {
      messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(messages)]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.preventDefault();
    dispatch(updateCommentAction(event.currentTarget.value));
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    sendMessage(state.comment);
    dispatch(clearCommentAction());
  };

  return (
    <Flex
      sx={{
        boxShadow: "small",
        flexDirection: "column",
        height: "100%",
        minHeight: 400,
        backgroundColor: "white",
      }}
      role="log"
    >
      <div sx={{ flex: "1", position: "relative" }}>
        <ol
          sx={{
            overflowY: "scroll",
            height: "100%",
            width: "100%",
            position: "absolute",
            top: 0,
            left: 0,
          }}
          ref={messageListRef}
        >
          {messages.map((message, index) => (
            <li key={index}>
              {isSystemMessage(message) ? (
                <SystemMessage
                  body={message.text}
                  level={message.metadata.level}
                  key={index}
                />
              ) : (
                <Fragment>
                  {previousMessageWasSentMinutesAgo(
                    15,
                    message,
                    messages[index - 1]
                  ) && (
                    <SystemMessage
                      body={dayjs(message.sentAt).format("h:mmA")}
                      level="info"
                      key={`system_message_${index}`}
                    />
                  )}
                  <Message
                    author={message.sender?.name}
                    body={message.text}
                    key={index}
                    avatar={message.sender?.avatar}
                    sentByCurrentUser={message.sender?.uid === user?.uid}
                  />
                </Fragment>
              )}
            </li>
          ))}
        </ol>
      </div>
      <form
        noValidate
        onSubmit={handleSubmit}
        sx={{ flex: 0 }}
        autoComplete="off"
      >
        <Input
          type="text"
          value={state.comment}
          onChange={handleChange}
          placeholder="Leave a comment..."
          name="comment"
          aria-label="Enter comment"
          disabled={!state.isReady}
          sx={{
            borderColor: "gray",
            borderWidth: "1px 0 0 0",
            borderStyle: "solid",
            flex: "0 1 auto",
            paddingTop: "24px",
            paddingBottom: "24px",
            paddingLeft: "16px",
            color: "primary",
            fontFamily: "body",
            "&:focus": {
              borderColor: "gray",
            },
            "&::placeholder": {
              color: "primary",
              opacity: 0.3,
            },
            "&:disabled": {
              backgroundColor: "light-gray",
            },
          }}
        />
      </form>
    </Flex>
  );
};

const withProvider: FunctionComponent<{}> = () => (
  <CometChatService>
    <Chat />
  </CometChatService>
);

export default withProvider;
