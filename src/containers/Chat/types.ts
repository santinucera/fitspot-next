/**
 *
 *
 * Types for Chat
 *
 *
 */

export interface State {
  comment: string;
  isReady: boolean;
}

export enum ActionTypes {
  UPDATE_COMMENT,
  CLEAR_COMMENT,
  IS_READY,
}

export interface UpdateCommentAction {
  type: ActionTypes.UPDATE_COMMENT;
  comment: string;
}

export interface ClearCommentAction {
  type: ActionTypes.CLEAR_COMMENT;
}

export interface IsReadyAction {
  type: ActionTypes.IS_READY;
}

export type Action = UpdateCommentAction | ClearCommentAction | IsReadyAction;
