/**
 *
 *
 * Tests for <Message />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Message from "./Message";

describe("<Message />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Message
        author="some author"
        body="some body"
        avatar="some url"
        sentByCurrentUser={false}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
