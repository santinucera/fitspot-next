/**
 *
 *
 * <SystemMessage />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";

interface SystemMessageProps {
  level: "info" | "error";
  body: string;
}

const SystemMessage: FunctionComponent<SystemMessageProps> = ({
  level,
  body,
}) => {
  return (
    <Box
      color={level === "error" ? "red" : "primary"}
      opacity="0.5"
      margin="16px"
      sx={{ textAlign: "center" }}
    >
      {body}
    </Box>
  );
};

export default SystemMessage;
