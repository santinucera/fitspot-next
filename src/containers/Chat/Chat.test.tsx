/**
 *
 *
 * Tests for <Chat />
 *
 *
 */

import React from "react";
import { waitFor } from "@testing-library/react";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import Chat, { reducer, updateCommentAction, clearCommentAction } from "./Chat";
import { State, ActionTypes } from "./types";

describe("actions", () => {
  describe("updateCommentAction", () => {
    it("should return UpdateCommentAction", () => {
      const comment = "some comment";
      const expected = {
        type: ActionTypes.UPDATE_COMMENT,
        comment,
      };
      expect(updateCommentAction(comment)).toEqual(expected);
    });
  });

  describe("clearCommentAction", () => {
    it("should return ClearCommentAction", () => {
      const expected = {
        type: ActionTypes.CLEAR_COMMENT,
      };
      expect(clearCommentAction()).toEqual(expected);
    });
  });
});

describe("reducer", () => {
  let state: State;
  beforeEach(async () => {
    state = {
      comment: "",
      isReady: false,
    };
  });
  describe("UPDATE_COMMENT", () => {
    it("should update comment", () => {
      const comment = "some comment";
      expect(reducer(state, updateCommentAction(comment))).toEqual({
        ...state,
        comment,
      });
    });
  });
  describe("CLEAR_COMMENT", () => {
    it("should clear the comment", () => {
      state = {
        ...state,
        comment: "some comment",
      };
      expect(reducer(state, clearCommentAction())).toEqual({
        ...state,
        comment: "",
      });
    });
  });
});

describe("<Chat />", () => {
  const sessionId = "1234";

  it.skip("should disable input until previous messages are loaded", async () => {
    const { getByLabelText } = renderWithMemoryRouter(
      "/sessions/:sessionId",
      [`/sessions/${sessionId}`],
      <Chat />
    );
    const input = getByLabelText("Enter comment");
    expect(input).toBeDisabled();
    await waitFor(() => expect(input).not.toBeDisabled());
  });

  it.todo("should load messages");

  it.todo("should show error if CometChat request fails");
});
