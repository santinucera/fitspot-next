/**
 *
 *
 * <Message />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import Avatar from "components/Avatar";
import H2 from "components/H2";

interface MessageProps {
  author?: string;
  body: string;
  avatar?: string;
  sentByCurrentUser?: boolean;
}

const Message: FunctionComponent<MessageProps> = ({
  author,
  body,
  avatar,
  sentByCurrentUser,
}) => {
  if (sentByCurrentUser) {
    return (
      <Flex
        role="document"
        sx={{
          marginBottom: "16px",
          padding: "0 16px",
          flexDirection: "row",
          alignItems: "stretch",
        }}
      >
        <Box sx={{ flex: "1 1 auto" }}>
          <H2
            sx={{
              fontSize: [0, 0],
              lineHeight: "32px",
              marginBottom: "0px",
              textAlign: "right",
            }}
          >
            {author}
          </H2>
          <Box sx={{ fontSize: 0, textAlign: "right" }}>{body}</Box>
        </Box>
        <Box sx={{ flex: "0 0 auto", marginLeft: "8px" }}>
          <Avatar size={32} url={avatar} />
        </Box>
      </Flex>
    );
  } else {
    return (
      <Flex
        role="document"
        sx={{
          marginBottom: "16px",
          padding: "0 16px",
          flexDirection: "row",
          alignItems: "stretch",
        }}
      >
        <Box sx={{ flex: "0 0 auto", marginRight: "8px" }}>
          <Avatar size={32} url={avatar} />
        </Box>
        <Box sx={{ flex: "0 1 auto" }}>
          <H2
            sx={{ fontSize: [0, 0], lineHeight: "32px", marginBottom: "0px" }}
          >
            {author}
          </H2>
          <Box sx={{ fontSize: 0 }}>{body}</Box>
        </Box>
      </Flex>
    );
  }
};

export default Message;
