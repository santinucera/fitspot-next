/**
 *
 *
 * Tests for <SystemMessage />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SystemMessage from "./SystemMessage";

describe("<SystemMessage />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SystemMessage level="info" body="foo" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
