/**
 *
 *
 * Tests for SettingsAccount
 *
 *
 */

import React from "react";
import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from "@testing-library/react";
import SettingsAccount from "./SettingsAccount";
import { ToastProvider } from "hooks/useToast";
import {
  createUser,
  setUserPasswordResetResponse,
} from "test-utils/user-service-test-utils";
import ModalProvider from "containers/Modal";
import server from "services/_mocks/server";
import { rest } from "msw";
import { postDeleteAccountUrl } from "services/AuthService";
import { queryCache } from "react-query";

describe("<SettingsAccount />", () => {
  let oldPasswordInput: HTMLElement;
  let newPasswordInput: HTMLElement;

  const originalWindow = global.window;

  beforeEach(() => {
    queryCache.setQueryData("user", createUser());
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        href: "",
      },
      writable: true,
    });
    render(
      <ModalProvider>
        <ToastProvider>
          <SettingsAccount />
        </ToastProvider>
      </ModalProvider>
    );
    oldPasswordInput = screen.getByPlaceholderText("Old Password");
    newPasswordInput = screen.getByPlaceholderText("New Password");
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    expect(spy).not.toHaveBeenCalled();
  });

  it("should change a users password", async () => {
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      screen.getByTestId(/toast-item/i);
    });
    expect(screen.getByTestId(/toast-item/i)).toHaveTextContent("Success!");
  });

  it("should show form errors", async () => {
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Please enter your old password."
      );
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Please enter a new password."
      );
    });
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasd" } });
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId("change-password-form")).toHaveTextContent(
        "Password must be at least 8 characters."
      );
    });
  });

  it("should show toast when update password fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserPasswordResetResponse(500);
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent(
        "Error!Failed to change password."
      );
    });
    expect(global.console.error).toHaveBeenCalled();
  });

  it("should show specific message when old password does not match", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setUserPasswordResetResponse(400, undefined, "old password mismatch");
    fireEvent.change(oldPasswordInput, { target: { value: "asdfasdfasdf" } });
    fireEvent.change(newPasswordInput, { target: { value: "asdfasdf" } });
    const submitButton = screen.getByText(/save changes/i);
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent(
        "Error!Old password does not match."
      );
    });
    expect(global.console.error).toHaveBeenCalled();
  });

  it("should disable a user account", async () => {
    fireEvent.click(screen.getByTestId("delete-account-button"));
    await waitFor(() => {
      expect(screen.getByTestId("modal")).toBeInTheDocument();
    });
    const button = await within(screen.getByTestId("modal")).findByText(
      /deactivate my account/i
    );
    fireEvent.click(button);
    await waitFor(() => {
      expect(global.window.location.href).toBe("/user/login");
    });
  });

  it("should handle errors disabling a user account", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    server.use(
      rest.post(postDeleteAccountUrl, (req, res, ctx) => {
        return res(ctx.status(400));
      })
    );
    fireEvent.click(screen.getByTestId("delete-account-button"));
    await waitFor(() => {
      expect(screen.getByTestId("modal")).toBeInTheDocument();
    });
    const button = await within(screen.getByTestId("modal")).findByText(
      /deactivate my account/i
    );
    fireEvent.click(button);
    await waitFor(() => {
      expect(screen.getByTestId(/toast-item/i)).toHaveTextContent("Error!");
    });
    expect(global.console.error).toHaveBeenCalled();
  });
});
