/*
 *
 *
 * SettingsAccount
 *
 *
 */

/** @jsx jsx */

import { jsx, Box, Button, Grid, Flex, Text, Heading } from "theme-ui";
import { FunctionComponent, SyntheticEvent } from "react";
import { useForm } from "react-hook-form";
import Input from "components/Input";
import useToast from "hooks/useToast";
import { Divider } from "@material-ui/core";
import { useModal } from "containers/Modal/Modal";
import ConfirmationModal from "components/ConfirmationModal";
import H2 from "components/H2";
import { useQueryUserGetData } from "services/UserService";
import { PasswordPatchRequestData } from "services/UserService";
import {
  useMutationAuthPostLogoutService,
  useMutationPostDeleteAccountService,
} from "services/AuthService";
import useUpdatePassword from "hooks/useUpdatePassword";

interface SettingsAccountProps {}

const defaultValues = {
  oldPassword: "",
  newPassword: "",
};

const SettingsAccount: FunctionComponent<SettingsAccountProps> = () => {
  const user = useQueryUserGetData();
  const email = user?.email || "";

  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const { handleSubmit, control, errors, reset } = useForm<
    PasswordPatchRequestData
  >({
    defaultValues: {
      email,
      ...defaultValues,
    },
  });

  const [updatePassword, { isLoading }] = useUpdatePassword(() =>
    reset({
      email,
      ...defaultValues,
    })
  );

  const [logoutMutation] = useMutationAuthPostLogoutService();

  const [deleteAccountMutation] = useMutationPostDeleteAccountService({
    mutationConfig: {
      onSuccess: async () => {
        await logoutMutation();
        window.location.href = "/user/login";
      },
      onError() {
        addToast({
          title: "Error!",
          description:
            "There was an problem. Please contact your administrator.",
          type: "error",
        });
      },
    },
  });

  const onSubmit = handleSubmit((data) => {
    updatePassword(data);
  });

  const openConfirmationModal = (event: SyntheticEvent) => {
    event.preventDefault();
    setModal(
      <ConfirmationModal
        title="Warning!"
        body={`Are you sure you want to deactivate your membership with ${user?.companyList[0].name}?<br>This action is not reversible.<br>To delete your account and your account data permanently, please email contact@tenspot.com.`}
        confirmationText="Deactivate my Account"
        handleConfirmation={() => {
          deleteAccountMutation();
        }}
        handleCancel={removeModal}
      />
    );
  };

  return (
    <Box>
      <H2>Change Password</H2>
      <Box
        data-testid="change-password-form"
        as="form"
        sx={{ mt: 7 }}
        onSubmit={onSubmit}
      >
        <Grid gap={5} columns={0} mb={7} sx={{ maxWidth: 780 }}>
          <Input
            control={control}
            label="Email"
            name="email"
            disabled={true}
            helperText="You cannot edit your email"
          />
          <Input
            control={control}
            label="Old Password"
            name="oldPassword"
            type="password"
            placeholder="Old Password"
            errors={errors}
            rules={{
              minLength: {
                value: 8,
                message: "Password must be at least 8 characters.",
              },
              required: {
                value: true,
                message: "Please enter your old password.",
              },
            }}
          />
          <Input
            control={control}
            label="New Password"
            name="newPassword"
            type="password"
            placeholder="New Password"
            errors={errors}
            rules={{
              minLength: {
                value: 8,
                message: "Password must be at least 8 characters.",
              },
              required: {
                value: true,
                message: "Please enter a new password.",
              },
            }}
          />
        </Grid>
        <Button type="submit" mb={9} disabled={isLoading}>
          {isLoading ? "Saving..." : "Save Changes"}
        </Button>
        <Divider />
        <Flex sx={{ mt: 6 }}>
          <Box>
            <Heading sx={{ fontSize: 3 }}>Deactivate account</Heading>
            <Text>Deactivate your account and your account data</Text>
          </Box>
          <Button
            data-testid="delete-account-button"
            sx={{ ml: "auto" }}
            variant="danger"
            onClick={openConfirmationModal}
          >
            Deactivate Your Account
          </Button>
        </Flex>
      </Box>
    </Box>
  );
};

export default SettingsAccount;
