/**
 *
 *
 * Tests for <AvatarCropper />
 *
 *
 */

import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import user from "@testing-library/user-event";
import AvatarCropper from "./AvatarCropper";

const OriginalFileReader = window.FileReader;

describe("<AvatarCropper />", () => {
  beforeAll(() => {
    Object.defineProperty(global, "FileReader", {
      writable: true,
      value: jest.fn().mockImplementation(() => ({
        readAsDataURL: jest.fn(),
        onLoad: jest.fn(),
      })),
    });
  });
  afterAll(() => {
    window.FileReader = OriginalFileReader;
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <AvatarCropper
        initialComponent={() => <div />}
        onFinishUpload={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
  it("should", async () => {
    const { getByTestId, queryByText } = render(
      <AvatarCropper
        initialComponent={({ onClick }) => (
          <button data-testid="button" onClick={onClick} />
        )}
        onFinishUpload={() => {}}
      />
    );
    fireEvent.click(getByTestId("button"));
    await waitFor(() => {
      const file = new File(["hello"], "hello.png", { type: "image/png" });
      user.upload(getByTestId("input-file"), file);
    });
    await waitFor(() => {
      expect(queryByText("Upload Picture")).toBeInTheDocument();
    });
  });
});
