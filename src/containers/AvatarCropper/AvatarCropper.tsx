/*
 *
 *
 * AvatarCropper
 *
 *
 */

/** @jsx jsx */
import { jsx, Button, Flex, Box, Heading } from "theme-ui";
import {
  FunctionComponent,
  useState,
  Fragment,
  createRef,
  ReactNode,
} from "react";
import ImageCropper from "components/ImageCropper";
import Spinner from "components/Spinner";
import { useMutationUserPostImageService } from "services/UserService";

interface AvatarCropperProps {
  onFinishUpload: (params: { avatarId: number; avatarUrl: string }) => void;
  initialComponent: (params: { onClick: () => void }) => ReactNode;
  showInitialStepAfterUpload?: boolean;
}

const AvatarCropper: FunctionComponent<AvatarCropperProps> = ({
  onFinishUpload,
  initialComponent,
  showInitialStepAfterUpload = true,
}) => {
  const [cropperInstance, setCropperIntance] = useState<Cropper>();
  const [step, setStep] = useState(0);
  const [image, setImage] = useState<any>();

  const [handleImageUpdate] = useMutationUserPostImageService({
    mutationConfig: {
      onSuccess({ id }) {
        onFinishUpload({ avatarId: id, avatarUrl: image });
        if (showInitialStepAfterUpload) setStep(0);
      },
    },
  });

  const fileRef = createRef<HTMLInputElement>();

  const handleInputClick = () => {
    if (fileRef.current) fileRef.current.click();
  };

  const handleImageChosen = (event: React.SyntheticEvent) => {
    event.preventDefault();
    const target = event.target as HTMLInputElement;
    const files = target.files as FileList;
    if (files.length === 0) return;
    const reader = new FileReader();
    reader.onload = () => {
      setImage(reader.result);
    };
    reader.readAsDataURL(files[0]);
    setStep(1);
  };

  const handleImageUpload = () => {
    cropperInstance?.getCroppedCanvas()?.toBlob((blob: Blob | null) => {
      const formData = new FormData();
      if (blob) {
        formData.append("file", blob);
        handleImageUpdate(formData);
        setStep(2);
      }
    });
  };

  return (
    <Flex
      py={5}
      sx={{
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
      }}
    >
      <input
        ref={fileRef}
        hidden
        data-testid="input-file"
        type="file"
        onChange={handleImageChosen}
      />
      <Box
        sx={{
          height: step === 0 ? "auto" : 0,
          overflow: "hidden",
          width: "100%",
        }}
      >
        {initialComponent({ onClick: handleInputClick })}
      </Box>
      {step === 1 && (
        <Fragment>
          <Box mb={6}>
            <ImageCropper
              src={image}
              onInitialized={(cropper: Cropper) => setCropperIntance(cropper)}
            />
          </Box>
          <Button variant="secondary" onClick={handleImageUpload}>
            Upload Picture
          </Button>
        </Fragment>
      )}
      {step === 2 && (
        <Fragment>
          <Heading>Uploading Picture</Heading>
          <Spinner />
        </Fragment>
      )}
    </Flex>
  );
};

export default AvatarCropper;
