/**
 *
 *
 * <SessionsAttended />
 *
 *
 */
/** @jsx jsx */
import { Flex, jsx, Text } from "theme-ui";
import { FunctionComponent, Fragment, useMemo } from "react";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import SessionCardGrid from "components/SessionCardGrid";
import dayjs from "utils/days";
import Spinner from "components/Spinner";
import { useQuerySessionsGetService } from "services/SessionService";
import H2 from "components/H2";
import NonRatedSessionsAlerts from "containers/NonRatedSessionsAlerts";
import { useRateSession } from "hooks";

interface SessionsAttendedProps {}

const urlParams = {
  limit: 60,
  short: true,
  attended: true,
  desc: true,
  dtEnd: dayjs().utc().format(),
  dtStart: dayjs().subtract(2, "month").utc().format(),
};

const SessionsAttended: FunctionComponent<SessionsAttendedProps> = () => {
  const {
    isFetchedAfterMount,
    data: attendedSessions,
  } = useQuerySessionsGetService({
    queryConfig: {
      staleTime: 0,
    },
    urlParams,
  });

  const { rateSession } = useRateSession({
    attendedSessions,
    sessionsQueryParams: urlParams,
  });

  const completedSessions = useMemo(
    () => attendedSessions?.filter(({ status }) => status === 2),
    [attendedSessions]
  );

  if (!isFetchedAfterMount) return <Spinner />;

  if (attendedSessions === undefined) {
    return <div>there was an error</div>;
  }

  return (
    <div>
      <NonRatedSessionsAlerts
        attendedSessions={completedSessions}
        sessionsQueryParams={urlParams}
      />
      <H2 sx={{ my: 8 }}>Attended Sessions</H2>
      {completedSessions?.length ? (
        <SessionCardGrid
          title="Past Sessions"
          sessions={completedSessions}
          showSessionInfo={false}
          cardActions={({
            sessionId,
            userSessionRating,
          }: {
            sessionId: number;
            userSessionRating?: any;
          }) => {
            const rating = userSessionRating?.ratingPoints || 0;
            return (
              <Flex sx={{ flexDirection: "column", width: "100%" }}>
                <Text sx={{ fontSize: 1, mb: 2 }}>Your rating</Text>
                <Flex
                  sx={{ justifyContent: "space-between", alignItems: "center" }}
                >
                  <Rating
                    name={sessionId.toString()}
                    size="large"
                    value={rating}
                    readOnly={!!rating}
                    onChange={(_, overall) => {
                      rateSession({ sessionId, rating: { overall: overall! } });
                    }}
                    sx={{
                      ".MuiRating-icon": {
                        color: "yellow",
                      },
                    }}
                    emptyIcon={
                      <StarBorderIcon
                        sx={{ color: "primary" }}
                        fontSize="inherit"
                      />
                    }
                  />
                  <Text sx={{ fontSize: 6, display: "flex" }}>
                    <Text sx={{ fontWeight: "bold" }}>{rating}</Text>
                    {" / 5"}
                  </Text>
                </Flex>
              </Flex>
            );
          }}
        />
      ) : (
        <Fragment>You haven't attended any sessions.</Fragment>
      )}
    </div>
  );
};

export default SessionsAttended;
