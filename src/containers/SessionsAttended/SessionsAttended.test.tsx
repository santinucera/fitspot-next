/**
 *
 *
 * Tests for SessionsAttended
 *
 *
 */

import React from "react";
import { waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  createCustomerDashboardSession,
  setSessionsResponse,
} from "test-utils/session-service-test-utils";
import SessionsAttended from "./SessionsAttended";
import { ReactQueryConfigProvider } from "react-query";

describe("<SessionsAttended />", () => {
  it("should not log errors in console", async () => {
    const spy = jest.spyOn(global.console, "error");
    const name = "name of some session";
    setSessionsResponse([
      {
        ...createCustomerDashboardSession(),
        name,
        isRSVP: true,
        wasAttended: true,
        status: 2,
      },
    ]);
    const { getByText } = renderWithRouter(<SessionsAttended />);
    // SessionCards display the name of the session
    await waitFor(() => expect(getByText(name)));
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render SessionCard", async () => {
    const name = "name of some session";
    setSessionsResponse([
      {
        ...createCustomerDashboardSession(),
        name,
        isRSVP: true,
        wasAttended: true,
        status: 2,
      },
    ]);
    const { getByText } = renderWithRouter(<SessionsAttended />);
    // SessionCards display the name of the session
    await waitFor(() => {
      expect(getByText(name)).toBeInTheDocument();
    });
  });

  it("should render error", async () => {
    const spy = jest
      .spyOn(global.console, "error")
      .mockImplementation(jest.fn());

    setSessionsResponse(undefined);

    const { getByText, getByTestId } = renderWithRouter(
      <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
        <SessionsAttended />
      </ReactQueryConfigProvider>
    );

    await waitForElementToBeRemoved(() => getByTestId("spinner"));
    await waitFor(() => {
      expect(getByText("there was an error")).toBeInTheDocument();
    });

    expect(spy).toHaveBeenCalled();
  });
});
