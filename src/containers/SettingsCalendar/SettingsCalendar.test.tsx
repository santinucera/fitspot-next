/**
 *
 *
 * Tests for <SettingsCalendar />
 *
 *
 */

import React from "react";
import {
  render,
  waitFor,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import SettingsCalendar from "./SettingsCalendar";
import { CalendarPreference } from "services/CalendarService";
import {
  setCalendarSettingsGetResponse,
  setCalendarSettingsPostResponse,
} from "test-utils/calendar-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";
import { ToastProvider } from "hooks/useToast";

describe("<SettingsCalendar />", () => {
  const { getByTestId, getByTitle, getByText } = screen;

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SettingsCalendar />);
    expect(spy).not.toHaveBeenCalled();
  });

  describe("Calendar Prompt Checkbox", () => {
    let checkbox: Element;
    beforeEach(async () => {
      renderWithRouter(
        <ToastProvider>
          <SettingsCalendar />
        </ToastProvider>
      );

      const checkboxContainer = await waitFor(() =>
        getByTitle(/calendar-toggle/i)
      );
      checkbox = checkboxContainer.querySelector('input[type="checkbox"]')!;
    });
    it("should un-check checkbox and show a success toast when calendar prompts is disabled", async () => {
      setCalendarSettingsPostResponse(200);

      fireEvent.click(checkbox);
      await waitFor(() => expect(checkbox.checked).toBe(false));
      await waitFor(() => expect(getByText("Success!")).toBeInTheDocument());
      await waitFor(() =>
        expect(getByText("Calendar settings updated.")).toBeInTheDocument()
      );
    });

    it("should check checkbox and show a success toast when calendar prompts is enabled", async () => {
      setCalendarSettingsPostResponse(200);

      fireEvent.click(checkbox);

      await waitFor(() => getByTestId("remove-toast-button"));

      fireEvent.click(getByTestId("remove-toast-button"));

      await waitForElementToBeRemoved(() => getByTestId("remove-toast-button"));

      fireEvent.click(checkbox);

      await waitFor(() => expect(checkbox.checked).toBe(true));
      await waitFor(() => expect(getByText("Success!")).toBeInTheDocument());
      await waitFor(() =>
        expect(getByText("Calendar settings updated.")).toBeInTheDocument()
      );
    });

    it("should show a failure toast when calendar prompts enable request fails", async () => {
      jest.spyOn(global.console, "error").mockImplementation(() => {});

      setCalendarSettingsPostResponse(500);

      fireEvent.click(checkbox);

      await waitFor(() => expect(getByText("Error!")).toBeInTheDocument());
      await waitFor(() =>
        expect(
          getByText("Failed to update calendar settings, please try again.")
        ).toBeInTheDocument()
      );
    });

    it("should show a failure toast when calendar prompts disable request fails", async () => {
      setCalendarSettingsPostResponse(200);

      fireEvent.click(checkbox);

      await waitFor(() => getByTestId("remove-toast-button"));

      fireEvent.click(getByTestId("remove-toast-button"));

      await waitForElementToBeRemoved(() => getByTestId("remove-toast-button"));

      setCalendarSettingsPostResponse(500);
      fireEvent.click(checkbox);

      await waitFor(() => expect(getByText("Error!")).toBeInTheDocument());
      await waitFor(() =>
        expect(
          getByText("Failed to update calendar settings, please try again.")
        ).toBeInTheDocument()
      );
    });

    it.todo(
      "should un-check checkbox when calendar prompts enable request fails"
    );

    it.todo(
      "should re-check checkbox when calendar prompts disable request fails"
    );
  });

  it("should redirect to import link when importing calendar", async () => {
    global.window = Object.create(window);
    Object.defineProperty(global.window, "open", {
      value: jest.fn(),
    });

    render(<SettingsCalendar />);

    await waitFor(() => {
      fireEvent.click(getByText("Import Calendar"));
      expect(global.window.open).toBeCalledWith(
        "https://bit.ly/tenspot-gcal",
        "_blank"
      );
    });
  });

  it("should check calendar selection", async () => {
    setCalendarSettingsGetResponse(200, {
      preference: CalendarPreference.GOOGLE,
    });
    renderWithRouter(
      <ToastProvider>
        <SettingsCalendar />
      </ToastProvider>
    );
    await waitFor(() =>
      expect(getByTestId("calendar-option-google")).toContainElement(
        getByTestId("icon-circle-check")
      )
    );
    fireEvent.click(getByTestId("calendar-option-input-apple"));
    await waitFor(() => expect(getByText("Calendar settings updated.")));
    expect(getByTestId("calendar-option-apple")).toContainElement(
      getByTestId("icon-circle-check")
    );
  });

  it("should re-check previous calendar selection and show a toast if the request fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    setCalendarSettingsGetResponse(200, {
      preference: CalendarPreference.GOOGLE,
    });
    renderWithRouter(
      <ToastProvider>
        <SettingsCalendar />
      </ToastProvider>
    );
    await (
      await waitFor(() => expect(getByTestId("calendar-option-google")))
    ).toContainElement(getByTestId("icon-circle-check"));
    setCalendarSettingsPostResponse(500);
    fireEvent.click(getByTestId("calendar-option-input-apple"));
    expect(getByTestId("calendar-option-apple")).toContainElement(
      getByTestId("icon-circle-check")
    );
    await waitFor(() =>
      expect(getByText("Failed to update calendar settings, please try again."))
    );
    expect(getByTestId("calendar-option-google")).toContainElement(
      getByTestId("icon-circle-check")
    );
  });
});
