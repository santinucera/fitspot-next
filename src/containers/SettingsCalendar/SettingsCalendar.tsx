/*
 *
 *
 * SettingsCalendar
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Divider, Button } from "theme-ui";
import { FunctionComponent, useState, Fragment } from "react";
import { queryCache } from "react-query";
import useToast from "hooks/useToast";
import H2 from "components/H2";
import Switch from "components/Switch";
import FormRow from "components/FormRow";
import Spinner from "components/Spinner";
import CalendarOptions from "components/CalendarOptions";

import {
  useQueryCalendarGetSettingsService,
  useMutationCalendarPostSettingsService,
  CalendarPreference,
} from "services/CalendarService";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface SettingsCalendarProps {}

const SettingsCalendar: FunctionComponent<SettingsCalendarProps> = () => {
  const { addToast } = useToast();
  const { isFetching } = useQueryCalendarGetSettingsService({
    queryConfig: {
      onSuccess: (data) => {
        setCalendarPreference(data.preference);
      },
    },
  });

  const [calendarPreference, setCalendarPreference] = useState(0);

  const [mutation] = useMutationCalendarPostSettingsService({
    onMutate: (params) => {
      const oldCalendarPreference = calendarPreference;
      setCalendarPreference(
        params.preference ||
          CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED
      );
      // Rollback
      return () => {
        setCalendarPreference(oldCalendarPreference);
      };
    },
    onSuccess: () => {
      addToast({
        title: "Success!",
        description: "Calendar settings updated.",
        type: "success",
      });
      // Refetch user
      queryCache.invalidateQueries("user");
    },
    onError: (_, __, rollback) => {
      rollback();
      addToast({
        title: "Error!",
        description: "Failed to update calendar settings, please try again.",
        type: "error",
      });
    },
  });

  const handleCalendarSelection = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { value } = event.target;
    setCalendarPreference(() => {
      const nextState = Number(value);
      mutation({ preference: nextState });
      return nextState;
    });
  };

  // Toggle calendar prompts
  const handleToggle = () => {
    setCalendarPreference(() => {
      const nextState =
        calendarPreference === CalendarPreference.HIDE_PROMPTS
          ? CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED
          : CalendarPreference.HIDE_PROMPTS;
      mutation({ preference: nextState });
      return nextState;
    });
  };

  const handleImportCalendar = () => {
    window.open("https://bit.ly/tenspot-gcal", "_blank");
  };

  if (isFetching) return <Spinner />;

  return (
    <Box>
      <H2>Calendar Settings</H2>
      <FormRow
        title="Calendar Prompts"
        content="When you enable calendar settings, you will be prompted to add an event to your calendar whenever you RSVP to an event."
        action={
          <Switch
            onChange={handleToggle}
            name="calendar"
            title="calendar-toggle"
            checked={
              calendarPreference ===
                CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED ||
              calendarPreference > 1
            }
          />
        }
      />
      {(calendarPreference ===
        CalendarPreference.SHOW_PROMPTS_NO_CALENDAR_SELECTED ||
        calendarPreference > 1) && (
        <Fragment>
          <Flex sx={{ flexWrap: "wrap", mx: -2 }}>
            <CalendarOptions
              handleCalendarSelection={handleCalendarSelection}
              calendarPreference={calendarPreference}
            />
          </Flex>
          <Divider />
        </Fragment>
      )}
      <FormRow
        title="Import our calendar"
        content="Pro tip: You can add our schedule to your Google Calendar. Just click import calendar to add all Ten Spot upcoming sessions to your own calendar."
        action={<Button onClick={handleImportCalendar}>Import Calendar</Button>}
      />
    </Box>
  );
};

export default SettingsCalendar;
