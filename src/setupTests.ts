// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom/extend-expect";

/**
 * Mock requests
 */
import server from "./services/_mocks/server";
// Establish API mocking before all tests.
beforeAll(() => server.listen());
// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => server.resetHandlers());
// Clean up after the tests are finished.
afterAll(() => server.close());

afterEach(() => {
  require("react-query").queryCache.clear();
});

/**
 * Mocks the HumanConnect api.
 */
const originalWindow = global.window;
beforeEach(async () => {
  jest.clearAllMocks();
  global.window = Object.create(window);
  Object.defineProperty(global.window, "HumanConnect", {
    value: {
      on(event: string, cb: () => void) {
        if (event === "connect") cb();
      },
    },
    writable: true,
  });
  Object.defineProperty(global.window, "matchMedia", {
    writable: true,
    value: jest.fn().mockImplementation((query) => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // Deprecated
      removeListener: jest.fn(), // Deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  });
});
afterEach(() => {
  global.window = originalWindow;
});

// Mock element scrollTo
Element.prototype.scrollTo = () => {};
Element.prototype.scrollIntoView = () => {};
