/**
 *
 *
 * Tests for useUsStates
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import useUsStates from "./useUsStates";

describe("useUsStates", () => {
  it("useUsStates should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      useUsStates();
      return <div></div>;
    };
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });
});
