/**
 *
 *
 * useFlyOut
 *
 *
 */
/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui";
import { FC, MouseEvent, useEffect, useRef } from "react";
import { useState, createContext, useContext, useCallback } from "react";
import { createPortal } from "react-dom";
import { AnimatePresence, motion } from "framer-motion";
import IconX from "components/IconX";

type ContextValue = {
  content: React.ReactNode | null;
  setFlyOut: (value: React.SetStateAction<React.ReactNode>) => void;
  removeFlyOut: () => void;
};

const initialContext: ContextValue = {
  content: null,
  setFlyOut: () => {},
  removeFlyOut: () => {},
};

const FlyOutContext = createContext<ContextValue>(initialContext);

interface FlyOutProps {
  remove: (event: MouseEvent<HTMLElement>) => void;
}

const FlyOut: FC<FlyOutProps> = ({ children, remove }) => {
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    function handleClickOutside(event: any) {
      if (ref.current && !ref.current.contains(event.target)) remove(event);
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, remove]);

  return (
    <Box
      data-testid="fly-out"
      sx={{
        zIndex: 999999,
        width: "100vw",
        height: "100vh",
        position: "fixed",
        top: 0,
        bottom: 0,
        display: "block",
      }}
    >
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition: { duration: 0.2 } }}
        exit={{ opacity: 0, transition: { duration: 0.7 } }}
      >
        <Flex
          sx={{
            position: "fixed",
            width: "100%",
            height: "-webkit-fill-available",
            bg: "rgba(0,0,0,0.7)",
            boxShadow: "small",
            top: 0,
            left: 0,
            justifyContent: "flex-end",
          }}
        >
          <motion.div
            ref={ref}
            initial={{ x: "100%" }}
            animate={{
              x: 0,
              transition: { duration: 0.2 },
            }}
            exit={{ x: "100%", transition: { duration: 0.1 } }}
          >
            <Flex
              sx={{
                bg: "white",
                position: "relative",
                top: 0,
                right: 0,
                height: "-webkit-fill-available",
                width: ["calc(100vw - 30px)", 520],
              }}
            >
              <Flex sx={{ p: 4, width: "100%" }}>
                <Box
                  onClick={remove}
                  sx={{
                    position: "absolute",
                    right: 0,
                    top: 0,
                    m: 3,
                    cursor: "pointer",
                  }}
                >
                  <IconX />
                </Box>
                {children}
              </Flex>
            </Flex>
          </motion.div>
        </Flex>
      </motion.div>
    </Box>
  );
};

export const FlyOutProvider: FC = ({ children }) => {
  const [content, setFlyOut] = useState(initialContext.content);
  const removeFlyOut = useCallback(() => {
    setFlyOut(null);
  }, [setFlyOut]);

  return (
    <FlyOutContext.Provider value={{ content, setFlyOut, removeFlyOut }}>
      {children}
      {createPortal(
        <AnimatePresence>
          {content && <FlyOut remove={removeFlyOut}>{content}</FlyOut>}
        </AnimatePresence>,
        document.body
      )}
    </FlyOutContext.Provider>
  );
};

const useFlyOut = () => useContext(FlyOutContext);

export default useFlyOut;
