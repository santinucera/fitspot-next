/**
 *
 *
 * Tests for useCategorizedTrainerSessions
 *
 *
 */

import React from "react";
import { queryCache } from "react-query";
import { screen } from "@testing-library/react";
import useCategorizedTrainerSessions from "./useCategorizedTrainerSessions";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import dayjs from "utils/days";
import { SessionStatus } from "services/SessionService";
import { createTrainerSessionModel } from "test-utils/session-service-test-utils";

const { getByTestId } = screen;

describe("useCategorizedTrainerSessions", () => {
  it("should separate trainer sessions into categories sorted by start date ASC", () => {
    const session = createTrainerSessionModel();

    // Put all dates in the future so nothing is filtered out.
    const today = dayjs().add(1, "hour");
    const tomorrow = dayjs().add(1, "day");
    const newSession1 = {
      ...session,
      name: "New 1",
      status: SessionStatus.new,
      startDate: today.subtract(1, "minute"),
    };
    const newSession2 = {
      ...session,
      name: "New 2",
      status: SessionStatus.new,
      startDate: tomorrow.add(1, "minute"),
    };
    const inProgressSession1 = {
      ...session,
      name: "In Progress 1",
      status: SessionStatus.inProgress,
      startDate: today,
    };
    const inProgressSession2 = {
      ...session,
      name: "In Progress 2",
      status: SessionStatus.inProgress,
      startDate: tomorrow,
    };
    const pendingSession1 = {
      ...session,
      name: "Pending 1",
      status: SessionStatus.pending,
      startDate: today,
    };
    const pendingSession2 = {
      ...session,
      name: "Pending 2",
      status: SessionStatus.pending,
      startDate: tomorrow,
    };
    const upcomingSession1 = {
      ...session,
      name: "Upcoming 1",
      status: SessionStatus.accepted,
      startDate: today,
    };
    const upcomingSession2 = {
      ...session,
      name: "Upcoming 2",
      status: SessionStatus.accepted,
      startDate: tomorrow,
    };

    queryCache.setQueryData("trainers-sessions", [
      newSession2,
      upcomingSession2,
      pendingSession2,
      inProgressSession2,
      newSession1,
      upcomingSession1,
      pendingSession1,
      inProgressSession1,
    ]);

    const expected = {
      inProgress: [inProgressSession1, inProgressSession2],
      pending: [newSession1, pendingSession1, pendingSession2, newSession2],
      upcoming: [upcomingSession1, upcomingSession2],
    };

    const El = () => {
      const categorizedSessions = useCategorizedTrainerSessions();
      return (
        <div data-testid="data">{JSON.stringify(categorizedSessions)}</div>
      );
    };

    renderWithMemoryRouter("/*", [`/?session_id=1`], <El />);

    expect(getByTestId("data")).toHaveTextContent(JSON.stringify(expected));
  });

  it("should remove pending sessions that started before the current time", () => {
    const pendingSession = {
      ...createTrainerSessionModel(),
      status: SessionStatus.pending,
    };
    const pastSession = {
      ...pendingSession,
      startDate: dayjs().subtract(1, "hour"),
    };
    const futureSession = {
      ...pendingSession,
      startDate: dayjs().add(1, "hour"),
    };

    queryCache.setQueryData("trainers-sessions", [pastSession, futureSession]);

    const expected = {
      inProgress: [],
      pending: [futureSession],
      upcoming: [],
    };

    const El = () => {
      const categorizedSessions = useCategorizedTrainerSessions();
      return (
        <div data-testid="data">{JSON.stringify(categorizedSessions)}</div>
      );
    };

    renderWithMemoryRouter("/*", [`/?session_id=1`], <El />);

    expect(getByTestId("data")).toHaveTextContent(JSON.stringify(expected));
  });

  it("should return an empty TrainerSessionModel if sessions is undefined", () => {
    queryCache.setQueryData("trainers-sessions", undefined);
    const El = () => {
      const categorizedSessions = useCategorizedTrainerSessions();
      return (
        <div data-testid="data">{JSON.stringify(categorizedSessions)}</div>
      );
    };

    renderWithMemoryRouter("/*", [`/?session_id=1`], <El />);

    const expected = {
      inProgress: [],
      pending: [],
      upcoming: [],
    };

    expect(getByTestId("data")).toHaveTextContent(JSON.stringify(expected));
  });
});
