/**
 *
 *
 * Tests for useSessionStatus
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import useSessionStatus from "./useSessionStatus";
import dayjs, { Dayjs } from "dayjs";

const { getByTestId } = screen;
const renderComponent = (startDate?: Dayjs, endDate?: Dayjs) => {
  const El = () => {
    const {
      isSessionUpcoming,
      isSessionStartingSoon,
      isSessionLive,
      isSessionCompleted,
    } = useSessionStatus({ startDate, endDate });
    return (
      <div data-testid="root">
        {isSessionUpcoming && <div>upcoming</div>}
        {isSessionStartingSoon && <div>soon</div>}
        {isSessionLive && <div>live</div>}
        {isSessionCompleted && <div>completed</div>}
      </div>
    );
  };
  render(<El />);
};

describe("useSessionStatus", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      useSessionStatus({});
      return <div></div>;
    };
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render no session statuses", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent();
    const root = getByTestId("root");
    expect(root).toMatchInlineSnapshot(`
      <div
        data-testid="root"
      />
    `);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render upcoming session", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent(dayjs().add(1, "d"), dayjs().add(2, "d"));
    const root = getByTestId("root");
    expect(root).toMatchInlineSnapshot(`
      <div
        data-testid="root"
      >
        <div>
          upcoming
        </div>
      </div>
    `);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render soon status 15 min before session starts", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent(dayjs().add(10, "m"), dayjs().add(2, "d"));
    const root = getByTestId("root");
    expect(root).toMatchInlineSnapshot(`
      <div
        data-testid="root"
      >
        <div>
          soon
        </div>
      </div>
    `);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render live session status", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent(dayjs().subtract(1, "m"), dayjs().add(1, "h"));
    const root = getByTestId("root");
    expect(root).toMatchInlineSnapshot(`
      <div
        data-testid="root"
      >
        <div>
          live
        </div>
      </div>
    `);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render completed session status", () => {
    const spy = jest.spyOn(global.console, "error");
    renderComponent(dayjs().subtract(1, "d"), dayjs().subtract(1, "h"));
    const root = getByTestId("root");
    expect(root).toMatchInlineSnapshot(`
      <div
        data-testid="root"
      >
        <div>
          completed
        </div>
      </div>
    `);
    expect(spy).not.toHaveBeenCalled();
  });
});
