/**
 *
 *
 * Tests for useSessionRsvp
 *
 *
 */

import React from "react";
import {
  waitFor,
  screen,
  fireEvent,
  within,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import useSessionRsvp from "./useSessionRSVP";
import { queryCache, ReactQueryConfigProvider } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import ModalProvider from "containers/Modal";
import { useQuerySessionGetService } from "services/SessionService";
import { renderWithRouter } from "test-utils/render-with-router";
import {
  createSession,
  setSessionResponse,
  setSessionRsvpResponse,
} from "test-utils/session-service-test-utils";
import { ToastProvider } from "hooks/useToast";

const { getByTestId, getByText } = screen;

const sessionId = 1;

const renderComponent = (isRSVP: boolean = false) => {
  const El = () => {
    /**
     * Session request
     */
    const { isFetchedAfterMount, data } = useQuerySessionGetService({
      urlParams: {
        sessionId: sessionId.toString(),
      },
    });

    const { handleSessionRSVP, handleSessionCancelRSVP } = useSessionRsvp({
      urlParams: {
        sessionId: sessionId.toString(),
      },
      isRSVP,
    });

    if (!isFetchedAfterMount) return null;

    return (
      <div>
        <button
          data-testid="reserve"
          onClick={() => handleSessionRSVP({ sessionId, isRSVP: true })}
        >
          reserve
        </button>
        <button
          data-testid="cancel"
          onClick={() => handleSessionCancelRSVP(sessionId)}
        >
          cancel
        </button>
        <div data-testid="reserved">
          {data?.participants[0]?.isRSVP ? "yes" : "no"}
        </div>
      </div>
    );
  };
  renderWithRouter(
    <ReactQueryConfigProvider config={{ queries: { retry: 0 } }}>
      <ModalProvider>
        <ToastProvider>
          <El />
        </ToastProvider>
      </ModalProvider>
    </ReactQueryConfigProvider>
  );
};

describe("useSessionRsvp", () => {
  it("should not log errors in console", () => {
    queryCache.setQueryData("user", createUser());
    const spy = jest.spyOn(global.console, "error");
    renderComponent();
    expect(spy).not.toHaveBeenCalled();
  });

  it("should throw an error if a user is not in the query cache", () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    expect(() => renderComponent()).toThrow();
    expect(global.console.error).toHaveBeenCalled();
  });

  it("should reserve a session and open a confirmation modal", async () => {
    const user = createUser();
    queryCache.setQueryData("user", user);
    let session = createSession();
    session = {
      ...session,
      id: sessionId,
      participants: [],
    };
    setSessionResponse(200, session);
    renderComponent();
    await waitFor(() => getByTestId("reserve"));
    fireEvent.click(getByTestId("reserve"));
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(
        within(getByTestId("modal")).getByText("Spot Reserved")
      ).toBeInTheDocument();
    });
  });

  it("should show an error toast reserving a session fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const user = createUser();
    queryCache.setQueryData("user", user);
    setSessionRsvpResponse(500);
    renderComponent();
    await waitFor(() => getByTestId("reserve"));
    fireEvent.click(getByTestId("reserve"));
    await waitFor(() => {
      expect(
        getByText("Failed to RSVP to this session, please try again.")
      ).toBeInTheDocument();
    });
    expect(global.console.error).toHaveBeenCalled();
  });

  it("should open a confirmation modal and cancel a reserved a session when confirmed", async () => {
    const user = createUser();
    queryCache.setQueryData("user", user);
    let session = createSession();
    session = {
      ...session,
      id: sessionId,
      participants: [
        {
          ...session.participants[0],
          userId: user.id,
          isRSVP: true,
        },
      ],
    };
    setSessionResponse(200, session);
    renderComponent(true);
    const cancelBtn = await waitFor(() => getByTestId("cancel"));
    fireEvent.click(cancelBtn);
    expect(getByTestId("reserved")).toHaveTextContent("yes");
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(
        within(getByTestId("modal")).getByText(
          "Are you sure you want to cancel your spot?"
        )
      ).toBeInTheDocument();
    });
    /**
     * When the the confirmation button is click the optimistic update sill remove the
     * user from the list and update the UI. When the query settles the user is removed.
     */
    setSessionResponse(200, { ...session, participants: [] });
    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    await waitFor(() => {
      expect(getByTestId("reserved")).toHaveTextContent("no");
    });
  });

  it("should open a confirmation modal and show an error when un-reserving a session fails", async () => {
    const user = createUser();
    queryCache.setQueryData("user", user);
    let session = createSession();
    session = {
      ...session,
      id: sessionId,
      participants: [
        {
          ...session.participants[0],
          userId: user.id,
          isRSVP: true,
        },
      ],
    };
    setSessionResponse(200, session);
    setSessionRsvpResponse(500);
    renderComponent(true);
    const cancelBtn = await waitFor(() => getByTestId("cancel"));
    fireEvent.click(cancelBtn);
    expect(getByTestId("reserved")).toHaveTextContent("yes");
    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(
        within(getByTestId("modal")).getByText(
          "Are you sure you want to cancel your spot?"
        )
      ).toBeInTheDocument();
    });
    fireEvent.click(getByTestId("confirm-button"));
    await waitFor(() => {
      expect(
        getByText(
          "Failed to cancel the reservation to this session, please try again."
        )
      ).toBeInTheDocument();
    });
  });

  it("should open a confirmation modal and not cancel a reserved a session when a user wants to keep the reservation", async () => {
    const user = createUser();
    queryCache.setQueryData("user", user);
    let session = createSession();
    session = {
      ...session,
      id: sessionId,
      participants: [
        {
          ...session.participants[0],
          userId: user.id,
          isRSVP: true,
        },
      ],
    };
    setSessionResponse(200, session);
    renderComponent(true);
    const cancelBtn = await waitFor(() => getByTestId("cancel"));
    fireEvent.click(cancelBtn);
    expect(getByTestId("reserved")).toHaveTextContent("yes");

    await waitFor(() => {
      expect(getByTestId("modal")).toBeInTheDocument();
      expect(
        within(getByTestId("modal")).getByText(
          "Are you sure you want to cancel your spot?"
        )
      ).toBeInTheDocument();
    });

    fireEvent.click(getByTestId("confirm-button"));
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    await waitFor(() => {
      expect(getByTestId("reserved")).toHaveTextContent("yes");
    });
  });
});
