/**
 *
 *
 * useExternalCalendar
 *
 *
 */
import dayjs, { Dayjs } from "dayjs";
import { useCallback } from "react";
import { CalendarPreference } from "services/CalendarService";
import slugify from "utils/slugify";

// UTC without punctuation
export const formatDate = (date: Dayjs): string =>
  date
    .utc()
    .format()
    .replace(/-|:|\.\d\d\d/g, "");

/**
 * Format duration as HHmm.
 */
export const calculateDuration = (startDate: Dayjs, endDate: Dayjs) => {
  const duration = dayjs.duration(endDate.diff(startDate));
  // Add a leading zero if the value is one digit
  const addLeadingZero = (value: string) =>
    value.length === 1 ? `0${value}` : value;
  const hours = duration.get("hours").toString();
  const minutes = duration.get("minutes").toString();
  return `${addLeadingZero(hours)}${addLeadingZero(minutes)}`;
};

export interface CalendarEventOptions {
  calendarPreference: CalendarPreference;
  id: number;
  title: string | null;
  description: string | null;
  startDate: Dayjs;
  endDate: Dayjs;
  path: string;
}

export const createEvent = ({
  calendarPreference,
  id,
  title,
  description,
  startDate,
  endDate,
  path,
}: CalendarEventOptions): void => {
  const encodedTitle = title ? encodeURIComponent(title) : "";
  const location = `https://tenspot.co${path}`;
  const encodedLocation = encodeURIComponent(location);
  const encodedDescription = encodeURIComponent(
    `${description}\n\nLive stream enabled 15 minutes before session starts. \n\n${location}`
  );
  const formattedStartDate = formatDate(startDate);
  const formattedEndDate = formatDate(endDate);

  switch (calendarPreference) {
    case CalendarPreference.GOOGLE:
      window.open(
        `https://calendar.google.com/calendar/r/eventedit?action=TEMPLATE&dates=${formattedStartDate}/${formattedEndDate}&location=${encodedLocation}&text=${encodedTitle}&details=${encodedDescription}`
      );
      break;
    case CalendarPreference.YAHOO:
      window.open(
        `https://calendar.yahoo.com/?v=60&view=d&type=20&title=${encodedTitle}&st=${formattedStartDate}&dur=${calculateDuration(
          startDate,
          endDate
        )}&desc=${encodedDescription}&in_loc=${encodedLocation}`
      );
      break;
    case CalendarPreference.OUTLOOK_WEB:
      window.open(
        `https://outlook.live.com/owa/?rru=addevent&startdt=${formattedStartDate}&enddt=${formattedEndDate}&subject=${encodedTitle}&location=${encodedLocation}&body=${encodedDescription}&allday=false&uid=&path=/calendar/view/Month`
      );
      break;
    default:
      const now = formatDate(dayjs());
      const ics = [
        "BEGIN:VCALENDAR",
        "VERSION:2.0",
        "PRODID:-//TENSPOT//TENSPOT//EN",
        "METHOD:PUBLISH",
        "BEGIN:VEVENT",
        `UID:FS${now}${id}@tenspot.com`,
        `URL:${document.URL}`,
        `DTSTART:${formattedStartDate}`,
        `DTEND:${formattedEndDate}`,
        `DTSTAMP:${now}`,
        `SUMMARY:${title}`,
        `DESCRIPTION:${description}\\n\\nLive stream enabled 15 minutes before session starts.\\n\\n${location}`,
        `LOCATION:${location}`,
        "END:VEVENT",
        "END:VCALENDAR",
      ].join("\r\n");
      const link = document.createElement("a");
      link.href = `data:text/calendar;charset=utf8,${encodeURIComponent(ics)}`;
      link.setAttribute("download", `${title ? slugify("", title) : ""}.ics`);
      link.setAttribute("data-testid", "ics-download-link");
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      break;
  }
};

const useExternalCalendar = () => {
  const createExternalCalendarEvent = useCallback(
    async ({
      id,
      title,
      description,
      startDate,
      endDate,
      calendarPreference,
      path,
    }: CalendarEventOptions): Promise<void> => {
      createEvent({
        calendarPreference,
        id,
        title,
        description,
        startDate,
        endDate,
        path,
      });
    },
    []
  );
  return { createExternalCalendarEvent };
};

export default useExternalCalendar;
