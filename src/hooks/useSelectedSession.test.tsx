/**
 *
 *
 * Tests for useSelectedSession
 *
 *
 */

import React from "react";
import { waitFor } from "@testing-library/react";
import { queryCache } from "react-query";
import useSelectedSession from "./useSelectedSession";
import { renderWithMemoryRouter } from "test-utils/render-with-router";
import { createTrainerSessionModel } from "test-utils/session-service-test-utils";

describe("useSelectedSession", () => {
  it("useSelectedSession should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      useSelectedSession();
      return <div></div>;
    };
    renderWithMemoryRouter("/*", [`/?session_id=1`], <El />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("useSelectedSession should render data", async () => {
    const id = 1;
    const session = { ...createTrainerSessionModel(), id };
    queryCache.setQueryData("trainers-sessions", [session]);
    const El = () => {
      const { selectedSession, selectedSessionId } = useSelectedSession();
      return (
        <div>
          <div data-testid="session">{JSON.stringify(selectedSession)}</div>
          <div data-testid="id">{selectedSessionId}</div>
        </div>
      );
    };
    const { getByTestId } = renderWithMemoryRouter(
      "/*",
      [`/?session_id=${id}`],
      <El />
    );
    await waitFor(() => {
      expect(getByTestId("session")).toHaveTextContent(JSON.stringify(session));
    });
  });
});
