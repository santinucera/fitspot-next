/**
 *
 *
 * Tests for useUpdatePassword
 *
 *
 */
import React, { useEffect } from "react";
import { render, waitFor, screen } from "@testing-library/react";
import useUpdatePassword from "./useUpdatePassword";
import { ToastProvider } from "hooks/useToast";
import { setUserPasswordResetResponse } from "test-utils/user-service-test-utils";

const { getByTestId } = screen;

describe("useUpdatePassword", () => {
  it("should call onSuccessCallback and show a toast when update succeeds", async () => {
    const onSuccessCallback = jest.fn();
    const El = () => {
      const [updatePassword] = useUpdatePassword(onSuccessCallback);
      useEffect(() => {
        updatePassword({
          email: "some email",
          oldPassword: "hunter2",
          newPassword: "hunter3",
        });
      }, [updatePassword]);
      return null;
    };
    render(
      <ToastProvider>
        <El />
      </ToastProvider>
    );
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        "Success!Your password has been updated."
      )
    );
  });

  it("should call onFailureCallback and show a toast when update fails", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const onFailureCallback = jest.fn();
    setUserPasswordResetResponse(500);
    const El = () => {
      const [updatePassword] = useUpdatePassword(undefined, onFailureCallback);
      useEffect(() => {
        updatePassword({
          email: "some email",
          oldPassword: "hunter2",
          newPassword: "hunter3",
        });
      }, [updatePassword]);
      return null;
    };
    render(
      <ToastProvider>
        <El />
      </ToastProvider>
    );
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        "Error!Failed to change password."
      )
    );
    expect(onFailureCallback).toHaveBeenCalled();
  });

  it("should show specific message when old password does not match", async () => {
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    const onFailureCallback = jest.fn();
    setUserPasswordResetResponse(400, undefined, "old password mismatch");
    const El = () => {
      const [updatePassword] = useUpdatePassword(undefined, onFailureCallback);
      useEffect(() => {
        updatePassword({
          email: "some email",
          oldPassword: "hunter2",
          newPassword: "hunter3",
        });
      }, [updatePassword]);
      return null;
    };
    render(
      <ToastProvider>
        <El />
      </ToastProvider>
    );
    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent(
        "Old password does not match."
      )
    );
    expect(onFailureCallback).toHaveBeenCalled();
  });
});
