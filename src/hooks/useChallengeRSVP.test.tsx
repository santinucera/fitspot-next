/**
 *
 *
 * Tests for useChallengeRSVP
 *
 *
 */
import React, { useEffect } from "react";
import { waitFor, screen } from "@testing-library/react";
import { queryCache } from "react-query";
import {
  createChallenge,
  setChallengeRSVPPostResponse,
} from "test-utils/challenge-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";
import { ToastProvider } from "hooks/useToast";
import { createUser } from "test-utils/user-service-test-utils";
import useChallengeRSVP from "./useChallengeRSVP";
import {
  challengeModel,
  ChallengeRsvpPostParams,
} from "services/ChallengeService";

const mockReportChallengeRSVP = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportChallengeRSVP: mockReportChallengeRSVP,
  }))
);

const challengesQueryKey = "current-challenges";

const renderComponent = (params: ChallengeRsvpPostParams) => {
  const El = () => {
    const { handleChallengeRSVP } = useChallengeRSVP(challengesQueryKey);
    useEffect(() => {
      handleChallengeRSVP(params);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return <div />;
  };

  renderWithRouter(
    <ToastProvider>
      <El />
    </ToastProvider>
  );
};

describe("handleChallengeRSVP", () => {
  const { getByTestId } = screen;

  it("should RSVP for a challenge, show a toast, and report to analytics", async () => {
    const user = {
      ...createUser(),
      calendarPreference: 3,
    };
    queryCache.setQueryData("user", user);
    let challenge = challengeModel(createChallenge());
    queryCache.setQueryData("current-challenges", [challenge]);

    renderComponent({ challengeId: challenge.id, isRSVP: true });

    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Challenge Joined")
    );
    expect(mockReportChallengeRSVP).toHaveBeenCalledWith({
      userId: user.id,
      challengeId: challenge.id,
      isRSVP: true,
    });
  });

  it("should un-RSVP for a challenge", async () => {
    const user = {
      ...createUser(),
      calendarPreference: 3,
    };
    queryCache.setQueryData("user", user);
    let challenge = challengeModel(createChallenge());
    queryCache.setQueryData("current-challenges", [challenge]);

    renderComponent({ challengeId: challenge.id, isRSVP: false });

    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Challenge Canceled")
    );
    expect(mockReportChallengeRSVP).toHaveBeenCalledWith({
      userId: user.id,
      challengeId: challenge.id,
      isRSVP: false,
    });
  });

  it("should show an error toast when the request fails", async () => {
    // Swallow request error
    const errorSpy = jest
      .spyOn(global.console, "error")
      .mockImplementation(() => {});
    setChallengeRSVPPostResponse(500);
    const user = {
      ...createUser(),
      calendarPreference: 3,
    };
    queryCache.setQueryData("user", user);
    let challenge = challengeModel(createChallenge());
    queryCache.setQueryData("current-challenges", [challenge]);

    renderComponent({ challengeId: challenge.id, isRSVP: false });

    await waitFor(() =>
      expect(getByTestId(/toast-item/)).toHaveTextContent("Error!")
    );
    expect(mockReportChallengeRSVP).not.toHaveBeenCalled();
    expect(errorSpy).toHaveBeenCalled();
  });
});
