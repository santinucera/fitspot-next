/**
 *
 *
 * useSelectedSession
 *
 *
 */
import {
  TrainerSessionModel,
  useQueryTrainersSessionsGetData,
} from "services/SessionService";
import { useSearchParams } from "react-router-dom";

const useSelectedSession = (): {
  selectedSession: TrainerSessionModel | null;
  selectedSessionId: number | null;
} => {
  const [searchParams] = useSearchParams();
  const selectedSessionId = searchParams.get("session_id");
  const sessions = useQueryTrainersSessionsGetData();
  const selectedSession = sessions?.find(
    ({ id }) => id === Number(selectedSessionId)
  );

  return {
    selectedSession: selectedSession || null,
    selectedSessionId: Number(selectedSessionId) || null,
  };
};

export default useSelectedSession;
