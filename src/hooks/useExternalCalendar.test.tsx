/**
 *
 *
 * Tests for useExternalCalendar
 *
 *
 */

import React, { useEffect } from "react";
import { waitFor } from "@testing-library/react";
import dayjs from "utils/days";
import useExternalCalendar, {
  formatDate,
  calculateDuration,
  createEvent,
} from "./useExternalCalendar";
import { CalendarPreference } from "services/CalendarService";
import { setCalendarSettingsGetResponse } from "test-utils/calendar-service-test-utils";
import { createUser } from "test-utils/user-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";
import { queryCache } from "react-query";

describe("formatDate", () => {
  it("should return UTC format with the punctuation removed", () => {
    const date = dayjs(new Date(0));
    expect(formatDate(date)).toBe("19700101T000000Z");
  });
});

describe("calculateDuration", () => {
  it("should return the duration in HHmm format", () => {
    const startDate = dayjs();
    const endDate = dayjs().add(10, "hour").add(10, "minute");
    expect(calculateDuration(startDate, endDate)).toBe("1010");
  });

  it("should add leading zeros", () => {
    const startDate = dayjs();
    const endDate = dayjs().add(1, "hour").add(1, "minute");
    expect(calculateDuration(startDate, endDate)).toBe("0101");
  });
});

describe("createEvent", () => {
  const id = 1;
  const title = "some title";
  const location = `https://tenspot.co/dashboard/sessions/${id}`;
  const description = "some description";
  const encodedDescription = encodeURIComponent(
    `${description}\n\nLive stream enabled 15 minutes before session starts. \n\n${location}`
  );
  const startDate = dayjs(new Date(0));
  const endDate = startDate.add(1, "hour");

  beforeEach(async () => {
    window.open = jest.fn();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("should open a google url", () => {
    createEvent({
      calendarPreference: CalendarPreference.GOOGLE,
      id,
      title,
      description,
      startDate,
      endDate,
      path: `/dashboard/sessions/${id}`,
    });

    expect(window.open).toHaveBeenCalledWith(
      `https://calendar.google.com/calendar/r/eventedit?action=TEMPLATE&dates=${formatDate(
        startDate
      )}/${formatDate(endDate)}&location=${encodeURIComponent(
        location
      )}&text=${encodeURIComponent(title)}&details=${encodedDescription}`
    );
  });

  it("should open a yahoo url", () => {
    createEvent({
      calendarPreference: CalendarPreference.YAHOO,
      id,
      title,
      description,
      startDate,
      endDate,
      path: `/dashboard/sessions/${id}`,
    });
    expect(window.open).toHaveBeenCalledWith(
      `https://calendar.yahoo.com/?v=60&view=d&type=20&title=${encodeURIComponent(
        title
      )}&st=${formatDate(startDate)}&dur=${calculateDuration(
        startDate,
        endDate
      )}&desc=${encodedDescription}&in_loc=${encodeURIComponent(location)}`
    );
  });

  it("should open an Outlook Web url", () => {
    createEvent({
      calendarPreference: CalendarPreference.OUTLOOK_WEB,
      id,
      title,
      description,
      startDate,
      endDate,
      path: `/dashboard/sessions/${id}`,
    });
    expect(window.open).toHaveBeenCalledWith(
      `https://outlook.live.com/owa/?rru=addevent&startdt=${formatDate(
        startDate
      )}&enddt=${formatDate(endDate)}&subject=${encodeURIComponent(
        title
      )}&location=${encodeURIComponent(
        location
      )}&body=${encodedDescription}&allday=false&uid=&path=/calendar/view/Month`
    );
  });

  it.each([
    CalendarPreference.APPLE,
    CalendarPreference.OUTLOOK_DESKTOP,
    CalendarPreference.ICAL,
  ])(
    "should download and ics file when calendarPreference is %s",
    (calendarPreference) => {
      jest.spyOn(document.body, "appendChild");
      createEvent({
        calendarPreference,
        id,
        title,
        description,
        startDate,
        endDate,
        path: `/dashboard/sessions/${id}`,
      });
      // TODO: test href value
      expect(document.body.appendChild).toHaveBeenCalled();
    }
  );
});

describe("createExternalCalendarEvent", () => {
  const date = dayjs(new Date(1970, 1, 1));
  const calendarEventOptions = (calendarPreference: number) => ({
    id: 1,
    title: "some title",
    description: "some description",
    startDate: date,
    endDate: date,
    calendarPreference,
    path: `/dashboard/sessions/1`,
  });

  beforeEach(async () => {
    // Swallow `Error: Not implemented: navigation (except hash changes)`
    jest.spyOn(console, "error").mockImplementation(() => {});
    window.open = jest.fn();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it.each([
    CalendarPreference.GOOGLE,
    CalendarPreference.YAHOO,
    CalendarPreference.OUTLOOK_WEB,
  ])(
    "should open calendar link when preference is %d",
    async (preference: CalendarPreference) => {
      queryCache.setQueryData("user", {
        ...createUser(),
        calendarPreference: preference,
      });
      const El = () => {
        const { createExternalCalendarEvent } = useExternalCalendar();
        useEffect(() => {
          createExternalCalendarEvent(calendarEventOptions(preference));
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);
        return <div></div>;
      };

      setCalendarSettingsGetResponse(200, { preference });
      renderWithRouter(<El />);
      await waitFor(() => expect(window.open).toHaveBeenCalled());
    }
  );

  it.each([
    CalendarPreference.GOOGLE,
    CalendarPreference.YAHOO,
    CalendarPreference.OUTLOOK_WEB,
  ])(
    "should open calendar link when preference is %d",
    async (preference: CalendarPreference) => {
      const spy = jest.spyOn(window, "open").mockImplementation(jest.fn());
      queryCache.setQueryData("user", {
        ...createUser(),
        calendarPreference: preference,
      });
      const El = () => {
        const { createExternalCalendarEvent } = useExternalCalendar();
        useEffect(() => {
          createExternalCalendarEvent(calendarEventOptions(preference));
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);
        return <div></div>;
      };

      setCalendarSettingsGetResponse(200, { preference });
      renderWithRouter(<El />);
      await waitFor(() => expect(spy).toHaveBeenCalled());
    }
  );

  it.each([
    CalendarPreference.APPLE,
    CalendarPreference.OUTLOOK_DESKTOP,
    CalendarPreference.ICAL,
  ])(
    "should download .ics when preference is %d",
    async (preference: CalendarPreference) => {
      const spy = jest.spyOn(document.body, "appendChild");
      queryCache.setQueryData("user", {
        ...createUser(),
        calendarPreference: preference,
      });
      const El = () => {
        const { createExternalCalendarEvent } = useExternalCalendar();
        useEffect(() => {
          createExternalCalendarEvent(calendarEventOptions(preference));
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);
        return <div></div>;
      };

      setCalendarSettingsGetResponse(200, { preference });
      renderWithRouter(<El />);
      await waitFor(() => expect(spy).toHaveBeenCalled());
    }
  );
});
