/**
 *
 *
 * Tests for useRateSession
 *
 *
 */

import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import { ToastProvider } from "hooks/useToast";
import ModalProvider from "containers/Modal";
import useRateSession from "./useRateSession";
import {
  createNonRatedSessionModel,
  setNonRatedSessionsResponse,
  createCustomerDashboardSessionModel,
  setRateSessionResponse,
} from "test-utils/session-service-test-utils";

it("should not log errors in console", () => {
  const spy = jest.spyOn(global.console, "error");
  const El = () => {
    useRateSession({});
    return <div></div>;
  };
  render(<El />);
  expect(spy).not.toHaveBeenCalled();
});

it("should remove session after rating session", async () => {
  const sessions = [createNonRatedSessionModel()];
  setNonRatedSessionsResponse(sessions);
  const El = () => {
    const { rateSession, nonRatedSessions } = useRateSession({});
    const handleRate = () =>
      rateSession({ sessionId: sessions[0].id, rating: { overall: 5 } });
    return (
      <div>
        {nonRatedSessions?.map((session) => (
          <span key={session.id}>{session.name}</span>
        ))}
        <button onClick={handleRate}>Rate</button>
      </div>
    );
  };
  const { getByRole, queryByText } = render(<El />);
  await waitFor(() => {
    expect(queryByText(sessions[0].name)).toBeInTheDocument();
  });
  const button = await getByRole("button");
  await waitFor(() => {
    fireEvent.click(button);
  });
  expect(queryByText(sessions[0].name)).toBeNull();
});

it("should change rating of the session after it is rated", async () => {
  const sessions = [createCustomerDashboardSessionModel()];
  const El = () => {
    const { rateSession } = useRateSession({ attendedSessions: sessions });
    const handleRate = () =>
      rateSession({ sessionId: sessions[0].id, rating: { overall: 5 } });
    return (
      <div>
        {sessions?.map((session) => (
          <span key={session.id}>
            {session?.userSessionRating?.ratingPoints || 0}
          </span>
        ))}
        <button onClick={handleRate}>Rate</button>
      </div>
    );
  };
  const { getByRole, queryByText, getByTestId } = render(
    <ModalProvider>
      <ToastProvider>
        <El />
      </ToastProvider>
    </ModalProvider>
  );
  await waitFor(() => {
    expect(queryByText("0")).toBeInTheDocument();
  });
  const button = await getByRole("button");
  await waitFor(() => {
    fireEvent.click(button);
  });
  expect(queryByText("5")).toBeNull();
  await waitFor(() => {
    expect(
      queryByText("You successfully rated the session.")
    ).toBeInTheDocument();
  });
  await waitFor(() => {
    expect(getByTestId("modal")).toBeInTheDocument();
  });
});

it("should not change the rating of the session after the mutation fails", async () => {
  jest.spyOn(global.console, "error").mockImplementation(() => {});
  const sessions = [createCustomerDashboardSessionModel()];
  const El = () => {
    const { rateSession } = useRateSession({ attendedSessions: sessions });
    const handleRate = () =>
      rateSession({ sessionId: sessions[0].id, rating: { overall: 5 } });
    return (
      <div>
        <button onClick={handleRate}>Rate</button>
      </div>
    );
  };
  const { getByRole, queryByText } = render(
    <ToastProvider>
      <El />
    </ToastProvider>
  );
  const button = await getByRole("button");
  setRateSessionResponse(500);
  fireEvent.click(button);
  await waitFor(() => {
    expect(
      queryByText("An error occurred, please try again.")
    ).toBeInTheDocument();
  });
});
