/**
 *
 *
 * Tests for useHumanApiModal
 *
 *
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import useHumanApiModal from "./useHumanApiModal";
import { mockResponseData } from "services/HumanApiService";
import { createUser } from "test-utils/user-service-test-utils";
import { queryCache } from "react-query";
import { User } from "services/types";

const mockReportHumanApiDeviceConnected = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportHumanApiDeviceConnected: mockReportHumanApiDeviceConnected,
  }))
);

describe("useHumanApiModal", () => {
  let user: User;
  beforeEach(async () => {
    user = createUser();
    queryCache.setQueryData("user", () => user);
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      useHumanApiModal();
      return <div></div>;
    };
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render data", async () => {
    const El = () => {
      const { token } = useHumanApiModal();
      return (
        <div>
          {token && <div data-testid="data">{JSON.stringify(token)}</div>}
        </div>
      );
    };
    const { getByTestId } = render(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(mockResponseData.token)
      );
    });
  });

  it("should report a user connected to a device through rudder analytics", async () => {
    const El = () => {
      const { token } = useHumanApiModal();
      return (
        <div>
          {token && <div data-testid="data">{JSON.stringify(token)}</div>}
        </div>
      );
    };
    const { getByTestId } = render(<El />);
    await waitFor(() => {
      expect(getByTestId("data")).toHaveTextContent(
        JSON.stringify(mockResponseData.token)
      );
    });

    expect(mockReportHumanApiDeviceConnected).toHaveBeenCalledWith({
      userId: user.id,
    });
  });
});
