/**
 *
 *
 * useUpdatePassword
 *
 *
 */
import {
  useMutationUserPatchPasswordService,
  PasswordPatchRequestData,
} from "services/UserService";
import useToast from "hooks/useToast";
import { MutationResultPair } from "react-query";
import { User } from "services/types";
import { AxiosError } from "axios";

type UseUpdatePassword = (
  onSuccessCallback?: () => void | undefined,
  onErrorCallback?: () => void | undefined
) => MutationResultPair<User, AxiosError, PasswordPatchRequestData, () => void>;

const useUpdatePassword: UseUpdatePassword = (
  onSuccessCallback,
  onErrorCallback
) => {
  const { addToast } = useToast();
  return useMutationUserPatchPasswordService({
    mutationConfig: {
      onSuccess() {
        addToast({
          title: "Success!",
          description: "Your password has been updated.",
          type: "success",
        });
        if (onSuccessCallback) onSuccessCallback();
      },
      onError(error) {
        addToast({
          title: "Error!",
          description:
            error.response?.status === 400 &&
            /old password mismatch/.test(error?.response?.data?.message)
              ? "Old password does not match."
              : "Failed to change password.",
          type: "error",
        });
        if (onErrorCallback) onErrorCallback();
      },
    },
  });
};

export default useUpdatePassword;
