/**
 *
 *
 * Tests for useSessionsRSVP
 *
 *
 */
import React, { Fragment } from "react";
import {
  waitFor,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import {
  createCustomerDashboardSession,
  setSessionsResponse,
  createStandardSession,
  setFeaturedSessionsResponse,
} from "test-utils/session-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";
import ModalProvider from "containers/Modal";
import {
  CustomerDashboardSession,
  useQuerySessionsGetService,
  useQueryFeaturedSessionsGetService,
  customerDashboardSessionModel,
  standardSessionModel,
} from "services/SessionService";
import useSessionsRSVP from "./useSessionsRSVP";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";

const mockReportRSVP = jest.fn();

jest.mock("hooks/useAnalytics", () =>
  jest.fn().mockImplementation(() => ({
    reportRSVP: mockReportRSVP,
  }))
);

const renderComponent = (session: CustomerDashboardSession) => {
  const El = () => {
    const { isSuccess: isSessionsSuccess } = useQuerySessionsGetService();

    // TODO: refactor to pass query key instead, then use in Session container
    const { handleRSVP, handleCancelRSVP } = useSessionsRSVP({
      sessionsQueryParams: {},
    });
    return (
      <div>
        {isSessionsSuccess && (
          <Fragment>
            <button
              type="button"
              onClick={() =>
                handleRSVP({
                  sessionId: session.id,
                  isRSVP: true,
                })
              }
            >
              join
            </button>
            <button type="button" onClick={() => handleCancelRSVP(session.id)}>
              cancel
            </button>
          </Fragment>
        )}
      </div>
    );
  };
  renderWithRouter(
    <ModalProvider>
      <El />
    </ModalProvider>
  );
};

describe("handleRSVP", () => {
  beforeEach(async () => {
    window.open = jest.fn();
  });

  afterEach(async () => {
    jest.restoreAllMocks();
  });

  const { getByTestId, getByText } = screen;

  it("should RSVP for a session, report to analytics, and show the Add to Calendar modal", async () => {
    const user = {
      ...createUser(),
      calendarPreference: 3,
    };
    queryCache.setQueryData("user", user);

    // Swallow `Not implemented: navigation (except hash changes)` errors
    jest.spyOn(global.console, "error").mockImplementation(() => {});
    let session = createCustomerDashboardSession();

    setSessionsResponse([session]);

    renderComponent(session);

    const button = await waitFor(() => getByText("join"));
    fireEvent.click(button);
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("add-to-calendar-button"));
    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(mockReportRSVP).toHaveBeenCalledWith({
      isRSVP: true,
      userId: user.id,
      sessionId: session.id,
      companyId: session.companyId,
      category: session.activityCategory,
    });
  });

  it("should RSVP for a session, report to analytics, select a default calendar, and show the Add to Calendar modal", async () => {
    /**
     * Mock the user with a default calendar
     */
    const user = createUser();
    queryCache.setQueryData("user", user);

    let session = createCustomerDashboardSession();

    setSessionsResponse([session]);

    renderComponent(session);

    const button = await waitFor(() => getByText("join"));
    fireEvent.click(button);
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("add-to-calendar-button"));

    // Selects a calender
    fireEvent.click(getByTestId("calendar-option-google"));
    fireEvent.click(getByTestId("add-to-calendar-button"));

    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(mockReportRSVP).toHaveBeenCalledWith({
      isRSVP: true,
      userId: user.id,
      sessionId: session.id,
      companyId: session.companyId,
      category: session.activityCategory,
    });
  });

  it("should show a modal if canceling a reservation and report to analytics if canceled", async () => {
    /**
     * Mock the user with a default calendar
     */
    const user = createUser();
    queryCache.setQueryData("user", user);

    let session = createCustomerDashboardSession();

    setSessionsResponse([session]);

    renderComponent(session);

    const button = await waitFor(() => getByText("cancel"));
    fireEvent.click(button);
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("confirm-button"));

    await waitForElementToBeRemoved(() => getByTestId("modal"));
    expect(mockReportRSVP).toHaveBeenCalledWith({
      isRSVP: false,
      userId: user.id,
      sessionId: session.id,
      companyId: session.companyId,
      category: session.activityCategory,
    });
  });

  it("should reload sessions and featured sessions", async () => {
    // Swallow `Not implemented: navigation (except hash changes)` errors
    jest.spyOn(global.console, "error").mockImplementation(() => {});

    const user = {
      ...createUser(),
      calendarPreference: 3,
    };
    queryCache.setQueryData("user", user);

    const id = 1;
    const session = { ...createCustomerDashboardSession(), isRSVP: false, id };
    setSessionsResponse([session]);
    const featuredSession = { ...createStandardSession(), isRSVP: false, id };
    setFeaturedSessionsResponse(200, [featuredSession]);

    const El = () => {
      const { data: sessions } = useQuerySessionsGetService();
      const { data: featuredSessions } = useQueryFeaturedSessionsGetService();

      const { handleRSVP, handleCancelRSVP } = useSessionsRSVP({
        sessionsQueryParams: {},
      });

      return (
        <div>
          {sessions && (
            <div data-testid="sessions">{JSON.stringify(sessions)}</div>
          )}
          {featuredSessions && (
            <div data-testid="featured-sessions">
              {JSON.stringify(featuredSessions)}
            </div>
          )}

          <button
            type="button"
            onClick={() =>
              handleRSVP({
                sessionId: session.id,
                isRSVP: true,
              })
            }
          >
            join
          </button>
          <button type="button" onClick={() => handleCancelRSVP(session.id)}>
            cancel
          </button>
        </div>
      );
    };

    renderWithRouter(
      <ModalProvider>
        <El />
      </ModalProvider>
    );

    const sessions = await waitFor(() => getByTestId("sessions"));
    expect(sessions).toHaveTextContent(
      JSON.stringify([customerDashboardSessionModel(session)])
    );
    const featuredSessions = await waitFor(() =>
      getByTestId("featured-sessions")
    );
    expect(featuredSessions).toHaveTextContent(
      JSON.stringify([standardSessionModel(featuredSession)])
    );

    const updatedSession = { ...session, isRSVP: true };
    setSessionsResponse([updatedSession]);
    const updatedFeaturedSession = { ...featuredSession, isRSVP: true };
    setFeaturedSessionsResponse(200, [updatedFeaturedSession]);

    fireEvent.click(getByText("join"));
    await waitFor(() => getByTestId("modal"));
    fireEvent.click(getByTestId("add-to-calendar-button"));
    await waitForElementToBeRemoved(() => getByTestId("modal"));

    await waitFor(() =>
      expect(sessions).toHaveTextContent(
        JSON.stringify([customerDashboardSessionModel(updatedSession)])
      )
    );
  });
});
