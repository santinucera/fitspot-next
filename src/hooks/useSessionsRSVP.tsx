/**
 *
 *
 * useSessionsRSVP
 *
 *
 */
import React from "react";
import { queryCache } from "react-query";
import { Dayjs } from "dayjs";
import useToast from "hooks/useToast";
import {
  useMutationSessionRsvpPostService,
  SessionsGetQueryParams,
  StandardSessionModel,
  CustomerDashboardSessionModel,
} from "services/SessionService";
import { useQueryUserGetData } from "services/UserService";
import useAnalytics from "hooks/useAnalytics";
import { useModal } from "containers/Modal/Modal";
import WatchTogetherModal from "containers/WatchTogetherModal";
import CancelReservationModal from "components/CancelReservationModal";

interface SessionsRSVPParams {
  sessionsQueryParams?: SessionsGetQueryParams;
}

type RollbackFn = () => void;

const optimisticallyUpdateSessions = <
  T extends CustomerDashboardSessionModel | StandardSessionModel
>(
  queryKey: string | any[],
  sessionId: number,
  isRSVP: boolean
): RollbackFn => {
  queryCache.cancelQueries(queryKey);
  const previousSessions = queryCache.getQueryData<T[]>(queryKey) || [];
  queryCache.setQueryData<T[]>(queryKey, (): T[] => {
    return previousSessions.map((session: T) => {
      if (session.id === sessionId) {
        return {
          ...session,
          isRSVP,
        };
      }
      return session;
    });
  });

  // Rollback
  return () => queryCache.setQueryData<T[]>(queryKey, previousSessions);
};

/**
 * Type guard to differentiate between CustomerDashboardSessionModel ("sessions")
 * and StandardSessionModel ("featured-sessions").
 */
const isCustomerDashboardSessionModel = (
  session: CustomerDashboardSessionModel | StandardSessionModel
): session is CustomerDashboardSessionModel => {
  return (session as CustomerDashboardSessionModel).startDate !== undefined;
};

/**
 * Return [startDate, endDate] tuple from either type of session.
 */
const getDates = (
  session: CustomerDashboardSessionModel | StandardSessionModel
): [Dayjs, Dayjs] => {
  const startDate = isCustomerDashboardSessionModel(session)
    ? session.startDate
    : session.dates.start;
  const endDate = isCustomerDashboardSessionModel(session)
    ? session.endDate
    : session.dates.end;
  return [startDate, endDate];
};

const useSessionsRSVP = ({ sessionsQueryParams }: SessionsRSVPParams) => {
  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const sessionsQueryKey = ["sessions", sessionsQueryParams];
  const featuredSessionsQueryKey = "featured-sessions";
  const user = useQueryUserGetData();
  const { reportRSVP } = useAnalytics();

  const findSessionById = (
    id: number
  ): CustomerDashboardSessionModel | StandardSessionModel | undefined => {
    const dashboardSessions =
      queryCache.getQueryData<CustomerDashboardSessionModel[]>(
        sessionsQueryKey
      ) || [];
    const featuredSessions =
      queryCache.getQueryData<StandardSessionModel[]>(
        featuredSessionsQueryKey
      ) || [];

    return [...dashboardSessions, ...featuredSessions].find(
      (session) => session.id === id
    );
  };

  /**
   * Reserve a session.
   */
  const [handleRSVP] = useMutationSessionRsvpPostService({
    mutationConfig: {
      onMutate: ({ sessionId, isRSVP }) => {
        const rollbackSessions = optimisticallyUpdateSessions<
          CustomerDashboardSessionModel
        >(sessionsQueryKey, sessionId, isRSVP);

        const rollbackFeaturedSessions = optimisticallyUpdateSessions<
          StandardSessionModel
        >(featuredSessionsQueryKey, sessionId, isRSVP);

        // Rollback
        return () => {
          rollbackSessions();
          rollbackFeaturedSessions();
        };
      },
      onSuccess: (__, { sessionId, isRSVP }) => {
        const session = findSessionById(sessionId);
        // Something is wrong with the implementation if there is no session.
        if (!session)
          throw Error(`a session does not exist with id: ${sessionId}`);
        const [startDate, endDate] = getDates(session);

        if (session && isRSVP) {
          setModal(
            <WatchTogetherModal
              type="session"
              handleClose={removeModal}
              id={session.id}
              title={session.name}
              description={session.description}
              startDate={startDate}
              endDate={endDate}
            />
          );
        } else {
          addToast({
            title: "Reservation Canceled",
            description: `${session.name} | ${startDate.format(
              "ddd MMM d, h:ss"
            )} - ${endDate.format("h:ss A")}`,
            type: "success",
          });
        }

        if (user && session) {
          reportRSVP({
            isRSVP,
            userId: user.id,
            sessionId: sessionId,
            companyId: isCustomerDashboardSessionModel(session)
              ? session.companyId
              : null,
            category: isCustomerDashboardSessionModel(session)
              ? session.activityCategory
              : session.activity?.category || null,
          });
        }
      },
      onError: (_, __, rollback) => {
        rollback();
        addToast({
          title: "Error!",
          description: "An error occurred, please try again.",
          type: "error",
        });
      },
      onSettled: () => {
        queryCache.invalidateQueries(sessionsQueryKey);
        queryCache.invalidateQueries(featuredSessionsQueryKey);
      },
    },
  });

  /**
   * Launch a modal to confirm canceling a session.
   */
  const handleCancelRSVP = (sessionId: number) => {
    const session = findSessionById(sessionId);
    // Something is wrong with the implementation if there is no session.
    if (!session)
      throw Error(`a session does not exist with id:  ${sessionId}`);
    const [startDate, endDate] = getDates(session);

    setModal(
      <CancelReservationModal
        name={session.name}
        startDate={startDate}
        endDate={endDate}
        handleClose={removeModal}
        confirmCancellation={() => {
          handleRSVP({ sessionId, isRSVP: false });
          removeModal();
        }}
      />
    );
  };

  return { handleRSVP, handleCancelRSVP };
};

export default useSessionsRSVP;
