/**
 *
 *
 * useAnalytics
 *
 *
 */
import googleAnalytics, { reportGAEvent } from "utils/google-analytics";
import rudderStack, { rudderStackTrack } from "utils/rudder-stack";
import isProduction from "utils/is-production";

interface ReportRSVPOptions {
  isRSVP: boolean; // RSVP or un-RSVP?
  userId: number;
  sessionId: number;
  companyId: number | null;
  category: string | null;
}
/**
 * Report session RSVP or un-RSVP to GA and Rudder Stack.
 */
const reportRSVP = ({
  isRSVP,
  userId,
  sessionId,
  companyId,
  category,
}: ReportRSVPOptions): void => {
  reportGAEvent({
    category: "rsvpd_session",
    action: isRSVP ? "rsvpd" : "unrsvpd",
    label: sessionId.toString(),
    sessionId,
    companyId,
    activityCategory: category,
  });

  rudderStackTrack("fe_rsvpd_session", {
    user_id: userId.toString(),
    session_id: sessionId.toString(),
    rsvp: isRSVP,
  });
};

interface ReportAttendedOptions {
  userId: number;
  sessionId: number;
  companyId: number | null;
  category: string | null;
}
/**
 * Report session attended to analytics.
 */
const reportAttended = ({
  userId,
  sessionId,
  companyId,
  category,
}: ReportAttendedOptions): void => {
  reportGAEvent({
    category: "attended_session",
    action: "attended",
    label: sessionId.toString(),
    sessionId,
    companyId,
    activityCategory: category,
  });

  rudderStackTrack("fe_joined_session", {
    user_id: userId.toString(), // This isn't snake case like the rest, but consistent with old app
    session_id: sessionId.toString(),
    attended: true,
  });
};

interface ReportChallengeRSVPOptions {
  userId: number;
  challengeId: number;
  isRSVP: boolean; // Response data from enterprise/accept-challenge/:challengeId
}
/**
 * Report challenge RSVP.
 */
const reportChallengeRSVP = ({
  userId,
  challengeId,
  isRSVP,
}: ReportChallengeRSVPOptions) => {
  rudderStackTrack("fe_joined_challenge", {
    user_id: userId.toString(),
    challenge_id: challengeId.toString(),
    joined_success: isRSVP,
  });
};

interface ReportAddPointsOptions {
  userId: number;
  challengeId: number;
}
/**
 * Report add points to challenge.
 */
const reportAddPoints = ({
  userId,
  challengeId,
}: ReportAddPointsOptions): void => {
  rudderStackTrack("fe_challenge_data", {
    user_id: userId.toString(),
    challenge_id: challengeId.toString(),
    activity_data_success: true,
  });
};

interface ReportOnDemandViewOptions {
  userId: number;
  videoId: number;
}
/**
 * Report on-demand video views.
 *
 * This is currently unused, we've switched to triggering
 * reportOnDemandPlay when the play button is clicked on the
 * video player.
 */
const reportOnDemandView = ({
  userId,
  videoId,
}: ReportOnDemandViewOptions): void => {
  rudderStackTrack("fe_viewed_ondemand", {
    user_id: userId.toString(),
    sessions_id: videoId.toString(), // Extra "s" in "sessions_id" is inconsistent but correct
  });
};

export interface ReportOnDemandPlayOptions {
  userId: number;
  videoId: number;
}
/**
 * Report on-demand video play.
 */
export const reportOnDemandPlay = ({
  userId,
  videoId,
}: ReportOnDemandPlayOptions): void => {
  rudderStackTrack("fe_played_ondemand", {
    user_id: userId.toString(),
    sessions_id: videoId.toString(),
  });
};

interface ReportJoinPrivateGroupOptions {
  sessionId: number;
  userId: number;
  companyId: number | null;
}
/**
 * Report clicks on the Join Private Group button in sessions.
 */
const reportJoinPrivateGroup = ({
  sessionId,
  userId,
  companyId,
}: ReportJoinPrivateGroupOptions): void => {
  rudderStackTrack("fe_session_group_video", {
    session_id: sessionId, // This special snowflake gets sent as a number.
    user_id: userId.toString(),
    company_id: companyId?.toString(),
  });
};

interface ReportHumanApiDeviceConnectedOptions {
  userId: number;
}
/**
 * Report devices connected through human api.
 */
const reportHumanApiDeviceConnected = ({
  userId,
}: ReportHumanApiDeviceConnectedOptions) => {
  rudderStackTrack("fe_connect_device", {
    user_id: userId.toString(),
  });
};

/**
 * Track page views in GA.
 */
const reportPageView = (pathname: string): void => {
  if (isProduction() && process.env.REACT_APP_GA_TRACKING_CODE) {
    // Create tracker
    googleAnalytics.initialize(process.env.REACT_APP_GA_TRACKING_CODE, {
      gaOptions: { siteSpeedSampleRate: 100 },
    });
    googleAnalytics.set({ page: pathname });
    googleAnalytics.pageview(pathname);
    rudderStack.page();
  }
};

/**
 * Map a user to an anonymous ID in Rudder Stack.
 *
 * Not currently used as this is called in the legacy app on login.
 * Apparently this works because the rudder-stack package saves the
 * ID to local storage.
 *
 * Keeping this method around for when we move login to the Next app.
 */
const reportUser = (userId: number, companyId?: number): void => {
  if (isProduction()) {
    rudderStack.identify(userId.toString(), {
      company_id: companyId && companyId.toString(),
    });
  }
};

const useAnalytics = () => ({
  reportRSVP,
  reportAttended,
  reportChallengeRSVP,
  reportAddPoints,
  reportOnDemandView,
  reportOnDemandPlay,
  reportJoinPrivateGroup,
  reportHumanApiDeviceConnected,
  reportPageView,
  reportUser,
});

export default useAnalytics;
