/**
 *
 *
 * Tests for useLocalStorage
 *
 *
 */

import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import useLocalStorage from "./useLocalStorage";

describe("useLocalStorage", () => {
  beforeEach(() => {
    Object.defineProperty(window, "localStorage", {
      value: {
        getItem: jest.fn(() => JSON.stringify("Item")),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null),
      },
      writable: true,
    });
  });

  it("useLocalStorage should get item of localstorage", async () => {
    const key = "key";
    const El = () => {
      const { getItem } = useLocalStorage();
      const item = getItem<string>(key);
      return <div data-testid="data">{item}</div>;
    };
    const { getByTestId } = render(<El />);
    await waitFor(() => {
      expect(window.localStorage.getItem).toHaveBeenLastCalledWith(
        `tenspot_${key}`
      );
      expect(getByTestId("data")).toHaveTextContent("Item");
    });
  });

  it("useLocalStorage should set item in localstorage", async () => {
    const key = "key";
    const El = () => {
      const { setItem } = useLocalStorage();
      return <button onClick={() => setItem(key, "Item")}>Click</button>;
    };
    const { getByRole } = render(<El />);
    await waitFor(() => fireEvent.click(getByRole("button")));
    await waitFor(() => {
      expect(window.localStorage.setItem).toHaveBeenLastCalledWith(
        `tenspot_${key}`,
        JSON.stringify("Item")
      );
    });
  });
  it("useLocalStorage should remove item in localstorage", async () => {
    const key = "key";
    const El = () => {
      const { removeItem } = useLocalStorage();
      return <button onClick={() => removeItem(key)}>Click</button>;
    };
    const { getByRole } = render(<El />);
    await waitFor(() => fireEvent.click(getByRole("button")));
    await waitFor(() => {
      expect(window.localStorage.removeItem).toHaveBeenLastCalledWith(
        `tenspot_${key}`
      );
    });
  });
});
