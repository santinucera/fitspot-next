/**
 *
 *
 * useCategorizedTrainerSessions
 *
 *
 */
import {
  useQueryTrainersSessionsGetData,
  TrainerSessionModel,
  SessionStatus,
} from "services/SessionService";
import dayjs from "utils/days";

export type CategorizedTrainerSessions = {
  inProgress: TrainerSessionModel[];
  pending: TrainerSessionModel[];
  upcoming: TrainerSessionModel[];
};

const useCategorizedTrainerSessions = () => {
  const sessions = useQueryTrainersSessionsGetData();

  const sortByDateAsc = (a: TrainerSessionModel, b: TrainerSessionModel) =>
    a.startDate.unix() - b.startDate.unix();
  const now = dayjs();
  const reducer = (
    acc: CategorizedTrainerSessions,
    session: TrainerSessionModel
  ) => {
    const { startDate, status } = session;
    switch (status) {
      case SessionStatus.inProgress:
        acc.inProgress.push(session);
        break;
      case SessionStatus.new: // concat with pending
      case SessionStatus.pending:
        // Don't include sessions that have already started
        if (startDate.isAfter(now)) acc.pending.push(session);
        break;
      case SessionStatus.accepted:
        acc.upcoming.push(session);
        break;
      default:
        break;
    }

    return acc;
  };
  const accumulator: CategorizedTrainerSessions = {
    inProgress: [],
    pending: [],
    upcoming: [],
  };

  return sessions
    ? sessions.sort(sortByDateAsc).reduce(reducer, accumulator)
    : accumulator;
};

export default useCategorizedTrainerSessions;
