/**
 *
 *
 * Tests for useToast
 *
 *
 */
import React, { useEffect } from "react";
import {
  fireEvent,
  waitFor,
  render,
  waitForElementToBeRemoved,
  act,
} from "@testing-library/react";
import useToast, {
  ToastProvider,
  addToastAction,
  removeToastAction,
  Toast,
  State,
  reducer,
} from "./useToast";

describe("reducer", () => {
  describe("ADD_TOAST", () => {
    const toast: Toast = {
      id: 1,
      title: "some title",
      description: "some description",
      type: "error",
    };
    const state: State = {
      toasts: [],
    };
    it("should add a toast", () => {
      expect(reducer(state, addToastAction(toast))).toEqual({
        toasts: [toast],
      });
    });
  });

  describe("REMOVE_TOAST", () => {
    it("should remove a toast", () => {
      const state: State = {
        toasts: [
          {
            id: 1,
            title: "some title",
            description: "some description",
            type: "error",
          },
        ],
      };
      expect(reducer(state, removeToastAction(1))).toEqual({ toasts: [] });
    });
  });
});

describe("components", () => {
  afterAll(() => {
    jest.clearAllTimers();
  });

  it("should add and remove toasts", async () => {
    const title1 = "first title";
    const description1 = "first description";
    const title2 = "second title";
    const description2 = "second description";
    const El = () => {
      const { addToast } = useToast();
      useEffect(() => {
        addToast({
          title: title1,
          description: description1,
          type: "error",
        });
        addToast({
          title: title2,
          description: description2,
          type: "error",
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return <div></div>;
    };

    const { getAllByTestId, getByText, queryByTestId } = render(
      <ToastProvider>
        <El />
      </ToastProvider>
    );

    const toasts = await waitFor(() => getAllByTestId(/toast-item/));
    expect(toasts.length).toBe(2);
    expect(getByText(title1)).toBeInTheDocument();
    expect(getByText(description1)).toBeInTheDocument();
    expect(getByText(title2)).toBeInTheDocument();
    expect(getByText(description2)).toBeInTheDocument();
    const closeButtons = getAllByTestId("remove-toast-button");
    fireEvent.click(closeButtons[0]);
    await waitFor(() => expect(getAllByTestId(/toast-item/)).toHaveLength(1));
    fireEvent.click(closeButtons[1]);
    await waitFor(() => expect(queryByTestId(/toast-item/)).toBeNull());
  });

  it("should remove the toast automatically after 4 seconds", async () => {
    jest.useFakeTimers();
    const El = () => {
      const { addToast } = useToast();
      useEffect(() => {
        addToast({
          title: "some title",
          description: "some description",
          type: "error",
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return <div></div>;
    };
    const { getByTestId, queryByTestId } = render(
      <ToastProvider>
        <El />
      </ToastProvider>
    );

    await waitFor(() => getByTestId(/toast-item/));
    act(() => {
      jest.advanceTimersByTime(4000);
    });
    await waitForElementToBeRemoved(() => queryByTestId(/toast-item/));
  });
});
