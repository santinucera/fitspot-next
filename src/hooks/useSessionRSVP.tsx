/**
 *
 *
 * useSessionRsvp
 *
 *
 */

import React from "react";
import useToast from "hooks/useToast";
import { queryCache } from "react-query";
import {
  createParticipant,
  SessionModel,
  useMutationSessionRsvpPostService,
} from "services/SessionService";
import { useQueryUserGetData } from "services/UserService";
import useAnalytics from "hooks/useAnalytics";
import { useModal } from "containers/Modal/Modal";
import WatchTogetherModal from "containers/WatchTogetherModal";
import CancelReservationModal from "components/CancelReservationModal";

export interface SessionRsvpParams {
  urlParams: {
    sessionId: string;
  };
  isRSVP: boolean;
}

const useSessionRsvp = (params: SessionRsvpParams) => {
  const { isRSVP, urlParams } = params;
  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const { reportRSVP } = useAnalytics();

  /**
   * Gets the user from the cache.
   */
  const user = useQueryUserGetData();

  const sessionQueryKey = ["session", urlParams];

  const openSessionModal = () => {
    const session = queryCache.getQueryData<SessionModel>(sessionQueryKey);
    if (!session) return;
    setModal(
      <WatchTogetherModal
        type="session"
        handleClose={removeModal}
        id={session.id}
        title={session.name}
        description={session.description}
        startDate={session.startDate}
        endDate={session.endDate}
      />
    );
  };

  const handleSessionCancelRSVP = (sessionId: number) => {
    const session = queryCache.getQueryData<SessionModel>(sessionQueryKey);

    if (!session) throw Error(`a session does not exist with id: ${sessionId}`);

    setModal(
      <CancelReservationModal
        name={session.name}
        startDate={session.startDate}
        endDate={session.endDate}
        handleClose={removeModal}
        confirmCancellation={() => {
          handleSessionRSVP({ sessionId, isRSVP: false });
          removeModal();
        }}
      />
    );
  };

  const [handleSessionRSVP] = useMutationSessionRsvpPostService({
    mutationConfig: {
      // Optimistic update
      onMutate: () => {
        const previousSession = queryCache.getQueryData<SessionModel>(
          sessionQueryKey
        );
        if (previousSession && user) {
          // Cancel any in-progress session queries
          queryCache.cancelQueries(sessionQueryKey);
          // Save the current session
          // Set the RSVP status of the participant
          queryCache.setQueryData<SessionModel>(
            sessionQueryKey,
            (): SessionModel => {
              // If the user is RSVP-ed, remove her from the participants list
              if (isRSVP) {
                return {
                  ...previousSession,
                  participants: previousSession.participants.filter(
                    (participant) => participant.userId !== user?.id
                  ),
                };
              } else {
                // Otherwise add a participant
                return {
                  ...previousSession,
                  participants: [
                    ...previousSession.participants,
                    createParticipant(user),
                  ],
                };
              }
            }
          );
        }
        // Rollback - set session to previous value
        return () => queryCache.setQueryData(sessionQueryKey, previousSession);
      },
      onError: (_, params, rollback) => {
        const description = params.isRSVP
          ? "Failed to RSVP to this session, please try again."
          : "Failed to cancel the reservation to this session, please try again.";
        addToast({
          title: "Error!",
          description,
          type: "error",
        });
        rollback();
      },
      onSuccess: (__, params) => {
        const session = queryCache.getQueryData<SessionModel>(sessionQueryKey);

        if (params.isRSVP && session) {
          // Show add to calendar modal if this is an RSVP
          openSessionModal();
        } else {
          // Otherwise show an un-RSVP toast
          addToast({
            title: "Success!",
            description: "Reservation cancelled.",
            type: "success",
          });
        }
        if (user && session) {
          // Report the event to analytics
          reportRSVP({
            isRSVP: params.isRSVP,
            userId: user.id,
            sessionId: session.id,
            companyId: session.companyId,
            category: session.activityCategory,
          });
        }
      },
      onSettled: () => {
        queryCache.invalidateQueries(sessionQueryKey);
      },
    },
  });

  return { handleSessionRSVP, handleSessionCancelRSVP, openSessionModal };
};

export default useSessionRsvp;
