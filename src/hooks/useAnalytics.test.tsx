/**
 *
 *
 * Tests for useAnalytics
 *
 *
 */
import React, { useEffect } from "react";
import { render } from "@testing-library/react";
import googleAnalytics, { reportGAEvent } from "utils/google-analytics";
import rudderStack, { rudderStackTrack } from "utils/rudder-stack";
import useAnalytics from "./useAnalytics";

jest.mock("utils/google-analytics");
jest.mock("utils/rudder-stack");

const methods = {
  reportGAEvent,
  rudderStackTrack,
};
const reportGAEventSpy = jest.spyOn(methods, "reportGAEvent");
const rudderStackTrackSpy = jest.spyOn(methods, "rudderStackTrack");

afterEach(() => {
  jest.resetAllMocks();
});

describe("reportRSVP", () => {
  it("should report RSVP", () => {
    const isRSVP = true;
    const userId = 123;
    const sessionId = 456;
    const companyId = 789;
    const category = "some category";

    const El = () => {
      const { reportRSVP } = useAnalytics();
      useEffect(() => {
        reportRSVP({
          isRSVP,
          userId,
          sessionId,
          companyId,
          category,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(reportGAEventSpy).toHaveBeenCalledWith({
      category: "rsvpd_session",
      action: "rsvpd",
      label: sessionId.toString(),
      sessionId,
      companyId,
      activityCategory: category,
    });
    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_rsvpd_session", {
      user_id: userId.toString(),
      session_id: sessionId.toString(),
      rsvp: isRSVP,
    });
  });
});

describe("reportAttended", () => {
  it("should report attended session", () => {
    const userId = 123;
    const sessionId = 456;
    const companyId = 789;
    const category = "some category";

    const El = () => {
      const { reportAttended } = useAnalytics();
      useEffect(() => {
        reportAttended({
          userId,
          sessionId,
          companyId,
          category,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(reportGAEventSpy).toHaveBeenCalledWith({
      category: "attended_session",
      action: "attended",
      label: sessionId.toString(),
      sessionId,
      companyId,
      activityCategory: category,
    });
    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_joined_session", {
      user_id: userId.toString(),
      session_id: sessionId.toString(),
      attended: true,
    });
  });
});

describe("reportChallengeRSVP", () => {
  it("should report challenge RSVP", () => {
    const userId = 123;
    const challengeId = 456;

    const El = () => {
      const { reportChallengeRSVP } = useAnalytics();
      useEffect(() => {
        reportChallengeRSVP({
          userId,
          challengeId,
          isRSVP: true,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_joined_challenge", {
      user_id: userId.toString(),
      challenge_id: challengeId.toString(),
      joined_success: true,
    });
  });
});

describe("reportAddPoints", () => {
  it("should report add points", () => {
    const userId = 123;
    const challengeId = 456;

    const El = () => {
      const { reportAddPoints } = useAnalytics();
      useEffect(() => {
        reportAddPoints({
          userId,
          challengeId,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_challenge_data", {
      user_id: userId.toString(),
      challenge_id: challengeId.toString(),
      activity_data_success: true,
    });
  });
});

describe("reportOnDemandView", () => {
  it("should report add points", () => {
    const userId = 123;
    const videoId = 456;

    const El = () => {
      const { reportOnDemandView } = useAnalytics();
      useEffect(() => {
        reportOnDemandView({
          userId,
          videoId,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_viewed_ondemand", {
      user_id: userId.toString(),
      sessions_id: videoId.toString(),
    });
  });
});

describe("reportOnDemandPlay", () => {
  it("should report add points", () => {
    const userId = 123;
    const videoId = 456;

    const El = () => {
      const { reportOnDemandPlay } = useAnalytics();
      useEffect(() => {
        reportOnDemandPlay({
          userId,
          videoId,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_played_ondemand", {
      user_id: userId.toString(),
      sessions_id: videoId.toString(),
    });
  });
});

describe("reportJoinPrivateGroup", () => {
  it("should report add points", () => {
    const userId = 123;
    const sessionId = 456;
    const companyId = 789;

    const El = () => {
      const { reportJoinPrivateGroup } = useAnalytics();
      useEffect(() => {
        reportJoinPrivateGroup({
          userId,
          sessionId,
          companyId,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_session_group_video", {
      user_id: userId.toString(),
      session_id: sessionId,
      company_id: companyId.toString(),
    });
  });
});

describe("reportHumanApiDeviceConnected", () => {
  it("should report add points", () => {
    const userId = 123;

    const El = () => {
      const { reportHumanApiDeviceConnected } = useAnalytics();
      useEffect(() => {
        reportHumanApiDeviceConnected({
          userId,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(rudderStackTrackSpy).toHaveBeenCalledWith("fe_connect_device", {
      user_id: userId.toString(),
    });
  });
});

describe("reportPageView", () => {
  it("should report page views to GA and Rudder Stack", () => {
    const gaSetSpy = jest.spyOn(googleAnalytics, "set");
    const gaPageviewSpy = jest.spyOn(googleAnalytics, "pageview");
    const rudderStackPageSpy = jest.spyOn(rudderStack, "page");
    const pathname = "/some/path";

    const El = () => {
      const { reportPageView } = useAnalytics();
      useEffect(() => {
        reportPageView(pathname);
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(gaSetSpy).toHaveBeenCalledWith({ page: pathname });
    expect(gaPageviewSpy).toHaveBeenCalledWith(pathname);
    expect(rudderStackPageSpy).toHaveBeenCalled();
  });
});

describe("reportUser", () => {
  it("should report user", () => {
    const identifySpy = jest.spyOn(rudderStack, "identify");
    const userId = 123;
    const companyId = 456;

    const El = () => {
      const { reportUser } = useAnalytics();
      useEffect(() => {
        reportUser(userId, companyId);
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, []);
      return null;
    };

    render(<El />);

    expect(identifySpy).toHaveBeenCalledWith(userId.toString(), {
      company_id: companyId.toString(),
    });
  });
});
