/**
 *
 *
 * useToast
 *
 *
 */
/** @jsx jsx */
import { jsx } from "theme-ui";
import {
  FunctionComponent,
  useReducer,
  createContext,
  useContext,
} from "react";
import ToastComponent from "components/Toast";

/**
 * ############################################################################
 *
 * State
 *
 * ############################################################################
 */
export interface Toast {
  id: number;
  title: string;
  description: string;
  type: "error" | "success";
}

export interface State {
  readonly toasts: Toast[];
}
export const initialState: State = {
  toasts: [],
};

/**
 * ############################################################################
 *
 * Actions
 *
 * ############################################################################
 */
export enum ActionTypes {
  ADD_TOAST = "ADD_TOAST",
  REMOVE_TOAST = "REMOVE_TOAST",
}

interface AddToastAction {
  type: ActionTypes.ADD_TOAST;
  toast: Toast;
}
export const addToastAction = (toast: Toast): AddToastAction => ({
  type: ActionTypes.ADD_TOAST,
  toast,
});

interface RemoveToastAction {
  type: ActionTypes.REMOVE_TOAST;
  id: number;
}
export const removeToastAction = (id: number): RemoveToastAction => ({
  type: ActionTypes.REMOVE_TOAST,
  id,
});

type Action = AddToastAction | RemoveToastAction;

/**
 * ############################################################################
 *
 * Reducer
 *
 * ############################################################################
 */
export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.ADD_TOAST:
      return { toasts: [...state.toasts, action.toast] };
    case ActionTypes.REMOVE_TOAST:
      return { toasts: state.toasts.filter((toast) => toast.id !== action.id) };
    // No default
  }
};

/**
 * ############################################################################
 *
 * Context
 *
 * ############################################################################
 */

export type AddToastParams = Omit<Toast, "id">;

/**
 * Add a toast.
 *
 * Toasts automatically disappear after 4 seconds.
 */
export type AddToast = (toast: AddToastParams) => void;

/**
 * Remove a toast.
 */
export type RemoveToast = (id: number) => void;

interface ContextValue {
  toasts: Toast[];
  addToast: AddToast;
  removeToast: RemoveToast;
}

const initialContext: ContextValue = {
  toasts: [],
  addToast: () => {}, // noop default callback
  removeToast: () => {}, // noop default callback
};

const ToastContext = createContext(initialContext);

const useToast = () => useContext(ToastContext);

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
export const ToastProvider: FunctionComponent = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const removeToast: RemoveToast = (id) => {
    dispatch(removeToastAction(id));
  };

  const addToast: AddToast = (toast) => {
    const id = Math.floor(Math.random() * 10000);
    dispatch(addToastAction({ ...toast, id }));
  };

  return (
    <ToastContext.Provider
      value={{ toasts: state.toasts, addToast, removeToast }}
    >
      <ToastComponent />
      {children}
    </ToastContext.Provider>
  );
};

export default useToast;
