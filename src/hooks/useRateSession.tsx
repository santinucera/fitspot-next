/**
 *
 *
 * useRateSession
 *
 *
 */
/** @jsx jsx */
import { jsx } from "theme-ui";
import {
  SessionsGetQueryParams,
  SessionsModel,
  useMutationRateSessionPostService,
  useQuerySessionsGetNonRatedService,
} from "services/SessionService";
import { queryCache } from "react-query";
import useToast from "hooks/useToast";
import { useModal } from "containers/Modal/Modal";
import RateSessionsModal from "containers/RateSessionsModal";

export interface RateSessionParams {
  attendedSessions?: SessionsModel;
  sessionsQueryParams?: SessionsGetQueryParams;
}

const useRateSession = ({
  attendedSessions,
  sessionsQueryParams,
}: RateSessionParams) => {
  const { addToast } = useToast();
  const { setModal, removeModal } = useModal();
  const { data: nonRatedSessions } = useQuerySessionsGetNonRatedService();

  const [rateSession] = useMutationRateSessionPostService({
    mutationConfig: {
      onMutate: ({ sessionId, rating: { overall } }) => {
        // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
        queryCache.cancelQueries(["non-rated-sessions"]);
        if (attendedSessions) {
          queryCache.cancelQueries(["sessions", sessionsQueryParams]);
        }

        // Snapshot the previous value
        const previousUnratedSessions = queryCache.getQueryData([
          "non-rated-sessions",
        ]);

        const previousSessions = queryCache.getQueryData([
          "sessions",
          sessionsQueryParams,
        ]);

        // Optimistically update to the new value
        queryCache.setQueryData(
          ["non-rated-sessions"],
          nonRatedSessions?.filter((session) => session.id !== sessionId)
        );
        if (attendedSessions) {
          queryCache.setQueryData(
            ["sessions", sessionsQueryParams],
            attendedSessions.map((attendedSession) =>
              attendedSession.id === sessionId
                ? {
                    ...attendedSession,
                    rating: overall,
                    userSessionRating: {
                      ...attendedSession.userSessionRating,
                      ratingPoints: overall,
                    },
                  }
                : attendedSession
            )
          );
        }

        // Return a rollback function
        return () => {
          queryCache.setQueryData(
            ["non-rated-sessions"],
            previousUnratedSessions
          );
          if (attendedSessions) {
            queryCache.setQueryData(
              ["sessions", sessionsQueryParams],
              previousSessions
            );
          }
        };
      },
      onSuccess: (_, { sessionId, rating }) => {
        const session =
          attendedSessions?.find((session) => session.id === sessionId) ||
          nonRatedSessions?.find((session) => session.id === sessionId);

        if (session)
          setModal(
            <RateSessionsModal
              ratings={{
                // TODO- get these somewhere
                ratingPoints: 1,
                ratingStreamQuality: 1,
                ratingTrainer: 1,
                ratingLocation: 1,
              }}
              session={session}
              mutationConfig={{
                onSuccess: () => {
                  addToast({
                    title: "Success!",
                    description: "You successfully rated the session.",
                    type: "success",
                  });
                  removeModal();
                  queryCache.invalidateQueries(["sessions"]);
                },
                onError: () => {
                  addToast({
                    title: "Error!",
                    description: "An error occurred, please try again.",
                    type: "error",
                  });
                },
              }}
            />
          );
        addToast({
          title: "Success!",
          description: "You successfully rated the session.",
          type: "success",
        });
      },
      onError: (err, session, rollback) => {
        rollback();
        addToast({
          title: "Error!",
          description: "An error occurred, please try again.",
          type: "error",
        });
      },
      onSettled: () => {
        queryCache.invalidateQueries(["non-rated-sessions"]);
        if (attendedSessions) {
          queryCache.invalidateQueries(["sessions", sessionsQueryParams]);
        }
      },
    },
  });
  return { rateSession, nonRatedSessions };
};

export default useRateSession;
