/**
 *
 *
 * useLocalStorage
 *
 *
 */

export interface LocalStorageParams {}

const useLocalStorage = () => {
  const getItem: <T>(key: string) => T | null = (key: string) => {
    const item = localStorage.getItem(`tenspot_${key}`);
    return item ? JSON.parse(item) : null;
  };
  const setItem = (key: string, value: any) =>
    localStorage.setItem(`tenspot_${key}`, JSON.stringify(value));
  const removeItem = (key: string) => localStorage.removeItem(`tenspot_${key}`);
  return { getItem, setItem, removeItem };
};

export default useLocalStorage;
