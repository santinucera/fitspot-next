/**
 *
 *
 * useHumanApiModal
 *
 *
 */

import { useEffect } from "react";
import { queryCache } from "react-query";
import { useQueryHumanApiGetTokenService } from "services/HumanApiService";

import { useQueryUserGetData } from "services/UserService";
import useAnalytics from "hooks/useAnalytics";

export interface HumanApiModalParams {}

const useHumanApiModal = () => {
  const user = useQueryUserGetData();
  const isHumanApiConnected = user?.humanApiConnected;
  const { data: humanApiData } = useQueryHumanApiGetTokenService();
  const { reportHumanApiDeviceConnected } = useAnalytics();

  /**
   * This is needed for now until we rebuild the human api connection.
   * The first time the useQueryHumanApiGetTokenService gets called
   * the user should be connected to the human api via an `session token`.
   * After the user is created an `id token` is return.
   */
  useEffect(() => {
    if (isHumanApiConnected || !humanApiData?.token) return;
    /**
     * Re-fetches the user and steps leader to show users connected in the
     * step leaderboard
     */
    queryCache.refetchQueries("user");
    queryCache.refetchQueries(["step-leaders"]);
  }, [isHumanApiConnected, humanApiData]);

  useEffect(() => {
    if (!humanApiData?.token) return;

    // this is a hack to get the human api to work.
    const event = document.createEvent("Event");
    event.initEvent("load", true, true);
    window.dispatchEvent(event);

    /**
     * When a user connects a device log the event
     */
    window.HumanConnect.on("connect", () => {
      if (user?.id) {
        reportHumanApiDeviceConnected({
          userId: user.id,
        });
      }
    });
  }, [humanApiData, reportHumanApiDeviceConnected, user]);

  return { token: humanApiData?.token };
};

export default useHumanApiModal;
