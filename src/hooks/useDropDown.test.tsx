/**
 *
 *
 * Test for useDropDown
 *
 *
 */

import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import useDropDown from "./useDropDown";

describe("<DropDownDrawer />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      useDropDown({
        options: [],
        currentValue: undefined,
        onChange: () => {},
      });
      return <div></div>;
    };
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should show and hide a DOM element", async () => {
    const El = () => {
      const { isOpen, open, close, toggle } = useDropDown({
        options: [],
        currentValue: undefined,
        onChange: () => {},
      });
      return (
        <div>
          <div
            onClick={() => {
              open();
            }}
          >
            open
          </div>
          <div
            onClick={() => {
              close();
            }}
          >
            close
          </div>
          <div
            onClick={() => {
              toggle();
            }}
          >
            toggle
          </div>
          {isOpen && <div>element</div>}
        </div>
      );
    };
    render(<El />);
    expect(screen.queryByText("element")).not.toBeInTheDocument();
    fireEvent.click(screen.getByText("open"));
    expect(screen.getByText("element")).toBeInTheDocument();
    fireEvent.click(screen.getByText("close"));
    expect(screen.queryByText("element")).not.toBeInTheDocument();
    fireEvent.click(screen.getByText("toggle"));
    expect(screen.queryByText("element")).toBeInTheDocument();
    fireEvent.click(screen.getByText("toggle"));
    expect(screen.queryByText("element")).not.toBeInTheDocument();
  });

  it("should select an option and handle onChange", async () => {
    const options = [
      { title: "one", value: 1 },
      { title: "two", value: 2 },
    ];

    const onChange = jest.fn();

    const El = () => {
      const { currentOption, setSelected } = useDropDown({
        options,
        currentValue: undefined,
        onChange,
      });
      return (
        <>
          <div data-testid="current">{JSON.stringify(currentOption)}</div>
          <div>
            {options.map(({ title, value }) => (
              <div key={value} onClick={() => setSelected({ title, value })}>
                {title}
              </div>
            ))}
          </div>
        </>
      );
    };
    render(<El />);
    expect(screen.getByTestId("current")).toHaveTextContent("");
    fireEvent.click(screen.getByText("one"));
    expect(screen.getByTestId("current")).toHaveTextContent(
      JSON.stringify(options[0])
    );
    expect(onChange).toBeCalled();
  });
});
