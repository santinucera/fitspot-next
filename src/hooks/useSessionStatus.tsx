/**
 *
 *
 * useSessionStatus
 *
 *
 */
import { Dayjs } from "dayjs";
import { useEffect, useRef, useState } from "react";
import dayjs from "utils/days";

export interface SessionStatusParams {
  startDate?: Dayjs;
  endDate?: Dayjs;
}

const useSessionStatus = ({ startDate, endDate }: SessionStatusParams) => {
  const timeoutId = useRef<NodeJS.Timeout | null>(null);
  const [sessionStatus, setSessionStatus] = useState({
    isSessionUpcoming: false,
    isSessionLive: false,
    isSessionStartingSoon: false,
    isSessionCompleted: false,
  });

  useEffect(() => {
    /**
     * If a session isn't dates do nothing with the state.
     */
    if (!startDate || !endDate) return;

    const checkSession = () => {
      const isSessionUpcoming = dayjs().isBefore(
        startDate.subtract(15, "minute")
      );

      const isSessionLive = dayjs().isBetween(startDate, endDate);

      const isSessionStartingSoon = dayjs().isBetween(
        startDate,
        startDate.subtract(15, "minute")
      );

      const isSessionCompleted = dayjs().isAfter(endDate);

      if (
        isSessionLive !== sessionStatus.isSessionLive ||
        isSessionStartingSoon !== sessionStatus.isSessionStartingSoon ||
        isSessionCompleted !== sessionStatus.isSessionCompleted ||
        isSessionUpcoming !== sessionStatus.isSessionUpcoming
      ) {
        setSessionStatus({
          isSessionUpcoming,
          isSessionStartingSoon,
          isSessionLive,
          isSessionCompleted,
        });
      }
    };

    // run first time through
    checkSession();
    // start the interval and check the status every 10 seconds.
    timeoutId.current = setInterval(checkSession, 10000);

    return () => {
      if (!timeoutId.current) return;
      clearInterval(timeoutId.current);
    };
  }, [startDate, endDate, sessionStatus]);

  return sessionStatus;
};

export default useSessionStatus;
