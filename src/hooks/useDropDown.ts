/**
 *
 *
 * useDropDown
 *
 *
 */

import { useEffect, useReducer } from "react";

export interface UseDropDownOption {
  title: string;
  value: number | string;
}

interface UseDropDownProps {
  options: UseDropDownOption[];
  currentValue?: any;
  onChange: React.Dispatch<React.SetStateAction<any>>;
}

interface OpenAction {
  type: ActionTypes.OPEN;
}

interface CloseAction {
  type: ActionTypes.CLOSE;
}

interface ToggleAction {
  type: ActionTypes.TOGGLE;
}

interface SetSelectedAction {
  type: ActionTypes.SET_SELECTED;
  option: UseDropDownOption;
}

enum ActionTypes {
  OPEN,
  CLOSE,
  TOGGLE,
  SET_SELECTED,
}

type Action = OpenAction | CloseAction | ToggleAction | SetSelectedAction;

interface Actions {
  close: () => void;
  open: () => void;
  toggle: () => void;
  setSelected: (option: UseDropDownOption) => void;
}

interface State {
  isOpen: boolean;
  currentOption?: UseDropDownOption;
}

const dropDownReducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.OPEN:
      return { ...state, isOpen: true };
    case ActionTypes.CLOSE:
      return { ...state, isOpen: false };
    case ActionTypes.TOGGLE:
      return { ...state, isOpen: !state.isOpen };
    case ActionTypes.SET_SELECTED:
      return { ...state, isOpen: false, currentOption: action.option };
  }
};

const useDropDown = ({ options, currentValue, onChange }: UseDropDownProps) => {
  const currentOption = options.find(({ value }) => value === currentValue);

  const [state, dispatch] = useReducer(dropDownReducer, {
    isOpen: false,
    currentOption,
  });

  useEffect(() => {
    if (state.currentOption?.value === currentValue) return;
    onChange(state.currentOption?.value);
  }, [onChange, state.currentOption, currentValue]);

  const dispatchers: Actions = {
    close: () => dispatch({ type: ActionTypes.CLOSE }),
    open: () => dispatch({ type: ActionTypes.OPEN }),
    toggle: () => dispatch({ type: ActionTypes.TOGGLE }),
    setSelected: (option) =>
      dispatch({ type: ActionTypes.SET_SELECTED, option: option }),
  };

  return { ...state, ...dispatchers };
};

export default useDropDown;
