/**
 *
 *
 * useChallengeRSVP
 *
 *
 */

import { queryCache } from "react-query";
import useToast, { AddToastParams } from "hooks/useToast";
import { useQueryUserGetData } from "services/UserService";
import useAnalytics from "hooks/useAnalytics";
import {
  ChallengeModel,
  useMutationChallengeRsvpPostService,
} from "services/ChallengeService";

const useChallengeRSVP = (challengesCacheKey: string) => {
  const { addToast } = useToast();
  const { reportChallengeRSVP } = useAnalytics();
  /**
   * Gets the user from the cache.
   */
  const user = useQueryUserGetData();

  /**
   * `handleChallengeRSVP` is the mutation function reserves a challenge
   */
  const [handleChallengeRSVP] = useMutationChallengeRsvpPostService({
    mutationConfig: {
      onMutate: (variables) => {
        queryCache.cancelQueries(challengesCacheKey);
        const previousChallenges = queryCache.getQueryData<ChallengeModel[]>(
          challengesCacheKey
        );
        queryCache.setQueryData<ChallengeModel[]>(
          challengesCacheKey,
          (oldChallenges): ChallengeModel[] => {
            let updatedChallenge: ChallengeModel[];
            if (oldChallenges?.length) {
              updatedChallenge = oldChallenges.map((challenge) => {
                if (challenge.id === variables.challengeId) {
                  return {
                    ...challenge,
                    isJoined: Boolean(variables.isRSVP),
                  };
                }
                return challenge;
              });
            } else {
              updatedChallenge = [];
            }
            return updatedChallenge;
          }
        );
        // Rollback - set challenge back to previous value
        return () =>
          queryCache.setQueryData(challengesCacheKey, previousChallenges);
      },
      onSuccess: (response, { challengeId, isRSVP }) => {
        const challenges = queryCache.getQueryData<ChallengeModel[]>(
          challengesCacheKey
        );
        const challenge = challenges?.find(
          (challenge) => challenge.id === challengeId
        );
        let toastParams: AddToastParams;

        if (isRSVP) {
          const startDateFormatted = challenge?.startDate.format(
            "ddd D, h:mma"
          );
          const endDateFormatted = challenge?.endDate.format("h:mma");
          toastParams = {
            title: "Challenge Joined",
            description: `${challenge?.name} - ${startDateFormatted} - ${endDateFormatted}`,
            type: "success",
          };
        } else {
          toastParams = {
            title: "Challenge Canceled",
            description: `${challenge?.name} challenge canceled`,
            type: "success",
          };
        }

        addToast(toastParams);

        if (user && challenge) {
          reportChallengeRSVP({
            userId: user.id,
            challengeId: challenge.id,
            isRSVP,
          });
        }
      },
      onError: (_, __, rollback) => {
        rollback();
        addToast({
          title: "Error!",
          description: "An error occurred, please try again.",
          type: "error",
        });
      },
      onSettled: () => {
        queryCache.invalidateQueries(challengesCacheKey);
      },
    },
  });

  return { handleChallengeRSVP };
};

export default useChallengeRSVP;
