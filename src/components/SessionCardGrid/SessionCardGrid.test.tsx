/**
 *
 *
 * Tests for <SessionCardGrid />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import SessionCardGrid from "./SessionCardGrid";
import { createCustomerDashboardSessionModel } from "test-utils/session-service-test-utils";
import { CustomerDashboardSessionModel } from "services/SessionService";

describe("<SessionCardGrid />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SessionCardGrid sessions={[]} cardActions={() => <div></div>} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render SessionCard", () => {
    const session: CustomerDashboardSessionModel = {
      ...createCustomerDashboardSessionModel(),
      name: "the name of the session",
    };
    const { getByText } = render(
      <SessionCardGrid sessions={[session]} cardActions={() => <div></div>} />
    );
    expect(getByText(session.name)).toBeInTheDocument();
  });
});
