/**
 *
 *
 * <SessionCardGrid />
 *
 *
 */

/** @jsx jsx */
import { jsx, Grid } from "theme-ui";
import { FunctionComponent } from "react";
import SessionCard from "components/SessionCard/SessionCard";
import { SessionsModel } from "services/SessionService";
import dayjs from "utils/days";

interface SessionCardGridProps {
  title?: string;
  sessions: SessionsModel;
  showSessionInfo?: boolean;
  cardActions: any;
}

const SessionCardGrid: FunctionComponent<SessionCardGridProps> = ({
  sessions,
  cardActions,
  showSessionInfo = true,
}) => {
  return (
    <Grid
      sx={{
        gap: 6,
        gridTemplateColumns: "repeat(auto-fit, 330px)",
        justifyContent: ["center", "center", "normal"],
      }}
    >
      {sessions.map(
        ({
          id,
          name,
          description,
          trainerName,
          trainerAvatar,
          participantsCount,
          userSessionRating,
          isRSVP,
          startDate,
          endDate,
        }) => {
          return (
            <SessionCard
              key={id}
              name={name}
              description={description}
              startDate={startDate}
              endDate={endDate}
              trainerName={trainerName}
              trainerAvatar={trainerAvatar}
              participantsCount={participantsCount}
              showSessionInfo={showSessionInfo}
              actions={cardActions({
                sessionId: id,
                userSessionRating,
                isRSVP,
                isActiveSession: dayjs().isBetween(startDate, endDate),
              })}
            />
          );
        }
      )}
    </Grid>
  );
};

export default SessionCardGrid;
