/**
 *
 *
 * <IconStarFilled />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconStarFilledProps {
  size?: number;
  className?: string;
}

const IconStarFilled: FunctionComponent<IconStarFilledProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <path
        d="M8.472 8.085l-7.588.348-.104.01C.005 8.565-.29 9.61.347 10.142l5.95 4.96-2.032 7.68-.022.103c-.132.808.742 1.424 1.424.951L12 19.442l6.333 4.393.086.054c.696.384 1.534-.285 1.316-1.107L17.703 15.1l5.95-4.959.078-.072c.556-.573.2-1.598-.615-1.636l-7.59-.348-2.66-7.46a.911.911 0 00-1.731 0l-2.663 7.46z"
        // transform="translate(2 1)"
        fill="currentColor"
      />
    </svg>
  );
};

IconStarFilled.defaultProps = {
  size: 24,
};

export default IconStarFilled;
