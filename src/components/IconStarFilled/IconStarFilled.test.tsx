/**
 *
 *
 * Tests for <IconStarFilled />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconStarFilled from "./IconStarFilled";

describe("<IconStarFilled />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconStarFilled />);
    expect(spy).not.toHaveBeenCalled();
  });
});
