/**
 *
 *
 * Tests for <Video />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Video from "./Video";

describe("<Video />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Video src="some string" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
