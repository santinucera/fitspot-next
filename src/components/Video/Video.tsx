/**
 *
 *
 * <Video />
 *
 *
 */

/** @jsx jsx */
import { jsx, Embed } from "theme-ui";
import { FunctionComponent } from "react";

interface VideoProps {
  src: string;
}

const Video: FunctionComponent<VideoProps> = ({ src }) => {
  return <Embed src={src} data-testid="video" />;
};

export default Video;
