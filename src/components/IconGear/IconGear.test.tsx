/**
 *
 *
 * Tests for <IconGear />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconGear from "./IconGear";

describe("<IconGear />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconGear />);
    expect(spy).not.toHaveBeenCalled();
  });
});
