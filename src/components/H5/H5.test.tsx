/**
 *
 *
 * Tests for <H5 />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import H5 from "./H5";

describe("<H5 />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<H5>foo</H5>);
    expect(spy).not.toHaveBeenCalled();
  });
});
