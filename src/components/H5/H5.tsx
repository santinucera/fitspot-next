/**
 *
 *
 * <H5 />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, HeadingProps } from "theme-ui";
import { FunctionComponent } from "react";

interface H4Props extends HeadingProps {
  light?: boolean;
}

const H4: FunctionComponent<H4Props> = ({ light, ...props }) => {
  return (
    <Heading
      as="h5"
      variant="text.h5"
      sx={{
        fontWeight: light ? "body" : "heading",
      }}
      {...props}
    />
  );
};

export default H4;
