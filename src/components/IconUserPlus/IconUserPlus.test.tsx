/**
 *
 *
 * Tests for <IconUserPlus />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconUserPlus from "./IconUserPlus";

describe("<IconUserPlus />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconUserPlus />);
    expect(spy).not.toHaveBeenCalled();
  });
});
