/**
 *
 *
 * Tests for <IconCopy />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconCopy from "./IconCopy";

describe("<IconCopy />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconCopy />);
    expect(spy).not.toHaveBeenCalled();
  });
});
