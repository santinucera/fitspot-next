/**
 *
 *
 * <IconCopy />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconCopyProps {
  size?: number;
  className?: string;
}

const IconCopy: FunctionComponent<IconCopyProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      data-testid="icon-copy"
      fill="currentColor"
    >
      <path d="M22.31 24H9a1.71 1.71 0 01-1.71-1.71v-4h1.6v4a.15.15 0 00.15.14h13.27a.15.15 0 00.15-.14V9a.15.15 0 00-.15-.15h-3.9V7.3h3.9A1.72 1.72 0 0124 9v13.29A1.72 1.72 0 0122.31 24z" />
      <path d="M15 16.7H1.71A1.72 1.72 0 010 15V1.71A1.72 1.72 0 011.71 0H15a1.72 1.72 0 011.7 1.71V15a1.72 1.72 0 01-1.7 1.7zm0-15.13H1.71a.14.14 0 00-.14.14V15a.15.15 0 00.14.15H15a.15.15 0 00.15-.15V1.71a.15.15 0 00-.15-.14z" />
    </svg>
  );
};

IconCopy.defaultProps = {
  size: 24,
};

export default IconCopy;
