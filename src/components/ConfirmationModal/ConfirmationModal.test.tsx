/**
 *
 *
 * Tests for <ConfirmationModal />
 *
 *
 */

import React, { useEffect } from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ConfirmationModal from "./ConfirmationModal";
import ModalProvider from "containers/Modal";
import { useModal } from "containers/Modal/Modal";

const { getByTestId } = screen;

describe("<ConfirmationModal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <ConfirmationModal
        title="Confirm"
        body="body copy"
        confirmationText="confirm"
        handleConfirmation={() => {}}
        cancelText="cancel"
        handleCancel={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should show loading indicator", () => {
    const El = () => {
      const { setModal } = useModal();
      useEffect(() => {
        setModal(
          <ConfirmationModal
            title="Confirm"
            handleConfirmation={() => {}}
            handleCancel={() => {}}
          />
        );
      }, [setModal]);
      return null;
    };
    render(
      <ModalProvider>
        <El />
      </ModalProvider>
    );
    const confirmButton = getByTestId("confirm-button");
    fireEvent.click(confirmButton);
    expect(confirmButton).toContainElement(getByTestId("loading-indicator"));
    expect(getByTestId("cancel-button")).toBeDisabled();
  });
});
