/**
 *
 *
 * <ConfirmationModal />
 *
 *
 */

/** @jsx jsx */
import { useState } from "react";
import { jsx, Box, Heading, Button, Text, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import LoadingButton from "components/LoadingButton";

interface ConfirmationModalProps {
  title: string;
  body?: string;
  confirmationText?: string;
  handleConfirmation: () => void;
  cancelText?: string;
  handleCancel: () => void;
}

const ConfirmationModal: FunctionComponent<ConfirmationModalProps> = ({
  title,
  body,
  confirmationText,
  handleConfirmation,
  cancelText,
  handleCancel,
}) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  return (
    <Box sx={{ p: 5, width: "100%" }} data-testid="confirmation-modal">
      <Flex
        sx={{
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          py: 6,
        }}
      >
        <Heading
          as="h2"
          sx={{
            fontSize: 3,
          }}
        >
          {title}
        </Heading>
        {body && (
          <Text
            as="p"
            dangerouslySetInnerHTML={{ __html: body }}
            sx={{ textAlign: "center", py: 4, fontWeight: "normal" }}
          />
        )}
      </Flex>

      <Flex sx={{ alignItems: "center", justifyContent: "center" }}>
        <LoadingButton
          variant="danger"
          sx={{ mr: 3 }}
          onClick={() => {
            setIsLoading(true);
            handleConfirmation();
          }}
          isLoading={Boolean(isLoading)}
          data-testid="confirm-button"
        >
          {confirmationText || "Confirm"}
        </LoadingButton>
        <Button
          variant="secondary"
          onClick={handleCancel}
          data-testid="cancel-button"
          disabled={isLoading}
        >
          {cancelText || "Cancel"}
        </Button>
      </Flex>
    </Box>
  );
};

export default ConfirmationModal;
