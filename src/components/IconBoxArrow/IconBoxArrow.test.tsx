/**
 *
 *
 * Tests for <IconBoxArrow ?>
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconBoxArrow from "./IconBoxArrow";

describe("<IconBoxArrow />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconBoxArrow />);
    expect(spy).not.toHaveBeenCalled();
  });
});
