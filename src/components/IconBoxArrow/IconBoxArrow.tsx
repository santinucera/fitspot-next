/**
 *
 *
 * <IconBoxArrow />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

enum Directions {
  up = "rotate(-180 40 40)",
  down = "rotate(-180 40 40)",
  left = "180deg",
  right = "0deg",
}

interface IconBoxArrowProps {
  direction?: "up" | "down" | "left" | "right";
}

const IconBoxArrow: FunctionComponent<IconBoxArrowProps> = ({
  direction = "right",
}) => {
  return (
    <svg width={40} height={40} viewBox="0 0 40 40" fill="none">
      <path
        opacity={0.05}
        transform="rotate(-180 40 40)"
        fill="#000037"
        d="M39.9998 40H79.9998V80H39.9998z"
      />
      <path
        sx={{
          transform: `rotate(${Directions[direction]})`,
          transformOrigin: "center",
        }}
        d="M17 26l6-6-6-6"
        stroke="#000037"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default IconBoxArrow;
