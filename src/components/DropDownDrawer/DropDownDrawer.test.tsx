/**
 *
 *
 * Tests for <DropdownDrawer />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import DropDownDrawer from "./DropDownDrawer";

describe("<DropDownDrawer />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <DropDownDrawer
        value={1}
        title="Choose one"
        options={[]}
        onChange={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Choose one";
    const { getByText } = render(
      <DropDownDrawer
        value={1}
        title="Choose one"
        options={[]}
        onChange={() => {}}
      />
    );
    const expected = new RegExp(value, "i");
    expect(getByText(expected)).toHaveTextContent(expected);
  });
});
