/**
 *
 *
 * <DropDownDrawer />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text } from "theme-ui";
import { FunctionComponent } from "react";
import useDropDown, { UseDropDownOption } from "hooks/useDropDown";

interface DropDownDrawerProps {
  options: UseDropDownOption[];
  title: string;
  onChange: (value: number) => void;
  value: number | null;
}

const DropDownDrawer: FunctionComponent<DropDownDrawerProps> = ({
  options,
  title,
  onChange,
  value,
}) => {
  const { currentOption, isOpen, toggle, setSelected } = useDropDown({
    options: options,
    currentValue: value,
    onChange,
  });

  return (
    <Box
      sx={{
        borderStyle: "solid",
        borderColor: "gray",
        borderWidth: 1,
        width: "100%",
      }}
    >
      <Box onClick={toggle} sx={{ cursor: "pointer", px: 4, py: 3 }}>
        <Text sx={{ fontWeight: 700, mt: 2 }}>{title}</Text>
        <Box>{currentOption?.title || "Choose an option"}</Box>
      </Box>

      {isOpen && (
        <Box sx={{ px: 4, pb: 3 }}>
          {options.map((option) => (
            <Box
              key={option.value}
              onClick={() => setSelected(option)}
              sx={{
                color:
                  currentOption?.value === option.value ? "blue" : "dark-gray",
                cursor: "pointer",
                py: 2,
              }}
            >
              {option.title}
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
};

export default DropDownDrawer;
