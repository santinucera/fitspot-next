/**
 *
 *
 * Tests for <IconCheckMark />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconCheckMark from "./IconCheckMark";

describe("<IconCheckMark />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconCheckMark />);
    expect(spy).not.toHaveBeenCalled();
  });
});
