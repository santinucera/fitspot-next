/**
 *
 *
 * <IconCheckMark />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconCheckMarkProps {
  size?: number;
  className?: string;
}

const IconCheckMark: FunctionComponent<IconCheckMarkProps> = ({
  size,
  ...props
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      {...props}
    >
      <path
        fill="currentColor"
        d="M9.51 17.55a1 1 0 01-.76-.34l-2.28-2.67A1 1 0 018 13.25L9.51 15l6.85-8a1 1 0 011.51 1.29l-7.61 8.9a1 1 0 01-.75.36z"
      />
    </svg>
  );
};

IconCheckMark.defaultProps = {
  size: 24,
};

export default IconCheckMark;
