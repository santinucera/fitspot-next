/**
 *
 *
 * Tests for <IconApps />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconApps from "./IconApps";

describe("<IconApps />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconApps size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
