/**
 *
 *
 * <IconApps />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconAppsProps {
  size: number;
  className?: string;
}

const IconApps: FunctionComponent<IconAppsProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        d="M21.894 15.553a1 1 0 01-.345 1.283l-.102.058-10 5a1 1 0 01-.77.052l-.124-.052-10-5a1 1 0 01.787-1.835l.107.047L11 19.88l9.553-4.775a1 1 0 011.341.447zm0-5a1 1 0 01-.345 1.283l-.102.058-10 5a1 1 0 01-.77.052l-.124-.052-10-5a1 1 0 01.787-1.835l.107.047L11 14.88l9.553-4.775a1 1 0 011.341.447zM10.553.106a1 1 0 01.894 0l10 5a1 1 0 010 1.788l-10 5a1 1 0 01-.894 0l-10-5a1 1 0 010-1.788zM11 2.118L3.236 6 11 9.881 18.763 6 11 2.118z"
        fill="currentColor"
        transform="translate(1 1)"
      />
    </svg>
  );
};

export default IconApps;
