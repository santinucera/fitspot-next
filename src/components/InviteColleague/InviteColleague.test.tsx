/**
 *
 *
 * Tests for <InviteColleague />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import InviteColleague from "./InviteColleague";

describe("<InviteColleague />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <InviteColleague
        title="Add a Colleague to this Session"
        message="..."
        url={"/foo"}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
