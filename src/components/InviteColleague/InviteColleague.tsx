/**
 *
 *
 * <InviteColleague />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Flex, Button } from "theme-ui";
import { FunctionComponent } from "react";
import H2 from "components/H2";
import CopyToClipBoard from "components/CopyToClipBoard";
import { useModal } from "containers/Modal/Modal";

interface InviteColleagueProps {
  title: string;
  message: string;
  url: string;
}

const InviteColleague: FunctionComponent<InviteColleagueProps> = ({
  title,
  message,
  url,
}) => {
  const { removeModal } = useModal();
  return (
    <Flex sx={{ flexDirection: "column", alignItems: "center", py: 7, px: 3 }}>
      <H2 sx={{ fontSize: 6 }}>{title}</H2>
      <Text sx={{ my: 6, color: "medium-gray", textAlign: "center" }}>
        {message}
      </Text>
      <Box
        sx={{
          mb: 7,
          width: "100%",
        }}
      >
        <CopyToClipBoard value={url} />
      </Box>
      <Button variant="secondary" onClick={removeModal}>
        Done
      </Button>
    </Flex>
  );
};

export default InviteColleague;
