/**
 *
 *
 * <ChallengeCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Flex, Divider, Progress, Button } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import TextTruncate from "react-text-truncate";
import H3 from "components/H3";
import IconCalendar from "components/IconCalendar";
import IconCircleCheck from "components/IconCircleCheck";
import Avatar from "components/Avatar";
import ordinalSuffix from "utils/ordinal-suffix";
import { Link } from "react-router-dom";
import { ChallengeLeaderBoardItem } from "services/ChallengeService";
import { Dayjs } from "dayjs";

interface ChallengeCardProps {
  id: number;
  isJoined: boolean;
  isActive: boolean;
  name: string;
  description: string;
  leaderboard: ChallengeLeaderBoardItem[];
  hasParticipants: boolean;
  participantsCount: number;
  startDate: Dayjs;
  endDate: Dayjs;
  daysLeft: number;
  totalDays: number;
  totalPossiblePoints: number;
  rank: number;
  totalPoints: number;
  handleRSVP?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

/**
 * Displays a ChallengeModel object.
 */
const ChallengeCard: FunctionComponent<ChallengeCardProps> = ({
  id,
  isJoined,
  isActive,
  name,
  description,
  leaderboard,
  hasParticipants,
  participantsCount,
  startDate,
  endDate,
  daysLeft,
  totalDays,
  totalPossiblePoints,
  rank,
  totalPoints,
  handleRSVP,
  ...props
}) => {
  return (
    <Flex
      key={id}
      sx={{
        bg: "white",
        p: 5,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "gray",
        flexDirection: "column",
        height: 400,
        width: 330,
      }}
      {...props}
    >
      <Box sx={{ mb: 4 }}>
        <H3 sx={{ fontWeight: "300", fontSize: 4 }}>{name}</H3>
        <Flex sx={{ mb: 3 }}>
          {((isJoined && !isActive) || !isJoined) && (
            <Flex sx={{ mr: 3, alignItems: "center", color: "darkGray" }}>
              <IconCalendar sx={{ mr: 3, opacity: 0.6 }} />
              {startDate.format("MMM D")} - {endDate.format("MMM D")}
            </Flex>
          )}
          {isJoined && (
            <Text sx={{ display: "flex", alignItems: "center" }}>
              <IconCircleCheck sx={{ mr: 3 }} /> Joined
            </Text>
          )}
        </Flex>
        <Divider />
        {isActive && isJoined && (
          <Box sx={{ my: 4 }}>
            <Flex sx={{ mb: 4 }}>
              <Box>Time remaining</Box>
              <Box sx={{ ml: "auto" }}>
                {daysLeft}
                <span sx={{ opacity: 0.4 }}>/{totalDays} Days</span>
              </Box>
            </Flex>
            <Flex>
              <Box>Your points</Box>
              <Box sx={{ ml: "auto" }}>
                {totalPoints}
                <span sx={{ opacity: 0.4 }}>/{totalPossiblePoints} pts</span>
              </Box>
            </Flex>
            <Progress
              sx={{
                bg: "#f7f8ff",
                color: "blue",
                mt: 3,
                height: "8px",
              }}
              max={totalPossiblePoints}
              value={totalPoints}
            />
          </Box>
        )}
        {hasParticipants && participantsCount >= 1 && (
          <Flex
            sx={{
              alignItems: "center",
              my: 5,
            }}
          >
            <Flex
              sx={{
                display: "inline-flex",
                flexDirection: "row-reverse",
              }}
            >
              {leaderboard?.slice(0, 3).map((p) => (
                <Box
                  key={p.userId}
                  sx={{
                    height: 40,
                    width: 40,
                    zIndex: 1,
                    ":not(:last-child)": { ml: "-20px" },
                  }}
                >
                  <Avatar
                    size={40}
                    url={p.avatar?.url}
                    sx={{
                      filter: "drop-shadow(0px 4px 8px rgba(0, 0, 55, 0.09))",
                    }}
                  />
                </Box>
              ))}
            </Flex>
            <Text sx={{ ml: hasParticipants ? 3 : 0, opacity: 0.6 }}>
              {isActive && isJoined ? (
                <Text>
                  You’re in{" "}
                  <span sx={{ fontWeight: 700 }}>
                    {rank}
                    {ordinalSuffix(rank || 0)} place
                  </span>
                  , keep it up!
                </Text>
              ) : (
                <Fragment>
                  <Text as="span" sx={{ fontWeight: "700" }}>
                    {participantsCount} people{" "}
                  </Text>
                  you know have joined this challenge!
                </Fragment>
              )}
            </Text>
          </Flex>
        )}
        {!(isActive && isJoined) && (
          <Text sx={{ mt: 4, mb: 7, opacity: 0.6, overflow: "hidden" }}>
            <TextTruncate line={4} text={description} />
          </Text>
        )}
      </Box>
      <Flex sx={{ mt: "auto" }}>
        {!isJoined && (
          <Button sx={{ width: "47.5%" }} onClick={handleRSVP}>
            Join
          </Button>
        )}
        <Link
          to={`/dashboard/challenges/${id}`}
          sx={{
            ml: "auto",
            variant: isJoined ? "buttons.secondary" : "buttons.underlined",
            width: isJoined ? "100%" : "50%",
          }}
        >
          See Details
        </Link>
      </Flex>
    </Flex>
  );
};

export default ChallengeCard;
