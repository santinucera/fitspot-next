/**
 *
 *
 * Tests for <ChallengeCard />
 *
 *
 */

import React from "react";
import ChallengeCard from "./ChallengeCard";
import dayjs from "utils/days";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<ChallengeCard />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <ChallengeCard
        key={1}
        id={1}
        isJoined={true}
        isActive={true}
        name={"HI"}
        description={"..."}
        leaderboard={[]}
        hasParticipants={true}
        participantsCount={2}
        startDate={dayjs()}
        endDate={dayjs()}
        daysLeft={1}
        totalDays={2}
        totalPoints={3}
        totalPossiblePoints={5}
        rank={1}
        handleRSVP={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
