/**
 *
 *
 * <CopyToClipBoard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text } from "theme-ui";
import { FunctionComponent, useEffect, useState } from "react";
import copy from "copy-to-clipboard";
import IconCopy from "components/IconCopy";

interface CopyToClipBoardProps {
  value: string;
  label?: string;
}

const CopyToClipBoard: FunctionComponent<CopyToClipBoardProps> = ({
  value,
  label,
}) => {
  const [showCopySuccess, setShowCopySuccess] = useState<boolean>(false);
  useEffect(() => {
    if (showCopySuccess) setTimeout(() => setShowCopySuccess(false), 1500);
  }, [showCopySuccess]);
  return (
    <Box sx={{ width: "100%", position: "relative" }}>
      {label && (
        <Text
          as="label"
          sx={{
            mb: 1,
            fontSize: 0,
            color: "medium-gray",
          }}
        >
          {label}
        </Text>
      )}
      <Flex
        sx={{
          border: 2,
          borderStyle: "solid",
          borderWidth: "2px",
          borderColor: "gray",
          height: "52px",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <input
          sx={{
            width: "100%",
            ml: 4,
            color: "medium-gray",
            border: "none",
            bg: "transparent",
          }}
          defaultValue={value}
          readOnly
        />
        <Flex
          onClick={() => {
            copy(value);
            setShowCopySuccess(true);
          }}
          sx={{
            height: "100%",
            justifyContent: "center",
            alignItems: "center",
            px: 3,
            bg: "gray",
            cursor: "pointer",
          }}
        >
          <IconCopy />
        </Flex>
      </Flex>
      <Text
        sx={{
          position: "absolute",
          display: showCopySuccess ? "block" : "none",
          textAlign: "right",
          mt: 1,
          fontSize: 0,
          color: "medium-gray",
          right: 0,
          bottom: "-22px",
        }}
      >
        Link copied to clipboard
      </Text>
    </Box>
  );
};

export default CopyToClipBoard;
