/**
 *
 *
 * Tests for <CopyToClipBoard />
 *
 *
 */

import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import CopyToClipBoard from "./CopyToClipBoard";

describe("<CopyToClipBoard />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "prompt", {
      value: () => {},
      writable: true,
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<CopyToClipBoard value="some string" label="label" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should copy to clipboard", () => {
    render(<CopyToClipBoard value="foo" label="label" />);
    fireEvent.click(screen.getByTestId("icon-copy"));
    expect(screen.getByText("Link copied to clipboard")).toBeInTheDocument();
  });
});
