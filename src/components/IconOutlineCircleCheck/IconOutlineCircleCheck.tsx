/**
 *
 *
 * <IconOutlineCircleCheck />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconOutlineCircleCheckProps {
  size?: number;
  className?: string;
}

const IconOutlineCircleCheck: FunctionComponent<IconOutlineCircleCheckProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      data-testid="icon-outlined-circle-check"
      fill="currentColor"
    >
      <path d="M12 24a11.85 11.85 0 01-4.16-.74 12 12 0 01-5-19A12 12 0 0116.88 1 1.09 1.09 0 1116 3 9.82 9.82 0 003.76 17.34a9.82 9.82 0 0016.1.54 9.89 9.89 0 002-5.88v-1A1.09 1.09 0 1124 11v1a12 12 0 01-8.6 11.5 11.85 11.85 0 01-3.4.5z" />
      <path d="M12.35 15.83a1 1 0 01-.74-.31l-3.18-3.17a1.06 1.06 0 011.5-1.5l2.42 2.43 9.84-9.85a1.06 1.06 0 111.5 1.5L13.1 15.52a1.06 1.06 0 01-.75.31z" />
    </svg>
  );
};

IconOutlineCircleCheck.defaultProps = {
  size: 24,
};

export default IconOutlineCircleCheck;
