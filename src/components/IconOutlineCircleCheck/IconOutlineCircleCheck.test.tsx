/**
 *
 *
 * Tests for <IconOutlineCircleCheck />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconOutlineCircleCheck from "./IconOutlineCircleCheck";

describe("<IconOutlineCircleCheck />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconOutlineCircleCheck />);
    expect(spy).not.toHaveBeenCalled();
  });
});
