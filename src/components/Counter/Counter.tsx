/**
 *
 *
 * <Counter />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Input } from "theme-ui";
import { FunctionComponent, useRef } from "react";
import IconAddCircle from "components/IconAddCircle";
import IconSubtractCircle from "components/IconSubtractCircle";

interface CounterButtonProps {
  onClick?: () => void;
}
const CounterButton: FunctionComponent<CounterButtonProps> = ({
  children,
  onClick,
  ...props
}) => {
  return (
    <Flex
      as={"button"}
      role="button"
      onClick={onClick}
      sx={{
        cursor: "pointer",
        justifyContent: "center",
        alignItems: "center",
        fontSize: 4,
        width: 34,
        height: 34,
        borderRadius: "100%",
        fontFamily: "system",
        padding: 0,
      }}
      {...props}
    >
      {children}
    </Flex>
  );
};

interface CounterProps {
  value: number;
  step?: number;
  min?: number;
  max?: number;
  onChange: (value: number) => void;
}

const Counter: FunctionComponent<CounterProps> = ({
  value,
  step,
  min,
  max,
  onChange,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);
  return (
    <Flex sx={{ fontSize: 5 }}>
      <CounterButton
        onClick={() => {
          inputRef.current?.stepDown();
          if (inputRef.current) onChange(Number(inputRef.current.value));
        }}
        sx={{
          color: value === min ? "dark-gray" : "primary",
        }}
      >
        <IconSubtractCircle />
      </CounterButton>
      <Input
        ref={inputRef}
        type="number"
        step={step}
        min={min}
        max={max}
        defaultValue={value}
        disabled
        sx={{
          fontSize: 5,
          mx: 3,
          p: 0,
          margin: 0,
          width: 80,
          textAlign: "center",
          fontWeight: 700,
          appearance: "textfield",
          bg: "transparent",
          border: "none",
        }}
      />
      <CounterButton
        onClick={() => {
          inputRef.current?.stepUp();
          if (inputRef.current) onChange(Number(inputRef.current.value));
        }}
        sx={{
          color: value === max ? "dark-gray" : "primary",
        }}
      >
        <IconAddCircle />
      </CounterButton>
    </Flex>
  );
};

export default Counter;
