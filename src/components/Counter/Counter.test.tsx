/**
 *
 *
 * Tests for <Counter />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Counter from "./Counter";

describe("<Counter />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Counter value={1} onChange={() => {}} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
