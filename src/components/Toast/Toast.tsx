/**
 *
 *
 * <Toast />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Flex, Text } from "theme-ui";
import { FunctionComponent, useEffect } from "react";
import { motion, AnimatePresence } from "framer-motion";
import IconX from "components/IconX";
import useToast, { RemoveToast } from "hooks/useToast";

interface ToastItemProps {
  id: number;
  title: string;
  description: string;
  removeToast: RemoveToast;
}

interface ToastProps {}

const ToastItem: FunctionComponent<ToastItemProps> = ({
  id,
  title,
  description,
  removeToast,
}) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      removeToast(id);
    }, 4000); // delay

    return () => {
      clearTimeout(timer);
    };
  }, [id, removeToast]);

  return (
    <motion.div
      key={id}
      initial={{ opacity: 0, y: 50 }}
      animate={{ opacity: 1, y: 0, transition: { duration: 0.3 } }}
      exit={{ opacity: 0, transition: { duration: 0.3 } }}
      data-testid={`toast-item-${id}`}
      sx={{
        minWidth: ["100%", "560px"],
        px: 0,
        my: 3,
        boxShadow: "toast",
        bg: "white",
      }}
    >
      <Flex
        sx={{
          flexDirection: "row",
        }}
      >
        <Flex
          sx={{
            ml: 4,
            justifyContent: "center",
            alignItems: "center",
            px: 4,
            color: "purple",
            borderBottom: "2px solid",
            borderColor: "purple",
          }}
        >
          <IconX />
        </Flex>
        <Flex
          sx={{
            width: "100%",
            borderBottom: "2px solid",
            borderColor: "light-purple",
            flexDirection: "column",
            py: 5,
            pl: 3,
          }}
        >
          <Heading sx={{ fontSize: 3 }}>{title}</Heading>
          <Text sx={{ color: "dark-gray", pt: 1 }}>{description}</Text>
        </Flex>
        <Flex
          sx={{
            ml: "auto",
            justifyContent: "center",
            alignItems: "center",
            px: 3,
            mr: 4,
            borderBottom: "2px solid",
            borderColor: "light-purple",
            cursor: "pointer",
          }}
          onClick={() => removeToast(id)}
          role="button"
          data-testid="remove-toast-button"
        >
          <IconX />
        </Flex>
      </Flex>
    </motion.div>
  );
};

const Toast: FunctionComponent<ToastProps> = () => {
  const { toasts, removeToast } = useToast();
  return (
    <Box>
      <Flex
        sx={{
          position: "fixed",
          right: 0,
          top: 0,
          zIndex: 9999,
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <AnimatePresence>
          {toasts.map(({ id, title, description }: any) => (
            <ToastItem
              key={id}
              id={id}
              title={title}
              description={description}
              removeToast={removeToast}
            />
          ))}
        </AnimatePresence>
      </Flex>
    </Box>
  );
};

export default Toast;
