/**
 *
 *
 * Tests for <Toast />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Toast from "./Toast";

describe("<Toast />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Toast />);
    expect(spy).not.toHaveBeenCalled();
  });
});
