/**
 *
 *
 * Tests for <IconInfo />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconInfo from "./IconInfo";

describe("<IconInfo />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconInfo />);
    expect(spy).not.toHaveBeenCalled();
  });
});
