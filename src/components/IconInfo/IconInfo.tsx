/**
 *
 *
 * <IconInfo />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconInfoProps {
  size?: number;
  className?: string;
}

const IconInfo: FunctionComponent<IconInfoProps> = ({ size, className }) => {
  return (
    <svg
      sx={{ width: size, height: size }}
      className={className}
      viewBox="0 0 25 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.6859 22C18.2126 22 22.6928 17.5228 22.6928 12C22.6928 6.47715 18.2126 2 12.6859 2C7.15922 2 2.67896 6.47715 2.67896 12C2.67896 17.5228 7.15922 22 12.6859 22Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.6858 16V12"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.6858 8H12.6958"
        stroke="#000037"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default IconInfo;
