/**
 *
 *
 * <IconSession />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconSessionProps {
  size: number;
  className?: string;
}

const IconSession: FunctionComponent<IconSessionProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        fill="currentColor"
        d="M18 10a1 1 0 011 1v7a1 1 0 01-1 1h-7a1 1 0 01-1-1v-7a1 1 0 011-1zM7 10a1 1 0 011 1v7a1 1 0 01-1 1H0a1 1 0 01-1-1v-7a1 1 0 011-1zm10 2h-5v5h5v-5zM6 12H1v5h5v-5zM7-1a1 1 0 011 1v7a1 1 0 01-1 1H0a1 1 0 01-1-1V0a1 1 0 011-1zm11 0a1 1 0 011 1v7a1 1 0 01-1 1h-7a1 1 0 01-1-1V0a1 1 0 011-1zM6 1H1v5h5V1zm11 0h-5v5h5V1z"
        transform="translate(3 3)"
      />
    </svg>
  );
};

export default IconSession;
