/**
 *
 *
 * Tests for <IconSession />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconSession from "./IconSession";

describe("<IconSession />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconSession size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
