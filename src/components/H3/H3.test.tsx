/**
 *
 *
 * Tests for <H3 />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import H3 from "./H3";

describe("<H3 />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<H3 />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Heading H3";
    const { getByRole } = render(<H3>Heading H3</H3>);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
