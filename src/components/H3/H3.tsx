/**
 *
 *
 * <H3 />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, HeadingProps } from "theme-ui";
import { FunctionComponent } from "react";

const H3: FunctionComponent<HeadingProps> = (props) => {
  return <Heading as="h3" variant="text.h3" {...props} />;
};

export default H3;
