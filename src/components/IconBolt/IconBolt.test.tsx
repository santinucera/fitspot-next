/**
 *
 *
 * Tests for <IconBolt />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconBolt from "./IconBolt";

describe("<IconBolt />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconBolt />);
    expect(spy).not.toHaveBeenCalled();
  });
});
