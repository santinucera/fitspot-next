/**
 *
 *
 * <IconClock />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconClockProps {
  size?: number;
  className?: string;
}

const IconClock: FunctionComponent<IconClockProps> = ({ size, className }) => {
  return (
    <svg
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12 1C18.0751 1 23 5.92486 23 12C23 18.0751 18.0751 23 12 23C5.92491 23 1.00005 18.0751 1.00005 12C1.00649 5.92758 5.92758 1.00649 12 1ZM12 21.4285C17.2073 21.4285 21.4286 17.2072 21.4286 12C21.4286 6.79272 17.2073 2.57141 12 2.57141C6.79277 2.57141 2.57146 6.79272 2.57146 12C2.57707 17.2049 6.79507 21.4229 12 21.4285Z"
        fill="#636485"
        stroke="#636485"
        strokeWidth="0.5"
      />
      <path
        d="M11.9999 4.92871C12.4338 4.92871 12.7856 5.28049 12.7856 5.71444V11.2144H18.2856C18.7195 11.2144 19.0713 11.5662 19.0713 12.0002C19.0713 12.4341 18.7195 12.7859 18.2856 12.7859H11.9999C11.5659 12.7859 11.2141 12.4341 11.2141 12.0001V5.71444C11.2141 5.28049 11.5659 4.92871 11.9999 4.92871Z"
        fill="#636485"
        stroke="#636485"
        strokeWidth="0.5"
      />
    </svg>
  );
};

IconClock.defaultProps = {
  size: 24,
};

export default IconClock;
