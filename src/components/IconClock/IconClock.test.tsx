/**
 *
 *
 * Tests for <IconClock />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconClock from "./IconClock";

describe("<IconClock />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconClock />);
    expect(spy).not.toHaveBeenCalled();
  });
});
