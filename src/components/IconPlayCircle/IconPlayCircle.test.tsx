/**
 *
 *
 * Tests for <IconPlayCircle />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconPlayCircle from "./IconPlayCircle";

describe("<IconPlayCircle />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconPlayCircle />);
    expect(spy).not.toHaveBeenCalled();
  });
});
