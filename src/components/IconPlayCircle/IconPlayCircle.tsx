/**
 *
 *
 * <IconPlayCircle />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconPlayCircleProps {}

const IconPlayCircle: FunctionComponent<IconPlayCircleProps> = () => {
  return (
    <svg width={56} height={58} viewBox="0 0 56 58" fill="none">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M56 28.824c0 15.66-12.536 28.355-28 28.355S0 44.484 0 28.824 12.536.469 28 .469s28 12.695 28 28.355c0 0 0 15.66 0 0z"
        fill="#fff"
      />
      <path
        d="M39.198 28.825l-16.8 11.341V17.482l16.8 11.343z"
        fill="#000037"
      />
    </svg>
  );
};

export default IconPlayCircle;
