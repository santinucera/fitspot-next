/**
 *
 *
 * Tests for <ProtectedRoute />
 *
 *
 */

import React from "react";
import ProtectedRoute from "./ProtectedRoute";
import { UserType } from "services/UserService";
import { queryCache } from "react-query";
import { createUser } from "test-utils/user-service-test-utils";
import {
  renderWithRouter,
  renderWithMemoryRouter,
} from "test-utils/render-with-router";
import { useLocation, Routes } from "react-router-dom";
import { waitFor } from "@testing-library/react";

describe("<ProtectedRoute />", () => {
  const adminUser = createUser(UserType.admin);
  const trainerUser = createUser(UserType.trainer);
  const customerUser = createUser(UserType.customer);
  const originalWindow = global.window;
  beforeEach(async () => {
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        href: "",
      },
      writable: true,
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    queryCache.setQueryData("user", () => adminUser);
    renderWithRouter(
      <ProtectedRoute
        path="/"
        element={<div />}
        guards={[UserType.customer, UserType.admin]}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it.each([
    [adminUser, [UserType.admin]],
    [trainerUser, [UserType.trainer]],
    [customerUser, [UserType.customer]],
  ])("should redirect to the correct route", (user, guards) => {
    const El = () => {
      queryCache.setQueryData("user", () => user);
      const location = useLocation();
      return (
        <div>
          <div data-testid="location">{location.pathname}</div>
          <ProtectedRoute
            path="/"
            guards={guards}
            element={<div>Success!</div>}
          />
        </div>
      );
    };
    const { getByTestId, getByText } = renderWithMemoryRouter(
      "/*",
      ["/"],
      <El />
    );
    expect(getByTestId("location")).toHaveTextContent("/");
    expect(getByText("Success!")).toBeInTheDocument();
  });

  it.each([
    [adminUser, [UserType.trainer]],
    [trainerUser, [UserType.customer]],
    [customerUser, [UserType.admin]],
  ])("should redirect to the auth route", async (user, guards) => {
    const El = () => {
      queryCache.setQueryData("user", () => user);
      return (
        <div>
          <Routes>
            <ProtectedRoute
              path="/"
              guards={guards}
              element={<div>Success!</div>}
            />
          </Routes>
        </div>
      );
    };
    renderWithMemoryRouter("/*", ["/"], <El />);
    await waitFor(() => {
      expect(window.location.href).toBe("/user/auth?next=/");
    });
  });
});
