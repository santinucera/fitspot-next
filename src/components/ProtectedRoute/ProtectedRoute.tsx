/**
 *
 *
 * <ProtectedRoute />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import { Route } from "react-router-dom";
import { useQueryUserGetData, UserType } from "services/UserService";

interface ProtectedRouteProps {
  element: React.ReactElement;
  guards: UserType[];
  path: string;
}

const ProtectedRoute: FunctionComponent<ProtectedRouteProps> = ({
  element,
  guards,
  path,
}) => {
  // The user should be cached at this point.
  const user = useQueryUserGetData();
  const hasPermission = guards.some((u) => u === user.userType);
  const loginUrl = `/user/auth?next=/`;

  if (hasPermission === false) {
    window.location.href = loginUrl;
    return null;
  }

  return (
    <Fragment>
      <Route path={path} element={element} />
    </Fragment>
  );
};

export default ProtectedRoute;
