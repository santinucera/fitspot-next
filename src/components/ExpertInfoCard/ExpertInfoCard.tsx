/**
 *
 *
 * <ExpertInfoCardCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, Flex, Text, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import IconArrow from "components/IconArrow";
import { Link } from "react-router-dom";
import Avatar from "components/Avatar";
import IconStarFilled from "components/IconStarFilled";

interface ExpertInfoCardProps {
  avatar: string | null;
  firstName: string | null;
  lastName: string | null;
  publicId: string | null;
  rating: number | null;
}

const ExpertInfoCard: FunctionComponent<ExpertInfoCardProps> = ({
  avatar,
  firstName,
  lastName,
  publicId,
  rating,
}) => {
  return (
    <Flex
      sx={{
        flexDirection: "column",
        bg: "white",
        py: 6,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Avatar url={avatar} size={100} />
      <Heading as="h2" sx={{ my: 3 }}>
        {firstName} {lastName?.charAt(0)}.
      </Heading>
      <Flex
        sx={{
          alignItems: "center",
          lineHeight: 0,
          color: "medium-gray",
        }}
      >
        <Text sx={{ mr: 1 }}>{rating}</Text>
        <IconStarFilled size={9} />
      </Flex>
      <Divider sx={{ minWidth: 158, my: 4 }} />
      <Link
        to={`/dashboard/trainers/${publicId}`}
        sx={{
          display: "flex",
          textDecoration: "none",
          color: "primary",
          fontSize: 0,
          alignItems: "center",
        }}
      >
        More about me <IconArrow direction="right" size={18} />
      </Link>
    </Flex>
  );
};

export default ExpertInfoCard;
