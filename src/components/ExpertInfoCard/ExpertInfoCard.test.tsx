/**
 *
 *
 * Tests for <ExpertInfoCard />
 *
 *
 */

import React from "react";
import ExpertInfoCard from "./ExpertInfoCard";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<ExpertInfoCard />", () => {
  it.only("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <ExpertInfoCard
        avatar={"foo"}
        firstName={"foo"}
        lastName={"foo"}
        publicId={"foo"}
        rating={4}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
