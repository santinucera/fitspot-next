/**
 *
 *
 * Tests for <CancelReservationModal />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import CancelReservationModal from "./CancelReservationModal";
import dayjs from "utils/days";

describe("<CancelReservationModal />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <CancelReservationModal
        name="Kung foo"
        startDate={dayjs()}
        endDate={dayjs()}
        confirmCancellation={() => {}}
        handleClose={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
