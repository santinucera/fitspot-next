/**
 *
 *
 * <CancelReservationModal />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Button, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { Dayjs } from "dayjs";
import IconOutlineX from "components/IconOutlineX";

interface CancelReservationModalProps {
  name: string;
  startDate: Dayjs;
  endDate: Dayjs;
  confirmCancellation: () => void;
  handleClose: () => void;
}

const CancelReservationModal: FunctionComponent<CancelReservationModalProps> = ({
  name,
  startDate,
  endDate,
  confirmCancellation,
  handleClose,
}) => {
  return (
    <Box sx={{ p: 5, width: "100%" }} data-testid="cancel-reservation-modal">
      <Flex
        sx={{
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          py: 6,
        }}
      >
        <IconOutlineX size={58} sx={{ color: "purple", mb: 3 }} />
        <Text sx={{ my: 3, textAlign: "center", fontWeight: 700 }}>
          Are you sure you want to cancel your spot?
        </Text>
        <Text sx={{ my: 3, color: "medium-gray", textAlign: "center" }}>
          {name}
          <br />
          {startDate.format("ddd MMM d, h:ss")}
          {" - "}
          {endDate.format("h:ss A")}
        </Text>
        <Box
          sx={{
            mt: 6,
          }}
        >
          <Button
            sx={{
              mb: 3,
              width: "255px",
            }}
            onClick={confirmCancellation}
            data-testid="confirm-button"
          >
            Confirm cancellation
          </Button>
          <Button
            sx={{
              width: "255px",
            }}
            variant="secondary"
            onClick={handleClose}
            data-testid="cancel-button"
          >
            Keep my reservation
          </Button>
        </Box>
      </Flex>
    </Box>
  );
};

export default CancelReservationModal;
