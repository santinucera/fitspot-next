/**
 *
 *
 * <IconHome />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconHomeProps {
  size: number;
  className?: string;
}

const IconHome: FunctionComponent<IconHomeProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        fill="currentColor"
        transform="translate(2 1)"
        d="M3 22a3 3 0 01-3-3V8a1 1 0 01.386-.79l9-7a1 1 0 011.228 0l9 7A1 1 0 0120 8v11a3 3 0 01-3 3zm7-19.734L2 8.488V19a1 1 0 00.206.608l.087.1A1 1 0 003 20h3v-9a1 1 0 01.883-.993L7 10h6a1 1 0 01.993.883L14 11v9h3a1 1 0 00.608-.206l.1-.087A1 1 0 0018 19V8.489l-8-6.223zM12 12H8v8h4v-8z"
      />
    </svg>
  );
};

export default IconHome;
