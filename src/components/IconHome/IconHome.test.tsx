/**
 *
 *
 * Tests for <IconHome />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconHome from "./IconHome";

describe("<IconHome />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconHome size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
