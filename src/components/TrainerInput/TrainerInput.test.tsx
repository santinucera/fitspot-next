/**
 *
 *
 * Tests for <TrainerInput />
 *
 *
 */

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import TrainerInput from "./TrainerInput";
import { useForm } from "react-hook-form";

describe("<TrainerInput />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      const { control } = useForm({
        defaultValues: {
          name: "John",
        },
      });
      return (
        <div>
          <TrainerInput
            control={control}
            label="First Name"
            name="name"
            placeholder="First Name"
          />
        </div>
      );
    };
    const { getByPlaceholderText } = render(<El />);
    const input = getByPlaceholderText("First Name") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "Frank" } });
    expect(input.value).toBe("Frank");
    expect(spy).not.toHaveBeenCalled();
  });
});
