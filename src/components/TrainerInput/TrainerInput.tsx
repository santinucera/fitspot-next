/**
 *
 *
 * <TrainerInput />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import Input, { InputProps } from "components/Input";

const TrainerInput: FunctionComponent<InputProps> = (props) => {
  return (
    <Input sx={{ "& .MuiOutlinedInput-root": { bg: "white" } }} {...props} />
  );
};

export default TrainerInput;
