/**
 *
 *
 * Tests for <IconPlus />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconPlus from "./IconPlus";

describe("<IconPlus />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconPlus />);
    expect(spy).not.toHaveBeenCalled();
  });
});
