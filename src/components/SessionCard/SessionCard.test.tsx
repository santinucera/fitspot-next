/**
 *
 *
 * Tests for <SessionCard />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import dayjs from "dayjs";
import SessionCard from "./SessionCard";

describe("<SessionCard />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <SessionCard
        participantsCount={999}
        trainerName="Bruce Lee"
        startDate={dayjs()}
        endDate={dayjs()}
        trainerAvatar="/path"
        name="Foo"
        description="Hi"
        actions={<div>actions</div>}
        showSessionInfo
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render actions components", () => {
    const value = "actions";
    const { getByRole } = render(
      <SessionCard
        participantsCount={999}
        trainerName="Bruce Lee"
        startDate={dayjs()}
        endDate={dayjs()}
        trainerAvatar="/path"
        name="Foo"
        description="Hi"
        actions={<div role="button">actions</div>}
        showSessionInfo
      />
    );
    const expected = new RegExp(value, "i");
    expect(getByRole("button")).toHaveTextContent(expected);
  });
});
