/**
 *
 *
 * <SessionCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text, Heading, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import { Dayjs } from "dayjs";
import IconUsers from "components/IconUsers";
import Avatar from "components/Avatar";
import SessionTime from "components/SessionTime";
import TextTruncate from "react-text-truncate";

interface SessionCardProps {
  name: string;
  description: string;
  startDate: Dayjs;
  endDate: Dayjs;
  trainerName: string;
  trainerAvatar: string | null;
  participantsCount: number;
  actions: any;
  showSessionInfo: boolean;
}

const SessionCard: FunctionComponent<SessionCardProps> = ({
  name,
  description,
  startDate,
  endDate,
  trainerName,
  trainerAvatar,
  participantsCount,
  actions,
  showSessionInfo,
}) => {
  return (
    <Flex p={6} bg="white" sx={{ boxShadow: "small", flexDirection: "column" }}>
      <Heading
        as="h3"
        sx={{
          fontSize: 4,
        }}
      >
        <TextTruncate line={2} text={name} />
      </Heading>
      <Text as="h5">
        <SessionTime startDate={startDate} endDate={endDate} />
      </Text>
      <Text
        sx={{
          minHeight: 160,
          mt: 3,
        }}
      >
        <TextTruncate line={6} text={description} />
      </Text>
      <Divider />
      {showSessionInfo && (
        <Box sx={{ mb: 4 }}>
          <Flex sx={{ pt: 2, alignItems: "center" }}>
            <Avatar url={trainerAvatar} />
            <Heading ml={4}>{trainerName}</Heading>
          </Flex>
          <Flex sx={{ pt: 4, alignItems: "center" }}>
            <IconUsers />
            <Heading ml={4}>{participantsCount} Going</Heading>
          </Flex>
        </Box>
      )}
      <Flex sx={{ mt: "auto", width: "100%", justifyContent: "space-between" }}>
        {actions}
      </Flex>
    </Flex>
  );
};

export default SessionCard;
