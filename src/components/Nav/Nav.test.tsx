/**
 *
 *
 * Tests for <Nav />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Nav from "./Nav";

describe("<Nav />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Nav />);
    expect(spy).not.toHaveBeenCalled();
  });
});
