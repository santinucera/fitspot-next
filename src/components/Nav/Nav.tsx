/**
 *
 *
 * <Nav />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui";
import { FunctionComponent } from "react";
import { motion, AnimatePresence } from "framer-motion";

interface NavProps {
  isOpen: boolean;
  dark?: boolean;
}

const variants = {
  open: { opacity: 1, x: 0 },
  closed: { opacity: 0, x: "-100%" },
};

const Nav: FunctionComponent<NavProps> = ({ children, isOpen, dark }) => {
  const color = dark ? "primary" : "white";
  return (
    <Flex
      sx={{
        position: ["fixed", "fixed", "sticky"],
        display: [
          isOpen ? "block" : "none",
          isOpen ? "block" : "none",
          "block",
        ],
        width: ["100%", "100%", 260],
        flexDirection: "column",
        flex: "none",
        height: "100%",
        zIndex: 1300,
        top: 0,
        bg: ["rgba(0,0,0,0.7)", "rgba(0,0,0,0.7)", "transparent"],
      }}
      as="nav"
      // Test ID is necessary here because finding elements by the "navigation" aria
      // landmark role is not supported by React Testing Library.
      data-testid="nav"
    >
      <Box
        sx={{
          display: ["block", "block", "none"],
          height: "100%",
        }}
      >
        <AnimatePresence>
          <motion.div
            animate={isOpen ? "open" : "closed"}
            variants={variants}
            transition={{ type: "ease" }}
            sx={{
              width: 260,
              flexDirection: "column",
              pt: 7,
              px: 5,
              bg: color,
              height: "100%",
              position: "relative",
            }}
          >
            {children}
          </motion.div>
        </AnimatePresence>
      </Box>
      <Box
        sx={{
          display: ["none", "block"],
          pt: 7,
          px: 5,
          height: "100vh",
          bg: color,
        }}
      >
        {children}
      </Box>
    </Flex>
  );
};

export default Nav;
