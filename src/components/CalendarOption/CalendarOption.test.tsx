/**
 *
 *
 * Tests for <CalendarOption />
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import CalendarOption from "./CalendarOption";

describe("<CalendarOption />", () => {
  const { getByTestId, queryByTestId } = screen;

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <CalendarOption
        title="option 2"
        checked={true}
        src="..."
        value={2}
        onChange={() => {}}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should show a check mark", () => {
    render(
      <CalendarOption
        title="option 2"
        checked={true}
        src="..."
        value={2}
        onChange={() => {}}
      />
    );
    expect(getByTestId("icon-circle-check")).toBeInTheDocument();
  });

  it("should not show a check mark", () => {
    render(
      <CalendarOption
        title="option 2"
        checked={false}
        src="..."
        value={2}
        onChange={() => {}}
      />
    );
    expect(queryByTestId("icon-circle-check")).toBeNull();
  });
});
