/**
 *
 *
 * <CalendarOption />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box, Text } from "theme-ui";
import { FunctionComponent } from "react";
import IconCircleCheck from "components/IconCircleCheck";
import slugify from "utils/slugify";

interface CalendarOptionProps {
  title: string;
  checked: boolean;
  src: string;
  value: number;
  onChange: any;
}

const CalendarOption: FunctionComponent<CalendarOptionProps> = ({
  title,
  checked,
  src,
  value,
  onChange,
}) => {
  const slug = slugify("-", title);

  return (
    <Flex
      as="label"
      sx={{
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        borderStyle: "solid",
        borderColor: "gray",
        borderWidth: 1,
        position: "relative",
        bg: "white",
        cursor: "pointer",
        m: 3,
        height: 126,
        width: 126,
      }}
      data-testid={`calendar-option-${slug}`}
    >
      <input
        type="radio"
        name="calendar"
        value={value}
        onChange={onChange}
        hidden
        data-testid={`calendar-option-input-${slug}`}
      />
      {checked && (
        <Box
          className="icon"
          sx={{
            position: "absolute",
            right: 0,
            top: 0,
            color: "primary",
            p: 3,
          }}
        >
          <IconCircleCheck />
        </Box>
      )}
      <Box
        sx={{
          opacity: checked ? 1 : 0.3,
          height: 50,
          width: 50,
          mb: 3,
          backgroundSize: 50,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundImage: `url(${src})`,
        }}
      />
      <Text sx={{ fontSize: 0 }}>{title}</Text>
    </Flex>
  );
};

export default CalendarOption;
