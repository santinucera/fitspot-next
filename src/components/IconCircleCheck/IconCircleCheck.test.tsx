/**
 *
 *
 * Tests for <IconCircleCheck />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconCircleCheck from "./IconCircleCheck";

describe("<IconCircleCheck />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconCircleCheck />);
    expect(spy).not.toHaveBeenCalled();
  });
});
