/**
 *
 *
 * <IconCircleCheck />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconCircleCheckProps {
  size?: number;
  className?: string;
}

const IconCircleCheck: FunctionComponent<IconCircleCheckProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      data-testid="icon-circle-check"
    >
      <path
        d="M10 0c5.523 0 10 4.477 10 10s-4.477 10-10 10S0 15.523 0 10 4.477 0 10 0zm4.554 6.223a.8.8 0 00-1.13.023l-5.578 5.8-1.27-1.319-.074-.068a.8.8 0 00-1.078 1.178l1.846 1.918.076.07a.8.8 0 001.077-.07l6.154-6.4.065-.077a.8.8 0 00-.088-1.055z"
        transform="translate(2 1)"
        fill="currentColor"
      />
    </svg>
  );
};

IconCircleCheck.defaultProps = {
  size: 24,
};

export default IconCircleCheck;
