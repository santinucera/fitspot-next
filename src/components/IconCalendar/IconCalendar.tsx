/**
 *
 *
 * <IconCalendar />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconCalendarProps {
  size?: number;
  className?: string;
}

const IconCalendar: FunctionComponent<IconCalendarProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        d="M13-1a1 1 0 01.993.883L14 0v1h2a3 3 0 012.995 2.824L19 4v14a3 3 0 01-3 3H2a3 3 0 01-3-3V4a3 3 0 013-3h2V0a1 1 0 011.993-.117L6 0v1h6V0a1 1 0 011-1zm4 10H1v9a1 1 0 00.883.993L2 19h14a1 1 0 001-1V9zM4 3H2a1 1 0 00-1 1v3h16V4a1 1 0 00-.883-.993L16 3h-2v1a1 1 0 01-1.993.117L12 4V3H6v1a1 1 0 01-1.993.117L4 4V3z"
        fill="currentColor"
        transform="translate(3 2)"
      />
    </svg>
  );
};

IconCalendar.defaultProps = {
  size: 24,
};

export default IconCalendar;
