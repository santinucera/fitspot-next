/**
 *
 *
 * Tests for <IconCalendar />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconCalendar from "./IconCalendar";

describe("<IconCalendar />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconCalendar size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
