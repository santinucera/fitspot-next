/**
 *
 *
 * <DatePicker />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import { Controller, ValidationRules } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import { DatePicker as MUIDatePicker } from "@material-ui/pickers";
import { Box } from "@material-ui/core";
import dayjs from "utils/days";

interface DatePickerProps {
  control: any;
  name: string;
  label: string;
  rules?: ValidationRules;
  [x: string]: any;
}

const DatePicker: FunctionComponent<DatePickerProps> = ({
  control,
  rules,
  name,
  label,
  ...props
}) => {
  return (
    <Box
      mb={3}
      sx={{
        "label.Mui-focused": {
          color: "blue",
        },
        ".MuiInput-underline:after": {
          borderBottomColor: "blue",
        },
      }}
    >
      <Controller
        rules={rules}
        name={name}
        render={({ onChange, value }) => (
          <MUIDatePicker
            {...props}
            renderInput={(props) => (
              <TextField
                {...props}
                sx={{
                  "& label.Mui-focused": {
                    color: "blue",
                  },
                  "& .MuiOutlinedInput-root": {
                    "& fieldset": {
                      borderColor: "dark-gray",
                    },
                    "&:hover fieldset": {
                      borderColor: "blue",
                    },
                    "&.Mui-focused fieldset": {
                      borderColor: "blue",
                    },
                  },
                }}
                fullWidth
                variant="outlined"
                InputLabelProps={{ shrink: true }}
              />
            )}
            value={dayjs(value).format()}
            label={label}
            onChange={(date) => onChange(date)}
          />
        )}
        control={control}
      />
    </Box>
  );
};

DatePicker.defaultProps = {
  rules: {},
};

export default DatePicker;
