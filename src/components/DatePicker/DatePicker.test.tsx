/**
 *
 *
 * Tests for <DatePicker />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import DateFnsAdapter from "@material-ui/pickers/adapter/date-fns";
import DatePicker from "./DatePicker";
import { useForm } from "react-hook-form";
import usLocale from "date-fns/locale/en-US";
const staticDateAdapter = new DateFnsAdapter({ locale: usLocale });

describe("<DatePicker />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      const { control } = useForm({
        defaultValues: {
          date: null,
        },
      });
      return (
        <div>
          <DatePicker
            control={control}
            label="Date Picker"
            name="date"
            dateAdapter={staticDateAdapter}
          />
        </div>
      );
    };
    render(<El />);
    expect(spy).not.toHaveBeenCalled();
  });
});
