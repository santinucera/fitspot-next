/**
 *
 *
 * <OnDemandItem />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text } from "theme-ui";
import { FunctionComponent } from "react";
import H3 from "components/H3";
import { Link } from "react-router-dom";
import IconPlayCircle from "components/IconPlayCircle";
import { Dayjs } from "dayjs";

interface OnDemandItemProps {
  id: number;
  name: string;
  trainer: string;
  startDate: Dayjs;
  thumbnailUrl: string;
}

const OnDemandItem: FunctionComponent<OnDemandItemProps> = ({
  id,
  name,
  trainer,
  startDate,
  thumbnailUrl,
}) => {
  return (
    <Box key={id} data-testid={`on-demand-video-${id}`}>
      <Link
        to={`/dashboard/on-demand/${id}`}
        sx={{
          display: "flex",
          color: "white",
          backgroundColor: "primary",
          backgroundImage: `url(${thumbnailUrl})`,
          backgroundSize: "cover",
          height: 220,
          textDecoration: "none",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <IconPlayCircle />
      </Link>
      <Flex
        sx={{
          mt: "auto",
          py: 4,
          width: "100%",
          alignItems: "center",
        }}
      >
        <Box>
          <H3 sx={{ fontWeight: 400 }}>{name}</H3>
          <Text>{trainer}</Text>
          <Text>{startDate.format("MM/DD/YYYY")}</Text>
        </Box>
      </Flex>
    </Box>
  );
};

export default OnDemandItem;
