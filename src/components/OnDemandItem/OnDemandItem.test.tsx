/**
 *
 *
 * Tests for <OnDemandItem />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import { fireEvent, waitFor } from "@testing-library/react";
import OnDemandItem from "./OnDemandItem";
import dayjs from "dayjs";
import {
  renderWithMemoryRouter,
  renderWithRouter,
} from "test-utils/render-with-router";

describe("<OnDemandItem />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <OnDemandItem
        id={1}
        name="test"
        trainer="test"
        startDate={dayjs()}
        thumbnailUrl="test"
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render name and trainer", async () => {
    const spy = jest.spyOn(global.console, "error");
    const { getByText } = renderWithRouter(
      <OnDemandItem
        id={1}
        name="test"
        trainer="trainer"
        startDate={dayjs()}
        thumbnailUrl="url"
      />
    );
    await waitFor(() => expect(getByText("test")).toBeInTheDocument());
    await waitFor(() => expect(getByText("trainer")).toBeInTheDocument());

    expect(spy).not.toHaveBeenCalled();
  });
});
