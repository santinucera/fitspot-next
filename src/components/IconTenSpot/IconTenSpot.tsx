/**
 *
 *
 * <IconTenSpot />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconTenSpotProps {
  size?: number;
  className?: string;
}

const IconTenSpot: FunctionComponent<IconTenSpotProps> = ({
  size,
  ...props
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      {...props}
    >
      <path
        fill="currentColor"
        d="M11.775 4.761l3.447-1.295v8.752l-3.447 1.295V4.761zm6.15-4.758L9 3.358v11.195l-3.117-1.176V2.19L.074 0 0 .054v2.355l3.116 1.173v8.75l-3.04-1.144-.076.054v2.365L9 17l9-3.386V.054l-.074-.051z"
        transform="translate(3 2)"
      />
    </svg>
  );
};

IconTenSpot.defaultProps = {
  size: 24,
};

export default IconTenSpot;
