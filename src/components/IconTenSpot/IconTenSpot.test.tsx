/**
 *
 *
 * Tests for <IconTenSpot />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconTenSpot from "./IconTenSpot";

describe("<IconTenSpot />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconTenSpot />);
    expect(spy).not.toHaveBeenCalled();
  });
});
