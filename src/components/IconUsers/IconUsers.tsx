/**
 *
 *
 * <IconUsers />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconUsersProps {
  size?: number;
  className?: string;
}

const IconUsers: FunctionComponent<IconUsersProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        fill="currentColor"
        d="M13 12a5 5 0 014.993 4.736L18 17v2a1 1 0 01-1.993.117L16 19v-2a3 3 0 00-2.802-2.993L13 14H5a3 3 0 00-2.993 2.802L2 17v2a1 1 0 01-1.993.117L0 19v-2a5 5 0 014.736-4.993L5 12h8zM9 0a5 5 0 100 10A5 5 0 009 0zm0 2a3 3 0 110 6 3 3 0 010-6zM19.032 12.88a1 1 0 011.218-.718 5 5 0 013.743 4.583L24 17v2a1 1 0 01-1.993.117L22 19v-2a3 3 0 00-2.25-2.902 1 1 0 01-.718-1.218zM15.031.882a1 1 0 011.217-.72 5 5 0 010 9.687 1 1 0 01-.607-1.902l.111-.036a3 3 0 00.212-5.75l-.212-.062a1 1 0 01-.72-1.217z"
        transform="translate(0 0)"
      />
    </svg>
  );
};

IconUsers.defaultProps = {
  size: 24,
};

export default IconUsers;
