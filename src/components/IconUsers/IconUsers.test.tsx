/**
 *
 *
 * Tests for <IconUsers />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconUsers from "./IconUsers";

describe("<IconUsers />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconUsers size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
