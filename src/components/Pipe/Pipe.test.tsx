/**
 *
 *
 * Tests for <Pipe />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Pipe from "./Pipe";

describe("<Pipe />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Pipe />);
    expect(spy).not.toHaveBeenCalled();
  });
});
