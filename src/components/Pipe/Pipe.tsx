/**
 *
 *
 * <Pipe />
 *
 *
 */

/** @jsx jsx */
import { jsx, Text } from "theme-ui";
import { FC } from "react";

const Pipe: FC = () => {
  return (
    <Text as="span" sx={{ mx: [2, 3] }}>
      |
    </Text>
  );
};

export default Pipe;
