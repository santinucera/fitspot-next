/**
 *
 *
 * Tests for <IconDollarSign />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconDollarSign from "./IconDollarSign";

describe("<IconDollarSign />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconDollarSign size={24} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
