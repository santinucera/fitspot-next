/**
 *
 *
 * <IconDollarSign />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconDollarSignProps {
  size?: number;
  className?: string;
}

const IconDollarSign: FunctionComponent<IconDollarSignProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <g clipPath="url(#clip0)">
        <path
          d="M12.3926 0.506836V24.5068"
          stroke="#000037"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M18.5529 4.87036H9.3123C8.16861 4.87036 7.07176 5.27263 6.26304 5.98868C5.45433 6.70473 5 7.6759 5 8.68854C5 9.70119 5.45433 10.6724 6.26304 11.3884C7.07176 12.1045 8.16861 12.5067 9.3123 12.5067H15.4727C16.6164 12.5067 17.7133 12.909 18.522 13.625C19.3307 14.3411 19.785 15.3123 19.785 16.3249C19.785 17.3376 19.3307 18.3087 18.522 19.0248C17.7133 19.7408 16.6164 20.1431 15.4727 20.1431H5"
          stroke="#000037"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect
            width="24"
            height="24"
            fill="white"
            transform="translate(0 0.506836)"
          />
        </clipPath>
      </defs>
    </svg>
  );
};

export default IconDollarSign;
