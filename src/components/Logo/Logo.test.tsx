/**
 *
 *
 * Tests for <Logo />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Logo from "./Logo";

describe("<Logo />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Logo />);
    expect(spy).not.toHaveBeenCalled();
  });
});
