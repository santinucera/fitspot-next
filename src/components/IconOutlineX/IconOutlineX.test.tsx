/**
 *
 *
 * Tests for <IconOutlineX />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconOutlineX from "./IconOutlineX";

describe("<IconOutlineX />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconOutlineX />);
    expect(spy).not.toHaveBeenCalled();
  });
});
