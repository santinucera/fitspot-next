/**
 *
 *
 * <IconOutlineX />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconOutlineXProps {
  size?: number;
  className?: string;
}

const IconOutlineX: FunctionComponent<IconOutlineXProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      data-testid="icon-outlined-x"
      fill="currentColor"
    >
      <path d="M12 24a12 12 0 1112-12 12 12 0 01-12 12zm0-21.82A9.82 9.82 0 1021.82 12 9.83 9.83 0 0012 2.18z" />
      <path d="M8.73 16.36A1.05 1.05 0 018 16a1.08 1.08 0 010-1.5L14.5 8a1.1 1.1 0 111.55 1.5L9.5 16a1.05 1.05 0 01-.77.36z" />
      <path d="M15.27 16.36a1.07 1.07 0 01-.77-.36L8 9.5A1.09 1.09 0 119.5 8l6.55 6.55a1.09 1.09 0 01-.78 1.86z" />
    </svg>
  );
};

IconOutlineX.defaultProps = {
  size: 24,
};

export default IconOutlineX;
