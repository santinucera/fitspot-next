/**
 *
 *
 * Tests for <ErrorBoundary />
 *
 *
 */

import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import ErrorBoundary from "./ErrorBoundary";
import { queryCache } from "react-query";

const { getByText, getByTestId } = screen;

describe("<ErrorBoundary />", () => {
  const originalWindow = global.window;
  beforeEach(async () => {
    jest.restoreAllMocks();
    global.window = Object.create(window);
    Object.defineProperty(global.window, "location", {
      value: {
        href: "",
      },
      writable: true,
    });
  });

  afterEach(() => {
    global.window = originalWindow;
  });

  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ErrorBoundary>happy</ErrorBoundary>);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a child", () => {
    render(<ErrorBoundary>happy</ErrorBoundary>);
    expect(getByText("happy")).toBeInTheDocument();
  });

  it("should render an error message when a component throws an error", () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();
    const message = "💥 BOOM 💥";
    const Bomb = () => {
      throw new Error(message);
    };
    render(
      <ErrorBoundary>
        <Bomb />
      </ErrorBoundary>
    );
    expect(getByText(message)).toBeInTheDocument();
    expect(spy).toHaveBeenCalled();
  });

  it("should render an error, reset the query cache and set the window location", () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();
    const message = "💥 BOOM 💥";
    const Bomb = () => {
      throw new Error(message);
    };

    const El = () => {
      queryCache.setQueryData("foo", "bar");
      return (
        <ErrorBoundary>
          <Bomb />
        </ErrorBoundary>
      );
    };
    render(<El />);
    expect(getByText(message)).toBeInTheDocument();
    expect(queryCache.getQueryData("foo")).toBeDefined();
    fireEvent.click(getByTestId("error-fallback-button"));
    expect(queryCache.getQueryData("foo")).toBe(undefined);
    expect(window.location.href).toBe("/");
    expect(spy).toHaveBeenCalled();
  });
});
