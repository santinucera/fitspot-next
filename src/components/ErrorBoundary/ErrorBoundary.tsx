/**
 *
 *
 * <ErrorBoundary />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FC } from "react";
import { ErrorBoundary as ReactErrorBoundary } from "react-error-boundary";
import ErrorFallback from "components/ErrorFallback";
import { queryCache } from "react-query";

const ErrorBoundary: FC = ({ children }) => {
  return (
    <ReactErrorBoundary
      FallbackComponent={ErrorFallback}
      onReset={() => {
        queryCache.clear();
        window.location.href = "/";
      }}
    >
      {children}
    </ReactErrorBoundary>
  );
};

export default ErrorBoundary;
