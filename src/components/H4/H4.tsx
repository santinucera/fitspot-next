/**
 *
 *
 * <H4 />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, HeadingProps } from "theme-ui";
import { FunctionComponent } from "react";

const H4: FunctionComponent<HeadingProps> = (props) => {
  return <Heading as="h4" variant="text.h4" {...props} />;
};

export default H4;
