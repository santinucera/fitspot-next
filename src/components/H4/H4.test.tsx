/**
 *
 *
 * Tests for <H4 />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import H4 from "./H4";

describe("<H4 />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<H4 myProp="some string" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Heading H4";
    const { getByRole } = render(<H4>Heading H4</H4>);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
