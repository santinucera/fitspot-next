/**
 *
 *
 * <ChallengeWinnerAvatar />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Text } from "theme-ui";
import { FC } from "react";
import Avatar from "components/Avatar";
import ribbon1 from "images/ribbon-1.png";
import ribbon2 from "images/ribbon-2.png";
import ribbon3 from "images/ribbon-3.png";
import ordinalSuffix from "utils/ordinal-suffix";

interface Ribbons {
  [x: number]: string;
}

const ribbons: Ribbons = {
  1: ribbon1,
  2: ribbon2,
  3: ribbon3,
};

interface ChallengeWinnerAvatarProps {
  rank: number;
  url?: string;
  name: string;
}

const ChallengeWinnerAvatar: FC<ChallengeWinnerAvatarProps> = ({
  rank,
  url,
  name,
}) => {
  const isFirstPlace = rank === 1;
  return (
    <Flex
      data-testid="winner-avatar"
      sx={{
        flexDirection: "column",
        alignItems: "center",
        textAlign: "center",
        mx: 4,
      }}
    >
      <Avatar url={url} size={isFirstPlace ? 70 : 51} />
      <img
        sx={{
          width: isFirstPlace ? "27px" : "19px",
          mt: isFirstPlace ? "-16px" : "-12px",
        }}
        src={ribbons?.[rank]}
        alt={`ranking ${rank}`}
      />
      <Text sx={{ fontStretch: "condensed", fontSize: 1 }}>{name}</Text>
      <Text
        sx={{
          fontSize: 0,
          color: isFirstPlace ? "dark-yellow" : "dark-gray",
        }}
      >
        {rank}
        {ordinalSuffix(rank)} Place
      </Text>
    </Flex>
  );
};

export default ChallengeWinnerAvatar;
