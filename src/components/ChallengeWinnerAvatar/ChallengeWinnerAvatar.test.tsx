/**
 *
 *
 * Tests for <ChallengeWinnerAvatar />
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import ChallengeWinnerAvatar from "./ChallengeWinnerAvatar";

describe("<ChallengeWinnerAvatar />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ChallengeWinnerAvatar rank={1} url="/" name="bob" />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render a challenge winner avatar in first place", () => {
    render(<ChallengeWinnerAvatar rank={1} url="/" name="bob" />);
    expect(screen.getByTestId("winner-avatar")).toMatchSnapshot();
  });
  it("should render a challenge winner avatar in second place", () => {
    render(<ChallengeWinnerAvatar rank={2} url="/" name="bob" />);
    expect(screen.getByTestId("winner-avatar")).toMatchSnapshot();
  });
  it("should render a challenge winner avatar in third place", () => {
    render(<ChallengeWinnerAvatar rank={3} url="/" name="bob" />);
    expect(screen.getByTestId("winner-avatar")).toMatchSnapshot();
  });
});
