/**
 *
 *
 * <Pagination />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex } from "theme-ui";
import { FunctionComponent, useEffect } from "react";
import { usePagination } from "@material-ui/lab";
import IconBoxArrow from "components/IconBoxArrow";

interface PaginationProps {
  page: number;
  count: number;
  onChange?: (event: React.ChangeEvent<unknown>, page: number) => void;
}

const Pagination: FunctionComponent<PaginationProps> = ({
  page,
  count,
  onChange,
}) => {
  const { items } = usePagination({
    page,
    count,
    onChange,
  });

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [page]);

  return (
    <Flex
      sx={{ flexDirection: "row", alignItems: "center" }}
      data-testid="pagination"
    >
      {items.map(({ page, type, selected, ...item }, index) => {
        let children = null;
        if (type === "start-ellipsis" || type === "end-ellipsis") {
          children = "…";
        } else if (type === "page") {
          children = (
            <button
              type="button"
              sx={{
                width: "40px",
                height: "40px",
                bg: selected ? "primary" : "",
                color: selected ? "white" : "",
              }}
              {...item}
            >
              {page}
            </button>
          );
        } else {
          children = (
            <Flex
              as="button"
              {...item}
              sx={{ opacity: item.disabled ? 0.3 : 1 }}
            >
              {type === "next" && <IconBoxArrow direction="right" />}
              {type === "previous" && <IconBoxArrow direction="left" />}
            </Flex>
          );
        }

        return <Box key={index}>{children}</Box>;
      })}
    </Flex>
  );
};

export default Pagination;
