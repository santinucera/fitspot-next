/**
 *
 *
 * Tests for <Pagination />
 *
 *
 */

import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import Pagination from "./Pagination";

describe("<Pagination />", () => {
  beforeAll(() => {
    window.scrollTo = jest.fn();
  });
  afterAll(() => {
    jest.clearAllMocks();
  });
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Pagination page={2} count={2} onChange={() => {}} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should handle on-change callback", () => {
    const callback = jest.fn((v) => v);
    render(<Pagination page={2} count={2} onChange={callback} />);
    fireEvent.click(screen.getByText("2"));
    expect(callback).toHaveBeenCalledWith(...callback.mock.calls[0]);
  });
});
