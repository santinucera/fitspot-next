/**
 *
 *
 * <LeadersCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, Flex, Text, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import { ResponseData as LeadersResponseData } from "services/WearablesService";
import Avatar from "components/Avatar";
import thousands from "utils/thousands";
import H3 from "components/H3";

interface LeadersCardProps {
  title: string;
  isFetching: boolean;
  leaders?: LeadersResponseData[];
}

const LeadersCard: FunctionComponent<LeadersCardProps> = ({
  title,
  leaders,
}) => {
  return (
    <Flex
      sx={{
        flexDirection: "column",
        flex: "1 1 auto",
        bg: "white",
        p: 5,
        boxShadow: "small",
      }}
    >
      <H3 sx={{ fontWeight: "300", fontSize: 4 }}>{title}</H3>
      <Flex sx={{ mt: 3, justifyContent: "space-between" }}>
        <Text>Ranking</Text>
        <Text>Steps</Text>
      </Flex>
      <Divider />
      <Flex
        sx={{
          py: 5,
          flexDirection: "column",
        }}
      >
        {leaders?.map(({ rank, id, name, avatarUrl, total }) => (
          <Flex sx={{ py: 4 }} key={id}>
            <Text sx={{ width: 20 }}>{rank}.</Text>
            <Avatar url={avatarUrl} sx={{ mx: 5 }} />
            <Text>{name}</Text>
            <Text ml="auto">{thousands(total)}</Text>
          </Flex>
        ))}
      </Flex>
    </Flex>
  );
};

export default LeadersCard;
