/**
 *
 *
 * Tests for <LeadersCard />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import LeadersCard from "./LeadersCard";

describe("<LeadersCard />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<LeadersCard leaders={[]} isFetching={false} title="Leaders" />);
    expect(spy).not.toHaveBeenCalled();
  });
});
