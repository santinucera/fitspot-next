/**
 *
 *
 * <ScrollIndicator />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import Range from "utils/range";

interface ScrollIndicatorProps {
  count: number;
  scrollProgress: number;
}

const ScrollIndicator: FunctionComponent<ScrollIndicatorProps> = ({
  count,
  scrollProgress,
}) => {
  return (
    <Flex
      sx={{
        display: ["flex", "none"],
        marginTop: 5,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {Range(0, count).map((_, index) => (
        <div
          sx={{
            bg: scrollProgress === index ? "primary" : "dark-gray",
            borderRadius: "50%",
            width: "8px",
            height: "8px",
            marginRight: 2,
          }}
          key={index}
        />
      ))}
    </Flex>
  );
};

export default ScrollIndicator;
