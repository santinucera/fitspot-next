/**
 *
 *
 * Tests for <ScrollIndicator />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import ScrollIndicator from "./ScrollIndicator";

describe("<ScrollIndicator />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ScrollIndicator count={2} scrollProgress={3} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
