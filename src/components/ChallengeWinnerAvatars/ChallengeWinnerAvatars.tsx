/**
 *
 *
 * <ChallengeWinnerAvatars />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { FunctionComponent, useMemo } from "react";
import { ChallengeLeaderBoardItem } from "services/ChallengeService";
import ChallengeWinnerAvatar from "components/ChallengeWinnerAvatar/ChallengeWinnerAvatar";

interface ChallengeWinnerAvatarsProps {
  leaderboard: ChallengeLeaderBoardItem[];
}

const ChallengeWinnerAvatars: FunctionComponent<ChallengeWinnerAvatarsProps> = ({
  leaderboard,
  ...props
}) => {
  const winners = useMemo(() => {
    const w = leaderboard.slice(0, 3);
    /**
     * Shows the top three in the correct order
     * if there is a top three [2nd, 1st, 3rd]
     */
    if (w.length === 3) return [w[1], w[0], w[2]];
    return w;
  }, [leaderboard]);

  return (
    <Flex
      data-testid="challenge-winners"
      sx={{ justifyContent: "center", alignItems: "center" }}
      {...props}
    >
      {winners.map(({ userId, avatar, rank, firstName, lastName }) => (
        <ChallengeWinnerAvatar
          key={userId}
          url={avatar?.url}
          rank={rank}
          name={`${firstName} ${lastName?.charAt(0)}`}
        />
      ))}
    </Flex>
  );
};

export default ChallengeWinnerAvatars;
