/**
 *
 *
 * Tests for <ChallengeWinnerAvatars />
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import ChallengeWinnerAvatars from "./ChallengeWinnerAvatars";
import { createChallengeLeaderBoard } from "test-utils/challenge-service-test-utils";

describe("<ChallengeWinnerAvatars />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ChallengeWinnerAvatars leaderboard={[]} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should render top three winners", () => {
    const leaders = createChallengeLeaderBoard();
    render(<ChallengeWinnerAvatars leaderboard={leaders} />);
    expect(screen.getByTestId("challenge-winners")).toMatchSnapshot();
  });
  it("should render top two winners", () => {
    const leaders = createChallengeLeaderBoard().slice(0, 2);
    render(<ChallengeWinnerAvatars leaderboard={leaders} />);
    expect(screen.getByTestId("challenge-winners")).toMatchSnapshot();
  });
  it("should render only one winner", () => {
    const leaders = createChallengeLeaderBoard().slice(0, 1);
    render(<ChallengeWinnerAvatars leaderboard={leaders} />);
    expect(screen.getByTestId("challenge-winners")).toMatchSnapshot();
  });
});
