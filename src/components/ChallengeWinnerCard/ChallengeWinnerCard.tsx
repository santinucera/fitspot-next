/**
 *
 *
 * <ChallengeWinnerCard />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex, Text, Divider } from "theme-ui";
import { FC } from "react";
import IconCalendar from "components/IconCalendar";
import { Dayjs } from "dayjs";
import { ChallengeLeaderBoardItem } from "services/ChallengeService";
import { Link } from "react-router-dom";
import ChallengeWinnerAvatars from "components/ChallengeWinnerAvatars";

interface ChallengeWinnerCardProps {
  name: string;
  id: number;
  startDate: Dayjs;
  endDate: Dayjs;
  leaderboard: ChallengeLeaderBoardItem[];
  myRank: number;
  myPoints: number;
}

const ChallengeWinnerCard: FC<ChallengeWinnerCardProps> = ({
  name,
  id,
  startDate,
  endDate,
  leaderboard,
  myRank,
  myPoints,
}) => {
  return (
    <Flex
      data-testid="challenge-winner-card"
      sx={{
        bg: "white",
        p: 4,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "gray",
        flexDirection: "column",
        height: 400,
        width: 330,
      }}
    >
      <Box sx={{ mb: 4 }}>
        <Text sx={{ fontWeight: 400, fontSize: 3 }}>{name}</Text>
        <Flex sx={{ mr: 3, mb: 3, alignItems: "center", color: "medium-gray" }}>
          <IconCalendar sx={{ mr: 3, opacity: 0.6 }} />
          {startDate.format("MMM D")} - {endDate.format("MMM D")}
        </Flex>
        <Divider />
      </Box>
      <ChallengeWinnerAvatars leaderboard={leaderboard} />
      <Flex sx={{ mt: "auto" }}>
        <Box sx={{ mr: 7 }}>
          <Text sx={{ fontSize: 0, color: "medium-gray" }}>My Rank</Text>
          <Box sx={{ fontSize: 5, fontWeight: 700 }}>
            {myRank}
            <span sx={{ mx: 2 }}>/</span>
            <span sx={{ color: "medium-gray" }}>{leaderboard.length}</span>
          </Box>
        </Box>
        <Box>
          <Text sx={{ fontSize: 0, color: "medium-gray" }}>My Points</Text>
          <Box sx={{ fontSize: 5, fontWeight: 700 }}>{myPoints}</Box>
        </Box>
      </Flex>
      <Flex sx={{ mt: "auto" }}>
        <Link
          to={`/dashboard/challenges/${id}`}
          sx={{
            ml: "auto",
            variant: "buttons.secondary",
            width: "100%",
          }}
        >
          See details
        </Link>
      </Flex>
    </Flex>
  );
};

export default ChallengeWinnerCard;
