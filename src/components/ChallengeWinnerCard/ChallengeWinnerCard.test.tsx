/**
 *
 *
 * Tests for <ChallengeWinnerCard />
 *
 *
 */

import React from "react";

import ChallengeWinnerCard from "./ChallengeWinnerCard";
import days from "utils/days";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<ChallengeWinnerCard />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(
      <ChallengeWinnerCard
        id={1}
        myPoints={10}
        myRank={10}
        name={"Card"}
        leaderboard={[]}
        startDate={days()}
        endDate={days()}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
