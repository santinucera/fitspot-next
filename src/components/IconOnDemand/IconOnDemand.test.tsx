/**
 *
 *
 * Tests for <IconOnDemand />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconOnDemand from "./IconOnDemand";

describe("<IconOnDemand />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconOnDemand />);
    expect(spy).not.toHaveBeenCalled();
  });
});
