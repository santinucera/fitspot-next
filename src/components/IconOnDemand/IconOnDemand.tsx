/**
 *
 *
 * <IconOnDemand />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconOnDemandProps {
  size?: number;
  className?: string;
}

const IconOnDemand: FunctionComponent<IconOnDemandProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      sx={{
        height: size,
        width: size,
      }}
      className={className}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M21.8386 5.77464C21.498 5.42347 21.0707 5.16842 20.6 5.03523C18.88 4.61523 12 4.61523 12 4.61523C12 4.61523 5.11996 4.61523 3.39996 5.07523C2.92921 5.20842 2.50194 5.46347 2.16131 5.81464C1.82068 6.16581 1.57875 6.60064 1.45996 7.07523C1.14518 8.82079 0.991197 10.5915 0.999961 12.3652C0.988741 14.1523 1.14273 15.9365 1.45996 17.6952C1.59092 18.1551 1.83827 18.5734 2.17811 18.9097C2.51794 19.246 2.93878 19.489 3.39996 19.6152C5.11996 20.0752 12 20.0752 12 20.0752C12 20.0752 18.88 20.0752 20.6 19.6152C21.0707 19.482 21.498 19.227 21.8386 18.8758C22.1792 18.5247 22.4212 18.0898 22.54 17.6152C22.8523 15.8828 23.0063 14.1256 23 12.3652C23.0112 10.5782 22.8572 8.79393 22.54 7.03523V7.03523C22.4212 6.56064 22.1792 6.12581 21.8386 5.77464Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.5 12.3652L9.75 9.09521V15.6352L15.5 12.3652Z"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

IconOnDemand.defaultProps = {
  size: 24,
};

export default IconOnDemand;
