/**
 *
 *
 * Tests for <ImageCropper />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import ImageCropper from "./ImageCropper";

describe("<ImageCropper />", () => {
  it("should render myProp", () => {
    const spy = jest.spyOn(global.console, "error");
    const onInitializedFn = jest.fn();
    render(
      <ImageCropper
        onInitialized={onInitializedFn}
        src={
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg=="
        }
      />
    );
    expect(spy).not.toHaveBeenCalled();
    expect(onInitializedFn.mock.calls.length).toBe(1);
  });
});
