/**
 *
 *
 * <ImageCropper />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";

interface ImageCropperProps {
  src: string;
  onInitialized: (instance: Cropper) => void | Promise<void>;
}

const ImageCropper: FunctionComponent<ImageCropperProps> = ({
  src,
  onInitialized,
}) => {
  return (
    <Box>
      <Cropper
        checkCrossOrigin={false}
        sx={{ height: 280, width: 280 }}
        initialAspectRatio={1}
        preview=".img-preview"
        src={src}
        viewMode={1}
        guides={true}
        dragMode="move"
        minCropBoxHeight={10}
        minCropBoxWidth={10}
        background={true}
        responsive={true}
        autoCropArea={1}
        scalable={true}
        aspectRatio={1}
        checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
        onInitialized={onInitialized}
      />
      <Box mt={4}>
        <Box
          className="img-preview"
          sx={{
            overflow: "hidden",
            width: 280,
            height: 280,
            borderRadius: "100%",
          }}
        />
      </Box>
    </Box>
  );
};

export default ImageCropper;
