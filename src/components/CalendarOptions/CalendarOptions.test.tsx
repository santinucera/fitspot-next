/**
 *
 *
 * Tests for <CalendarOptions />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import CalendarOptions from "./CalendarOptions";

describe("<CalendarOptions />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <CalendarOptions
        handleCalendarSelection={() => {}}
        calendarPreference={2}
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
