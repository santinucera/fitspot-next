/**
 *
 *
 * <CalendarOptions />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { Fragment, FunctionComponent } from "react";
import { CalendarPreference } from "services/CalendarService";
import ical from "images/ical.png";
import appleIcon from "images/apple.png";
import gmailIcon from "images/gmail.png";
import odaIcon from "images/oda.png";
import owaIcon from "images/owa.png";
import yahooIcon from "images/yahoo.png";
import CalendarOption from "components/CalendarOption/CalendarOption";

interface CalendarOptionsProps {
  handleCalendarSelection: (event: React.ChangeEvent<HTMLInputElement>) => void;
  calendarPreference: number;
}

const CalendarOptions: FunctionComponent<CalendarOptionsProps> = ({
  handleCalendarSelection,
  calendarPreference,
}) => {
  return (
    <Fragment>
      <CalendarOption
        onChange={handleCalendarSelection}
        title="Google"
        value={CalendarPreference.GOOGLE}
        checked={calendarPreference === CalendarPreference.GOOGLE}
        src={gmailIcon}
      />
      <CalendarOption
        onChange={handleCalendarSelection}
        title="Apple"
        value={CalendarPreference.APPLE}
        checked={calendarPreference === CalendarPreference.APPLE}
        src={appleIcon}
      />
      <CalendarOption
        onChange={handleCalendarSelection}
        title="Outlook Desktop"
        value={CalendarPreference.OUTLOOK_DESKTOP}
        checked={calendarPreference === CalendarPreference.OUTLOOK_DESKTOP}
        src={odaIcon}
      />
      <CalendarOption
        onChange={handleCalendarSelection}
        title="Outlook Web"
        value={CalendarPreference.OUTLOOK_WEB}
        checked={calendarPreference === CalendarPreference.OUTLOOK_WEB}
        src={owaIcon}
      />
      <CalendarOption
        onChange={handleCalendarSelection}
        title="Yahoo"
        value={CalendarPreference.YAHOO}
        checked={calendarPreference === CalendarPreference.YAHOO}
        src={yahooIcon}
      />
      <CalendarOption
        onChange={handleCalendarSelection}
        title="iCal"
        value={CalendarPreference.ICAL}
        checked={calendarPreference === CalendarPreference.ICAL}
        src={ical}
      />
    </Fragment>
  );
};

export default CalendarOptions;
