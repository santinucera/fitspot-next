/**
 *
 *
 * Tests for <PillarChip />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import PillarChip from "./PillarChip";

describe("<PillarChip />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<PillarChip pillar="Move" isLive={true} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
