/**
 *
 *
 * <PillarChip />
 *
 * A tag displaying the activity category for a session card.
 *
 * NOTE: make sure you add `position: relative` to the container's styles.
 */

/** @jsx jsx */
import { jsx, Box, Flex } from "theme-ui";
import { SystemStyleObject } from "@styled-system/css";
import { FunctionComponent } from "react";
import { Pillar } from "services/types";
import IconBolt from "components/IconBolt";

interface PillarChipProps {
  pillar: Pillar;
  isLive?: boolean;
  className?: string;
}

const getBackgroundColor = (pillar: Pillar): string => {
  switch (pillar) {
    case "Move":
      return "yellow";
    case "Play":
      return "green";
    case "Chill":
      return "blue";
    case "Eat":
      return "red";
    case "Thrive":
      return "purple";
  }
};

const sharedStyles: SystemStyleObject = {
  lineHeight: "20px",
  color: "white",
  fontWeight: 800,
  width: "48px",
  textAlign: "center",
};

const PillarChip: FunctionComponent<PillarChipProps> = ({
  pillar,
  isLive,
  className,
}) => {
  return (
    <Flex
      data-testid="pillar"
      className={className}
      sx={{
        flexDirection: "row",
        position: "absolute",
        top: 4,
        left: 4,
      }}
    >
      {isLive && (
        <Flex
          sx={{
            ...sharedStyles,
            backgroundColor: "forest-green",
            fontSize: "11px",
            textTransform: "uppercase",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginRight: "11px",
          }}
        >
          <Flex
            sx={{
              marginRight: "2px",
              // Bounding box is slightly bigger than the image in the SVG
              marginLeft: "-2px",
              alignItems: "center",
            }}
          >
            <IconBolt />
          </Flex>
          <Box>LIVE</Box>
        </Flex>
      )}
      <Box
        sx={{
          ...sharedStyles,
          backgroundColor: getBackgroundColor(pillar),
          fontSize: 0,
          textTransform: "lowercase",
        }}
      >
        {pillar}
      </Box>
    </Flex>
  );
};

export default PillarChip;
