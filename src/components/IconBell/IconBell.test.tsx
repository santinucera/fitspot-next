/**
 *
 *
 * Tests for <IconBell />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconBell from "./IconBell";

describe("<IconBell />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconBell />);
    expect(spy).not.toHaveBeenCalled();
  });
});
