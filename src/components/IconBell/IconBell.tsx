/**
 *
 *
 * <IconBell />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconBellProps {
  size?: number;
  className?: string;
}

const IconBell: FunctionComponent<IconBellProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <path
        d="M12.231 19.135a1 1 0 01.364 1.367 2.999 2.999 0 01-5.19 0A1 1 0 019.07 19.4l.065.097a1 1 0 001.657.109l.072-.109a1 1 0 011.367-.363zM10 0a7 7 0 017 7c0 2.812.493 4.95 1.307 6.512.28.538.578.966.875 1.296.176.196.31.316.378.363l.102.078c.68.588.278 1.751-.662 1.751H1c-.99 0-1.378-1.283-.555-1.832l.023-.017c.047-.037.143-.122.275-.26.293-.31.593-.72.88-1.245C2.476 12.08 3 9.9 3 7a7 7 0 017-7zm0 2a5 5 0 00-5 5c0 3.225-.602 5.734-1.622 7.604l-.138.245-.092.151h13.703a8.345 8.345 0 01-.163-.276l-.155-.288C15.566 12.58 15 10.125 15 7a5 5 0 00-5-5z"
        transform="translate(2 1)"
        fill="currentColor"
      />
    </svg>
  );
};

IconBell.defaultProps = {
  size: 24,
};

export default IconBell;
