/**
 *
 *
 * <IconReplay />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconReplayProps {
  size?: number;
  className?: string;
}

const IconReplay: FunctionComponent<IconReplayProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <g fill="currentColor" transform="translate(0 2)">
        <path d="M8.673 7.115v6.064c0 .755.838 1.193 1.479.78l4.608-3.02a.927.927 0 000-1.56l-4.608-3.043c-.64-.414-1.479.024-1.479.78z" />
        <path d="M5.495 9.672H4.164c.246-3.896 3.499-6.99 7.516-6.99a7.56 7.56 0 014.83 1.73.421.421 0 00.566-.024l1.035-1.096c.148-.17.148-.439-.025-.585C16.312 1.197 14.045.394 11.68.394 6.382.369 2.094 4.484 1.848 9.672H.69a.69.69 0 00-.592 1.047l2.39 3.897a.696.696 0 001.183 0l2.39-3.897c.296-.487-.024-1.047-.566-1.047zM23.31 9.575l-2.39-3.897a.696.696 0 00-1.183 0l-2.39 3.897a.69.69 0 00.591 1.047h1.33c-.246 3.896-3.498 6.99-7.515 6.99a7.559 7.559 0 01-4.83-1.73.421.421 0 00-.566.024l-1.06 1.096c-.148.17-.148.439.025.585 1.774 1.51 4.041 2.313 6.407 2.313 5.297 0 9.585-4.14 9.831-9.302h1.158c.543 0 .863-.56.592-1.023z" />
      </g>
    </svg>
  );
};

IconReplay.defaultProps = {
  size: 24,
};

export default IconReplay;
