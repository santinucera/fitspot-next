/**
 *
 *
 * Tests for <IconReplay />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconReplay from "./IconReplay";

describe("<IconReplay />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconReplay />);
    expect(spy).not.toHaveBeenCalled();
  });
});
