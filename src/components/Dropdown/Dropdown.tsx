/**
 *
 *
 * <Dropdown />
 *
 *
 */
/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui";
import { FunctionComponent, Fragment } from "react";
import {
  useEffect,
  useState,
  useRef,
  createContext,
  useContext,
  Dispatch,
  SetStateAction,
} from "react";

interface DropdownProps {
  handle: any;
  align?: string;
  width?: number;
}

type ContextState = {
  isOpen: boolean;
};

type ContextValue = {
  state: ContextState;
  setState: Dispatch<SetStateAction<ContextState>>;
};

const initialContext: ContextValue = {
  state: { isOpen: false },
  setState: () => {}, // noop default callback
};

const DropdownContext = createContext<ContextValue>(initialContext);

const useDropdownStore = () => useContext(DropdownContext);

const Dropdown: FunctionComponent<DropdownProps> = ({
  handle,
  children,
  align = "right",
  width,
}) => {
  const [state, setState] = useState(initialContext.state);
  const toggleOpen = () => setState({ isOpen: !isOpen });
  const close = () => setState({ isOpen: false });
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    function handleClickOutside(event: any) {
      if (ref.current && !ref.current.contains(event.target)) close();
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);

  const { isOpen } = state;

  return (
    <DropdownContext.Provider value={{ state, setState }}>
      <Box ref={ref} sx={{ position: "relative" }}>
        <Fragment>
          <Box css={{ cursor: "pointer" }} onClick={toggleOpen}>
            {handle}
          </Box>
          {isOpen && (
            <Box
              sx={{
                mt: 2,
                [align]: 0,
                py: 2,
                position: "absolute",
                bg: "white",
                boxShadow: "dropdown",
                minWidth: 260,
                zIndex: 99999,
                borderRadius: "2px",
                borderColor: "gray",
                borderWidth: "1px",
                borderStyle: "solid",
                width,
              }}
            >
              <Flex sx={{ flexDirection: "column" }}>{children}</Flex>
            </Box>
          )}
        </Fragment>
      </Box>
    </DropdownContext.Provider>
  );
};

interface DropdownButtonProps {
  onClick: Function;
}

const DropdownButton: FunctionComponent<DropdownButtonProps> = ({
  onClick,
  children,
}) => {
  const { setState } = useDropdownStore();
  return (
    <Box
      as="button"
      bg="white"
      sx={{
        textAlign: "left",
        px: 5,
        py: 2,
        color: "primary",
        cursor: "pointer",
        border: "none",
        fontFamily: "body",
        fontSize: 1,
        "&:hover": {
          color: "blue",
        },
      }}
      onClick={() => {
        setState({ isOpen: false });
        onClick();
      }}
    >
      {children}
    </Box>
  );
};

export { Dropdown, DropdownButton };
