/**
 *
 *
 * Tests for <Dropdown />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import { Dropdown } from "./Dropdown";

describe("<Dropdown />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Dropdown handle={<div>handle</div>} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render handle", () => {
    const value = "handle";
    const { getByRole } = render(
      <Dropdown handle={<div role="button">handle</div>} />
    );
    const expected = new RegExp(value, "i");
    expect(getByRole("button")).toHaveTextContent(expected);
  });
});
