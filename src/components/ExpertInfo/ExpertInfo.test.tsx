/**
 *
 *
 * Tests for <ExpertInfo />
 *
 *
 */

import React from "react";
import { waitFor } from "@testing-library/react";
import { render } from "@testing-library/react";

import ExpertInfo from "./ExpertInfo";

describe("<ExpertInfo />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <ExpertInfo
        trainerId=""
        avatar=""
        firstName="Bruce"
        lastName="lee"
        activityName="Kung-foo"
        bio="..."
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render props", async () => {
    const trainerId = "TrainerId";
    const avatar = "Avatar";
    const firstName = "First Name";
    const lastName = "Last Name";
    const activityName = "Activity Name";
    const bio = "Bio";

    const { queryByText } = render(
      <ExpertInfo
        trainerId={trainerId}
        avatar={avatar}
        firstName={firstName}
        lastName={lastName}
        activityName={activityName}
        bio={bio}
      />
    );

    await waitFor(() => {
      expect(queryByText(bio)).toBeInTheDocument();
    });
  });
});
