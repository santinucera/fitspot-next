/**
 *
 *
 * <ExpertInfo />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Text, Flex } from "theme-ui";
import { FunctionComponent, Fragment } from "react";

interface ExpertInfoProps {
  avatar: string | null;
  firstName: string | null;
  lastName: string | null;
  activityName: string | null;
  bio: string | null;
}

const ExpertInfo: FunctionComponent<ExpertInfoProps> = ({
  avatar,
  firstName,
  lastName,
  activityName,
  bio,
}) => {
  return (
    <Fragment>
      <Flex
        sx={{
          zIndex: 1,
          position: "relative",
          color: "white",
          bg: "primary",
          height: 240,
          p: 5,
          flexDirection: "column",
          flex: "1 1 0%",
          clipPath:
            "polygon(20% 0%, 100% 0, 100% 90%, 80% 100%, 0 100%, 0% 10%)",
          "&::before": {
            content: "''",
            width: "100%",
            height: "100%",
            backgroundImage: avatar ? `url(${avatar})` : "none",
            backgroundSize: "cover",
            backgroundPosition: "center",
            position: "absolute",
            top: 0,
            left: 0,
            opacity: 0.8,
            zIndex: -1,
          },
        }}
      >
        <Box mt="auto">
          <Heading as="h2">
            {firstName} {lastName?.charAt(0)}.
          </Heading>
          <Heading as="h3">{activityName}</Heading>
        </Box>
      </Flex>
      <Text
        as="p"
        style={{ textDecorationColor: "black", whiteSpace: "break-spaces" }}
        pt={5}
      >
        {bio}
      </Text>
    </Fragment>
  );
};

export default ExpertInfo;
