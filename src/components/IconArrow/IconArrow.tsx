/**
 *
 *
 * <IconArrow />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconArrowProps {
  size?: number;
  className?: string;
  direction?: "left" | "right" | "up" | "down";
}

const directions: Record<string, number> = {
  left: 0,
  right: 180,
  up: 90,
  down: -90,
};

const IconArrow: FunctionComponent<IconArrowProps> = ({
  size,
  className,
  direction = "left",
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
        transform: `rotate(${directions[direction]}deg)`,
      }}
      className={className}
    >
      <path
        d="M7.613.21l.094.083 6 6a1 1 0 01.083 1.32l-.083.094-6 6a1 1 0 01-1.497-1.32l.083-.094L10.584 8H1a1 1 0 01-.125-1.992L1 6h9.584L6.293 1.707A1 1 0 016.21.387l.083-.094A1 1 0 017.613.21z"
        fill="currentColor"
        transform="translate(5 5) rotate(-180 7 7)"
      />
    </svg>
  );
};

IconArrow.defaultProps = {
  size: 24,
};

export default IconArrow;
