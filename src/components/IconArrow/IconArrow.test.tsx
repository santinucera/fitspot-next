/**
 *
 *
 * Tests for <IconArrow />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconArrow from "./IconArrow";

describe("<IconArrow />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconArrow />);
    expect(spy).not.toHaveBeenCalled();
  });
});
