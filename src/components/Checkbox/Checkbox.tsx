/**
 *
 *
 * <Checkbox />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { withStyles } from "@material-ui/core";
import CB, { CheckboxProps } from "@material-ui/core/Checkbox";
import { Colors } from "styles";

const Checkbox = withStyles({
  root: {
    color: Colors.primary,
    "&$checked": {
      color: Colors.primary,
    },
  },
  checked: {},
})((props: CheckboxProps) => <CB color="default" {...props} />);

export default Checkbox;
