/**
 *
 *
 * Tests for <Checkbox />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Checkbox from "./Checkbox";

describe("<Checkbox />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Checkbox />);
    expect(spy).not.toHaveBeenCalled();
  });
});
