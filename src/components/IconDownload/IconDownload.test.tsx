/**
 *
 *
 * Tests for <IconDownload />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconDownload from "./IconDownload";

describe("<IconDownload />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconDownload />);
    expect(spy).not.toHaveBeenCalled();
  });
});
