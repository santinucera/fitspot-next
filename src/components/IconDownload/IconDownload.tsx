/**
 *
 *
 * <IconDownload />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconDownloadProps {
  size?: number;
  className?: string;
}

const IconDownload: FunctionComponent<IconDownloadProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 14 15"
      fill="none"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M11.8139 9.375V11.875C11.8139 12.2065 11.6958 12.5245 11.4856 12.7589C11.2754 12.9933 10.9903 13.125 10.693 13.125H2.84651C2.54922 13.125 2.26411 12.9933 2.0539 12.7589C1.84368 12.5245 1.72559 12.2065 1.72559 11.875V9.375"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.9668 6.25L6.76911 9.375L9.57142 6.25"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.76953 9.375V1.875"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default IconDownload;
