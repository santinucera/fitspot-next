/**
 *
 *
 * <Page />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Heading, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import IconArrow from "components/IconArrow";

interface PageProps {
  title?: string;
  as?: any;
  fontSize?: number;
  backTo?: string | null;
}

const Page: FunctionComponent<PageProps> = ({
  as,
  title,
  children,
  backTo,
}) => {
  return (
    <Box>
      <Flex sx={{ alignItems: "center" }}>
        {title && (
          <Heading
            as={as}
            variant={`text.${as}`}
            sx={{ m: 0 }}
            data-testid="page-title"
          >
            {title}
          </Heading>
        )}
        {backTo && (
          <Link
            to={backTo}
            sx={{ variant: "buttons.secondary" }}
            data-testid="go-back-button"
          >
            <IconArrow sx={{ mr: 3 }} /> Go Back
          </Link>
        )}
      </Flex>
      <Box py={[7, 8]}>{children}</Box>
    </Box>
  );
};

Page.defaultProps = {
  as: "h1",
  fontSize: 5,
  backTo: null,
};

export default Page;
