/**
 *
 *
 * Tests for <Page />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Page from "./Page";

describe("<Page />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Page as={`h2`} title={`Page Title`} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Page Title";
    const { getByRole } = render(<Page as={`h2`} title={`Page Title`} />);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
