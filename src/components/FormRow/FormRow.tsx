/*
 *
 *
 * FormRow
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Text, Flex, Divider } from "theme-ui";
import { FunctionComponent } from "react";
import H4 from "components/H4";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface FormRowProps {
  title: string;
  content: string;
  action: React.ReactElement;
}

const FormRow: FunctionComponent<FormRowProps> = ({
  title,
  content,
  action,
}) => {
  return (
    <Box>
      <Box sx={{ py: 3 }}>
        <Flex sx={{ flexDirection: ["column", "row"] }}>
          <Box sx={{ pr: [0, 7] }}>
            <H4>{title}</H4>
            <Text>{content}</Text>
          </Box>
          <Flex
            sx={{
              mt: [4, null],
              ml: "auto",
              justifyContent: "center",
              alignItems: "center",
              flex: "none",
              width: ["100%", "auto"],
            }}
          >
            {action}
          </Flex>
        </Flex>
      </Box>
      <Divider />
    </Box>
  );
};

export default FormRow;
