/**
 *
 *
 * Tests for <FormRow />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import FormRow from "./FormRow";

describe("<FormRow />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<FormRow title="row" content="row content" action={<div></div>} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
