/**
 *
 *
 * Tests for <Alert />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Alert from "./Alert";

describe("<Alert />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Alert
        onClose={() => {}}
        icon={<div></div>}
        backgroundColor="light-yellow"
        borderColor="yellow"
        secondaryTitle="SecondaryTitle"
        title="Title"
      />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render title, subtitle and icon", () => {
    const title = "Title";
    const secondaryTitle = "SecondaryTitle";
    const { getByText, getByTestId } = render(
      <Alert
        onClose={() => {}}
        icon={<div data-testid="data"></div>}
        backgroundColor="light-yellow"
        borderColor="yellow"
        secondaryTitle={secondaryTitle}
        title={title}
      />
    );
    expect(getByText(title)).toBeInTheDocument();
    expect(getByText(secondaryTitle)).toBeInTheDocument();
    expect(getByTestId("data")).toBeInTheDocument();
  });
});
