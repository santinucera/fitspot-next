/**
 *
 *
 * <Alert />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, Flex, Box } from "theme-ui";
import { FunctionComponent, ReactNode } from "react";
import H2 from "components/H2";
import IconX from "components/IconX";

interface AlertProps {
  title: string;
  onClose: () => void;
  backgroundColor: string;
  borderColor: string;
  secondaryTitle: string;
  icon: ReactNode;
}

const Alert: FunctionComponent<AlertProps> = ({
  title,
  children,
  onClose,
  backgroundColor,
  borderColor,
  secondaryTitle,
  icon,
}) => {
  return (
    <Flex
      sx={{
        height: ["auto", 75],
        px: [3, 6],
        py: [2, 3],
        borderColor,
        backgroundColor,
        borderWidth: 1,
        borderStyle: "solid",
        alignItems: "center",
        flexDirection: "row",
        mb: 2,
      }}
    >
      <Box sx={{ display: ["none", "flex"], mr: 6 }}>{icon}</Box>
      <Flex
        sx={{
          flexDirection: ["column", "row"],
          alignItems: ["flex-start", "center"],
          justifyContent: "space-between",
          height: "100%",
          width: "100%",
        }}
      >
        <Flex
          sx={{
            flexDirection: "column",
            justifyContent: "space-between",
            height: "100%",
            width: "100%",
          }}
        >
          <Heading
            as="h3"
            sx={{
              fontSize: 13,
            }}
          >
            {secondaryTitle}
          </Heading>
          <H2
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              mb: 0,
            }}
          >
            {title}
          </H2>
        </Flex>
        {children}
      </Flex>
      <Flex as="button" sx={{ cursor: "pointer" }} onClick={onClose}>
        <IconX sx={{ ml: 4 }} />
      </Flex>
    </Flex>
  );
};

export default Alert;
