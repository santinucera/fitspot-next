/**
 *
 *
 * Tests for <SessionTime />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import dayjs from "dayjs";
import SessionTime from "./SessionTime";

describe("<SessionTime />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<SessionTime startDate={dayjs()} endDate={dayjs()} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const date = dayjs();
    const { getByText } = render(
      <SessionTime startDate={date} endDate={date} />
    );
    expect(
      getByText(
        `${date.format("MMMM DD")}, ${date.format("h:mma")} - ${date.format(
          "h:mma"
        )}`
      )
    ).toBeInTheDocument();
  });
});
