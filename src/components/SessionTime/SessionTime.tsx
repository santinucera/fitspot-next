/**
 *
 *
 * <SessionTime />
 *
 *
 */

import React, { Fragment, FunctionComponent } from "react";
import { Dayjs } from "dayjs";

interface SessionTimeProps {
  startDate: Dayjs;
  endDate: Dayjs;
}

const SessionTime: FunctionComponent<SessionTimeProps> = ({
  startDate,
  endDate,
}) => {
  return (
    <Fragment>{`${startDate.format("MMMM DD")}, ${startDate.format(
      "h:mma"
    )} - ${endDate.format("h:mma")}`}</Fragment>
  );
};

export default SessionTime;
