/**
 *
 *
 * Tests for <IconAvatar />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconAvatar from "./IconAvatar";

describe("<IconAvatar />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconAvatar />);
    expect(spy).not.toHaveBeenCalled();
  });
});
