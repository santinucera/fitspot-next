/**
 *
 *
 * <IconAvatar />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconAvatarProps {
  size?: number;
  className?: string;
}

const IconAvatar: FunctionComponent<IconAvatarProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        d="M11 0c2.39 0 4.158.612 5.247 1.819 1.29 1.422 1.133 3.135 1.126 3.208l-.48 3.355c.091.242.135.506.135.77 0 .752-.36 1.51-1.052 1.705-.103 1.269-.81 2.332-1.69 3.117l5.052 1.532c1.577.59 2.662 1.418 2.662 3.142V22H0v-3.352c0-1.724 1.085-2.551 2.636-3.134l3.458-1.049 1.617-.491c-.88-.789-1.588-1.848-1.69-3.117-.693-.194-1.053-.953-1.053-1.705 0-.264.044-.528.136-.77l-.477-3.34c-.01-.088-.165-1.8 1.122-3.223C6.842.612 8.609 0 11 0zm2.753 15c-.974.45-2.012.697-2.668.697-.655 0-1.698-.247-2.671-.697L7 15.289 10.96 17 15 15.253zM11.002 4c-.36.643-1.039 1.562-3.645 1.623l-.38 2.11c-.032.18-.174.306-.34.306-.267 0-.43.196-.523.437l-.017.042c-.07.203-.097.429-.097.59 0 .367.135 1.064.637 1.064.19 0 .346.172.346.383 0 1.443.834 2.626 1.786 3.404.003 0 .003.003.007.003a.685.685 0 00.087.073c.782.617 1.626.965 2.139.965.512 0 1.357-.348 2.139-.965 0 0 .003-.008.007-.004.027-.023.051-.042.08-.069.003 0 .003-.003.006-.003.952-.778 1.786-1.96 1.786-3.404 0-.21.153-.383.347-.383.501 0 .633-.697.633-1.064 0-.157-.024-.376-.087-.575a6.639 6.639 0 00-.031-.069c-.093-.233-.252-.425-.515-.425-.167 0-.308-.126-.34-.306l-.384-2.11c-2.603-.06-3.281-.98-3.641-1.623z"
        fill="currentColor"
        fillRule="evenodd"
        transform="translate(1 3)"
      />
    </svg>
  );
};

IconAvatar.defaultProps = {
  size: 24,
};

export default IconAvatar;
