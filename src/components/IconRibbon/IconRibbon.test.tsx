/**
 *
 *
 * Tests for <IconRibbon />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconRibbon from "./IconRibbon";

describe("<IconRibbon />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconRibbon />);
    expect(spy).not.toHaveBeenCalled();
  });
});
