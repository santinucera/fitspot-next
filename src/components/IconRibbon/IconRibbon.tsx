/**
 *
 *
 * <IconRibbon />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconRibbonProps {
  size?: number;
  className?: string;
}

const IconRibbon: FunctionComponent<IconRibbonProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <path
        d="M8 0a8 8 0 014.861 14.354l1.13 8.514a1 1 0 01-1.41 1.04l-.095-.05L8 21.165l-4.486 2.691A1 1 0 012 22.975l.009-.107 1.13-8.513A8 8 0 018 0zm2.986 15.424A7.978 7.978 0 018 16a7.978 7.978 0 01-2.985-.575l-.751 5.65 3.222-1.933a1 1 0 01.906-.062l.122.062 3.221 1.933zM8 2a6 6 0 103.19 11.082.778.778 0 01.137-.089A6 6 0 008 2z"
        transform="translate(4 0)"
        fill="currentColor"
      />
    </svg>
  );
};

IconRibbon.defaultProps = {
  size: 24,
};

export default IconRibbon;
