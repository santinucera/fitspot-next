/**
 *
 *
 * <Carousel />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Grid, Box, Text } from "theme-ui";
import {
  FunctionComponent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { Link } from "react-router-dom";
import IconBoxArrow from "components/IconBoxArrow";
import H2 from "components/H2";
import ScrollIndicator from "components/ScrollIndicator";

interface CarouselProps {
  title?: string;
  count?: number;
  seeAllLink?: string;
}

/**
 * Call callback after scroll animation completes.
 */
const scrollStop = (el: HTMLDivElement, callback: () => void) => {
  let isScrolling: number;
  el?.addEventListener(
    "scroll",
    () => {
      window.clearTimeout(isScrolling);
      isScrolling = window.setTimeout(callback, 66);
    },
    false
  );
};

const getClosestEl = (children: HTMLCollection | undefined) => {
  let current = null;
  if (!children) return null;
  for (const el of Array.from(children)) {
    const l = el.getBoundingClientRect().left;
    if (l > 0) {
      current = el;
      break;
    }
  }
  return current;
};

type Directions = "forward" | "back";

const Carousel: FunctionComponent<CarouselProps> = ({
  title,
  count = 0,
  children,
  seeAllLink,
  ...props
}) => {
  const elRef = useRef<Element | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);
  const isScrollingRef = useRef<boolean>(false);
  const [isStart, setIsStart] = useState<boolean>(true);
  const [isEnd, setIsEnd] = useState<boolean>(false);

  const [scrollProgress, setScrollProgress] = useState(0);

  const handleArrows = useCallback(
    (direction: Directions) => () => {
      elRef.current = getClosestEl(ref.current?.children);

      if (!ref.current || !elRef.current || isScrollingRef.current) return;

      // Do not allow user to scroll past beginning
      if (isStart && direction === "back") {
        isScrollingRef.current = false;
        return;
      }

      // Do not allow user to scroll past end
      if (isEnd && direction === "forward") {
        isScrollingRef.current = false;
        return;
      }

      const rec = elRef.current.getBoundingClientRect();

      const pos = direction === "back" ? -rec.width : rec.width;

      isScrollingRef.current = true;
      ref.current?.scrollBy({ left: pos, top: 0, behavior: "smooth" });
    },
    [isEnd, isStart]
  );

  useEffect(() => {
    if (!ref.current) return;
    scrollStop(ref.current, () => {
      isScrollingRef.current = false;
      const scrollLeft = ref.current?.scrollLeft;
      setIsStart(scrollLeft === 0);
      setIsEnd(
        ref.current
          ? Math.ceil(scrollLeft!) >=
              ref.current.scrollWidth - ref.current.offsetWidth
          : false
      );
      /**
       * Gets the closest element and finds the index to set the progress indicator
       */
      elRef.current = getClosestEl(ref.current?.children);
      if (elRef.current && elRef.current.parentNode) {
        const currentIndex = Array.from(
          elRef.current.parentNode.children
        ).indexOf(elRef.current);
        setScrollProgress(currentIndex);
      }
    });
  }, [count]);

  return (
    <Box
      sx={{
        width: "100%",
        minHeight: 280,
        overflow: "hidden",
        bg: "white",
      }}
      {...props}
    >
      <Flex
        sx={{
          mb: 4,
          alignItems: "center",
          justifyContent: ["space-between", "flex-start"],
        }}
      >
        {title && (
          <H2 role="heading" sx={{ fontSize: 4, mb: 0, mr: 6 }}>
            {title}
          </H2>
        )}
        {seeAllLink && (
          <Link
            sx={{
              display: ["flex", "none"],
              alignItems: "center",
              justifyContent: "center",
              color: "primary",
              fontWeight: 700,
              mr: 3,
            }}
            to={seeAllLink}
            data-testid="see-all-link"
          >
            <Text>See all</Text>
          </Link>
        )}
        <button
          type="button"
          data-testid="carousel-left"
          sx={{
            height: 40,
            display: ["none", "flex"],
            mr: 3,
            padding: 0,
            "&:disabled": {
              opacity: "30%",
            },
          }}
          disabled={isStart}
          onClick={handleArrows("back")}
        >
          <IconBoxArrow direction="left" />
        </button>
        <button
          type="button"
          data-testid="carousel-right"
          sx={{
            height: 40,
            display: ["none", "flex"],
            padding: 0,
            "&:disabled": {
              opacity: "30%",
            },
          }}
          disabled={isEnd}
          onClick={handleArrows("forward")}
        >
          <IconBoxArrow direction="right" />
        </button>
      </Flex>
      <Grid
        data-testid="carousel-container"
        ref={ref}
        sx={{
          gap: 4,
          gridAutoFlow: "column",
          width: "100%",
          minHeight: 280,
          flexDirection: "row",
          justifyContent: "flex-start",
          alignItems: "stretch",
          flex: "1 1 100%",
          overflowX: "auto",
          scrollSnapType: "x mandatory",
          "::-webkit-scrollbar": {
            display: "none",
          },
          scrollbarWidth: "none",
          "> *": {
            flex: "1 1 100%",
            scrollSnapAlign: "start",
          },
        }}
      >
        {children}
      </Grid>
      <ScrollIndicator count={count} scrollProgress={scrollProgress} />
    </Box>
  );
};

export default Carousel;
