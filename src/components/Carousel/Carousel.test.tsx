/**
 *
 *
 * Tests for <Carousel />
 *
 *
 */

import React from "react";
import Carousel from "./Carousel";
import { renderWithRouter } from "test-utils/render-with-router";
import { screen } from "@testing-library/react";

const { getByRole, getByTestId, queryByTestId } = screen;

describe("<Carousel />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    renderWithRouter(<Carousel seeAllLink={"/link"} title="some string" />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a title and see all link", () => {
    const value = "some string";
    renderWithRouter(<Carousel seeAllLink={"/link"} title={value} />);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
    expect(getByTestId("see-all-link")).toBeInTheDocument();
  });

  it("should not render see all link if seeAllLink is not provided", () => {
    renderWithRouter(<Carousel title="some string" />);
    expect(queryByTestId("see-all-link")).toBeNull();
  });
});
