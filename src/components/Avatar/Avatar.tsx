/**
 *
 *
 * <Avatar />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import IconAvatar from "components/IconAvatar";

interface AvatarProps {
  url: any;
  size?: number;
}

const Avatar: FunctionComponent<AvatarProps> = ({ url, size, ...props }) => {
  return (
    <Box
      sx={{
        width: size,
        height: size,
        color: "blue",
        borderRadius: 9999,
        backgroundPosition: "center",
        backgroundSize: "cover",
        flex: "none",
        backgroundImage: url ? `url(${url})` : "none",
        backgroundColor: "white",
      }}
      as={url ? "div" : IconAvatar}
      {...props}
    />
  );
};

Avatar.defaultProps = {
  size: 24,
};

export default Avatar;
