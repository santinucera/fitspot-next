/**
 *
 *
 * Tests for <Avatar />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Avatar from "./Avatar";

describe("<Avatar />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Avatar url="https://images.generated.photos/qmdENySIv23bkva-PxTHsoxVbZQdB1Wka0ZPcH5shHY/rs:fit:512:512/Z3M6Ly9nZW5lcmF0/ZWQtcGhvdG9zL3Yy/XzAzMDE4MzAuanBn.jpg" />
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
