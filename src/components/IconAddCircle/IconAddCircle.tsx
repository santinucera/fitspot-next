/**
 *
 *
 * <IconAddCircle />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconAddCircleProps {}

const IconAddCircle: FunctionComponent<IconAddCircleProps> = () => {
  return (
    <svg width={36} height={36} viewBox="0 0 36 36" fill="none">
      <path
        d="M18 34.667c9.205 0 16.667-7.462 16.667-16.667 0-9.205-7.462-16.667-16.667-16.667C8.795 1.333 1.333 8.795 1.333 18c0 9.205 7.462 16.667 16.667 16.667zM18 11.333v13.334M11.333 18h13.334"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default IconAddCircle;
