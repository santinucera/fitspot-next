/**
 *
 *
 * Tests for <IconAddCircle />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconAddCircle from "./IconAddCircle";

describe("<IconAddCircle />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconAddCircle />);
    expect(spy).not.toHaveBeenCalled();
  });
});
