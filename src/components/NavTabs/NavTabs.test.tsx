/**
 *
 *
 * Tests for <NavTabs />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import NavTabs from "./NavTabs";

describe("<NavTabs />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<NavTabs items={[]} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
