/**
 *
 *
 * <NavTabs />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { NavLink as RouterNavLink } from "react-router-dom";

interface NavItems {
  to: string;
  label: string;
}

interface NavTabsProps {
  items: Array<NavItems>;
}

const NavTabs: FunctionComponent<NavTabsProps> = ({ items }) => {
  return (
    <Flex
      sx={{
        mb: 5,
        flexWrap: "wrap",
        borderBottom: 2,
        borderColor: "gray",
        borderStyle: "solid",
      }}
    >
      {items.map(({ to, label }) => (
        <Flex key={to}>
          <RouterNavLink
            end
            to={to}
            sx={{
              fontSize: 3,
              alignItems: "center",
              textDecoration: "none",
              px: 6,
              py: 1,
              color: "dark-gray",
              fontWeight: 400,
              top: "2px",
              "&.active": {
                color: "primary",
                borderBottom: 2,
                borderColor: "primary",
                borderStyle: "solid",
                fontWeight: 700,
              },
            }}
            css={{
              position: "relative",
            }}
          >
            {label}
          </RouterNavLink>
        </Flex>
      ))}
    </Flex>
  );
};

export default NavTabs;
