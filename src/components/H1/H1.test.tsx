/**
 *
 *
 * Tests for <H1 />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import H1 from "./H1";

describe("<H1 />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<H1 />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Heading H1";
    const { getByRole } = render(<H1>Heading H1</H1>);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
