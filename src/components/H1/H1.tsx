/**
 *
 *
 * <H1 />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, HeadingProps } from "theme-ui";
import { FunctionComponent } from "react";

const H1: FunctionComponent<HeadingProps> = (props) => {
  return <Heading as="h1" variant="text.h1" {...props} />;
};

export default H1;
