/**
 *
 *
 * Tests for <IconSubtractCircle />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconSubtractCircle from "./IconSubtractCircle";

describe("<IconSubtractCircle />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconSubtractCircle />);
    expect(spy).not.toHaveBeenCalled();
  });
});
