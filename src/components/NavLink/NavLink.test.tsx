/**
 *
 *
 * Tests for <NavLink />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import NavLink from "./NavLink";

const Icon = () => <div>icon</div>;

describe("<NavLink />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<NavLink to={"/"} icon={Icon} />, { wrapper: MemoryRouter });
    expect(spy).not.toHaveBeenCalled();
  });
});
