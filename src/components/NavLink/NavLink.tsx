/**
 *
 *
 * <NavLink />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { ElementType, FunctionComponent } from "react";
import { NavLink as RouterNavLink } from "react-router-dom";
import { alpha } from "@theme-ui/color";

export interface NavLinkProps {
  icon: ElementType;
  as?: ElementType;
  target?: string;
  href?: string;
  to?: string;
  end?: boolean;
  dark?: boolean;
}

const NavLink: FunctionComponent<NavLinkProps> = ({
  children,
  icon,
  as,
  dark,
  ...props
}) => {
  const Icon = icon;
  return (
    <Flex
      as={as || RouterNavLink}
      {...props}
      sx={{
        alignItems: "center",
        textDecoration: "none",
        p: 3,
        color: "dark-gray",
        "&:hover": {
          color: dark ? "white" : "primary",
          fontWeight: "600",
          "> div": {
            color: "purple",
          },
        },
        "&.active": {
          color: dark ? "white" : "primary",
          fontWeight: "600",
        },
        "&.active > div": {
          color: "purple",
          bg: alpha("purple", 0.2),
          fontWeight: "heading",
        },
      }}
    >
      <Flex sx={{ mr: 3, p: 4, borderRadius: 9999, flex: "none" }}>
        <Icon size={24} />
      </Flex>
      {children}
    </Flex>
  );
};

export default NavLink;
