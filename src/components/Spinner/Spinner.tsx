/**
 *
 *
 * <Spinner />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box, Flex } from "theme-ui";
import { FunctionComponent } from "react";
import { keyframes } from "@emotion/core";

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(359deg);
  }
`;

interface SpinnerProps {}

const Spinner: FunctionComponent<SpinnerProps> = () => {
  return (
    <Flex
      sx={{
        py: 6,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        data-testid="spinner"
        sx={{
          borderRadius: "100%",
          border: "2px solid #eee",
          borderTopColor: "blue",
          height: 40,
          width: 40,
        }}
        css={{
          animation: `${rotation} 0.5s ease infinite`,
        }}
      />
    </Flex>
  );
};

export default Spinner;
