/**
 *
 *
 * Tests for <Spinner />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Spinner from "./Spinner";

describe("<Spinner />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Spinner />);
    expect(spy).not.toHaveBeenCalled();
  });
});
