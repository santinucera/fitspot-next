/**
 *
 *
 * <Select />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import UISelect from "@material-ui/core/Select";
import { Controller } from "react-hook-form";
import { Box } from "@material-ui/core";

interface SelectProps {
  control: any;
  label: string;
  name: string;
  options?: {
    value: string | number;
    label: string | number;
  }[];
  className?: string;
}

const Select: FunctionComponent<SelectProps> = ({
  control,
  name,
  label,
  options,
  className,
}) => {
  return (
    <Box
      mb={3}
      sx={{
        "label.Mui-focused": {
          color: "blue",
        },
        input: {
          borderColor: "blue",
        },
        "& .MuiInputBase-root": {
          color: "primary",
        },
      }}
      className={className}
    >
      <FormControl
        fullWidth
        variant="outlined"
        sx={{
          "& label.Mui-focused": {
            color: "blue",
          },
          "& .MuiOutlinedInput-root": {
            "& fieldset": {
              borderColor: "dark-gray",
            },
            "&:hover fieldset": {
              borderColor: "blue",
            },
            "&.Mui-focused fieldset": {
              borderColor: "blue",
            },
          },
        }}
      >
        <InputLabel id="demo-simple-select-outlined-label">{label}</InputLabel>
        <Controller
          as={
            <UISelect
              labelId="demo-simple-select-outlined-label"
              label={label}
              fullWidth
            >
              <MenuItem value="" disabled>
                Placeholder
              </MenuItem>
              {options?.map(({ value, label }) => (
                <MenuItem key={value} value={value}>
                  {label}
                </MenuItem>
              ))}
            </UISelect>
          }
          name={name}
          control={control}
        />
      </FormControl>
    </Box>
  );
};

export default Select;
