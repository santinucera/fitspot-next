/**
 *
 *
 * Tests for <Select />
 *
 *
 */

import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Select from "./Select";
import { useForm } from "react-hook-form";

describe("<Select />", () => {
  it("should not log errors in console", async () => {
    const spy = jest.spyOn(global.console, "error");
    const El = () => {
      const { control } = useForm({
        defaultValues: {
          gender: 1,
        },
      });
      return (
        <div>
          <Select
            control={control}
            label="Select"
            name="gender"
            options={[
              { value: 1, label: "Option One" },
              { value: 2, label: "Option Two" },
            ]}
          />
        </div>
      );
    };
    render(<El />);
    fireEvent.mouseDown(screen.getByText("Option One"));
    await screen.findByText("Option Two");
    fireEvent.mouseDown(screen.getByText("Option Two"));
    expect(screen.getByText("Option Two")).toBeInTheDocument();
    expect(spy).not.toHaveBeenCalled();
  });
});
