/**
 *
 *
 * Tests for <IconX />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconX from "./IconX";

describe("<IconX />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconX />);
    expect(spy).not.toHaveBeenCalled();
  });
});
