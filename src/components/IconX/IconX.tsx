/**
 *
 *
 * <IconX />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconXProps {
  size?: number;
  className?: string;
}

const IconX: FunctionComponent<IconXProps> = ({ size, className }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{ width: size, height: size }}
      className={className}
    >
      <path
        fill="currentColor"
        transform="translate(5 5)"
        d="M1.613.21l.094.083L7 5.585 12.293.293a1 1 0 011.497 1.32l-.083.094L8.415 7l5.292 5.293a1 1 0 01-1.32 1.497l-.094-.083L7 8.415l-5.293 5.292a1 1 0 01-1.497-1.32l.083-.094L5.585 7 .293 1.707A1 1 0 011.613.21z"
      />
    </svg>
  );
};

IconX.defaultProps = {
  size: 24,
};

export default IconX;
