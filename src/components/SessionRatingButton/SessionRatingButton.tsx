/*
 *
 *
 * SessionRatingButton
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Button, Text } from "theme-ui";
import { FC, useCallback } from "react";
import { Dayjs } from "dayjs";
import { useModal } from "containers/Modal/Modal";
import RateSessionsModal from "containers/RateSessionsModal";
import { Rating } from "@material-ui/lab";
import StarBorderIcon from "@material-ui/icons/StarBorder";

/**
 * ############################################################################
 *
 * Components
 *
 * ############################################################################
 */
interface SessionRatingButtonProps {
  ratings: {
    ratingPoints: number | null;
    ratingStreamQuality: number | null;
    ratingTrainer: number | null;
    ratingLocation: number | null;
  };
  avatar: string;
  sessionName: string;
  sessionId: number;
  isLiveSession: boolean;
  startDate: Dayjs;
  endDate: Dayjs;
  onSuccess: () => void;
  onError: () => void;
}

const SessionRatingButton: FC<SessionRatingButtonProps> = ({
  ratings: { ratingPoints, ratingStreamQuality, ratingTrainer, ratingLocation },
  avatar,
  sessionName,
  sessionId,
  isLiveSession,
  startDate,
  endDate,
  onSuccess,
  onError,
}) => {
  const { setModal } = useModal();

  const hasRatedSession =
    ratingPoints !== null ||
    ratingStreamQuality !== null ||
    ratingTrainer !== null ||
    ratingLocation !== null;

  const handleSessionRating = useCallback(() => {
    setModal(
      <RateSessionsModal
        ratings={{
          ratingPoints,
          ratingStreamQuality,
          ratingTrainer,
          ratingLocation,
        }}
        session={{
          id: sessionId,
          name: sessionName,
          trainerAvatar: {
            url: avatar,
          },
          startDate,
          endDate,
          liveSession: isLiveSession,
        }}
        mutationConfig={{
          onSuccess,
          onError,
        }}
      />
    );
  }, [
    ratingPoints,
    ratingStreamQuality,
    ratingTrainer,
    ratingLocation,
    avatar,
    sessionName,
    sessionId,
    isLiveSession,
    startDate,
    endDate,
    onSuccess,
    onError,
    setModal,
  ]);

  return (
    <Flex
      data-testid="session-rating-button"
      onClick={handleSessionRating}
      sx={{ flexDirection: "column", alignItems: "flex-end" }}
    >
      <Button sx={{ width: ["100%", 150] }}>
        {hasRatedSession ? "Edit rating" : "Rate session"}
      </Button>
      {hasRatedSession && (
        <Flex sx={{ alignItems: "center", mt: 2 }}>
          <Text sx={{ fontSize: 1, mr: 2 }}>Your rating</Text>
          <Rating
            name="customized-empty"
            size="small"
            value={ratingPoints || 0}
            readOnly={true}
            sx={{
              ".MuiRating-icon": {
                color: "yellow",
              },
            }}
            emptyIcon={
              <StarBorderIcon sx={{ color: "primary" }} fontSize="inherit" />
            }
          />
        </Flex>
      )}
    </Flex>
  );
};

export default SessionRatingButton;
