/**
 *
 *
 * Tests for <SessionRatingButton />
 *
 *
 */

import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import dayjs from "dayjs";
import ModalProvider from "containers/Modal";
import SessionRatingButton from "./SessionRatingButton";
import {
  setRateSessionPatchResponse,
  setRateSessionResponse,
} from "test-utils/session-service-test-utils";

interface Props {
  rating?: number | null;
  onSuccess?: () => void;
  onError?: () => void;
}

const Component = ({
  rating = null,
  onSuccess = () => {},
  onError = () => {},
}: Props) => (
  <ModalProvider>
    <SessionRatingButton
      ratings={{
        ratingPoints: rating,
        ratingStreamQuality: rating,
        ratingTrainer: rating,
        ratingLocation: rating,
      }}
      sessionName={"Foo session"}
      avatar={""}
      startDate={dayjs()}
      endDate={dayjs()}
      isLiveSession={true}
      sessionId={1}
      onSuccess={onSuccess}
      onError={onError}
    />
  </ModalProvider>
);

const rateSession = async () => {
  const radioButtons = await screen.findAllByRole("radio");
  const btn = await screen.getByTestId("submit-button");
  await waitFor(() => fireEvent.click(radioButtons[2]));
  await waitFor(() => fireEvent.click(radioButtons[7]));
  await waitFor(() => fireEvent.click(btn));
};

describe("<SessionRatingButton />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Component />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render a ratings button for a user who has not rated a session", () => {
    render(<Component />);
    expect(screen.getByText("Rate session")).toBeInTheDocument();
  });

  it("should render a ratings button for a user who has rated a session", () => {
    render(<Component rating={2} />);
    expect(screen.getByText("Edit rating")).toBeInTheDocument();
    expect(screen.getByText("Your rating")).toBeInTheDocument();
    expect(screen.getByLabelText("2 Stars")).toBeInTheDocument();
  });

  it("should open a ratings modal", async () => {
    render(<Component />);
    fireEvent.click(screen.getByText("Rate session"));
    await waitFor(() => {
      expect(screen.getByTestId("modal")).toBeInTheDocument();
    });
  });

  it("should open a ratings modal and successfully rate a session that has not been rated", async () => {
    const onSuccess = jest.fn();
    render(<Component onSuccess={onSuccess} />);
    const button = await screen.getByText(/rate session/i);
    await fireEvent.click(button);
    await waitFor(() => screen.getByTestId("modal"));
    await rateSession();
    await waitFor(() => expect(onSuccess).toHaveBeenCalled());
  });

  it("should open a ratings modal and successfully edit a session rating", async () => {
    const onSuccess = jest.fn();
    render(<Component onSuccess={onSuccess} rating={5} />);
    const button = await screen.getByText(/edit rating/i);
    await fireEvent.click(button);
    await waitFor(() => screen.getByTestId("modal"));
    await rateSession();
    await waitFor(() => expect(onSuccess).toHaveBeenCalled());
  });

  it("should open a ratings modal and handle rating error on rating POST", async () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();
    const onError = jest.fn();
    render(<Component onError={onError} />);
    setRateSessionResponse(500);
    const button = await screen.getByText(/rate session/i);
    await fireEvent.click(button);
    await waitFor(() => screen.getByTestId("modal"));
    await rateSession();
    await waitFor(() => expect(onError).toHaveBeenCalled());
    expect(spy).toHaveBeenCalled();
  });

  it("should open a ratings modal and handle rating error on rating PATCH", async () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();
    const onError = jest.fn();
    render(<Component onError={onError} rating={5} />);
    setRateSessionPatchResponse(500);
    const button = await screen.getByText(/edit rating/i);
    await fireEvent.click(button);
    await waitFor(() => screen.getByTestId("modal"));
    await rateSession();
    await waitFor(() => expect(onError).toHaveBeenCalled());
    expect(spy).toHaveBeenCalled();
  });
});
