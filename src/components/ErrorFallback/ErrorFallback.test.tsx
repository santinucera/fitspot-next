/**
 *
 *
 * Tests for <ErrorFallback />
 *
 *
 */

import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import ErrorFallback from "./ErrorFallback";

const { getByText, getByTestId } = screen;

describe("<ErrorFallback />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ErrorFallback error={new Error()} resetErrorBoundary={() => {}} />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render an error message and call reset handler", () => {
    const fn = jest.fn();
    render(
      <ErrorFallback error={new Error("Boom!")} resetErrorBoundary={fn} />
    );
    expect(getByText("Boom!")).toBeInTheDocument();
    fireEvent.click(getByTestId("error-fallback-button"));
    expect(fn).toHaveBeenCalled();
  });
});
