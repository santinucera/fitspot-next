/**
 *
 *
 * <ErrorFallback />
 *
 *
 */

/** @jsx jsx */
import { jsx, Button, Text, Flex } from "theme-ui";
import { FunctionComponent } from "react";

interface ErrorFallbackProps {
  error: Error;
  resetErrorBoundary: () => void;
}

const ErrorFallback: FunctionComponent<ErrorFallbackProps> = ({
  error,
  resetErrorBoundary,
}) => {
  return (
    <Flex
      role="alert"
      data-testid="error-fallback"
      sx={{
        backgroundColor: "gray",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Flex
        sx={{
          width: 400,
          textAlign: "center",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Text>Something went wrong:</Text>
        <pre
          sx={{
            padding: "10px",
            background: "white",
            margin: "20px 0",
            borderRadius: "2px",
            width: "100%",
            fontFamily: "monospace !important",
          }}
        >
          {error.message}
        </pre>
        <Text sx={{ mb: 4 }}>
          If this error persists please contact support.
        </Text>
        <Button
          data-testid="error-fallback-button"
          onClick={resetErrorBoundary}
        >
          Reload the application
        </Button>
      </Flex>
    </Flex>
  );
};

export default ErrorFallback;
