/**
 *
 *
 * <Switch />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import { Switch as MuiSwitch } from "@material-ui/core";

interface SwitchProps {
  [x: string]: any;
}

const Switch: FunctionComponent<SwitchProps> = (props) => {
  return (
    <Box
      as={MuiSwitch}
      {...props}
      sx={{
        ".MuiSwitch-colorSecondary.Mui-checked:hover": {
          bg: "light-blue-opaque",
        },
        ".MuiSwitch-colorSecondary.Mui-checked": {
          color: "blue",
        },
        ".MuiSwitch-colorSecondary.Mui-checked + .MuiSwitch-track": {
          bg: "blue",
        },
      }}
    />
  );
};

export default Switch;
