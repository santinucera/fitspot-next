/**
 *
 *
 * Tests for <Switch />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Switch from "./Switch";

describe("<Switch />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Switch />);
    expect(spy).not.toHaveBeenCalled();
  });
});
