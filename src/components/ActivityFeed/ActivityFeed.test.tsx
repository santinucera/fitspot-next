/**
 *
 *
 * Tests for <ActivityFeed ?>
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import ActivityFeed from "./ActivityFeed";
import { createActivityFeedModel } from "test-utils/activity-feed-service-test-utils";
import { renderWithRouter } from "test-utils/render-with-router";

describe("<ActivityFeed />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<ActivityFeed feed={[]} />);
    expect(spy).not.toHaveBeenCalled();
  });
  it("should show data from header", () => {
    const feed = createActivityFeedModel();
    const { queryByText } = renderWithRouter(
      <ActivityFeed feed={feed} header={<span>Activity feed</span>} />
    );
    expect(queryByText("Activity feed")).toBeInTheDocument();
  });
  it("should hide date with prop", () => {
    const feed = createActivityFeedModel();
    const date = feed[0].date.fromNow();
    const { queryByText } = renderWithRouter(
      <ActivityFeed feed={feed} showDate={false} />
    );
    expect(queryByText(date)).toBeNull();
  });
});
