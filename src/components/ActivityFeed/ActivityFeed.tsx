/**
 *
 *
 * <ActivityFeed />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex, Box, Card, Text } from "theme-ui";
import { Fragment, FunctionComponent, ReactNode } from "react";
import {
  ActivityFeedModel,
  ActivityFeedEventKey,
  ActivityFeedData,
} from "services/ActivityFeedService";
import Avatar from "components/Avatar";

interface MessagesProps {
  eventKey: ActivityFeedEventKey;
  data: ActivityFeedData;
  name: string;
  showActivityName: boolean;
}

const Messages: FunctionComponent<MessagesProps> = ({
  eventKey,
  data,
  name,
  showActivityName,
}) => {
  switch (eventKey) {
    case "RSVP_SESSION":
      return (
        <Fragment>
          {name} reserved a spot for{" "}
          <span sx={{ fontWeight: 700 }}>{data.name}</span>
        </Fragment>
      );
    case "RSVP_CHALLENGE":
      return (
        <Fragment>
          {name} joined <span sx={{ fontWeight: 700 }}>{data.name}</span>
        </Fragment>
      );
    case "JOINED_SESSION":
      return (
        <Fragment>
          {name} joined the <span sx={{ fontWeight: 700 }}>{data.name}</span>{" "}
          session
        </Fragment>
      );
    case "CHALLENGE_ACTIVITY_COMPLETED":
      return (
        <Fragment>
          {name} scored {data.points} points{" "}
          {showActivityName && (
            <Fragment>
              in <span sx={{ fontWeight: 700 }}>{data.challengeName}</span>
            </Fragment>
          )}
        </Fragment>
      );
    case "CONNECT_WEARABLE":
      return (
        <Fragment>
          {name} connected a wearable device to the{" "}
          <span sx={{ fontWeight: 700 }}>Step Leaderboard</span>
        </Fragment>
      );
    case "JOINED_TEAM":
      return <Fragment>{name} joined the team</Fragment>;
    default:
      return <Fragment>unknown event</Fragment>;
  }
};

interface ActivityFeedProps {
  feed: ActivityFeedModel[];
  header?: ReactNode;
  showDate?: boolean;
  showActivityName?: boolean;
}

const ActivityFeed: FunctionComponent<ActivityFeedProps> = ({
  feed,
  header,
  showDate = true,
  showActivityName = true,
}) => {
  return (
    <Card
      sx={{
        bg: "white",
        p: 6,
      }}
    >
      {header}
      <Flex
        sx={{
          overflow: "auto",
          // TODO: figure out a better way to do this.
          height: "calc(100% - 70px)",
          maxHeight: 350,
          width: "calc(100% + 24px)",
          "::-webkit-scrollbar": {
            width: "4px",
          },
          "::-webkit-scrollbar-track": {
            background: "#f1f1f1",
          },
          "::-webkit-scrollbar-thumb": {
            background: "#999",
          },
          "::-webkit-scrollbar-thumb:hover": {
            background: "#555",
          },
          flex: "0 0 auto",
          flexDirection: "column",
          pr: 5,
        }}
      >
        {feed.map(({ id, date, eventKey, data, avatar, user: { name } }) => (
          <Flex key={id} sx={{ mb: 3, flex: 1, minHeight: "fit-content" }}>
            <Box sx={{ mr: 4, flex: "none" }}>
              <Avatar url={avatar?.url} size={40} />
            </Box>
            <Box>
              {showDate && (
                <Text sx={{ fontSize: 0, color: "dark-gray" }}>
                  {date.fromNow()}
                </Text>
              )}
              <Text>
                <Messages
                  eventKey={eventKey}
                  data={data}
                  name={name}
                  showActivityName={showActivityName}
                />
              </Text>
            </Box>
          </Flex>
        ))}
      </Flex>
    </Card>
  );
};

export default ActivityFeed;
