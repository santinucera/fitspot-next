/**
 *
 *
 * <Input />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";
import { Controller, FieldError } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import { Box } from "@material-ui/core";
import get from "lodash.get";

export interface InputProps {
  control: any;
  label?: string;
  name: string;
  type?: string;
  variant?: "standard" | "filled" | "outlined";
  errors?: FieldError | object;
  [x: string]: any;
}

const Input: FunctionComponent<InputProps> = ({
  control,
  label,
  name,
  type,
  errors,
  variant,
  ...props
}) => {
  const error = get(errors, name);
  return (
    <Box
      mb={3}
      sx={{
        "label.Mui-focused": {
          color: "blue",
        },
        "& .Mui-disabled": {
          color: "dark-gray",
        },
        ".MuiInput-underline:after": {
          borderBottomColor: "blue",
        },
        "& .MuiInputBase-root": {
          color: "primary",
        },
      }}
    >
      <Controller
        as={TextField}
        sx={{
          "& label.Mui-focused": {
            color: "blue",
          },
          "& .MuiOutlinedInput-root.Mui-disabled": {
            "& fieldset": {
              borderStyle: "dotted",
            },
            "&:hover fieldset": {
              borderColor: "dark-gray",
            },
          },
          "& .MuiOutlinedInput-root": {
            "& fieldset": {
              borderColor: "dark-gray",
            },
            "&:hover fieldset": {
              borderColor: "blue",
            },
            "&.Mui-focused fieldset": {
              borderColor: "blue",
            },
          },
        }}
        variant={variant}
        error={error && true}
        helperText={error?.message}
        type={type}
        fullWidth
        label={label}
        name={name}
        control={control}
        InputLabelProps={{ shrink: true }}
        {...props}
      />
    </Box>
  );
};

Input.defaultProps = {
  type: "text",
  errors: {},
  variant: "outlined",
};

export default Input;
