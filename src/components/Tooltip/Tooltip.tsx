/**
 *
 *
 * <Tooltip />
 *
 *
 */

/** @jsx jsx */
import { jsx, Flex } from "theme-ui";
import { FunctionComponent, ReactNode } from "react";
import MUITooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import { Colors } from "styles";

const LightTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: Colors.white,
    color: Colors.primary,
    borderRadius: 6,
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
    padding: 16,
    fontFamily: "Agrandir",
    fontSize: 13,
  },
}))(MUITooltip);

interface TooltipProps {
  text: string;
  children: ReactNode;
}

const Tooltip: FunctionComponent<TooltipProps> = ({ text, children }) => {
  return (
    <LightTooltip title={text} placement="top">
      <Flex>{children}</Flex>
    </LightTooltip>
  );
};

export default Tooltip;
