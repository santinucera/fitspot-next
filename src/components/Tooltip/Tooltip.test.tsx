/**
 *
 *
 * Tests for <Tooltip />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Tooltip from "./Tooltip";

describe("<Tooltip />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Tooltip text="Tooltip">
        <div></div>
      </Tooltip>
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
