/**
 *
 *
 * <Chip />
 *
 *
 */

/** @jsx jsx */
import { jsx, Box } from "theme-ui";
import { FunctionComponent } from "react";
import slugify from "utils/slugify";

interface ChipProps {
  label: string;
}

const Chip: FunctionComponent<ChipProps> = ({
  label,
  ...props // For passing `sx` prop styles
}) => {
  return (
    <Box
      {...props}
      sx={{
        backgroundColor: "dark-green",
        lineHeight: "22px",
        textTransform: "uppercase",
        borderRadius: 6,
        padding: "0 16px",
        fontSize: 11,
        fontWeight: 800,
        color: "white",
      }}
      data-testid={`chip-${slugify("-", label)}`}
    >
      {label}
    </Box>
  );
};

export default Chip;
