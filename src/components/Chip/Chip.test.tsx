/**
 *
 *
 * Tests for <Chip />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import Chip from "./Chip";

describe("<Chip />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<Chip label="some string" sx={{ mt: 10 }} />);
    expect(spy).not.toHaveBeenCalled();
  });
});
