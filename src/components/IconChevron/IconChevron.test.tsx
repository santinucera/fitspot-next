/**
 *
 *
 * Tests for <IconChevron />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconChevron from "./IconChevron";

describe("<IconChevron />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconChevron />);
    expect(spy).not.toHaveBeenCalled();
  });
});
