/**
 *
 *
 * <IconChevron />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconChevronProps {
  size?: number;
  className?: string;
}

const IconChevron: FunctionComponent<IconChevronProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <path
        d="M-.707-.707A1 1 0 01.613-.79l.094.083 6 6a1 1 0 01.083 1.32l-.083.094-6 6a1 1 0 01-1.497-1.32l.083-.094L4.585 6-.707.707A1 1 0 01-.79-.613l.083-.094z"
        transform="translate(10 6)"
        fill="currentColor"
      />
    </svg>
  );
};

IconChevron.defaultProps = {
  size: 24,
};

export default IconChevron;
