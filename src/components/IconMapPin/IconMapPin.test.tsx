/**
 *
 *
 * Tests for <IconMapPin />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import IconMapPin from "./IconMapPin";

describe("<IconMapPin />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<IconMapPin />);
    expect(spy).not.toHaveBeenCalled();
  });
});
