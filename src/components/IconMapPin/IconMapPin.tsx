/**
 *
 *
 * <IconMapPin />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FunctionComponent } from "react";

interface IconMapPinProps {
  size?: number;
  className?: string;
}

const IconMapPin: FunctionComponent<IconMapPinProps> = ({
  size,
  className,
}) => {
  return (
    <svg
      viewBox="0 0 24 24"
      sx={{
        width: size,
        height: size,
      }}
      className={className}
    >
      <path
        fill="currentColor"
        d="M10 0a10 10 0 0110 10c0 3.372-1.802 6.676-4.774 9.813a31.132 31.132 0 01-3.748 3.352l-.506.374-.417.293a1 1 0 01-1.11 0l-.417-.293a31.132 31.132 0 01-4.254-3.726C1.802 16.676 0 13.372 0 10A10 10 0 0110 0zm0 2a8 8 0 00-8 8c0 2.753 1.573 5.636 4.226 8.437a29.155 29.155 0 003.505 3.134l.269.201.521-.395.262-.206a29.155 29.155 0 002.991-2.734C16.427 15.637 18 12.753 18 10a8 8 0 00-8-8zm0 4a4 4 0 110 8 4 4 0 010-8zm0 2a2 2 0 100 4 2 2 0 000-4z"
      />
    </svg>
  );
};

IconMapPin.defaultProps = {
  size: 24,
};

export default IconMapPin;
