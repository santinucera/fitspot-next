/**
 *
 *
 * Tests for <Tabs />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import { Tabs, Tab } from "./Tabs";

describe("<Tabs />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <Tabs>
        <Tab>Im a tab</Tab>
      </Tabs>
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
