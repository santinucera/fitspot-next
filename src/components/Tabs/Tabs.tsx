/**
 *
 *
 * <Tabs />
 *
 *
 */

/** @jsx jsx */
import { jsx } from "theme-ui";
import { FC } from "react";
import {
  Tabs as ReachTabs,
  TabList as ReachTabsList,
  Tab as ReachTab,
  TabPanels as ReachTabPanels,
  TabPanel as ReachTabPanel,
} from "@reach/tabs";
import "@reach/tabs/styles.css";

interface TabsProps {
  onChange?: (index: any) => void;
}

export const TabList: FC = ({ children }) => {
  return (
    <ReachTabsList
      sx={{
        borderBottom: 1,
        borderColor: "gray",
        borderStyle: "solid",
        bg: "transparent",
      }}
    >
      {children}
    </ReachTabsList>
  );
};

export const Tabs: FC<TabsProps> = ({ onChange, children }) => {
  return <ReachTabs onChange={onChange}>{children}</ReachTabs>;
};

export const Tab: FC = ({ children, ...props }) => {
  return (
    <ReachTab
      sx={{
        fontSize: 5,
        py: 3,
        px: 4,
        fontFamily: "body",
        color: "darkGray",
        "&[data-selected]": {
          color: "primary",
          fontWeight: "bold",
          borderBottom: 1,
          borderBottomColor: "primary",
          borderBottomStyle: "solid",
        },
      }}
      {...props}
    >
      {children}
    </ReachTab>
  );
};

export const TabPanels: FC = ({ children }) => {
  return <ReachTabPanels>{children}</ReachTabPanels>;
};

export const TabPanel: FC = ({ children }) => {
  return <ReachTabPanel sx={{ outline: "none" }}>{children}</ReachTabPanel>;
};
