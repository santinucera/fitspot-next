/**
 *
 *
 * <LoadingButton />
 *
 *
 */

/** @jsx jsx */
import { jsx, Button, Spinner } from "theme-ui";
import { FunctionComponent } from "react";

interface LoadingButtonProps {
  isLoading: boolean;
  onClick: () => void;
  variant?: string;
}

const LoadingButton: FunctionComponent<LoadingButtonProps> = ({
  children,
  isLoading,
  onClick,
  variant,
  ...props
}) => {
  return (
    <Button {...props} disabled={isLoading} onClick={onClick} variant={variant}>
      {isLoading && (
        <Spinner
          strokeWidth={2}
          size={22}
          sx={{ color: "white", mr: 2 }}
          mr={2}
          data-testid="loading-indicator"
        />
      )}
      {children}
    </Button>
  );
};

export default LoadingButton;
