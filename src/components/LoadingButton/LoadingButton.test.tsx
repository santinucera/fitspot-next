/**
 *
 *
 * Tests for <LoadingButton />
 *
 *
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import LoadingButton from "./LoadingButton";

const { getByText, getByTestId } = screen;

describe("<LoadingButton />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(
      <LoadingButton isLoading={true} onClick={() => {}} variant="primary" />
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render children when loading", () => {
    const expected = "some string";
    render(
      <LoadingButton isLoading={true} onClick={() => {}} variant="primary">
        {expected}
      </LoadingButton>
    );
    expect(getByTestId("loading-indicator")).toBeInTheDocument();
    expect(getByText(expected)).toBeInTheDocument();
  });
});
