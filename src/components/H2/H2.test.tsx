/**
 *
 *
 * Tests for <H2 />
 *
 *
 */

import React from "react";
import { render } from "@testing-library/react";
import H2 from "./H2";

describe("<H2 />", () => {
  it("should not log errors in console", () => {
    const spy = jest.spyOn(global.console, "error");
    render(<H2 />);
    expect(spy).not.toHaveBeenCalled();
  });

  it("should render myProp", () => {
    const value = "Heading H2";
    const { getByRole } = render(<H2>Heading H2</H2>);
    const expected = new RegExp(value, "i");
    expect(getByRole("heading")).toHaveTextContent(expected);
  });
});
