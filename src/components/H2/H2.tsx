/**
 *
 *
 * <H2 />
 *
 *
 */

/** @jsx jsx */
import { jsx, Heading, HeadingProps } from "theme-ui";
import { FunctionComponent } from "react";

const H2: FunctionComponent<HeadingProps> = (props) => {
  return <Heading as="h2" variant="text.h2" {...props} />;
};

export default H2;
