import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "theme-ui";
import { Global } from "@emotion/core";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import App from "./containers/App";
import * as serviceWorker from "./serviceWorker";
import { globalStyles, theme } from "./styles";
import { BrowserRouter as Router } from "react-router-dom";
import { ReactQueryConfigProvider, ReactQueryConfig } from "react-query";
import { LocalizationProvider } from "@material-ui/pickers";
import DateFnsAdapter from "@material-ui/pickers/adapter/date-fns";
import { ToastProvider } from "hooks/useToast";
import { FlyOutProvider } from "hooks/useFlyOut";
import ModalProvider from "containers/Modal";
import isProduction from "utils/is-production";
import ErrorBoundary from "components/ErrorBoundary";

// Un-comment to mock http requests in development mode (npm start)
// if (process.env.NODE_ENV === "development") {
//   const worker = require("./_mocks/worker").default;
//   worker.start();
// }

const { REACT_APP_SENTRY_DSN } = process.env;

if (isProduction() && REACT_APP_SENTRY_DSN) {
  Sentry.init({
    dsn: REACT_APP_SENTRY_DSN,
    integrations: [new Integrations.BrowserTracing()],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });
}

const queryConfig: ReactQueryConfig = {
  queries: { refetchOnWindowFocus: false, retry: 0 },
};

ReactDOM.render(
  <LocalizationProvider dateAdapter={DateFnsAdapter}>
    <ReactQueryConfigProvider config={queryConfig}>
      <Global styles={globalStyles} />
      <ThemeProvider theme={theme}>
        <ErrorBoundary>
          <Router>
            <ToastProvider>
              <ModalProvider>
                <FlyOutProvider>
                  <App />
                </FlyOutProvider>
              </ModalProvider>
            </ToastProvider>
          </Router>
        </ErrorBoundary>
      </ThemeProvider>
    </ReactQueryConfigProvider>
  </LocalizationProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
