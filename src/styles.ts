/**
 *
 *
 * Theme
 *
 */

import { css } from "@emotion/core";
import emotionReset from "emotion-reset";
import { Theme } from "theme-ui";
import { alpha } from "@theme-ui/color";
// Imported fonts return a relative path as a string, but without
// a leading ".". The path is a location in the dev server or
// build directory, not to the local ./fonts directory.
import AgrandirRegularWoff from "./fonts/agrandir-regular-webfont.woff";
import AgrandirRegularWoff2 from "./fonts/agrandir-regular-webfont.woff2";
import AgrandirBoldWoff from "./fonts/agrandir-textbold-webfont.woff";
import AgrandirBoldWoff2 from "./fonts/agrandir-textbold-webfont.woff2";
import AgrandirTightWoff from "./fonts/agrandir-tight-webfont.woff";
import AgrandirTightWoff2 from "./fonts/agrandir-tight-webfont.woff2";
/**
 *
 * Fonts are defined in index.css. They can be used
 * anywhere in the app with these values.
 *
 * Use Fonts in styled-components like this:
 *
 * ```
 * import { Fonts } from '../../theme';
 *
 * const Foo =  styled.div`
 *   font-family: ${Fonts.AgrandirRegular};
 * `
 * ```
 *
 * Note that you can't destructure an enum.
 */
export enum Fonts {
  Agrandir = "Agrandir",
  AgrandirRegular = "Agrandir-Regular",
  AgrandirBold = "Agrandir-TextBold",
  AgrandirTight = "Agrandir-Tight",
}

export enum Colors {
  primary = "#000037",
  blue = "#577CFF",
  lightBlue = "#E6EBFF",
  lighterBlue = "#F7F8FF",
  lighterBlueOpaque = "rgba(87, 124, 255, 0.2)",
  purple = "#D3ABFF",
  lightPurple = "#F7EFFF",
  green = "#BCE6C2",
  lightGreen = "#e7f5e5",
  darkGreen = "#42cd21",
  forestGreen = "#008935",
  yellow = "#EFCE00",
  lightYellow = "#FDF8D9",
  darkYellow = "#E6C51B",
  red = "#FF4747",
  lightRed = "#FFE4E2",
  lighterRed = "rgba(255, 228, 226, 0.5)",
  lightGray = "#F7F7F7", // Page background
  mediumLightGray = "#F2F2F5", // Between page background and borders, trainer sessions active list item
  gray = "#EDEDED", // Borders, e.g. session chat border, calendar options
  darkGray = "#9999AF", // Main nav links
  darkerGray = "#636485", // Date subheading at top of sessions page
  mediumGray = "#9999AF", // Date subheading at top of sessions page
  white = "#FFF",
}

export const globalStyles = css`
  ${emotionReset}

  @font-face {
    font-family: "${Fonts.Agrandir}";
    src: local("${Fonts.AgrandirRegular}"),
      url(${AgrandirRegularWoff2}) format("woff2"),
      url(${AgrandirRegularWoff}) format("woff");
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: "${Fonts.Agrandir}";
    src: local("${Fonts.AgrandirBold}"),
      url(${AgrandirBoldWoff2}) format("woff2"),
      url(${AgrandirBoldWoff}) format("woff");
    font-weight: 600;
    font-style: normal;
  }

  @font-face {
    font-family: "${Fonts.Agrandir}";
    src: local("${Fonts.AgrandirTight}"),
      url(${AgrandirTightWoff2}) format("woff2"),
      url(${AgrandirTightWoff}) format("woff");
    font-weight: 300;
    font-style: normal;
    font-stretch: condensed;
  }

  *,
  *::after,
  *::before {
    box-sizing: border-box;
    font-family: ${Fonts.Agrandir} !important;
  }

  input:focus,
  select:focus,
  button:focus {
    outline: none !important;
  }

  input:focus {
    outline: none !important;
  }
  input[type="number"]::-webkit-outer-spin-button,
  input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  input[type="number"] {
    -moz-appearance: textfield;
  }
`;

export const theme: Theme = {
  breakpoints: ["768px", "1024px", "1440px", "1680px"],
  colors: {
    text: Colors.primary,
    primary: Colors.primary,
    blue: Colors.blue,
    "light-blue": Colors.lightBlue,
    "lighter-blue": Colors.lighterBlue,
    "light-blue-opaque": Colors.lighterBlueOpaque,
    purple: Colors.purple,
    "light-purple": Colors.lightPurple,
    green: Colors.green,
    "light-green": Colors.lightGreen,
    "dark-green": Colors.darkGreen,
    "forest-green": Colors.forestGreen,
    yellow: Colors.yellow,
    "light-yellow": Colors.lightYellow,
    "dark-yellow": Colors.darkYellow,
    red: Colors.red,
    "light-red": Colors.lightRed,
    "lighter-red": Colors.lighterRed,
    "light-gray": Colors.lightGray,
    "medium-light-gray": Colors.mediumLightGray,
    gray: Colors.gray,
    "medium-gray": Colors.mediumGray,
    "dark-gray": Colors.darkGray,
    "darker-gray": Colors.darkerGray,
    background: Colors.white,
    white: Colors.white,
  },
  space: [0, 4, 8, 12, 16, 18, 24, 32, 52, 64, 128, 256, 512],
  shadows: {
    small: "0 0 2px rgba(0, 0, 0, .125)",
    medium: "0px 1px 3px rgb(0 0 0 / 20%)",
    card: "0 0 2px rgba(0, 0, 0, .125)",
    toast: "0px 4px 10px rgb(0 0 0 / 10%)",
    dropdown: "0 0 10px 3px rgba(0,0,0,0.03)",
  },
  fonts: {
    body: `${Fonts.Agrandir}, sans-serif`,
    heading: `${Fonts.Agrandir}, sans-serif`,
    monospace: "Menlo, monospace",
    fontWeight: "normal",
  },
  fontSizes: [13, 15, 16, 18, 20, 24, 32, 48, 64, 96],
  fontWeights: {
    body: 300,
    heading: 600,
    bold: 600,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.25,
  },
  text: {
    heading: {
      fontFamily: "heading",
      lineHeight: "heading",
      fontWeight: "heading",
    },
    display: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      fontSize: [5, 6, 7],
    },
    body: {
      fontFamily: "body",
      fontWeight: "300",
      lineHeight: "normal",
    },
    caps: {
      textTransform: "uppercase",
      letterSpacing: "0.1em",
    },
    h1: {
      fontSize: [4, 6],
      mb: 3,
    },
    h2: {
      fontSize: [3, 5],
      mb: 2,
    },
    h3: {
      fontSize: [2, 3],
      mb: 2,
    },
    h4: {
      fontSize: [1, 2],
      mb: 2,
      opacity: 0.7,
      fontFamily: "body",
    },
    h5: {
      fontSize: [1],
      lineHeight: "25px",
    },
    light: {
      opacity: 0.8,
    },
  },
  buttons: {
    primary: {
      display: "flex",
      alignItems: "center",
      width: "fit-content",
      color: "white",
      fontFamily: "heading",
      fontWeight: "heading",
      bg: "primary",
      borderRadius: 0,
      py: 3,
      px: 6,
      cursor: "pointer",
      textDecoration: "none",
      justifyContent: "center",
      whiteSpace: "nowrap",
      ":disabled": {
        bg: alpha("primary", 0.1),
        color: alpha("primary", 0.1),
      },
      ":active": {
        bg: alpha("primary", 0.7),
      },
      height: 52,
    },
    secondary: {
      variant: "buttons.primary",
      bg: "white",
      color: "primary",
      borderColor: "dark-gray",
      borderWidth: 1,
      borderStyle: "solid",
      textAlign: "center",
      ":disabled": {
        bg: alpha("primary", 0.1),
        color: "primary",
        borderWidth: 0,
      },
      ":active": {
        bg: "gray",
      },
    },
    underlined: {
      bg: "white",
      color: "primary",
      fontFamily: "heading",
      fontWeight: "heading",
      textDecoration: "underline",
      textDecorationColor: Colors.mediumGray,
      textUnderlinePosition: "under",
      py: 3,
      textAlign: "center",
      ":disabled": {
        bg: alpha("primary", 0.1),
        color: "primary",
        borderWidth: 0,
      },
    },
    danger: {
      variant: "buttons.primary",
      color: "red",
      bg: "light-red",
      borderColor: "red",
      ":hover": {
        bg: "red",
        color: "white",
      },
    },
    light: {
      variant: "buttons.primary",
      bg: "gray",
      borderColor: "gray",
      ":hover": {
        bg: "gray",
      },
    },
  },
  forms: {
    label: {
      fontSize: 1,
      fontWeight: 200,
      color: "dark-gray",
    },
    input: {
      borderTop: 0,
      borderLeft: 0,
      borderRight: 0,
      borderBottom: 1,
      borderBottomStyle: "solid",
      borderColor: "dark-gray",
      borderRadius: 0,
      px: 0,
      py: 3,
      "&:focus": {
        borderColor: "blue",
        outline: "none",
      },
    },
    select: {
      variant: "forms.input",
    },
    textarea: {
      variant: "forms.input",
    },
    slider: {
      bg: "muted",
    },
  },
  links: {
    cta: {
      color: "blue",
      textDecoration: "none",
      fontSize: 6,
      fontWeight: "body",
      "&:hover, &:active": {
        color: "purple",
      },
    },
  },
  styles: {
    // Root styles are applied to the body
    root: {
      fontFamily: "body",
      fontWeight: "body",
      lineHeight: "body",
      fontSize: 15,
      color: "primary",
      button: {
        background: "transparent",
        border: "none",
        cursor: "pointer",
      },
    },
    a: {
      // default Link styles
      color: "blue",
      textDecoration: "none",
      fontSize: 1,
      fontWeight: "body",
      "&:hover, &:active": {
        color: "purple",
      },
    },
  },
};
