/**
 * Component Generator
 */

/* eslint strict: ["off"] */

"use strict";

const listContainerDirectories = require("../utils/listContainerDirectories");
const containerComponentExists = require("../utils/containerComponentExists");

module.exports = {
  description: "Add an unconnected component to a container",
  prompts: [
    {
      type: "list",
      name: "container",
      message: "Which container should this component go in?",
      choices: listContainerDirectories(),
    },
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Button",
      validate: (name, { container }) => {
        return containerComponentExists(container, name)
          ? `A component called ${name} already exists in the ${container} container.`
          : true;
      },
    },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    const directory = "../src/containers/{{container}}";
    const actions = [
      {
        type: "add",
        path: `${directory}/{{properCase name}}.tsx`,
        templateFile: "./component/Component.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/{{properCase name}}.test.tsx`,
        templateFile: "./component/Component.test.tsx.hbs",
        abortOnFail: true,
      },
    ];

    actions.push({
      type: "prettify",
      path: "/components/",
    });

    return actions;
  },
};
