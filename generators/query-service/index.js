/**
 * Component Generator
 */

/* eslint strict: ["off"] */

const listServiceDirectories = require("../utils/listServiceDirectories");

module.exports = {
  description: "Add a query service",
  prompts: [
    {
      type: "list",
      name: "service",
      message: "Which service should this query go in?",
      choices: listServiceDirectories(),
    },
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "GetStuff",
    },
    {
      type: "confirm",
      message: "Use TenSpot's api endpoint as default base url?",
      name: "useBaseURL",
      default: true,
    },
    {
      when: (response) => !response.useBaseURL,
      type: "input",
      name: "baseURL",
      default: "https://api.com/",
      message: "What is the base URL?",
    },
    {
      type: "input",
      name: "url",
      default: "some/endpoint",
      message: "What is the base PATH?",
    },
    {
      type: "input",
      name: "queryKey",
      default: "[KEY_NEEDED]",
      message: "What is the cache key? This should be unique.",
    },
    {
      type: "confirm",
      message: "Does the query require URL params?",
      name: "params",
      default: false,
    },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    const directory = "../src/services/{{properCase service}}";
    const actions = [
      {
        type: "add",
        path: `${directory}/useQuery{{properCase name}}Service.tsx`,
        templateFile: "./query-service/service.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/useQuery{{properCase name}}Service.test.tsx`,
        templateFile: "./query-service/service.test.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: `${directory}/index.ts`,
        pattern: new RegExp(/.*\/\/.*\[APPEND NEW IMPORTS\].+\n/),
        templateFile: "./query-service/appendServiceImports.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: `${directory}/_handlers.ts`,
        pattern: new RegExp(
          /.*\/\/.*\[APPEND NEW SERVICE MOCK HANDLER ABOVE\].+\n/
        ),
        templateFile: "./query-service/appendMockHandler.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: `${directory}/_handlers.ts`,
        pattern: new RegExp(
          /.*\/\/.*\[IMPORT MOCK URL AND RESPONSE ABOVE\].+\n/
        ),
        templateFile: "./query-service/importMockResponse.hbs",
        abortOnFail: true,
      },
      {
        type: "prettify-service",
      },
    ];

    return actions;
  },
};
