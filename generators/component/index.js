/**
 * Component Generator
 */

/* eslint strict: ["off"] */

"use strict";

const componentExists = require("../utils/componentExists");

module.exports = {
  description: "Add an unconnected component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Button",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    const directory = "../src/components/{{properCase name}}";
    const actions = [
      {
        type: "add",
        path: `${directory}/index.ts`,
        templateFile: "./component/index.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/{{properCase name}}.tsx`,
        templateFile: "./component/Component.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/{{properCase name}}.test.tsx`,
        templateFile: "./component/Component.test.tsx.hbs",
        abortOnFail: true,
      },
    ];

    actions.push({
      type: "prettify",
      path: "/components/",
    });

    return actions;
  },
};
