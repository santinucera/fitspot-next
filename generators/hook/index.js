/**
 * Hook Generator
 */

const hookExists = require("../utils/hookExists");

module.exports = {
  description: "Add a custom hook",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "CustomHook",
      validate: (name) => {
        return hookExists(`use${name}`)
          ? `A hook called use${name} already exists.`
          : true;
      },
    },
  ],
  actions: () => {
    const directory = "../src/hooks";
    return [
      {
        type: "add",
        path: `${directory}/use{{properCase name}}.tsx`,
        templateFile: "./hook/Hook.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/use{{properCase name}}.test.tsx`,
        templateFile: "./hook/Hook.test.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: `${directory}/index.ts`,
        pattern: new RegExp(/.*\/\/.*\[APPEND NEW IMPORTS\].+\n/),
        templateFile: "./hook/appendHooksImports.hbs",
        abortOnFail: true,
      },
      {
        type: "prettify",
        path: "/hooks",
      },
      {
        type: "prettify",
        name: "index.tsx",
      },
    ];
  },
};
