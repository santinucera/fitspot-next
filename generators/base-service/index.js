/**
 * Component Generator
 */

/* eslint strict: ["off"] */
const componentExists = require("../utils/componentExists");

module.exports = {
  description: "Add a service to contain query and mutation hooks",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Example",
      validate: (value) => {
        if (/.+/.test(`${value}Service`)) {
          return componentExists(`${value}Service`)
            ? "A service with this name already exists"
            : true;
        }
        return "The name is required";
      },
    },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    const directory = "../src/services/{{properCase name}}Service";
    const actions = [
      {
        type: "add",
        path: `${directory}/index.ts`,
        templateFile: "./base-service/index.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/types.ts`,
        templateFile: "./base-service/types.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/mockResponseData.ts`,
        templateFile: "./base-service/mockResponseData.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: `${directory}/_handlers.ts`,
        templateFile: "./base-service/handlers.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: "../src/services/_mocks/handlers.ts",
        pattern: new RegExp(
          /.*\/\/.*\[IMPORT MOCK URL AND RESPONSE ABOVE\].+\n/
        ),
        templateFile: "./base-service/appendRootImportMockHandler.hbs",
        abortOnFail: true,
      },
      {
        type: "modify",
        path: "../src/services/_mocks/handlers.ts",
        pattern: new RegExp(
          /.*\/\/.*\[APPEND NEW SERVICE MOCK HANDLER ABOVE\].+\n/
        ),
        templateFile: "./base-service/appendRootMockHandler.hbs",
        abortOnFail: true,
      },
    ];

    return actions;
  },
};
