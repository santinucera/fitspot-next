/**
 * hookExists
 *
 * Check whether the given hook exists in the hooks directory
 */

const fs = require("fs");
const path = require("path");

function hookExists(hook) {
  const files = fs.readdirSync(path.join(__dirname, `../../src/hooks`));
  return files.includes(`${hook}.tsx`);
}

module.exports = hookExists;
