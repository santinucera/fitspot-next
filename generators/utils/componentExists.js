/**
 * componentExists
 *
 * Check whether the given component exist in either the components or containers directory
 */

const fs = require("fs");
const path = require("path");

function componentExists(comp) {
  const components = fs.readdirSync(
    path.join(__dirname, "../../src/components")
  );
  const containers = fs.readdirSync(
    path.join(__dirname, "../../src/containers")
  );
  const services = fs.readdirSync(path.join(__dirname, "../../src/services"));
  return [...components, ...containers, ...services].indexOf(comp) >= 0;
}

module.exports = componentExists;
