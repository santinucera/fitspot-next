/**
 * containerComponentExists
 *
 * Check whether the given component exists in the container
 */

const fs = require("fs");
const path = require("path");

function containerComponentExists(container, component) {
  const files = fs.readdirSync(
    path.join(__dirname, `../../src/containers/${container}`)
  );
  return files.includes(`${component}.tsx`);
}

module.exports = containerComponentExists;
