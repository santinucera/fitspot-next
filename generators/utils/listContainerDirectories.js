/**
 * listContainerDirectories
 *
 * Check whether the given component exists in the container
 */
const fs = require("fs");
const path = require("path");

function listContainerDirectories() {
  const containerDirectories = fs.readdirSync(
    path.join(__dirname, "../../src/containers")
  );
  return containerDirectories.filter((dir) => /^\w+/.test(dir));
}

module.exports = listContainerDirectories;
