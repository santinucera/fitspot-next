/**
 * Container Generator
 */

const componentExists = require("../utils/componentExists");

module.exports = {
  description: "Add a container component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Count",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantExampleCode",
      message: "Include example code?",
    },
  ],
  actions: (data) => {
    // Generate index.ts and index.test.tsx
    const directory = "../src/containers/{{properCase name}}";
    let actions = [
      {
        type: "add",
        path: `${directory}/index.ts`,
        templateFile: "./container/index.ts.hbs",
        abortOnFail: true,
      },
    ];

    if (data.wantExampleCode) {
      actions = actions.concat([
        {
          type: "add",
          path: `${directory}/{{properCase name}}.tsx`,
          templateFile: "./container/Container.tsx.hbs",
          abortOnFail: true,
        },
        {
          type: "add",
          path: `${directory}/{{properCase name}}.test.tsx`,
          templateFile: "./container/Container.test.tsx.hbs",
          abortOnFail: true,
        },
      ]);
    } else {
      actions = actions.concat([
        {
          type: "add",
          path: `${directory}/{{properCase name}}.tsx`,
          templateFile: "./container/EmptyContainer.tsx.hbs",
          abortOnFail: true,
        },
        {
          type: "add",
          path: `${directory}/{{properCase name}}.test.tsx`,
          templateFile: "./container/EmptyContainer.test.tsx.hbs",
          abortOnFail: true,
        },
      ]);
    }

    actions.push({
      type: "prettify",
      path: "/containers/",
    });

    return actions;
  },
};
