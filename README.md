![tenspot app tests](https://github.com/Fitspot/next/workflows/Node.js%20CI/badge.svg)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `yarn generate`

Generates code complete with tests and helpful examples.

`yarn generate component` - generates a component with tests

Components are reusable and should not fetch data and only consume data via `props`.

`yarn generate container` - generates a container with tests

Containers should be for pages that fetch data and used by matched routes.

`yarn generate hook` - generates a custom hooks with tests

Hooks should be for hooks that are being reused by many components.

`yarn generate container component`

Container components are for one off components that are used only by the container itself. If you find yourself creating the same type of container component in two or more containers it would be wise to move it to the components folder.

`yarn generate query`

You can learn more about [React Query here](https://react-query.tanstack.com/docs/overview).

This will generate a react-query wrapper for fetching data.

Here are the prompts you'll be asked:

```
// The name of the query service
? What should it be called? SomeThingGet

// If you should default to tenspots api base url
// This will use the endpoint in the .env file for the environment
? Use tenspots api enpoint as default base url? Yes

// The path of the endpoint
? What is the base PATH? get/foo/path

// This is use in react-query to set the cache key in `queryCache`
? What is the cache key? This should be unique. foo
```

`yarn generate mutation`

This will generate a react-query wrapper for mutating data.

Here are the prompts you'll be asked:

```
// The name of the mutation service
? What should it be called? FooPost

// If you should default to tenspots api base url
// This will use the endpoint in the .env file for the environment
? Use tenspots api enpoint as default base url? Yes

// The path of the endpoint
? What is the base PATH? foo/post

// The method of the mutation
? What is the method of the request? (Use arrow keys)
❯ post
put
patch
```

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Local development

You'll need `yarn` version `1.22.1` set the version if needed `yarn set version 1.22.1`
You'll need brew installed https://brew.sh/

Install nginx
`brew install nginx`
create a file here called `tenspot.local`
-> `/usr/local/etc/nginx/servers/tenspot.local`
Add this to the file:

```
server {
  listen 80;
  server_name localhost.fitspotapp.com;
  gzip on;
  location /dashboard/ {
    proxy_pass http://127.0.0.1:3002;
    proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
  location / {
    proxy_pass http://127.0.0.1:3001;
    proxy_redirect off;
    proxy_set_header Host localhost;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
}
```

restart nginx `brew services restart nginx`

Add `127.0.0.1 localhost.fitspotapp.com` to your hosts file. -> `/ect/hosts`

clone the repo

cd into the repo and run `yarn` to install deps
run `yarn start` and open `http://localhost.fitspotapp.com/dashboard/` in your browser.

cd to the old repo and run the app to be able to login.

## Git commits

This repo uses [Commitizen](https://github.com/commitizen/cz-cli) for formatting git commit messages.

To commit you first add the files `git commit --all` and then run `yarn commit` and you will be prompted to fill out a commit message.
