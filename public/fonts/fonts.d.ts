/**
 *
 * Declare this as a module so it can be imported in TypeScript.
 *
 */

declare module "*.woff";
declare module "*.woff2";
